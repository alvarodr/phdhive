package com.phdhive.archetype.test.service;

import java.util.Locale;
import java.util.UUID;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.phdhive.archetype.dto.user.User;
import com.phdhive.archetype.service.IEmailService;
import com.phdhive.archetype.test.BaseSpringTest;

public class EmailServiceTest extends BaseSpringTest {

	@Autowired
	private IEmailService emailService;
	
	@Test
	public void sendmailTest(){
		User user = new User();
		user.setName("Alvaro");
		user.setEmail("alvarodr1983@gmail.com");
		user.setIdActivation(UUID.randomUUID().toString());
		user.setEnabled(false);
		
		emailService.sendEmailActivation(user, Locale.ENGLISH);
	}
}
