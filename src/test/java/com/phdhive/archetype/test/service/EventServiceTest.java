package com.phdhive.archetype.test.service;

import java.util.Collection;

import org.bson.types.ObjectId;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

import com.phdhive.archetype.dto.event.Event;
import com.phdhive.archetype.dto.user.User;
import com.phdhive.archetype.service.IEventService;
import com.phdhive.archetype.service.IUserService;
import com.phdhive.archetype.test.BaseSpringTest;

public class EventServiceTest extends BaseSpringTest {

	@Autowired
	private IEventService eventService;
	
	@Autowired
	private IUserService userService;
	
	//@Test
	public void linkUserEventTest(){
		String evt = "5280c54ec6d0921713e0667f";
		String user = "alvarodr@hotmail.com";
		
		Event event = eventService.linkUserToEvent(new ObjectId(evt), user);
		
		assertNotNull(event);
		assertNotNull(event.getUsers());
	}
	
	@Test
	public void getEventForUser() {
		User user = userService.readByEmail("alvarodr@hotmail.com");
		Collection<Event> evts = eventService.getEventByUser(new ObjectId(user.getId()));
		
		assertNotNull(evts);
		assertTrue(evts.size()>0);
	}
}
