package com.phdhive.archetype.test.service;

import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.bson.types.ObjectId;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;

import com.phdhive.archetype.dto.community.Community;
import com.phdhive.archetype.dto.document.PhdDocument;
import com.phdhive.archetype.dto.event.Event;
import com.phdhive.archetype.dto.master.types.TypeDataMaster;
import com.phdhive.archetype.dto.master.types.TypeDocumentMetaData;
import com.phdhive.archetype.dto.user.User;
import com.phdhive.archetype.service.ICommunityService;
import com.phdhive.archetype.service.IDataMasterService;
import com.phdhive.archetype.service.IDocumentService;
import com.phdhive.archetype.service.IEventService;
import com.phdhive.archetype.service.IUserService;
import com.phdhive.archetype.test.BaseSpringTest;

public class DocumentServiceTest extends BaseSpringTest{

	private ClassPathResource cpr = new ClassPathResource("attachment.docx");
	private ClassPathResource html = new ClassPathResource("test.html");
	
	@Autowired
	private IDocumentService documentService;
	
	@Autowired
	private IDataMasterService dataMasterService;
	
	@Autowired
	private ICommunityService communityService;
	
	@Autowired
	private IEventService eventService;
	
	@Autowired
	private IUserService userService;
	
	@Test
	public void saveDocumentTest() throws IOException{
		PhdDocument document1 = new PhdDocument();
		document1.setDocumentName("test");
		document1.setFile(cpr.getInputStream());
		Map<TypeDocumentMetaData, String> metaData1 = new HashMap<TypeDocumentMetaData, String>();
		metaData1.put(TypeDocumentMetaData.METADATA_OWNER, "alvarodr1982@gmail.com");
		document1.setMetaData(metaData1);
	
		ObjectId id = documentService.saveDocument(document1);
		assertNotNull(id);
		
		PhdDocument documentAux = documentService.findDocumentById(id);
		assertNotNull(documentAux);
		
		PhdDocument document2 = new PhdDocument();
		document2.setDocumentName("test1");
		document2.setFile(cpr.getInputStream());
		Map<TypeDocumentMetaData, String> metaData2 = new HashMap<TypeDocumentMetaData, String>();
		metaData2.put(TypeDocumentMetaData.METADATA_OWNER, "alvarodr1982@gmail.com");
		document2.setMetaData(metaData2);
		
		ObjectId id2 = documentService.saveDocument(document2);
		assertNotNull(id2);
		
		Collection<PhdDocument> documents = documentService.findDocumentsByMetada(metaData1);
		assertTrue(documents.size()==2);
		
//		documents = documentService.findDocumentsByMetada(metaData2);
//		assertTrue(documents.size()==2);
	}
	
	@Test
	public void findoByCommunity() throws IOException{
		User admin = new User();
		admin.setEmail("admin@phdhive.com");
		userService.createUser(admin);
		
		Community comunity = new Community("COM1", dataMasterService.getDataMaster(TypeDataMaster.TYPES_COUNTRIES, "ES"), admin);
		communityService.saveCommunity(comunity);
		
		Event event = new Event("EV1", "prueba", new Date(), new Date(), comunity);
		eventService.saveEvent(event);
		
		User usu1 = new User();
		usu1.setName("Usuario1");
		usu1.setEmail("usu1@phdhive.com");
		usu1.setEventsNames(new ArrayList<String>());
		usu1.getEventsNames().add(event.getName());
		
		PhdDocument document1 = new PhdDocument();
		document1.setDocumentName("test");
		document1.setFile(cpr.getInputStream());
		Map<TypeDocumentMetaData, String> metaData1 = new HashMap<TypeDocumentMetaData, String>();
		metaData1.put(TypeDocumentMetaData.METADATA_OWNER, "usu1@phdhive.com");
		document1.setMetaData(metaData1);
		
		usu1.setDocuments(new ArrayList<ObjectId>());
		document1 = documentService.findDocumentById(documentService.saveDocument(document1));
		usu1.getDocuments().add( new ObjectId(document1.getId()) );
		
		userService.createUser(usu1);
		
		User usu2 = new User();
		usu2.setName("Usuario2");
		usu2.setEmail("usu2@phdhive.com");
		usu2.setEventsNames(new ArrayList<String>());
		usu2.getEventsNames().add(event.getName());
		
		PhdDocument document2 = new PhdDocument();
		document2.setDocumentName("test1");
		document2.setFile(cpr.getInputStream());
		Map<TypeDocumentMetaData, String> metaData2 = new HashMap<TypeDocumentMetaData, String>();
		metaData2.put(TypeDocumentMetaData.METADATA_OWNER, "usu2@phdhive.com");
		document2.setMetaData(metaData2);
		
		usu2.setDocuments(new ArrayList<ObjectId>());
		usu2.getDocuments().add( new ObjectId(documentService.findDocumentById(documentService.saveDocument(document2)).getId()));
		
		userService.createUser(usu2);
		
		Collection<String> events = eventService.getEventsByCommunityId(new ObjectId(comunity.getId()));
		
		Collection<User> users = userService.getUserByCommunity(events);
		
		assertNotNull(users);
		assertTrue(users.size()==2);
		
		for (User user : users) {
			for (Object docId : user.getDocuments()) {
				assertNotNull(documentService.findDocumentById((ObjectId) docId));
			}
		}
	}
	
	@Test
	public void toHTMLTest() throws IOException{
		PhdDocument document = new PhdDocument();
		document.setDocumentName("test");
		document.setFile(cpr.getInputStream());
		assertNotNull(documentService.toHTML(document));
	}
	
	@Test
	public void toDocumentTest() throws IOException{
		PhdDocument document = new PhdDocument();
		
		documentService.toDocument(IOUtils.toString(html.getInputStream()), document);

		OutputStream outputStream = new FileOutputStream(cpr.getFile());
		IOUtils.copy(document.getFile(), outputStream);
		outputStream.close();
	}
}
