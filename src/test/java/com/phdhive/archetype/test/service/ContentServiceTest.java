package com.phdhive.archetype.test.service;

import static junit.framework.Assert.assertTrue;

import java.util.Collection;

import org.bson.types.ObjectId;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.phdhive.archetype.dto.content.Content;
import com.phdhive.archetype.dto.content.RelatedContent;
import com.phdhive.archetype.dto.master.DataMaster;
import com.phdhive.archetype.dto.master.types.TypeDataMaster;
import com.phdhive.archetype.service.IContentService;
import com.phdhive.archetype.service.IDocumentService;
import com.phdhive.archetype.test.BaseSpringTest;


public class ContentServiceTest extends BaseSpringTest {

	@Autowired
	private IContentService contentService;

	@Autowired
	private IDocumentService documentService;

//	private ClassPathResource cpr = new ClassPathResource("phd-hub.gif");	        
	
//	@Test
//	public void createTest() throws Exception{
//		
//		Content content = new Content();
//		content.setTitle("This is the title");
//		content.setHeader("This is the header");
//		content.setBody("This is the body");		
//		
//		Document image = new Document();
//		image.setDocumentName("test");
//		image.setFile(cpr.getInputStream());
//		String id = documentService.saveDocument(image);
//		assertNotNull(id);		
//		content.setImage(new ObjectId(id));
//		
//		List<Link> links = new ArrayList<Link>();
//		Link link1 = new Link("google","www.google.es");		
//		links.add(link1);
//		Link link2 = new Link("phd-hub","www.phd-hub.com");
//		links.add(link2);
//		content.setLinks(links);
//		
//		DataMaster category = DataMaster.createDatoMaestro(4,"ARTIC","Article");
//		content.setType(category);
//		
//		contentService.createContent(content);
//	
//		assertNotNull(content.getId());
//		assertNotNull(content.getImage());
//		assertNotNull(contentService.readAll());
//		
//		Content content2 = new Content();
//		content2.setId(content.getId());
//		content2 = contentService.findById(content2);
//		
//		assertNotNull(content2);
//
//		System.out.println("Category: "+ content2.getType().getDescription());
//		
//		links  = content2.getLinks();
//		System.out.println(((Link)links.get(0)).getDescription() + " " + ((Link)links.get(0)).getUrl());
//		System.out.println(((Link)links.get(1)).getDescription() + " " + ((Link)links.get(1)).getUrl());
//		
//	}
	
	
	@Test
	public void filterTest() throws Exception{
		
		Content content = new Content();
		content.setTitle("This is the title");
		content.setHeader("This is the header");
		content.setBody("This is the body");
		DataMaster category = DataMaster.createDatoMaestro(4,"ARTIC","Article");
		content.setType(category);		
		contentService.createContent(content);
		
		content = new Content();
		content.setTitle("This is the title 2");
		content.setHeader("This is the header 2");
		content.setBody("This is the body 2");
		category = DataMaster.createDatoMaestro(4,"ARTIC","Article");
		content.setType(category);		
		contentService.createContent(content);
		

		content = new Content();
		content.setTitle("This is the title 3");
		content.setHeader("This is the header 3");
		content.setBody("This is the body 3");
		category = DataMaster.createDatoMaestro(4,"DESC","Description");
		content.setType(category);		
		contentService.createContent(content);

		Collection<Content> contents = contentService.findByType(DataMaster.createDatoMaestro(TypeDataMaster.TYPES_CONTENT_TYPES.type(),"ARTIC","Article"));
		assertTrue(!contents.isEmpty());
		assertTrue(contents.size() == 2);

		contents = contentService.findByType(DataMaster.createDatoMaestro(TypeDataMaster.TYPES_CONTENT_TYPES.type(),"DESC","Description"));
		assertTrue(!contents.isEmpty());
		assertTrue(contents.size() == 1);

	}
	
	@Test
	public void relatedContentsTest() throws Exception{
		
		Content content1 = new Content();
		content1.setTitle("This is the title");
		content1.setHeader("This is the header");
		content1.setBody("This is the body");
		DataMaster category = DataMaster.createDatoMaestro(4,"ARTIC","Article");
		content1.setType(category);		
		contentService.createContent(content1);
		
		Content content2 = new Content();
		content2.setTitle("This is the title 2");
		content2.setHeader("This is the header 2");
		content2.setBody("This is the body 2");
		category = DataMaster.createDatoMaestro(4,"ARTIC","Article");
		content2.setType(category);		
		contentService.createContent(content2);
		

		Content content3 = new Content();
		content3.setTitle("This is the title 3");
		content3.setHeader("This is the header 3");
		content3.setBody("This is the body 3");
		category = DataMaster.createDatoMaestro(4,"DESC","Description");
		content3.setType(category);		
		contentService.createContent(content3);
		
		contentService.relateContents(content1, content2);
		contentService.relateContents(content2, content3);
		
		assertTrue(content1.getRelatedContentsAsList().size()==1);
		assertTrue(content2.getRelatedContentsAsList().size()==2);
		assertTrue(content3.getRelatedContentsAsList().size()==1);
		
		RelatedContent relCont = content3.getRelatedContentsAsList().get(0);
		assertTrue(relCont.getRelContentRef().toString().equalsIgnoreCase(content2.getId().toString()));
			
		//Prueba que al modificar el original, también se actualizan las copias
		content2.setTitle("This is the title 2 modified");
		contentService.updateContent(content2);
		//content3 tiene la copia antes de cambiar el título de content2
		//Por lo que hay que actualizar
		content3 = contentService.findById(new ObjectId(content3.getId()));
		relCont = content3.getRelatedContentsAsList().get(0);
		assertTrue(relCont.getRelContentName().equalsIgnoreCase(content2.getTitle()));

	}
		
}
