package com.phdhive.archetype.test.service;

import java.io.IOException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mock.web.MockMultipartFile;

import com.phdhive.archetype.dto.document.DocumentMultipart;
import com.phdhive.archetype.dto.user.User;
import com.phdhive.archetype.dto.word.Keyword;
import com.phdhive.archetype.service.IBulkService;
import com.phdhive.archetype.service.IKeyWordsService;
import com.phdhive.archetype.service.IKeywordStoreService;
import com.phdhive.archetype.service.ISolrService;
import com.phdhive.archetype.service.IUserService;
import com.phdhive.archetype.test.BaseSpringTest;

public class KeyWordsServiceTest extends BaseSpringTest {

	@Autowired
	private IKeyWordsService keyWordsService;

	@Autowired
	private IKeywordStoreService keyWordStoreService;

	@Autowired
	private ISolrService solrService;

	@Autowired
	private IUserService userService;

	@Autowired
	private IBulkService bulkService;
	
	@Test
	public void getKeyWordDocTest() throws IOException {
		ClassPathResource cpr = new ClassPathResource("attachment.docx");
		MockMultipartFile mockMultipartFile = null;
		mockMultipartFile = new MockMultipartFile("attachmnet.docx", cpr.getInputStream());
		DocumentMultipart doc = new DocumentMultipart();
		doc.setUserfile(mockMultipartFile);
		String words = keyWordsService.getKeyWordsAndFrequency(doc.toDocument());
		Assert.assertNotNull(words);
//		Assert.assertTrue(words.length > 0);
	}

	@Test
	public void getKeyWordStringTest() {
		String text = "The GAEDirectory is read only, that is, you can not use the Directory to build index! You should do indexing on another machine, then push the indices onto google appengine datastore with LuceneIndexPushUtil"
				+ "Because of the quota limitation of google appengine, GAELucene is not fit to run with huge indices, it does better for small indices, around 100Mb. For large changing indices, you need to find other solutions";

		List<Entry<String, Integer>> words = keyWordsService.getKeyWordsMap(text, new Locale("EN"));

		Assert.assertNotNull(words);
		Assert.assertTrue(words.size() > 0);
	}

	// Comprueba que tras cada búsqueda las keywords del usuario se actualizan
	@Test
	public void setUserKeyWords() throws Exception {

		List<Entry<String, Integer>> word_list = new ArrayList<Entry<String, Integer>>();
		Map.Entry<String, Integer> entry1 = new AbstractMap.SimpleEntry<String, Integer>("String1", 42);
		word_list.add(entry1);
		Map.Entry<String, Integer> entry2 = new AbstractMap.SimpleEntry<String, Integer>("String2", 42);
		word_list.add(entry2);
		Map.Entry<String, Integer> entry3 = new AbstractMap.SimpleEntry<String, Integer>("String3", 12);
		word_list.add(entry3);
		Map.Entry<String, Integer> entry4 = new AbstractMap.SimpleEntry<String, Integer>("String4", 100);
		word_list.add(entry4);
		Map.Entry<String, Integer> entry5 = new AbstractMap.SimpleEntry<String, Integer>("String5", 56);
		word_list.add(entry5);

		// Actualiza las palabras clave del usuario actual con las encontradas
		User user = new User();
		user.setEmail("test@test.com");
		user.setName("name");
		user.setPassword("password");
		user.setSurname("surname");
		user = userService.createUser(user);
		userService.udpateKeywords(user, word_list,false);

		Assert.assertNotNull(user.getKeywords());

		Keyword kword = new Keyword();
		kword.setWord(entry1.getKey());
		kword = keyWordStoreService.readByKeyword(kword);

		Assert.assertNotNull(kword);

		word_list = new ArrayList<Entry<String, Integer>>();
		Map.Entry<String, Integer> entry6 = new AbstractMap.SimpleEntry<String, Integer>("String4", 40);
		word_list.add(entry6);
		Map.Entry<String, Integer> entry7 = new AbstractMap.SimpleEntry<String, Integer>("String7", 42);
		word_list.add(entry7);
		userService.udpateKeywords(user, word_list,false);

		Assert.assertTrue(user.getKeywords().size() == 6);
		kword = new Keyword();
		kword.setWord("String4");
		kword = keyWordStoreService.readByKeyword(kword);
		Assert.assertTrue(kword.getTotalFreq() == 140);

		Map<String, Integer> keywords = user.getKeywords();
		Assert.assertTrue(keywords.get("String4") == 140);

	}
	
	
}
