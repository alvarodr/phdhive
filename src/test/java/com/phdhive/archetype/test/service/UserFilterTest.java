package com.phdhive.archetype.test.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.bson.types.ObjectId;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.phdhive.archetype.dto.community.Community;
import com.phdhive.archetype.dto.filter.UserFilter;
import com.phdhive.archetype.dto.master.DataMaster;
import com.phdhive.archetype.dto.master.types.TypeDataMaster;
import com.phdhive.archetype.dto.user.User;
import com.phdhive.archetype.service.ICommunityService;
import com.phdhive.archetype.service.IDataMasterService;
import com.phdhive.archetype.service.IEventService;
import com.phdhive.archetype.service.IUserService;
import com.phdhive.archetype.test.BaseSpringTest;

public class UserFilterTest extends BaseSpringTest {

	@Autowired
	private IUserService userService;
	
	@Autowired
	private IEventService eventService;
	
	@Autowired
	private ICommunityService communityService;
	
	@Autowired
	private IDataMasterService dataMasterService;
	
	@Test
	public void test() {
//		User user = userService.readByEmail("alvarodr@hotmail.com");
		Community community = communityService.getCommunityById(new ObjectId("5230658a2d5d27774b7dbd4f"));
//		Community community2 = communityService.getCommunityById(new ObjectId("523080fa2d5d27774b7dbd51"));
//		
//		Event event = new Event("EVT_TEST", "My event test", new Date(), new Date(), community);
//		
//		eventService.saveEvent(event);
//		
//		user.setEvents(new ArrayList<Event>());
//		user.getEvents().add(event);
//		
		DataMaster area = dataMasterService.getDataMaster(TypeDataMaster.TYPES_KNOWLEDGES_UNSECO, "12");
//		DataMaster discipline1 = dataMasterService.getDataMaster(TypeDataMaster.MATHEMATICS, "1206");
//		DataMaster discipline2 = dataMasterService.getDataMaster(TypeDataMaster.MATHEMATICS, "1207");
//		user.setAreas(new ArrayList<DataMaster>());
//		user.getAreas().add(area);
//		user.setDisciplines(new ArrayList<DataMaster>());
//		user.getDisciplines().add(discipline1);
//		userService.updateUser(user);
		
		UserFilter filter = new UserFilter();
		filter.setArea(area.getId());
		filter.setCommunity(new ObjectId(community.getId()));
//		filter.setDiscipline(discipline2.getId());
		List<User> users = userService.getUserByFilter(filter);
		assertNotNull(users);
		assertTrue(users.size()==1);
	}

}
