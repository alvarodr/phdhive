package com.phdhive.archetype.test.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.StringTokenizer;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.request.UpdateRequest;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.bson.types.ObjectId;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mock.web.MockMultipartFile;

import com.phdhive.archetype.dao.IUserDAO;
import com.phdhive.archetype.dto.document.DocumentMultipart;
import com.phdhive.archetype.dto.document.PhdDocument;
import com.phdhive.archetype.dto.event.Event;
import com.phdhive.archetype.dto.master.types.TypeDocumentFormat;
import com.phdhive.archetype.dto.master.types.TypeDocumentMetaData;
import com.phdhive.archetype.dto.user.User;
import com.phdhive.archetype.service.IDocumentService;
import com.phdhive.archetype.service.IEmailService;
import com.phdhive.archetype.service.IEventService;
import com.phdhive.archetype.service.ISolrService;
import com.phdhive.archetype.service.IUserService;
import com.phdhive.archetype.test.BaseSpringTest;

public class UserServiceTest extends BaseSpringTest {

	@Autowired
	private IUserService userService;

	@Autowired
	private IUserDAO userDao;

	@Autowired
	private ISolrService solrService;

	@Mock
	private HttpSolrServer solrServerMock;

	@Autowired
	private IEventService eventService;

	@Autowired
	private IDocumentService documentService;

	@Autowired
	private IEmailService emailService;

	private ClassPathResource cpr = new ClassPathResource("test.docx");

	@Test
	public void createTest() throws Exception {
		User user = new User();
		user.setEmail("test@test.com");
		user.setName("name");
		user.setPassword("password");
		user.setSurname("surname");
		user = userService.createUser(user);

		assertNotNull(user.getEmail());
	}

	@Test
	public void readTest() throws Exception {

		User user = new User();
		user.setEmail("test@test.com");
		user.setName("name");
		user.setPassword("password");
		user.setSurname("surname");
		user = userService.createUser(user);

		assertNotNull(userService.readById(user));
	}

	@Test
	public void updateTest() throws Exception {

		User user = new User();
		user.setEmail("test@test.com");
		user.setName("name");
		user.setPassword("password");
		user.setSurname("surname");
		user = userService.createUser(user);

		user = userService.readById(user);
		user.setName("name2");
		userService.updateUser(user);

		user = userService.readById(user);

		assertTrue(user.getName().equals("name2"));
	}

	@Test
	public void deleteTest() throws Exception {

		User user = new User();
		user.setEmail("test@test.com");
		user.setName("name");
		user.setPassword("password");
		user.setSurname("surname");
		user = userService.createUser(user);

		userService.deleteUser(user);

		assertNull(userService.readById(user));
	}

	@Test
	public void readAllTest() throws Exception {

		User user = new User();
		user.setEmail("test@test.com");
		user.setName("name");
		user.setPassword("password");
		user.setSurname("surname");
		user = userService.createUser(user);

		assertNotNull(userService.readAll());
	}

	@Test
	public void readByCriteriaTest() throws Exception {

		User user = new User();
		user.setEmail("test@test.com");
		user.setName("name");
		user.setPassword("password");
		user.setSurname("surname");
		user = userService.createUser(user);

		assertNotNull(userService.readByCriteriaName("name"));
	}

	@Test
	public void searchUsers2Test() throws Exception {

		List<User> results = userDao.queryLike("latest_article", "war");
		assertTrue(results.size() > 0);

	}

	@Test
	public void searchUsersTest() throws Exception {

		ClassPathResource cpr = new ClassPathResource("example.doc");
		MockMultipartFile mockMultipartFile = null;
		try {
			mockMultipartFile = new MockMultipartFile("example.doc",
					cpr.getInputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
		DocumentMultipart doc = new DocumentMultipart();
		doc.setUserfile(mockMultipartFile);

		// List<User> results = solrService.searhUsersSolr(doc);
		// assertTrue(results.size()>0 && userService.getQueryWords().size()>0);
	}

	@Test
	public void solrTest() throws Exception {

		SolrInputDocument document = new SolrInputDocument();
		document.addField("id", "552199");
		document.addField("name", "Gouda cheese wheel cheese");
		document.addField("bio", "Pepe Perez lived in Europe");
		document.addField("price", "49.99");

		SolrInputDocument document2 = new SolrInputDocument();
		document2.addField("id", "552198");
		document2.addField("name", "Gouda cheese cheese chair cheese");
		document2.addField("bio", "Ana López was born in Europe Europe Africa");
		document2.addField("price", "50.99");

		SolrInputDocument document3 = new SolrInputDocument();
		document3.addField("id", "552197");
		document3
				.addField("name",
						"Gouda table is a cheese mother chair in table water of the blues table guitar");
		document3.addField("bio", "Europe Africa Ceuta Africa Ceuta Ceuta");
		document3.addField("price", "49.99");

		UpdateRequest req = new UpdateRequest();
		req.add(document);
		req.add(document2);
		req.add(document3);
		// Usamos estrategia commitWithin para que se indexe cada X segundos
		// con el objetivo de optimizar la indexación
		req.setCommitWithin(1000);// Production:req.setCommitWithin(60000)

		SolrQuery query = new SolrQuery();
		query.set("fl", "score,*");
		// query.set("start","0");
		// query.set("rows","20");
		// query.set("debugQuery", "on");
		// query.setHighlight(true).setHighlightSnippets(1);
		query.setHighlight(true);
		query.setParam("hl.fl", "name,bio");
		query.setTerms(true);
		query.set("terms.fl", "name");
		query.set("tv.tf_idf", true);
		// En schema.xml se ha definido que todos los campos indexables se
		// copien
		// al campo genérico "text", de forma que las búsquedas se harán sobre
		// éste
		query.setQuery("text:Africa OR text:Ceuta OR text:table OR text:Pepe");
		QueryResponse rsp = solrServerMock.query(query);
		SolrDocumentList docList = rsp.getResults();

		Iterator<SolrDocument> iter = docList.iterator();
		while (iter.hasNext()) {
			SolrDocument resultDoc = iter.next();

			String id = (String) resultDoc.getFieldValue("id"); // id is the
																// uniqueKey
																// field
			if (rsp.getHighlighting().get(id) != null) {
				List<String> highlightSnippets = rsp.getHighlighting().get(id)
						.get("bio");
				if (highlightSnippets != null) {
					System.out.println("id: " + id);
					System.out.println("\n");
					for (String text : highlightSnippets) {
						System.out.println("found: " + findHighlight(text));
						System.out.println("\n");
					}
				}
			}
		}

		// assertTrue(docList.size() == 3);
		// assertEquals("552197", docList.get(0).get("id"));

		solrServerMock.deleteById("552197");
		solrServerMock.deleteById("552198");
		solrServerMock.deleteById("552199");
		solrServerMock.commit();

		rsp = solrServerMock.query(query);
		assertTrue(docList.size() == 0);
	}

	private List<String> findHighlight(String input) {

		List<String> words = new ArrayList<String>();
		StringTokenizer tokens = new StringTokenizer(input);
		while (tokens.hasMoreTokens()) {
			String word = tokens.nextToken();
			if (word.lastIndexOf("<em>") == 0) {
				word = word.substring(4, word.length() - 5);
				if (!contains(words, word)) {
					words.add(word);
				}
			}
		}
		return words;
	}

	@Test
	public void solrTestWithEvents() throws Exception {

		Event event1 = new Event();
		Event event2 = new Event();
		try {

			event1.setName("event1");
			event1.setDescription("This is event1");
			event1.setEndDate(new Date());
			eventService.saveEvent(event1);

			event2.setName("event2");
			event2.setDescription("This is event2");
			event2.setEndDate(new Date());
			eventService.saveEvent(event2);

			User user = new User();
			user.setEmail("garridomargolles@gmail.com");
			user.setName("Pepe Pérez");
			user.setBiograph("Pepe Perez cheese lived in Europe table");

			ClassPathResource cpr1 = new ClassPathResource("test.docx");
			MockMultipartFile mockMultipartFile1 = null;
			mockMultipartFile1 = new MockMultipartFile("test.docx",	cpr1.getInputStream());
			DocumentMultipart doc1 = new DocumentMultipart();
			doc1.setUserfile(mockMultipartFile1);
			PhdDocument document1 = doc1.toDocument();
			document1.setDocumentName("test");
			document1.setContentType(TypeDocumentFormat.MIMETYPE_DOCX.getTypeTika());
			document1.setFile(cpr.getInputStream());
			Map<TypeDocumentMetaData, String> metaData1 = new HashMap<TypeDocumentMetaData, String>();
			metaData1.put(TypeDocumentMetaData.METADATA_OWNER, user.getEmail());
			metaData1.put(TypeDocumentMetaData.METADATA_EVENT, event1.getId());
			document1.setMetaData(metaData1);
			document1 = documentService.findDocumentById(documentService.saveDocument(document1));

			List<ObjectId> documents = new ArrayList<ObjectId>();
			documents.add(new ObjectId(document1.getId()));
			user.setDocuments(documents);

			user = userService.createUser(user);

			eventService.linkUserToEvent(new ObjectId(event1.getId()), user.getEmail());
			eventService.linkUserToEvent(new ObjectId(event2.getId()), user.getEmail());

			User user2 = new User();
			user2.setEmail("jorgemargolles@yahoo.es");
			user2.setName("Ana López");
			user2.setBiograph("Ana López was born cheese in Europe Europe Africa");

			ClassPathResource cpr2 = new ClassPathResource("attachment.docx");

			MockMultipartFile mockMultipartFile2 = null;
			mockMultipartFile2 = new MockMultipartFile("attachmnet.docx", "attachmnet.docx", TypeDocumentFormat.MIMETYPE_DOCX.getTypeTika(), cpr2.getInputStream());

			DocumentMultipart doc2 = new DocumentMultipart();
			doc2.setUserfile(mockMultipartFile2);
			PhdDocument document2 = doc2.toDocument();
			document2.setDocumentName("test2");
			document2.setContentType(TypeDocumentFormat.MIMETYPE_DOCX.getTypeTika());
			Map<TypeDocumentMetaData, String> metaData2 = new HashMap<TypeDocumentMetaData, String>();
			metaData2.put(TypeDocumentMetaData.METADATA_OWNER, user2.getEmail());
			metaData2.put(TypeDocumentMetaData.METADATA_EVENT, event1.getId());
			document2.setMetaData(metaData2);
			document2 = documentService.findDocumentById(documentService.saveDocument(document2));

			List<ObjectId> documents2 = new ArrayList<ObjectId>();
			documents2.add(new ObjectId(document2.getId()));
			user2.setDocuments(documents2);

			user2 = userService.createUser(user2);
			eventService.linkUserToEvent(new ObjectId(event1.getId()), user2.getEmail());

			User user3 = new User();
			user3.setEmail("tito@test.com");
			user3.setName("Tito Puente");
			user3.setBiograph("Gouda table is a cheese mother chair in table water of the blues table guitar");
			user3 = userService.createUser(user3);
			eventService.linkUserToEvent(new ObjectId(event2.getId()), user3.getEmail());

			List<Entry<String, Integer>> list = new ArrayList<Entry<String, Integer>>();
			Map.Entry<String, Integer> entry1 = new AbstractMap.SimpleEntry<String, Integer>("tabl", 42);
			list.add(entry1);
			Map.Entry<String, Integer> entry2 = new AbstractMap.SimpleEntry<String, Integer>("water", 11);
			list.add(entry2);
			Map.Entry<String, Integer> entry3 = new AbstractMap.SimpleEntry<String, Integer>("chees", 124);
			list.add(entry3);

			Thread.sleep(2000);// Espera que se actualize solr

			// List<User> rsp = solrService.findUsers(list, event1);

			// Busca los eventos cuya fecha de finalización haya terminado
			List<Event> events = (List<Event>) eventService.getAllEvents();
			for (Event event : events) {
				Date now = new Date();
				if (now.after(event.getEndDateAsObject())) {

					// Manda un mail a todos los usuarios del evento
					List<User> users = event.getUsers();
					if (users != null) {
						for (User userEv : users) {
							// Busca el documento de este usuario asociado al evento
							Map<TypeDocumentMetaData, String> metaData = new HashMap<TypeDocumentMetaData, String>();
							metaData.put(TypeDocumentMetaData.METADATA_OWNER, userEv.getEmail());
							metaData.put(TypeDocumentMetaData.METADATA_EVENT, event.getId());
							Collection<PhdDocument> documentEventList = documentService.findDocumentsByMetada(metaData);
							for (PhdDocument documentEvent : documentEventList) {
								// TODO: Obtener el locale en base al idioma del evento
//								List<User> usersSearch = solrService.searhUsersSolr(documentEvent, new Locale("EN"), event);
								List<Entry<String, Integer>> foundWords = userService.getQueryWords();

								// Actualiza las palabras clave de usuario
								userService.udpateKeywords(userEv, foundWords,false);
								emailService.sendEventEndMessage(userEv, event, new Locale("EN"));
							}
						}
					}
				}
			}
			// assertTrue(rsp.size() == 2);
		} catch (Exception e) {
			userService.deleteAllUsers();
			eventService.removeEvent(event1);
			eventService.removeEvent(event2);
		}
		// userService.deleteUser(user);
		// userService.deleteUser(user2);
		// userService.deleteUser(user3);
	}

	@Test
	public void geoLocTest() throws IOException {
		User user = new User();
		user.setLastIP("192.168.1.1");
		String country = userService.getUserCountry(user).getDescription();
		assertTrue(country.equalsIgnoreCase("Reserved"));
	}

	private boolean contains(List<String> list, String input) {
		boolean result = false;
		for (String word : list) {
			if (word.equalsIgnoreCase(input)) {
				result = true;
			}
		}
		return result;
	}
}
