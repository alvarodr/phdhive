package com.phdhive.archetype.test.service;

import junit.framework.Assert;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.phdhive.archetype.dto.master.types.TypeLanguage;
import com.phdhive.archetype.service.ISnowballService;
import com.phdhive.archetype.test.BaseSpringTest;

/**
 * 
 * @author ADORU3N
 *
 */
public class SnowballServiceTest extends BaseSpringTest {
	
	private String[] words = {"arboleda", "doctorado", "investigacion", "marcapasos", "mariposa", "dictado"};
	
	@Autowired
	private ISnowballService snowballService;
	
	@Test
	public void test() {
		snowballService.getListStemmer(words, TypeLanguage.LANGUAGE_SPANISH);
		Assert.assertTrue(words.length>0);
	}
}
