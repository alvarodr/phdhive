package com.phdhive.archetype.test.service;

import java.io.IOException;

import javax.activation.MimetypesFileTypeMap;

import org.junit.Test;
import org.springframework.core.io.ClassPathResource;

import com.phdhive.archetype.test.BaseSpringTest;

public class MimeTypeTest extends BaseSpringTest {

	private MimetypesFileTypeMap mimeMap = null;
	
	private ClassPathResource cpr = new ClassPathResource("attachment.docx");
	
	@Test
	public void ConfigMimeTypeTest() throws IOException{
		mimeMap = new MimetypesFileTypeMap(new ClassPathResource("mime.type").getInputStream());
		System.out.println(mimeMap.getContentType(cpr.getFile()));
	}
}
