package com.phdhive.archetype.test.service;

import java.util.Collection;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.phdhive.archetype.dto.master.DataMaster;
import com.phdhive.archetype.dto.master.types.TypeDataMaster;
import com.phdhive.archetype.service.IDataMasterService;
import com.phdhive.archetype.test.BaseSpringTest;
import static junit.framework.Assert.*;

public class DataMasterTest extends BaseSpringTest {
	
	@Autowired
	private IDataMasterService dataMasterService;
	
	@Test
	public void getDataMaster(){
		Collection<DataMaster> countries = dataMasterService.findByType(TypeDataMaster.TYPES_COUNTRIES);
		assertTrue(!countries.isEmpty());
		assertTrue(countries.size() == 252);
		
		DataMaster dataMaster = dataMasterService.getDataMaster(TypeDataMaster.TYPES_COUNTRIES, "ES");
		assertNotNull(dataMaster);
		assertTrue(dataMaster.getDescription().equals("SPAIN"));
	}
	
	
	@Test
	public void test2() {
		Collection<DataMaster> contents = dataMasterService.findByType(TypeDataMaster.valueOf("TYPES_CONTENT_TYPES"));
		assertNotNull(contents);
		assertTrue(!contents.isEmpty());
	}
}
