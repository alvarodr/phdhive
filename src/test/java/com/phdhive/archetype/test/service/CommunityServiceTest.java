package com.phdhive.archetype.test.service;

import org.bson.types.ObjectId;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.phdhive.archetype.service.ICommunityService;
import com.phdhive.archetype.test.BaseSpringTest;

import static org.junit.Assert.*;

public class CommunityServiceTest extends BaseSpringTest {

	@Autowired
	private ICommunityService communityService;
	
	@Test
	public void checkId(){
		String id = "522831312d5dde0ee6d97521";
		assertNotNull(communityService.getCommunityById(new ObjectId(id)));
	}
	
}
