package com.phdhive.archetype.test;

import org.junit.Assert;
import org.junit.Test;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import org.simpleframework.xml.stream.Format;
import org.simpleframework.xml.stream.HyphenStyle;
import org.simpleframework.xml.stream.Style;

import com.phdhive.archetype.dto.linkedin.LinkedinXMLProfileDTO;

public class ThirdPartXMLTest {

	@Test
	public void linkedinXMLTest(){

		String body = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"+
			                "<person>"+
							"<id>J0KTKyOGc8</id>"+
							"<first-name>Jorge</first-name>"+
							"<last-name>Garrido</last-name>"+
							"<industry>Internet</industry>"+
							"<headline>Desarrollador Java</headline>"+
						"</person>";
		
		Style style = new HyphenStyle();
		Format format = new Format(style);
		Serializer serializer = new Persister(format);
		try {
			LinkedinXMLProfileDTO person = serializer.read(LinkedinXMLProfileDTO.class, body, false);
			String id = person.getId();
			Assert.assertNotNull(id);
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}
}
