package com.phdhive.archetype.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/spring/test-application.xml",
								   "classpath:/spring/test-mongo-template.xml",
                                   "classpath:/spring/test-web-config.xml",
                                   "classpath:/spring/test-email-job.xml"})
public class BaseSpringTestWithoutMongo {

	@Before @Test
    public void setUp() throws Exception {
 
	}
	
	@After @Test
    public void tearDown() {
        
    }
	
}
