package com.phdhive.archetype.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/spring/test-application.xml",
                                   "classpath:/spring/test-mongo-template.xml",
                                   "classpath:/spring/test-web-config.xml",
                                   "classpath:/spring/test-email-job.xml"})
public abstract class BaseSpringTest{

//    private static final String LOCALHOST = "127.0.0.1";
//    private static final String DB_NAME = "itest";
//    private static final int MONGO_TEST_PORT = 27028;
//
// 
//    private static MongodProcess mongoProcess;
//    private static Mongo mongo;
	
	protected Logger log = LoggerFactory.getLogger(this.getClass());
	
	
//    @BeforeClass
//    public static void initializeDB() throws IOException {
// 
//        RuntimeConfig config = new RuntimeConfig();
//        config.setExecutableNaming(new UserTempNaming());
// 
//        MongodStarter starter = MongodStarter.getInstance(config);
// 
//        MongodExecutable mongoExecutable = starter.prepare(new MongodConfig(Version.V2_2_0, MONGO_TEST_PORT, false));
//        mongoProcess = mongoExecutable.start();
// 
//        mongo = new Mongo(LOCALHOST, MONGO_TEST_PORT);
//        mongo.getDB(DB_NAME);
//    }
// 
//    @AfterClass
//    public static void shutdownDB() throws InterruptedException {
//        mongo.close();
//        mongoProcess.stop();
//    }
	
	@Before @Test
    public void setUp() throws Exception {
 
	}
	
	@After @Test
    public void tearDown() {
        
    }
}
