package com.phdhive.archetype.dao;

import java.util.Collection;

import org.bson.types.ObjectId;

import com.phdhive.archetype.dto.master.DataMaster;
import com.phdhive.archetype.dto.master.ITypeDataMaster;

/**
 * 
 * @author ADORU3N
 *
 */
public interface IDataMasterDAO extends IBaseDAO<DataMaster, ObjectId> {

	/**
	 * Devuelve todos los datos maestros de la aplicación.
	 * 
	 * @return
	 */
	Collection<DataMaster> findAll();

	/**
	 * Devuelve todos los datos maestros de un tipo determinado.
	 * 
	 * @param tipo
	 * @return
	 */
	Collection<DataMaster> findByType(ITypeDataMaster type);
	
	/**
	 * Devuelve un dato maestro
	 * 
	 * @param code
	 * @return
	 */
	DataMaster findByCode(String code);
	
	/**
	 * Devuelve un dato maestro dado el typo y el codigo
	 * 
	 * @param type
	 * @param code
	 * @return
	 */
	DataMaster findByeTypeCode (Integer type, String code);
}
