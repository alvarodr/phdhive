package com.phdhive.archetype.dao;

import java.util.List;
import java.util.Map;

import org.bson.types.ObjectId;

import com.phdhive.archetype.dto.document.PhdDocument;
import com.phdhive.archetype.dto.master.types.TypeDocumentMetaData;


/**
 * 
 * @author ADORU3N
 *
 */
public interface IDocumentDAO extends IBaseDAO<PhdDocument, ObjectId> {

	/**
	 * Guarda el documento
	 * @param document
	 * @return
	 */
	ObjectId saveDocument(PhdDocument document);
	
	/**
	 * Busca documentos filtrado por el valor de los metadatos
	 * @param metaData
	 * @return
	 */
	List<PhdDocument> findDocument(Map<TypeDocumentMetaData, String> metaData);
	
	/**
	 * Busca un documento por identificador
	 */
	PhdDocument findById(ObjectId id);
	
	/**
	 * Elimina un documento del store
	 * @param id
	 */
	void delete(ObjectId id);
}
