package com.phdhive.archetype.dao;

import java.util.Collection;
import java.util.List;

import org.bson.types.ObjectId;

import com.phdhive.archetype.dto.filter.UserFilter;
import com.phdhive.archetype.dto.user.User;

/**
 * 
 * @author Alvaro
 *
 */
public interface IUserDAO extends IBaseDAO<User, ObjectId> {

	/**
	 * 
	 * @param user
	 * @return
	 * @throws Exception
	 */
	User createUser(User user);
	
	/**
	 * 
	 * @return
	 */
	List<User> findAll();
	
	/**
	 * 
	 * @param name
	 * @return
	 */
	List<User> findByName(String name);
	
	/**
	 * 
	 * @param user
	 * @return
	 */
	User updateUser(User user);
	
	/**
	 * 
	 * @param user
	 */
	void deleteUser(User user);
	
	/**
	 * Borra todos los registros de la coleccion
	 */
	void deleteAll();
	
	/**
	 * 
	 * @param email
	 * @return
	 */
	User findByEmail(String email);
	
	/**
	 * 
	 * @param email
	 * @param id_active
	 * @return
	 */
	User findUserActive(ObjectId id, String id_active);
	
	/**
	 * 
	 * @param campo
	 * @param valor
	 * @return
	 */
	List<User> queryLike(String campo, String valor);
	
	/**
	 * 
	 * @param token
	 * @return
	 */
	User findByToken(String token);

	/**
	 * 
	 * @param start
	 * @param limit
	 * @return
	 */
	List<User> getUsersByPaging(Integer start, Integer limit);
	
	/**
	 * Devuelve una lista de usuarios pertenecientes a una comunidad
	 * @param events
	 * @return
	 */
	Collection<User> getUserByCommunity(Collection<String> events);
	
	/**
	 * 
	 * @param filter
	 * @return
	 */
	List<User> getUsersByFilter(UserFilter filter);
}
