package com.phdhive.archetype.dao.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.phdhive.archetype.dao.IEventDAO;
import com.phdhive.archetype.dao.IUserDAO;
import com.phdhive.archetype.dto.event.Event;
import com.phdhive.archetype.dto.filter.UserFilter;
import com.phdhive.archetype.dto.user.User;

/**
 * 
 * @author ADORU3N
 *
 */
@Repository
public class UserDAOImpl implements IUserDAO{
	
	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Autowired
	private IEventDAO eventDAO;
	
	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.IUserDAO#createUser(com.phdhive.archetype.dto.user.User)
	 */
	@Override
	public User createUser(User user){	
		if(user.getCreatedate()==null)// es nuevo
			user.setCreatedate(new Date());
		user.setUpdate(new Date());
		mongoTemplate.save(user);
		return user;
	} 

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.IUserDAO#findByEmail(java.lang.String)
	 */
	@Override
	public User findByEmail(String email){
		return mongoTemplate.findOne(new Query(Criteria.where("USER_EMAIL").is(email)), User.class);
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.IUserDAO#queryLike(java.lang.String, java.lang.String)
	 */
	@Override
	public List<User> queryLike(String campo, String valor){
		return mongoTemplate.find(new Query(Criteria.where(campo).regex(Pattern.compile(valor))), User.class);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.IUserDAO#findByName(java.lang.String)
	 */
	@Override
	public List<User> findByName(String name) {
		return mongoTemplate.find(new Query(Criteria.where("USER_NAME").is(name)), User.class);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.IUserDAO#deleteAll()
	 */
	@Override
	public void deleteAll() {
		mongoTemplate.dropCollection(User.class);
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.IUserDAO#updateUser(com.phdhive.archetype.dto.user.User)
	 */
	@Override
	public User updateUser(User user) {
		mongoTemplate.save(user);
		return user;
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.IUserDAO#deleteUser(com.phdhive.archetype.dto.user.User)
	 */
	@Override
	public void deleteUser(User user) {
		mongoTemplate.remove(user);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.IUserDAO#findAll()
	 */
	@Override
	public List<User> findAll() {
		return mongoTemplate.findAll(User.class);
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.IUserDAO#findUserActive(org.bson.types.ObjectId, java.lang.String)
	 */
	@Override
	public User findUserActive(ObjectId id, String idActive) {
		return mongoTemplate.findOne(new Query(Criteria.where("_id").is(id)
				.andOperator(Criteria.where("USER_ID_ACTIVACION").is(idActive))), User.class);
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.IUserDAO#findByToken(java.lang.String)
	 */
	@Override
	public User findByToken(String token){
		return mongoTemplate.findOne(new Query(Criteria.where("USER_TOKEN").is(token)), User.class);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.IBaseDAO#findById(java.io.Serializable)
	 */
	@Override
	public User findById(ObjectId id) {
		return mongoTemplate.findById(id, User.class);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.IBaseDAO#save(java.lang.Object)
	 */
	@Override
	public void save(User entity) {
		mongoTemplate.save(entity);
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.IUserDAO#getUsersByPaging(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public List<User> getUsersByPaging(Integer start, Integer limit){
		return mongoTemplate.find(new Query().limit(limit).skip(start), User.class);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.IUserDAO#getUserByCommunity(java.util.Collection)
	 */
	@Override
	public Collection<User> getUserByCommunity(Collection<String> events) {
		return mongoTemplate.find(new Query(Criteria.where("USER_EVENTS").all(events)), User.class);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.IUserDAO#getUsersByFilter(com.phdhive.archetype.dto.filter.UserFilter)
	 */
	@Override
	public List<User> getUsersByFilter(UserFilter filter) {
		Query query = new Query();
		
		if (filter.getCountry() != null) {
			query.addCriteria(Criteria.where("USER_EVENTS.EVT_OWNER.COMM_COUNTRY.MAST_CODE.$id").is(filter.getCountry()));
		}
		
		List<ObjectId> idEvents = new ArrayList<ObjectId>();
		if (filter.getCommunity() != null) {
			Collection<Event> events = eventDAO.getEventsByCommunityId(filter.getCommunity());
			for (Event e : events) {
				idEvents.add(new ObjectId(e.getId()));
			}
		}
		
		if (idEvents.isEmpty() && filter.getEvent() != null) {
			query.addCriteria(Criteria.where("USER_EVENTS").is(filter.getEvent()));
		}
		
		if (filter.getArea() != null) {
			query.addCriteria(Criteria.where("USER_AREA.$id").is(filter.getArea()));
		}
		
		if (filter.getDiscipline() != null) {
			query.addCriteria(Criteria.where("USER_DISCIPLINE.$id").is(filter.getDiscipline()));
		}
		
		if (filter.getStart()!=-1) {
			query.skip(filter.getStart()).limit(filter.getLimit());
		}
		
		return mongoTemplate.find(query, User.class);
	}
	
}
