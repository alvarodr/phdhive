package com.phdhive.archetype.dao.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.phdhive.archetype.dto.master.types.TypeDocumentMetaData;

/**
 * Mongo Utils.
 * 
 * @author ADORU3N
 *
 */
public class MongoUtils {

	/**
	 * Dado un mapa <clave, valor>, donde clave es el metadato y el valor es el criterio a buscar
	 * se devuelve una Query. 
	 * @param metaData
	 * @return
	 */
	public static Query metaDataToQuery(Map<TypeDocumentMetaData, String> metaData){
		Query query = new Query();
		Set<TypeDocumentMetaData> keys = metaData.keySet();
		for (TypeDocumentMetaData key : keys){
			query.addCriteria(Criteria.where("metadata." + key.getType()).is(metaData.get(key)));
		}
		return query;
	}
	
	/**
	 * Dado un mapa devuelve una coleccion de tipo DBObject para los metadatos
	 * @param metaData
	 * @return
	 */
	public static DBObject toDBObject(Map<TypeDocumentMetaData, String> metaData){
		DBObject dbObject = new BasicDBObject();
		Map<String, String> saveMetaData = new HashMap<String, String>();
		Set<TypeDocumentMetaData> keys = metaData.keySet();
		for (TypeDocumentMetaData key : keys) {
			saveMetaData.put(key.getType(), metaData.get(key));
		}
		dbObject.putAll(metaData==null ? new HashMap<String, String>() : saveMetaData);
		return dbObject;
	}
	
	/**
	 * Dada un DBObject devolvemos un mapa de String, String
	 * @param dbObject
	 * @return
	 */
	public static Map<TypeDocumentMetaData, String> fromDBObject(DBObject dbObject){
		Map<TypeDocumentMetaData, String> metaData = new HashMap<TypeDocumentMetaData, String>();
		Set<String> keys = dbObject.keySet();
		for (String key : keys){
			metaData.put(TypeDocumentMetaData.fromType(key), (String) dbObject.get(key));
		}
		return metaData;
	}
}
