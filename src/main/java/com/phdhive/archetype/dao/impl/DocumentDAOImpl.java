package com.phdhive.archetype.dao.impl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsOperations;
import org.springframework.stereotype.Repository;

import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSFile;
import com.phdhive.archetype.dao.IDocumentDAO;
import com.phdhive.archetype.dto.document.PhdDocument;
import com.phdhive.archetype.dto.master.types.TypeDocumentMetaData;
import com.phdhive.archetype.service.impl.DocumentServiceImpl;
import com.phdhive.archetype.service.validation.ServiceValidationException;
import com.phdhive.archetype.service.validation.fails.BusinessLogicFailCode;
import com.phdhive.archetype.service.validation.fails.ServiceValidationFail;

/**
 * 
 * @author ADORU3N
 *
 */
@Repository
public class DocumentDAOImpl implements IDocumentDAO {

	@Autowired
	private GridFsOperations gridTemplate;
	
	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.IDocumentDAO#findById(org.bson.types.ObjectId)
	 */
	@Override
	public PhdDocument findById(ObjectId id) {
		GridFSDBFile file = gridTemplate.findOne(new Query(Criteria.where("_id").is(id)));
		return toDocument(file);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.IBaseDAO#save(java.lang.Object)
	 */
	@Override
	public void save(PhdDocument entity) {
		save(entity);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.IDocumentDAO#saveDocument(com.phdhive.archetype.dto.document.PhdDocument)
	 */
	@Override
	public ObjectId saveDocument(PhdDocument document) {
		GridFSFile file = gridTemplate.store(document.getFile(), document.getDocumentName(),document.getContentType(), MongoUtils.toDBObject(document.getMetaData()));
		return  (ObjectId) file.getId();
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.IDocumentDAO#findDocument(java.util.Map)
	 */
	@Override
	public List<PhdDocument> findDocument(Map<TypeDocumentMetaData, String> metaData) {
		List<PhdDocument> documents = new ArrayList<PhdDocument>();
		List<GridFSDBFile> files = gridTemplate.find(MongoUtils.metaDataToQuery(metaData));
		for (GridFSDBFile file : files){
			documents.add(toDocument(file));
		}
		return documents;
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.IDocumentDAO#delete(org.bson.types.ObjectId)
	 */
	@Override
	public void delete(ObjectId id) {
		gridTemplate.delete(new Query(Criteria.where("_id").is(id)));
	}
	
	/*
	 * PRIVATE METHODS
	 */
	/**
	 * Transforma un GridFile en un Documento de PhD.
	 * 
	 * @param file
	 * @return
	 */
	private static PhdDocument toDocument(GridFSDBFile file){
		PhdDocument document = new PhdDocument();
		List<ServiceValidationFail> fails = new ArrayList<ServiceValidationFail>();
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		try {
			file.writeTo(os);
		} catch (IOException e) {
			fails.add(new ServiceValidationFail(BusinessLogicFailCode.DOCUMENT_FAIL_LOAD));
			throw new ServiceValidationException(DocumentServiceImpl.class.getName(), fails);
		}
		
		byte[] data = os.toByteArray();
		document.setFile(new ByteArrayInputStream(data));
		document.setId(file.getId().toString());
		document.setDocumentName(file.getFilename());
		document.setContentType(file.getContentType());
		document.setMetaData(MongoUtils.fromDBObject(file.getMetaData()));
		return document;
	}
}
