package com.phdhive.archetype.dao.impl;

import java.util.Collection;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.phdhive.archetype.dao.IEventDAO;
import com.phdhive.archetype.dto.community.Community;
import com.phdhive.archetype.dto.event.Event;
import com.phdhive.archetype.dto.event.states.EventState;

/**
 * 
 * @author ADORU3N
 *
 */
@Repository
public class EventDAOImpl implements IEventDAO {

	@Autowired
	private MongoTemplate mongoTemplate;
	
	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.IBaseDAO#findById(java.io.Serializable)
	 */
	@Override
	public Event findById(ObjectId id) {
		return mongoTemplate.findById(id, Event.class);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.IBaseDAO#save(java.lang.Object)
	 */
	@Override
	public void save(Event entity) {
		mongoTemplate.save(entity);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.IEventDAO#getAll()
	 */
	@Override
	public Collection<Event> getAll() {
		return mongoTemplate.findAll(Event.class);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.IEventDAO#getPaging(java.lang.Integer, java.lang.Integer, com.phdhive.archetype.dto.community.Community)
	 */
	@Override
	public Collection<Event> getPaging(Integer start, Integer limit, Community community) {
		Query query = new Query();
		if (community!=null){
			query.addCriteria(Criteria.where("EVT_OWNER.$id").is(new ObjectId(community.getId())));
			query.limit(limit).skip(start);
			return mongoTemplate.find(query, Event.class);
		}else{
			//Es el superadmin, debuelve todos los eventos
			return getAll();
		}

	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.IEventDAO#remove(com.phdhive.archetype.dto.event.Event)
	 */
	@Override
	public void remove(Event event) {
		mongoTemplate.remove(event);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.IEventDAO#getEventsByCommunityId(org.bson.types.ObjectId)
	 */
	@Override
	public Collection<Event> getEventsByCommunityId(ObjectId id) {
		return mongoTemplate.find(new Query(Criteria.where("EVT_OWNER.$id").is(id)), Event.class);
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.IEventDAO#getEventByName(java.lang.String)
	 */
	@Override
	public Event getEventByName(String evenName){
		return (mongoTemplate.find(new Query(Criteria.where("EVT_NAME").is(evenName)), Event.class)).get(0);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.IEventDAO#getEventByUser(org.bson.types.ObjectId)
	 */
	@Override
	public Collection<Event> getEventByUser(ObjectId id) {
		return mongoTemplate.find(new Query(Criteria.where("EVT_USERS.$id").is(id)), Event.class);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.IEventDAO#getActiveEvents()
	 */
	@Override
	public Collection<Event> getActiveEvents() {
		return mongoTemplate.find(new Query(Criteria.where("EVT_STATE").is(EventState.ACTIVE)), Event.class);
	}

}
