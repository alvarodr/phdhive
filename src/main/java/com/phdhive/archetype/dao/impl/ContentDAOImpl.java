package com.phdhive.archetype.dao.impl;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.phdhive.archetype.dao.IContentDAO;
import com.phdhive.archetype.dto.content.Content;
import com.phdhive.archetype.dto.content.RelatedContent;
import com.phdhive.archetype.dto.master.DataMaster;

/**
 * 
 * @author Jorge
 *
 */
@Repository
public class ContentDAOImpl implements IContentDAO {

	@Autowired
	private MongoTemplate mongoTemplate;

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.IBaseDAO#save(java.lang.Object)
	 */
	@Override
	public void save(Content content) {
		//TODO: Debería ir en el constructor de la clase Content
		content.setCreateDate(new Date());
		content.setUpdateDate(new Date());
		mongoTemplate.save(content);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.IContentDAO#updateContent(com.phdhive.archetype.dto.content.Content)
	 */
	@Override
	public Content updateContent(Content content) {

		Content contentFound = mongoTemplate.findById(content.getId(), Content.class);

		if (content.getRelatedContents() != null)
			contentFound.setRelatedContents(content.getRelatedContentsAsList());
		if (content.getTitle() != null) {
			// Si el título cambia hay que actualizar los contenidos que estén
			// relacionados
			if (!content.getTitle().equalsIgnoreCase(contentFound.getTitle())) {
				updateTitleInRelatedContents(content);
			}
			contentFound.setTitle(content.getTitle());
		}
		if (content.getHeader() != null)
			contentFound.setHeader(content.getHeader());
		if (content.getBody() != null)
			contentFound.setBody(content.getBody());
		if (content.getImage() != null)
			contentFound.setImage(content.getImage());
		if (content.getLinks() != null)
			contentFound.setLinks(content.getLinks());
		if (content.getType() != null)
			contentFound.setType(content.getTypeAsObject());

		content.setUpdateDate(new Date());

		mongoTemplate.save(contentFound);
		return contentFound;
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.IContentDAO#deleteContent(com.phdhive.archetype.dto.content.Content)
	 */
	@Override
	public void deleteContent(Content content) {
		mongoTemplate.remove(mongoTemplate.findById(content.getId(), Content.class));
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.IContentDAO#deleteAllContents()
	 */
	@Override
	public void deleteAllContents() {
		mongoTemplate.dropCollection(Content.class);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.IBaseDAO#findById(java.io.Serializable)
	 */
	@Override
	public Content findById(ObjectId id) {
		return mongoTemplate.findById(id, Content.class);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.IContentDAO#readAll()
	 */
	@Override
	public Collection<Content> readAll() {
		return mongoTemplate.findAll(Content.class);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.IContentDAO#getContentsPaging(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public List<Content> getContentsPaging(Integer start, Integer limit) {
		return mongoTemplate.find(new Query().limit(limit).skip(start), Content.class);
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.IContentDAO#findByType(com.phdhive.archetype.dto.master.DataMaster)
	 */
	@Override
	public Collection<Content> findByType(DataMaster tipo) {
		return mongoTemplate.find(new Query(Criteria.where("CONT_TYPE").is(tipo)), Content.class);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.IContentDAO#isEmpty()
	 */
	@Override
	public boolean isEmpty() {
		return mongoTemplate.findAll(Content.class).isEmpty();
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.IContentDAO#publish(com.phdhive.archetype.dto.content.Content)
	 */
	@Override
	public void publish(Content content) {
		content.setPublished(true);
		content.setPublishDate(new Date());
		this.updateContent(content);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.IContentDAO#unPublish(com.phdhive.archetype.dto.content.Content)
	 */
	@Override
	public void unPublish(Content content) {
		content.setPublished(false);
		this.updateContent(content);
	}
	
	/*
	 * PRIVATE METHODS
	 */
	
	/**
	 * Actualiza la referencia de este contenido en todos los que lo referencian
	 * 
	 * @param content
	 */
	private void updateTitleInRelatedContents(Content content) {

		for (RelatedContent relatedContentFromList : content.getRelatedContentsAsList()) {
			// Obtiene el contenido relacionado completo
			Content contentFromList = mongoTemplate.findById(relatedContentFromList.getRelContentRef(), Content.class);
			
			// Ya que la relación es bidireccional, se busca en lo relacionado
			// el contenido al que
			// se le está cambiando el título, o sea el actual, cambia el título
			// y lo actualiza
			for (RelatedContent relatedContentFromList2 : contentFromList.getRelatedContentsAsList()) {
				if (relatedContentFromList2.getRelContentRef().toString().equalsIgnoreCase(content.getId().toString())) {
					relatedContentFromList2.setRelContentName(content.getTitle());
					this.updateContent(contentFromList);
				}
			}
		}
	}
}
