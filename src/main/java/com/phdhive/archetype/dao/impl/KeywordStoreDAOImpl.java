package com.phdhive.archetype.dao.impl;

import java.util.Date;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.phdhive.archetype.dao.IKeywordStoreDAO;
import com.phdhive.archetype.dto.word.Keyword;

/**
 * 
 * @author Jorge
 *
 */
@Repository
public class KeywordStoreDAOImpl implements IKeywordStoreDAO {
	
	@Autowired
	private MongoTemplate mongoTemplate;
	
	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.IBaseDAO#save(java.lang.Object)
	 */
	@Override
	public void save(Keyword word) {
		word.setLastUpdate(new Date());
		mongoTemplate.save(word);		
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.IKeywordStoreDAO#deleteKeyword(com.phdhive.archetype.dto.word.Keyword)
	 */
	@Override
	public void deleteKeyword(Keyword word) {
		Keyword aux = this.readByKeyword(word);
		mongoTemplate.remove(mongoTemplate.findById(aux.getId(), Keyword.class) );
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.IKeywordStoreDAO#deleteAllKeywords()
	 */
	@Override
	public void deleteAllKeywords() {
		mongoTemplate.dropCollection(Keyword.class);
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.IKeywordStoreDAO#isEmpty()
	 */
	@Override
	public boolean isEmpty() {
		return mongoTemplate.findAll(Keyword.class).isEmpty();
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.IKeywordStoreDAO#readAll()
	 */
	@Override
	public List<Keyword> readAll() {
		return mongoTemplate.findAll(Keyword.class);
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.IBaseDAO#findById(java.io.Serializable)
	 */
	@Override
	public Keyword findById(ObjectId id) {
		return mongoTemplate.findById(id, Keyword.class);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.IKeywordStoreDAO#readByKeyword(com.phdhive.archetype.dto.word.Keyword)
	 */
	@Override
	public Keyword readByKeyword(Keyword word) {
		return mongoTemplate.findOne(new Query(Criteria.where("KWORD_WORD").is(word.getWord())), Keyword.class);
	}

}
