package com.phdhive.archetype.dao.impl;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.phdhive.archetype.dao.ICommunityDAO;
import com.phdhive.archetype.dto.community.Community;

/**
 * 
 * @author Alvaro
 *
 */
@Repository
public class CommunityDAOImpl implements ICommunityDAO {

	@Autowired
	private MongoTemplate mongoTemplate;
	
	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.IBaseDAO#findById(java.io.Serializable)
	 */
	@Override
	public Community findById(ObjectId id) {
		return mongoTemplate.findById(id, Community.class);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.IBaseDAO#save(java.lang.Object)
	 */
	@Override
	public void save(Community entity) {
		mongoTemplate.save(entity);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.ICommunityDAO#getAllCommunities()
	 */
	@Override
	public List<Community> getAllCommunities() {
		return mongoTemplate.findAll(Community.class);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.ICommunityDAO#deleteCommunity(com.phdhive.archetype.dto.community.Community)
	 */
	@Override
	public void deleteCommunity(Community community) {
		mongoTemplate.remove(community);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.ICommunityDAO#getCommunitiesPaging(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public List<Community> getCommunitiesPaging(Integer start, Integer limit) {
		return mongoTemplate.find(new Query().limit(limit).skip(start), Community.class);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.ICommunityDAO#getCommunityByAdmin(java.lang.String)
	 */
	@Override
	public Community getCommunityByAdmin(String email) {
		return mongoTemplate.findOne(new Query(Criteria.where("COMM_ADMIN.USER_EMAIL").is(email)), Community.class);
	}
}
