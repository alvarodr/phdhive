package com.phdhive.archetype.dao.impl;

import java.util.Collection;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.phdhive.archetype.dao.IDataMasterDAO;
import com.phdhive.archetype.dto.master.DataMaster;
import com.phdhive.archetype.dto.master.ITypeDataMaster;

/**
 * 
 * @author ADORU3N
 *
 */
@Repository
public class DataMasterDAOImpl implements IDataMasterDAO {

	@Autowired
	private MongoTemplate mongoTemplate;
	
	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.IDataMasterDAO#findAll()
	 */
	@Override
	public Collection<DataMaster> findAll() {
		return mongoTemplate.findAll(DataMaster.class);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.IDataMasterDAO#findByType(com.phdhive.archetype.dto.master.ITypeDataMaster)
	 */
	@Override
	public Collection<DataMaster> findByType(ITypeDataMaster tipo) {
		return mongoTemplate.find(new Query(Criteria.where("MAST_TYPE").is(tipo.type())), DataMaster.class);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.IBaseDAO#findById(java.io.Serializable)
	 */
	@Override
	public DataMaster findById(ObjectId id) {
		return mongoTemplate.findById(id, DataMaster.class);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.IBaseDAO#save(java.lang.Object)
	 */
	@Override
	public void save(DataMaster entity) {
		mongoTemplate.save(entity);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.IDataMasterDAO#findByCode(java.lang.String)
	 */
	@Override
	public DataMaster findByCode(String code) {
		return mongoTemplate.findOne(new Query(Criteria.where("MAST_CODE").is(code)), DataMaster.class);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.dao.IDataMasterDAO#findByeTypeCode(java.lang.Integer, java.lang.String)
	 */
	@Override
	public DataMaster findByeTypeCode(Integer type, String code) {
		return mongoTemplate.findOne(new Query(Criteria.where("MAST_TYPE").is(type)).addCriteria(Criteria.where("MAST_CODE").is(code)), DataMaster.class);
	}
}
