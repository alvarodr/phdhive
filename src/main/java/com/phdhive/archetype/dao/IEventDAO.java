package com.phdhive.archetype.dao;

import java.util.Collection;

import org.bson.types.ObjectId;

import com.phdhive.archetype.dto.community.Community;
import com.phdhive.archetype.dto.event.Event;
/**
 * 
 * @author Alvaro
 *
 */
public interface IEventDAO extends IBaseDAO<Event, ObjectId> {

	/**
	 * Devuelve una colección con todos los eventos
	 * @return
	 */
	Collection<Event> getAll();
	
	/**
	 * Devuelve una colección de eventos paginados
	 * @param start
	 * @param limit
	 * @param community
	 * @return
	 */
	Collection<Event> getPaging(Integer start, Integer limit, Community community);
	
	/**
	 * Elimina un evento
	 * @param event
	 */
	void remove(Event event);
	
	/**
	 * Devuelve una colección de eventos de una comunidad
	 * @param id
	 * @return
	 */
	Collection<Event> getEventsByCommunityId(ObjectId id);
	
	/**
	 * Recupera un evento dado un nombre
	 * @param evenName
	 * @return
	 */
	Event getEventByName(String evenName);
	
	/**
	 * Recupera la lista de eventos de un usuario
	 * @param email
	 * @return
	 */
	Collection<Event> getEventByUser(ObjectId id);
	
	/**
	 *  Lista los eventos activos
	 * @return
	 */
	public Collection<Event> getActiveEvents();
	
}
