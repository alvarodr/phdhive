package com.phdhive.archetype.dao;

import java.util.List;

import org.bson.types.ObjectId;

import com.phdhive.archetype.dto.word.Keyword;

/**
 * 
 * @author Jorge
 *
 */
public interface IKeywordStoreDAO extends IBaseDAO<Keyword, ObjectId> {

	/**
	 * 
	 * @return
	 */
	List<Keyword> readAll();

	/**
	 * 
	 * @param word
	 */
	void deleteKeyword(Keyword word);

	/**
	 * 
	 * @return
	 */
	boolean isEmpty();

	/**
	 * Borra todos los registros de la coleccion
	 */
	void deleteAllKeywords();
	
	/**
	 * 
	 * @param string
	 * @return
	 */
	 Keyword readByKeyword(Keyword word);
}
