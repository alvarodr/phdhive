package com.phdhive.archetype.dao;

import java.util.List;

import org.bson.types.ObjectId;

import com.phdhive.archetype.dto.community.Community;

/**
 * 
 * @author Alvaro
 *
 */
public interface ICommunityDAO extends IBaseDAO<Community, ObjectId> {

	/**
	 * Devuelve una lista con todas las comunidades existentes
	 * @return
	 */
	List<Community> getAllCommunities();
	
	/**
	 * Borra una comunidad
	 * @param community
	 */
	void deleteCommunity(Community community);
	
	/**
	 * Devuelve una lista de comunidades paginadas
	 * @param start
	 * @param limit
	 * @return
	 */
	List<Community> getCommunitiesPaging(Integer start, Integer limit);
	
	/**
	 * Devuelve la comunidad de la que es administrador
	 * @param email
	 * @return
	 */
	Community getCommunityByAdmin(String email);
}
