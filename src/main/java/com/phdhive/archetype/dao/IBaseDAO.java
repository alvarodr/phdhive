package com.phdhive.archetype.dao;

import java.io.Serializable;

import org.springframework.data.repository.Repository;

/**
 * 
 * @author ADORU3N
 *
 * @param <T>
 * @param <ID>
 */
public interface IBaseDAO<T, ID extends Serializable> extends Repository<T, ID> {
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	T findById(ID id);
	
	/**
	 * 
	 * @param entity
	 */
	void save(T entity);
}
