package com.phdhive.archetype.dao;

/**
 * 
 * @author Jorge
 *
 */

import java.util.Collection;
import java.util.List;

import org.bson.types.ObjectId;

import com.phdhive.archetype.dto.content.Content;
import com.phdhive.archetype.dto.master.DataMaster;

public interface IContentDAO extends IBaseDAO<Content, ObjectId> {
	
	/**
	 * 
	 * @param content
	 * @return
	 */
	Content updateContent(Content content);
	
	/**
	 * 
	 * @param content
	 */
	void deleteContent(Content content);

	/**
	 * Borra todos los registros de la coleccion
	 */
	void deleteAllContents();
	
	/**
	 * 
	 * @return
	 */
	boolean isEmpty();
	
	/**
	 * 
	 * @return Una coleción de todos de los contenidos
	 */
	Collection<Content> readAll();	
	
	/**
	 * Listado paginado
	 * @param start
	 * @param limit
	 * @return
	 */
	List<Content> getContentsPaging(Integer start, Integer limit);
	
	/**
	 * @param   type el tipo de contenido
	 * @return  Una coleción con todos de los contenidos del tipo especificado
	 */
	Collection<Content> findByType(DataMaster type); 
	
	/**
	 * 
	 * @param content
	 */
	void publish(Content content);
	
	/**
	 * 
	 * @param content
	 */
	void unPublish(Content content);

}
