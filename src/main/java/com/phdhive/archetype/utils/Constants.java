package com.phdhive.archetype.utils;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * 
 * @author Alvaro
 *
 */
public class Constants {
	public static final String USERS_FILE = "bulk/Reviewers.csv";
	public static final String DATA_MASTER_FILE = "bulk/DataMaster.csv";
	
	public static final String IP_SOURCE = "ipSource";
	
	public static final String NUMBER_FORMAT_2 = "###,###,##0.00";
	public static final String NUMBER_FORMAT_6 = "###,###,##0.000000";

	public static final String FORMATO_FECHA_POR_DEFECTO = "dd/MM/yyyy";
	public static final String FORMARTO_HORA = "HH:mm";
	public static final String FORMATO_FECHA_HORA = "dd/MM/yyyy HH:mm";

	public static final int MAYORIA_DE_EDAD = 18;

	public static final String TIPO_DOCUMENTACION_NIF = "N";
	public static final String TIPO_DOCUMENTACION_CIF = "C";
	public static final String TIPO_DOCUMENTACION_TITULARIDAD_FONDOS = "T";
	public static final String TIPO_DOCUMENTACION_CONTRATO_PENSIONES = "P";

	public static final String PAIS_ESPANA = "0011";

	public static final Calendar FECHA_POR_DEFECTO = new GregorianCalendar(1900, 0, 1);
	public static final String FECHA_POR_DEFECTO_FORMATEADA = "01/01/1900";

	public static final Integer NUMERO_IDENTIFICACION_LENGTH = 8;
	public static final String PATRON_NUMERO_IDENTIFICACION_SN = "SN\\d{6}";

	public static final String[] CARACTERES_CLAVE_TITULAR = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I",
			"J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };

	public static final String MONEDA_EURO = "€";

	public static final String SEPARADOR_MAILS = ",";
	public static final String PATTERN_EMAIL = 
			"[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*" + "@" + 
			"(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?";
	public static final String PATTERN_TELEFONO = "^\\d{9,}$";
	public static final String PATTERN_ALFANUMERICO = "[a-z|A-Z|0-9|ñ|Ñ]*";

	public static final String DIVISA_EURO_EUR = "EUR";

	/* DATOS SW_20_TIM en la tabla TFPTRASP */
	public static final String TRASPASO_INTERNO_PLAN_TIPO_MOVIMIENTO_PARCIAL = "P";
	public static final String TRASPASO_INTERNO_PLAN_TIPO_MOVIMIENTO_TOTAL = "T";

	public static final Integer NUM_MESES_POR_TRIMESTRE = 3;
	public static final Integer NUM_MESES_POR_SEMESTRE = 6;
	public static final Integer NUM_MESES_POR_ANYO = 12;

	public static final Double DOUBLE_100 = 100d;
	
	public static final String CATEGORIA_SALDOS_PENDIENTES = "999";
	
	public static final String PLANTILLA_CODIGO_CUENTA_CLIENTE = "AAAA-AAAA-AA-AAAAAAAAAA";

	//Horas de corte por defecto para el cálculo de fechas en planes cuando el producto no lo especifica
	public static final String SEPARADOR_HORAS = ":";
	
	//Constante USER SESSION
	public static final String PHD_USER_SESSION = "userSession";
	
	//Constant start login default
	public static final Integer DEFAULT_START_LOGIN_COUNT = 1;
}
