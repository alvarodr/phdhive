package com.phdhive.archetype.utils;

import org.springframework.security.core.GrantedAuthority;

import com.phdhive.archetype.dto.user.User;
import com.phdhive.archetype.security.Role;

/**
 * Utilidades de seguridad
 * @author ADORU3N
 *
 */
public class SecurityUtils {

	/**
	 * Identifica si un usuario tiene privilegios de administrador
	 * @param user
	 * @return
	 */
	public static Boolean isAdmin(User user){
		for (GrantedAuthority auth : user.getAuthorities()){
			if (Role.valueOf(auth.getAuthority()).equals(Role.ROLE_ADMIN))
				return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
	
}
