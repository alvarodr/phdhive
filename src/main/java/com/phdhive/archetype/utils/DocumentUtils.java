package com.phdhive.archetype.utils;

import java.io.IOException;

import com.phdhive.archetype.dto.master.types.TypeDocumentFormat;

/**
 * 
 * @author ADORU3N
 *
 */
public class DocumentUtils {

	/**
	 *  
	 * @param document
	 * @param format
	 * @return
	 * @throws IOException
	 */
	public static Boolean isFormatAllowed(String mimeType, TypeDocumentFormat format) throws IOException{
		return (mimeType.equals(format.getType()) || mimeType.equals(format.getTypeTika()));
	}
	
}
