package com.phdhive.archetype.utils;

import java.util.Collection;

import com.phdhive.archetype.dto.master.DataMaster;
import com.phdhive.archetype.dto.master.types.TypeDataMaster;
import com.phdhive.archetype.service.IDataMasterService;

/**
 * Utilidades para la gestión de datos maestros.
 * 
 * @author Alvaro
 *
 */
public final class DataMasterUtils {
	private static DataMasterUtils datoMaestroUtils;

	private IDataMasterService dataMasterService;

	private DataMasterUtils(IDataMasterService dataMasterService) {
		this.dataMasterService = dataMasterService;
	}

	public static DataMasterUtils getInstance(IDataMasterService dataMasterService) {
		if (datoMaestroUtils == null) {
			datoMaestroUtils = new DataMasterUtils(dataMasterService);
		}

		return datoMaestroUtils;
	}

	public static DataMaster getDataMaster(TypeDataMaster typeDataMaster, String code) {
		DataMaster result = null;

		if ((datoMaestroUtils != null) && (datoMaestroUtils.dataMasterService != null) && (typeDataMaster != null) && (code != null)) {
			result = datoMaestroUtils.dataMasterService.getDataMaster(typeDataMaster, code);
		}

		return result;
	}

	public static String translateDataMaster(TypeDataMaster typeDataMaster, String codigo) {
		String result = codigo;

		DataMaster dataMaster = getDataMaster(typeDataMaster, codigo);
		if (dataMaster != null) {
			result = dataMaster.getDescription();
		}

		return result;
	}
	
	public static Collection<DataMaster> getDataMasters(TypeDataMaster typeDataMaster) {
		Collection<DataMaster> result = null;
		
		result = datoMaestroUtils.dataMasterService.findByType(typeDataMaster);
		
		return result;
	}
}
