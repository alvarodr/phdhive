package com.phdhive.archetype.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.Map.Entry;

import com.phdhive.archetype.dto.master.DataMaster;

public class PhdhiveUtils {

	/**
	 * Conversion de una lista de datos maestrros a valores separados por comas.
	 * 
	 * @param datosMaestros
	 * @return
	 */
	public static String dataMasterToString(Collection<DataMaster> dataMasters) {
		StringBuffer result = new StringBuffer("");

		if (dataMasters != null) {
			for (DataMaster dataMaster : dataMasters) {
				result.append(dataMaster.getCode()).append(" : ").append(dataMaster.getDescription()).append(", ");
			}
		}

		// Quitamos la ultima coma
		if (result.length() > 0) {
			result.delete(result.length() - 2, result.length());
		}

		return result.toString();
	}
	
	//Comparador a para ordenar lista de objetos Entry<String, Integer>
	//en base a value
	public static Comparator<Entry<String, Integer>> BY_VALUE = new Comparator<Entry<String, Integer>>() {
		public int compare(final Entry<String, Integer> o1,
				Entry<String, Integer> o2) {
			return o2.getValue() - o1.getValue();
		}
	};
	
	public static Date toDate(String value){
		SimpleDateFormat sdf = new SimpleDateFormat(Constants.FORMATO_FECHA_HORA);
		try {
			return sdf.parse(value);
		} catch (ParseException e) {
			return null;
		}
	}
}
