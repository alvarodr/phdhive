package com.phdhive.archetype.utils;

import com.ckeditor.CKEditorConfig;

public class ConfigurationCKEditor {
	
	public static CKEditorConfig createConfig() {
		CKEditorConfig config = new CKEditorConfig();
		config.addConfigValue("toolbar", "Full");
		config.addConfigValue("uiColor", "#3B5998");
		//config.addConfigValue("skin", "kama");
		config.addConfigValue("width","90%");
		config.addConfigValue("height", "320px");
		return config;
	}
}
