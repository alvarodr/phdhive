package com.phdhive.archetype.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

import com.phdhive.archetype.dto.content.Content;
import com.phdhive.archetype.dto.document.PhdDocument;
import com.phdhive.archetype.dto.master.types.TypeDocumentMetaData;
import com.phdhive.archetype.service.exception.ServiceException;

public final class ComparatorUtils {
	public static final Comparator<? super Content> CONTENT_BY_ORDER = new Comparator<Content>() {
		@Override
		public int compare(Content p1, Content p2) {
			Integer c1 = p1.getOrder();
			Integer c2 = p2.getOrder();
			return c1.compareTo(c2);
		}
	};
	
	public static final Comparator<? super PhdDocument> DOCUMENT_BY_DATE_HOUR = new Comparator<PhdDocument>() {

		@Override
		public int compare(PhdDocument d1, PhdDocument d2) {
			SimpleDateFormat sdf = new SimpleDateFormat(Constants.FORMATO_FECHA_HORA);
			try {
				Date date1 = sdf.parse(d1.getMetaData().get(TypeDocumentMetaData.METADATA_DATE));
				Date date2 = sdf.parse(d2.getMetaData().get(TypeDocumentMetaData.METADATA_DATE));
				return date2.compareTo(date1);
			} catch (ParseException e) {
				throw new ServiceException(e.getMessage());
			}
		}
		
	};
	
	private ComparatorUtils() {}
}
