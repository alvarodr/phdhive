package com.phdhive.archetype.utils.ui.tags;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.phdhive.archetype.dto.document.PhdDocument;
import com.phdhive.archetype.dto.master.types.TypeDocumentMetaData;
import com.phdhive.archetype.service.IDocumentService;
import com.phdhive.archetype.utils.ComparatorUtils;

@Component
public class UserDocuments extends TagSupport {

	private static final long serialVersionUID = 1L;

	private String var;
	
    public int doStartTag() throws JspException {
        return super.doStartTag();
    }
    
	@Override
	public int doEndTag() throws JspException {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(pageContext.getServletContext());
		IDocumentService documentService = (IDocumentService) ctx.getBean(IDocumentService.class);
			
		String userEmail = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Map<TypeDocumentMetaData, String> metaData = new HashMap<TypeDocumentMetaData, String>();
		metaData.put(TypeDocumentMetaData.METADATA_OWNER, userEmail);
		List<String> documents = new ArrayList<String>();
		
		// Ordenamos por fecha mas reciente
		List<PhdDocument> documentsOrder = new ArrayList<PhdDocument>(documentService.findDocumentsByMetada(metaData));
		Collections.sort(documentsOrder, ComparatorUtils.DOCUMENT_BY_DATE_HOUR);
		
		for (PhdDocument doc : documentsOrder) {
			documents.add(doc.getDocumentName());
		}
		
		// Extraemos solo los cinco mas recientes
		if (documents != null && documents.size()>5) {
			documents = documents.subList(0, 4);
		}
			
		if (documents != null) {
			pageContext.setAttribute(var, documents);
		}
		return EVAL_PAGE;
	}
	
	public String getVar() {
		return var;
	}

	public void setVar(String var) {
		this.var = var;
	}
}
