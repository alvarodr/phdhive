package com.phdhive.archetype.utils.ui.tags;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.phdhive.archetype.dto.master.DataMaster;
import com.phdhive.archetype.dto.master.types.TypeDataMaster;
import com.phdhive.archetype.service.IDataMasterService;

public class DataMasterDisciplines extends TagSupport {

	private static final long serialVersionUID = -7948781302129008909L;

	private String var;

    public int doStartTag() throws JspException {
        return super.doStartTag();
    }
	
    public int doEndTag() throws JspException {
    	WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(pageContext.getServletContext());
    	IDataMasterService dataMasterService = ctx.getBean(IDataMasterService.class);
    	
    	Collection<DataMaster> knowledges = dataMasterService.findByType(TypeDataMaster.TYPES_KNOWLEDGES_UNSECO);
    	Map<String, String> disciplines = new HashMap<String, String>();
    	for (DataMaster dm : knowledges) {
    		Collection<DataMaster> disc = dataMasterService.findByType(TypeDataMaster.toTipoDatoMaestro(Integer.parseInt(dm.getCode())));
    		for (DataMaster sdm : disc) {
    			disciplines.put(sdm.getCode(), sdm.getDescription());
    		}
    	}
    	if (disciplines != null) {
    		pageContext.setAttribute(var, disciplines);
    	}
    	return EVAL_PAGE;
    }
    
	public String getVar() {
		return var;
	}

	public void setVar(String var) {
		this.var = var;
	}
	
}
