package com.phdhive.archetype.utils.ui.tags;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.lang.StringUtils;

import com.phdhive.archetype.service.exception.ServiceException;

public class MaskEmail extends TagSupport {

	private static final long serialVersionUID = 2906941018790669592L;
	
	private String email;
	
    public int doStartTag() throws JspException {
        return super.doStartTag();
    }
    
	@Override
	public int doEndTag() throws JspException {
		try {
			String charEncode = "";
			for (int i=1; i<email.length();i++){
				charEncode += "*";
			}
			pageContext.getOut().println(StringUtils.overlay(email, charEncode, 1, email.length() - 1));
		} catch (IOException e) {
			throw new ServiceException(e);
		}
		return EVAL_PAGE;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
}
