package com.phdhive.archetype.utils.ui.tags;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.phdhive.archetype.dto.user.User;
import com.phdhive.archetype.service.IUserService;

public class WordFrecuency extends TagSupport {

	private static final long serialVersionUID = -9149254399533311558L;

	private String var;
	
    public int doStartTag() throws JspException {
        return super.doStartTag();
    }
    
	@Override
	public int doEndTag() throws JspException {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(pageContext.getServletContext());
		IUserService userService = (IUserService) ctx.getBean(IUserService.class);
			
		String userEmail = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		User user = userService.readByEmail(userEmail);

		if (user.getKeywords()!=null && !user.getKeywords().isEmpty()) {
			List<Map.Entry<String, Integer>> list = new ArrayList<Map.Entry<String,Integer>>(user.getKeywords().entrySet());
			Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
				@Override
				public int compare(Entry<String, Integer> o1, Entry<String, Integer> o2) {
					return (o2.getValue().compareTo(o1.getValue()));
				}
			});
			if (list.size()>5)
				list = list.subList(0, 5);
			
			if (list != null) {
				pageContext.setAttribute(var, list);
			}
		}
			
		return EVAL_PAGE;
	}
	
	public String getVar() {
		return var;
	}

	public void setVar(String var) {
		this.var = var;
	}
}
