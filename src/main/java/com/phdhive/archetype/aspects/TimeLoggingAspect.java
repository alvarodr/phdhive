package com.phdhive.archetype.aspects;

import java.util.ArrayList;
import java.util.List;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;

@Aspect
@Order(1)
public class TimeLoggingAspect {

	private final static Logger log = LoggerFactory.getLogger(TimeLoggingAspect.class);
	
	private Boolean active = Boolean.FALSE;

	private boolean logParameters = true;

	private List<String> excludedServices = new ArrayList<String>();
	
	public boolean getActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public List<String> getExcludedServices() {
		return excludedServices;
	}

	public void setExcludedServices(List<String> excludedServices) {
		this.excludedServices = excludedServices;
	}

	public boolean getLogParameters() {
		return logParameters;
	}

	public void setLogParameters(boolean logParameters) {
		this.logParameters = logParameters;
	}

	/**
	 * @param params
	 *            array de objetos
	 * @return Un String con los valores de los objetos del array de entrada
	 *         separados por un espacio
	 */
	private String preparaParams(Object[] params) {
		String values = "";
		for (int i = 0; i < params.length; i++) {
			if (params[i] != null) {
				values += params[i].toString() + "  ";
			}
		}

		return values;
	}

	@Around("execution(* com.phdhive.archetype.service.impl.UserServiceImpl.searhUsersSolr(..))")
	public Object doBasicTiming(ProceedingJoinPoint pjp) throws Throwable {

		String clase = "";
		// En ciertos casos el nombre de clase no viene en el target sino en la
		// firma
		if (pjp.getTarget() != null)
			clase = pjp.getTarget().getClass().getName();
		else if (pjp.getSignature().getDeclaringTypeName() != null)
			clase = pjp.getSignature().getDeclaringTypeName();

		Object[] params = pjp.getArgs();

		long tiempoInicial = System.currentTimeMillis();

		Object retVal = pjp.proceed();

		long tiempoFinal = System.currentTimeMillis();

		log.debug("El método [" + clase + "." + pjp.getSignature().getName()
				+ "]" + " con los parámetros [" + preparaParams(params) + "]"
				+ " ha tardado en ejecutarse [" + (tiempoFinal - tiempoInicial)
				+ "] milisegundos ");

		return retVal;
	}
	
	@Around("execution(public * com.phdhive.archetype.*.service..I*Service.*(..))")
	public Object doLog(ProceedingJoinPoint joinPoint) throws Throwable {
		String serviceClassName = joinPoint.getTarget().getClass().getName();
		
		boolean doLogging = active && log.isInfoEnabled() && !excludedServices.contains(serviceClassName);
		
		if (doLogging) {
			StringBuffer sb = new StringBuffer();
			if (logParameters) {
				int i = 0;
				for (Object arg : joinPoint.getArgs()) {
					if (i != 0) {
						sb.append(", ");
					} else {
						sb.append("parameters: ");
					}
					sb.append("arg(").append(i).append(")=").append(arg);
					i++ ;
				}
			}
			
			Object params[] = {joinPoint.getTarget().getClass().getName(), joinPoint.getSignature().getName(), sb.toString()};
			log.info("START {}.{} {}", params);
		}
		
		try {
			return joinPoint.proceed();
		} catch (Exception e) {
			if (doLogging) {
				log.error("ERROR: ", e);
			}
			throw e;
		} finally {
			if (doLogging) {
				log.info("END {}.{}", joinPoint.getTarget().getClass().getName(), joinPoint.getSignature().getName());
			}
		}
	}
}
