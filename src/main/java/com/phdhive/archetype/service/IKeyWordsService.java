package com.phdhive.archetype.service;

import java.util.List;
import java.util.Locale;
import java.util.Map.Entry;

import com.phdhive.archetype.dto.document.PhdDocument;

public interface IKeyWordsService {

	/**
	 * 
	 * @param doc
	 * @return
	 */
	String[] getKeyWords(PhdDocument document, Locale locale);

	/**
	 * 
	 * @param documento
	 * @return
	 * @throws Exception
	 */
	String getKeyWordsAndFrequency(PhdDocument document);

	/**
	 * 
	 * @param text
	 * @return
	 */
	List<Entry<String, Integer>> getKeyWordsMap(String text, Locale locale);

	/**
	 * 
	 * @param text
	 * @return
	 */
	String stem(String text);

	/**
	 * 
	 * @return
	 */
	List<Entry<String, Integer>> getQueryWords();

	/**
	 * 
	 * @param queryWords
	 */
	void setQueryWords(List<Entry<String, Integer>> queryWords);

	/**
	 * 
	 * @param text
	 * @return
	 */
	List<String> getKeyWords(String text, Locale locale);

	/**
	 * 
	 * @param document
	 * @return
	 */
	List<Entry<String, Integer>> getKeyWordsAndFrecuency(String document, Locale locale);
}
