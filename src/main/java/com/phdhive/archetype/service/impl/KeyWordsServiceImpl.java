package com.phdhive.archetype.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map.Entry;

import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.extractor.WordExtractor;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.tika.Tika;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.phdhive.archetype.dto.document.PhdDocument;
import com.phdhive.archetype.dto.master.types.TypeDocumentFormat;
import com.phdhive.archetype.service.IKeyWordsService;
import com.phdhive.archetype.service.cue.lang.Counter;
import com.phdhive.archetype.service.cue.lang.NGramIterator;
import com.phdhive.archetype.service.cue.lang.stop.StopWords;
import com.phdhive.archetype.service.validation.ServiceValidationException;
import com.phdhive.archetype.service.validation.fails.BusinessLogicFail;
import com.phdhive.archetype.service.validation.fails.BusinessLogicFailCode;
import com.phdhive.archetype.service.validation.fails.ServiceValidationFail;
import com.phdhive.archetype.snowball.ext.EnglishStemmer;
import com.phdhive.archetype.utils.DocumentUtils;

/**
 * Servicio para la gestión de las palabras clave.
 * 
 * @author ADORU3N
 *
 */
@Service
public class KeyWordsServiceImpl implements IKeyWordsService {
	
	@Value("#{applicationConfig['number_of_search_keywords']}")
	private String number_of_keywords;	
	
	//Lista que contiene las palabras encontradas en el documento de la búsqueda actual
	private List<Entry<String, Integer>> queryWords;

	private String WHITE_SPACE = " ";
	
	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IKeyWordsService#getQueryWords()
	 */
	@Override
	public List<Entry<String, Integer>> getQueryWords() {
		return queryWords;
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IKeyWordsService#setQueryWords(java.util.List)
	 */
	@Override
	public void setQueryWords(List<Entry<String, Integer>> queryWords) {
		this.queryWords = queryWords;
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IKeyWordsService#getKeyWords(com.phdhive.archetype.dto.document.PhdDocument, java.util.Locale)
	 */
	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public String[] getKeyWords(PhdDocument document, Locale locale) {
		List<ServiceValidationFail> fail = new ArrayList<ServiceValidationFail>();
		WordExtractor docExtractor = null;
		XWPFWordExtractor docxExtractor = null;
		String strDocument = "";
		try {
			String mimeType = new Tika().detect(document.getFile());
			if (DocumentUtils.isFormatAllowed(mimeType, TypeDocumentFormat.MIMETYPE_DOC)) {
				HWPFDocument doc = new HWPFDocument(document.getFile());
				docExtractor = new WordExtractor(doc);
				String[] docArray = docExtractor.getParagraphText();

				for (String element : docArray) {
					if (element != null) {
						strDocument += element + "\n";
					}
				}
			} else if (DocumentUtils.isFormatAllowed(mimeType, TypeDocumentFormat.MIMETYPE_DOCX)) {
				XWPFDocument docx = new XWPFDocument(document.getFile());
				docxExtractor = new XWPFWordExtractor(docx);
				strDocument = docxExtractor.getText();
			} else{
				fail.add(new BusinessLogicFail(BusinessLogicFailCode.DOCUMENT_FORMAT));
				throw new ServiceValidationException(this.getClass().getName(), fail);
			}
		} catch (IOException e) {
			fail.add(new BusinessLogicFail(BusinessLogicFailCode.DOCUMENT_FAIL_LOAD));
			throw new ServiceValidationException(this.getClass().getName(), fail);
		}
		return listWord(strDocument, locale);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IKeyWordsService#getKeyWordsAndFrequency(com.phdhive.archetype.dto.document.PhdDocument)
	 */
	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public String getKeyWordsAndFrequency(PhdDocument document) {
		List<ServiceValidationFail> fail = new ArrayList<ServiceValidationFail>();
		WordExtractor docExtractor = null;
		XWPFWordExtractor docxExtractor = null;
		String strDocument = "";
		try{
			String mimeType = new Tika().detect(document.getFile());
			InputStream file = document.getFile();
			if (DocumentUtils.isFormatAllowed(mimeType, TypeDocumentFormat.MIMETYPE_DOCX)) {
				XWPFDocument docx = new XWPFDocument(OPCPackage.open(file));
				docxExtractor = new XWPFWordExtractor(docx);
				strDocument = docxExtractor.getText();
			} else if (DocumentUtils.isFormatAllowed(mimeType, TypeDocumentFormat.MIMETYPE_DOC)) {
					HWPFDocument doc = new HWPFDocument(file);
					docExtractor = new WordExtractor(doc);
					String[] docArray = docExtractor.getParagraphText();

					for (String element : docArray) {
						if (element != null) {
							strDocument += element + "\n";
						}
					}
			} else {
				fail.add(new ServiceValidationFail(BusinessLogicFailCode.DOCUMENT_FORMAT));
				throw new ServiceValidationException(this.getClass().getName(), fail);
			}
		} catch (IOException e){
			fail.add(new ServiceValidationFail(BusinessLogicFailCode.DOCUMENT_FAIL_LOAD));
			throw new ServiceValidationException(this.getClass().getName(), fail);			
		} catch (InvalidFormatException e) {
			fail.add(new ServiceValidationFail(BusinessLogicFailCode.INVALID_FORMAT_DOCUMENT));
			throw new ServiceValidationException(this.getClass().getName(), fail);
		}

		return strDocument;
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IKeyWordsService#getKeyWordsAndFrecuency(java.lang.String, java.util.Locale)
	 */
	@Override
	public List<Entry<String, Integer>> getKeyWordsAndFrecuency(String document, Locale locale){
		if (document != null){
			final Counter<String> ngrams_stem = new Counter<String>();
			for (final String ngram : new NGramIterator(1, document, locale, StopWords.English)) {
				//Añade el stem para la búsqueda
				ngrams_stem.note(stem(ngram.toLowerCase(locale)));
			}

			//Construye la lista de stems encontrados para sacarla por pantalla
			List<Entry<String, Integer>> total_stem_list = ngrams_stem.getAllByFrequency();
			List<Entry<String, Integer>> final_word_list = new ArrayList<Entry<String, Integer>>();
			for(int i=0;i<Integer.valueOf(number_of_keywords); i++){
				final_word_list.add(total_stem_list.get(i));
			}		
			setQueryWords(final_word_list);
			return total_stem_list;
		}
		return null;
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IKeyWordsService#getKeyWordsMap(java.lang.String, java.util.Locale)
	 */
	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public List<Entry<String, Integer>> getKeyWordsMap(String text, Locale locale) {
		final Counter<String> ngrams = new Counter<String>();
		for (final String ngram : new NGramIterator(1, text, locale, StopWords.English)) {
			ngrams.note(stem(ngram.toLowerCase(locale)));
		}
		return ngrams.getAllByFrequency();
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IKeyWordsService#getKeyWords(java.lang.String, java.util.Locale)
	 */
	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public List<String> getKeyWords(String text, Locale locale) {
		List<String> result = new ArrayList<String>();
		for (Entry<String, Integer> entry  : getKeyWordsMap(text, locale)) { 
		  	result.add(entry.getKey());
		}
		return result;
	}
	
	@Transactional(propagation=Propagation.REQUIRED, readOnly = true)
	@Override
	public String stem(String text) {
		String result = "";
		EnglishStemmer stemmer = new EnglishStemmer();
		String[] list = text.split(WHITE_SPACE);
		for(int i=0;i<list.length;i++){
			String word = list[i].toLowerCase();
			stemmer.setCurrent(word);
		    stemmer.stem();
		    if (i<list.length-1)
		      result += stemmer.getCurrent() + WHITE_SPACE;
		    else
		      result += stemmer.getCurrent();
		}		
		return result;
	}
	
	/*
	 * PRIVATE METHODS
	 */
	/**
	 * 
	 * @param hound
	 * @param locale
	 * @return
	 */
	private static String[] listWord(final String hound, final Locale locale) {
		
		final Counter<String> ngrams = new Counter<String>();
		for (final String ngram : new NGramIterator(1, hound, locale, StopWords.English)) {
			ngrams.note(ngram.toLowerCase(locale));
		}
				
		String[] words = new String[ngrams.getAllByFrequency().size()];
		int cont = 0;
		
		//Copia el resultado en una array de Strings 
		for (final Entry<String, Integer> e : ngrams.getAllByFrequency()
				.subList(0, ngrams.getAllByFrequency().size())) {
			words[cont] = e.getKey();
			cont++;
		}
		return words;
	}
}
