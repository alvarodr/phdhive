package com.phdhive.archetype.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.phdhive.archetype.dao.IContentDAO;
import com.phdhive.archetype.dto.content.Content;
import com.phdhive.archetype.dto.content.RelatedContent;
import com.phdhive.archetype.dto.master.DataMaster;
import com.phdhive.archetype.service.IContentService;

/**
 * Servicio para la gestión de los contenidos relacionados.
 * 
 * @author ADORU3N
 *
 */
@Service
public class ContentServiceImpl implements IContentService {

	@Autowired
	private IContentDAO contentDao;
	
	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IContentService#createContent(com.phdhive.archetype.dto.content.Content)
	 */
	@Override
	@Transactional
	public void createContent(Content content) {
		contentDao.save(content);
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IContentService#updateContent(com.phdhive.archetype.dto.content.Content)
	 */
	@Override
	@Transactional
	public Content updateContent(Content content) {
		return contentDao.updateContent(content);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IContentService#deleteContent(com.phdhive.archetype.dto.content.Content)
	 */
	@Override
	@Transactional
	public void deleteContent(Content content) {
		contentDao.deleteContent(content);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IContentService#deleteAllContents()
	 */
	@Override
	@Transactional
	public void deleteAllContents() {
		contentDao.deleteAllContents();
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IContentService#findById(org.bson.types.ObjectId)
	 */
	@Override
	@Transactional(readOnly = true)
	public Content findById(ObjectId id) {
		return contentDao.findById(id);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IContentService#readAll()
	 */
	@Override
	@Transactional(readOnly = true)
	public Collection<Content> readAll() {
		return contentDao.readAll();
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IContentService#getContentsPaging(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public List<Content> getContentsPaging(Integer start, Integer limit) {
		return contentDao.getContentsPaging(start, limit);
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IContentService#findByType(com.phdhive.archetype.dto.master.DataMaster)
	 */
	@Override
	@Transactional(readOnly = true)
	public Collection<Content> findByType(DataMaster type){
		return contentDao.findByType(type);
	}
	
	
	@Override
	@Transactional(readOnly = true)
	public Boolean isEmpty() {
		return contentDao.isEmpty();
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IContentService#relateContents(com.phdhive.archetype.dto.content.Content, com.phdhive.archetype.dto.content.Content)
	 */
	@Override
	public void relateContents(Content content1,Content content2) {
		
		RelatedContent relatedCont1 = new RelatedContent();
		RelatedContent relatedCont2 = new RelatedContent();
		List<RelatedContent> relatedContentList = new ArrayList<RelatedContent>();
		
		relatedCont1.setRelContentName(content1.getTitle());
		relatedCont1.setRelContentRef( new ObjectId(content1.getId()) );
		if (content2.getRelatedContents()!=null && !content2.getRelatedContents().equalsIgnoreCase(""))
		  relatedContentList = content2.getRelatedContentsAsList();		
		relatedContentList.add(relatedCont1);
		content2.setRelatedContents(relatedContentList);

		relatedCont2.setRelContentName(content2.getTitle());
		relatedCont2.setRelContentRef(new ObjectId(content2.getId()));		
		if (content1.getRelatedContents()!=null && !content1.getRelatedContents().equalsIgnoreCase(""))
		   relatedContentList = content1.getRelatedContentsAsList();
		else
			relatedContentList = new ArrayList<RelatedContent>();
		relatedContentList.add(relatedCont2);
		content1.setRelatedContents(relatedContentList);

		this.updateContent(content1);
		this.updateContent(content2);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IContentService#unRelateContents(com.phdhive.archetype.dto.content.Content, com.phdhive.archetype.dto.content.Content)
	 */
	@Override
	public void unRelateContents(Content content1,Content content2){
		List<RelatedContent> relatedContent1List =content1.getRelatedContentsAsList();
		int i = 0;
		while (relatedContent1List.size()>0 && i<relatedContent1List.size()){
			RelatedContent relCon = relatedContent1List.get(i);
	   		if (relCon.getRelContentRef().toString().equalsIgnoreCase(content2.getId())){
	   			relatedContent1List.remove(relCon);
	   			i--;
	   		}
	   		i++;
		}		
	   	content1.setRelatedContents(relatedContent1List);
	   	this.updateContent(content1);
	   	
		List<RelatedContent> relatedContent2List =content2.getRelatedContentsAsList();
		i = 0;
		while (relatedContent2List.size()>0 && i<relatedContent2List.size()){
			RelatedContent relCon = relatedContent2List.get(i);
	   		if (relCon.getRelContentRef().toString().equalsIgnoreCase(content1.getId())){
	   			relatedContent2List.remove(relCon);
	   			i--;
	   		}
	   		i++;
		}	
	   	content2.setRelatedContents(relatedContent2List);
	   	this.updateContent(content2);
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IContentService#publish(com.phdhive.archetype.dto.content.Content)
	 */
	@Override
	public void publish(Content content){
		contentDao.publish(content);
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IContentService#unPublish(com.phdhive.archetype.dto.content.Content)
	 */
	@Override
	public void unPublish(Content content){
		contentDao.unPublish(content);
	}
}
