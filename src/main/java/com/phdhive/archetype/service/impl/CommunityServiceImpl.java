package com.phdhive.archetype.service.impl;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.phdhive.archetype.dao.ICommunityDAO;
import com.phdhive.archetype.dto.community.Community;
import com.phdhive.archetype.service.ICommunityService;

/**
 * Servicio para la gestión de las comunidades.
 * 
 * @author ADORU3N
 *
 */
@Service
public class CommunityServiceImpl implements ICommunityService {

	@Autowired
	private ICommunityDAO communityDAO;
	
	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.ICommunityService#getAllCommunities()
	 */
	@Override
	public List<Community> getAllCommunities() {
		return communityDAO.getAllCommunities();
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.ICommunityService#getCommunityById(org.bson.types.ObjectId)
	 */
	@Override
	public Community getCommunityById(ObjectId id) {
		return communityDAO.findById(id);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.ICommunityService#saveCommunity(com.phdhive.archetype.dto.community.Community)
	 */
	@Override
	public void saveCommunity(Community community) {
		communityDAO.save(community);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.ICommunityService#deleteCommunity(com.phdhive.archetype.dto.community.Community)
	 */
	@Override
	public void deleteCommunity(Community community) {
		communityDAO.deleteCommunity(community);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.ICommunityService#getCommunitiesPaging(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public List<Community> getCommunitiesPaging(Integer start, Integer limit) {
		return communityDAO.getCommunitiesPaging(start, limit);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.ICommunityService#getCommunityForAdmin(java.lang.String)
	 */
	@Override
	public Community getCommunityForAdmin(String email) {
		return communityDAO.getCommunityByAdmin(email);
	}

}
