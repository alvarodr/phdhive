package com.phdhive.archetype.service.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.phdhive.archetype.dao.IDataMasterDAO;
import com.phdhive.archetype.dto.master.DataMaster;
import com.phdhive.archetype.dto.master.ITypeDataMaster;
import com.phdhive.archetype.service.IDataMasterService;

/**
 * Servicio para la gestión de los datos maestros.
 * 
 * @author ADORU3N
 *
 */
@Service
public class DataMaseterServiceImpl implements IDataMasterService {

	@Autowired
	private IDataMasterDAO dataMasterDAO;

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IDataMasterService#findByType(com.phdhive.archetype.dto.master.ITypeDataMaster)
	 */
	@Override
	@Transactional(readOnly = true)
	public Collection<DataMaster> findByType(ITypeDataMaster type) {
		return dataMasterDAO.findByType(type);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IDataMasterService#findByTypeAsMap(com.phdhive.archetype.dto.master.ITypeDataMaster)
	 */
	@Override
	@Transactional(readOnly = true)
	public Map<String, DataMaster> findByTypeAsMap(ITypeDataMaster type) {
		Map<String, DataMaster> mapDataMaster = new HashMap<String, DataMaster>();
		Collection<DataMaster> dataMaster = dataMasterDAO.findByType(type);
		for (DataMaster dm : dataMaster){
			mapDataMaster.put(dm.getCode(), dm);
		}
		return mapDataMaster;
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IDataMasterService#findAll()
	 */
	@Override
	@Transactional(readOnly = true)
	public Collection<DataMaster> findAll() {
		return dataMasterDAO.findAll();
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IDataMasterService#getDataMaster(com.phdhive.archetype.dto.master.ITypeDataMaster, java.lang.String)
	 */
	@Override
	@Transactional(readOnly = true)
	public DataMaster getDataMaster(ITypeDataMaster type, String code) {
		Collection<DataMaster> dataMaster = dataMasterDAO.findByType(type);
		for (DataMaster dm : dataMaster){
			if (dm.getCode().equals(code)){
				return dm;
			}
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IDataMasterService#getDataMasterByDesc(com.phdhive.archetype.dto.master.ITypeDataMaster, java.lang.String)
	 */
	@Override
	@Transactional(readOnly = true)
	public DataMaster getDataMasterByDesc(ITypeDataMaster type, String description) {
		Collection<DataMaster> dataMaster = dataMasterDAO.findByType(type);
		for (DataMaster dm : dataMaster){
			if (dm.getDescription().equals(description)){
				return dm;
			}
		}
		return null;
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IDataMasterService#createDataMaster(com.phdhive.archetype.dto.master.DataMaster)
	 */
	@Override
	@Transactional
	public void createDataMaster(DataMaster dataMaster) {
		dataMasterDAO.save(dataMaster);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IDataMasterService#getDataMasterByCode(java.lang.String)
	 */
	@Override
	@Transactional(readOnly = true)
	public DataMaster getDataMasterByCode(String code) {
		return dataMasterDAO.findByCode(code);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IDataMasterService#existsDataMaster(java.lang.Integer, java.lang.String)
	 */
	@Override
	@Transactional(readOnly = true)
	public DataMaster existsDataMaster(Integer type, String code) {
		return dataMasterDAO.findByeTypeCode(type, code);
	}
}
