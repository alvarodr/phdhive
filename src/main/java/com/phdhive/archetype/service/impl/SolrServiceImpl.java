package com.phdhive.archetype.service.impl;

import java.io.IOException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map.Entry;
import java.util.StringTokenizer;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.ConcurrentUpdateSolrServer;
import org.apache.solr.client.solrj.request.UpdateRequest;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.params.CommonParams;
import org.apache.solr.common.params.HighlightParams;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.phdhive.archetype.dao.IUserDAO;
import com.phdhive.archetype.dto.document.PhdDocument;
import com.phdhive.archetype.dto.event.Event;
import com.phdhive.archetype.dto.user.User;
import com.phdhive.archetype.service.IBulkService;
import com.phdhive.archetype.service.IKeyWordsService;
import com.phdhive.archetype.service.ISolrService;
import com.phdhive.archetype.service.validation.ServiceValidationException;
import com.phdhive.archetype.service.validation.fails.BusinessLogicFailCode;
import com.phdhive.archetype.service.validation.fails.ServiceValidationFail;

/**
 * Servicio de indexación y busqueda en Solr.
 * 
 * @author ADORU3N
 *
 */
@Service
public class SolrServiceImpl implements ISolrService {
	@Value("#{applicationConfig['solr_url']}")
	private String SOLR_URL;

	@Value("#{applicationConfig['number_of_search_keywords']}")
	private String number_of_keywords;

	@Autowired
	private IBulkService bulkService;

	@Autowired
	private IKeyWordsService keyWordsService;

	@Autowired
	private IUserDAO userDao;

	@Autowired
	@Qualifier("solrService")
	private SolrServer solrServer;
		
	final static int OP_NEW = 1;
	final static int OP_UPDATE = 2;
	final static int OP_REMOVE = 3;	
	
	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.ISolrService#searhUsersSolr(com.phdhive.archetype.dto.document.PhdDocument, java.util.Locale, com.phdhive.archetype.dto.event.Event)
	 */
	@Override
	@Transactional(readOnly = true)
	public List<User> searhUsersSolr(PhdDocument document, Locale locale,Event event) {
		List<ServiceValidationFail> fail = new ArrayList<ServiceValidationFail>();
		try{
			// Verificamos si es necesario cargar usuarios del fichero CSV
			String contentDocument = keyWordsService.getKeyWordsAndFrequency(document);
			List<Entry<String, Integer>> documentKeyWordsMap = keyWordsService.getKeyWordsAndFrecuency(contentDocument, locale);
			return findUsers(documentKeyWordsMap,event);					
		} catch (ServiceValidationException e) {
			throw new ServiceValidationException(this.getClass().getName(), e.getFails());
		} catch (Exception e) {
			fail.add(new ServiceValidationFail(BusinessLogicFailCode.DATA_LOAD_ERROR));
			throw new ServiceValidationException(this.getClass().getName(), fail);
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.ISolrService#searchUsersSolr(com.phdhive.archetype.dto.user.User, java.util.Locale)
	 */
	@Override
	@Transactional(readOnly = true)
	public List<User> searchUsersSolr(User user, Locale locale) {
		return findUsers(keyWordsService.getKeyWordsAndFrecuency(user.getLatestArticle(), locale),null);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.ISolrService#findUsers(java.util.List, com.phdhive.archetype.dto.event.Event)
	 */
	@Override
	@Transactional(readOnly = true)
	public List<User> findUsers(List<Entry<String, Integer>> documentKeyWordsMap,Event event) {
		
		String queryStr = "";
		List<User> users = new ArrayList<User>();
		SolrQuery query = new SolrQuery();

		try {
			query.set(CommonParams.FL, "score,*");
			query.set(CommonParams.START, "0");
			query.set(CommonParams.ROWS, "20");

			// Para obtener las palabras de la query encontradas en cada documento:
			query.setHighlight(true);
			query.set(HighlightParams.FIELDS, "research_area,latest_article,keywords");
			query.set(HighlightParams.FRAGSIZE, "5000");

			int statusCount = 0;
			for (Entry<String, Integer> listWord : documentKeyWordsMap) {
				queryStr += "text:" + listWord.getKey() + "^" + listWord.getValue()	+ " OR ";
				if (statusCount++ == Integer.valueOf(number_of_keywords))
					break;
			}

			queryStr = queryStr.substring(0, queryStr.length() - 4);// quita último
																	// OR
			if (event!=null){
				queryStr = "("+queryStr+")";
				queryStr += " AND events:"+event.getName();
			}
			query.setQuery(queryStr);

			QueryResponse queryResponse = solrServer.query(query);

			SolrDocumentList docList = queryResponse.getResults();
			String id = "";
			User user = new User();
			for (SolrDocument solrDoc : docList) {
				id = (String) solrDoc.getFieldValue("id");
				user = userDao.findById(new ObjectId(id));
				if (user != null) {
					Float score = (Float) solrDoc.getFieldValue("score");
					score *= 100;
					if (score>100) score = 100F;
					user.setScore(score);
					user = setUserFoundWords(user, queryResponse);
					users.add(user);
				}
			}
		} catch (SolrServerException e) {
			List<ServiceValidationFail> fail = new ArrayList<ServiceValidationFail>();
			fail.add(new ServiceValidationFail(BusinessLogicFailCode.SOLR_EXCEPTION));
			throw new ServiceValidationException(this.getClass().getName(), fail);
		}

		return users;
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.ISolrService#updateUserIndex(com.phdhive.archetype.dto.user.User, int)
	 */
	@Override
	@Transactional
	public void updateUserIndex(User user, int operation) {

		SolrServer solr = null;
		List<ServiceValidationFail> fails = new ArrayList<ServiceValidationFail>();
	
		try {
			if (operation==OP_UPDATE)
				// En Solr, no se pueden actualizar campos, sólo el objeto entero
				solr = new ConcurrentUpdateSolrServer(SOLR_URL, 1, 0);
			else
				solr = this.solrServer;

			if (operation==OP_UPDATE || operation==OP_NEW ){
					
				SolrInputDocument document = new SolrInputDocument();
				
				document.addField("id", user.getId());
				if (user.getKeywords() != null && !user.getKeywords().isEmpty()){
					Iterator<String> iter = user.getKeywords().keySet().iterator();
                    String keywords = "";
					while(iter.hasNext()){
						String key =  iter.next();
						keywords += key;
						keywords += " ";
					}
					document.addField("keywords", keywords);
				}
				if (user.getResearchArea() != null && !user.getResearchArea().equalsIgnoreCase(""))
					document.addField("research_area", keyWordsService.stem(user.getResearchArea()));
				if (user.getLatestArticle() != null && !user.getLatestArticle().equalsIgnoreCase(""))
					document.addField("latest_article", keyWordsService.stem(user.getLatestArticle()));
				if (user.getEventsNames()!=null){
					for(String eventName:user.getEventsNames())
					   document.addField("events", eventName);
				}
				
				UpdateRequest req = new UpdateRequest();
				req.add(document);
				req.setCommitWithin(100);
				req.process(solr);
			}else{ //remove
				solr.deleteByQuery("id:"+user.getId().toString());
				solr.commit();
			}
						
		} catch (IOException e){
			fails.add(new ServiceValidationFail(BusinessLogicFailCode.SOLR_CONNECTION_TIMEOUT));
			throw new ServiceValidationException(this.getClass().getName(), fails);
		} catch (SolrServerException e) {
			e.printStackTrace();
			fails.add(new ServiceValidationFail(BusinessLogicFailCode.SOLR_EXCEPTION));
			throw new ServiceValidationException(this.getClass().getName(), fails);
		}
	}

	/*
	 * PRIVATE METHODS
	 */
	/**
	 * 
	 * @param user
	 *            Usuario para el que se va a obtener su lista de palabras
	 *            encontradas
	 * @param rsp
	 *            Resultado de una búsqueda en Solr
	 * @return
	 */
	private User setUserFoundWords(User user, QueryResponse rsp) {

		String id = user.getObjectId().toStringMongod();
		List<Entry<String, Integer>> foundWords = new ArrayList<Entry<String, Integer>>();
		if (rsp.getHighlighting().get(id) != null) {
			final String fields[] = { "bio", "research_area", "latest_article", "keywords" };
			for (String field : fields) {
				List<String> highlightList = rsp.getHighlighting().get(id).get(field);
				String highlightText = "";
				if (highlightList != null) {
					highlightText = highlightList.get(0);
					foundWords = findHighlight(highlightText, foundWords);
				}
			}
		}

		user.setFoundWords(foundWords);

		return user;
	}

	/**
	 * Encuentra las palabras dentro de la estiqueta <em></em>
	 * 
	 * @param input
	 * @return
	 */
	private List<Entry<String, Integer>> findHighlight(String input,
			List<Entry<String, Integer>> foundWords) {

		StringTokenizer tokens = new StringTokenizer(input);
		while (tokens.hasMoreTokens()) {
			String word = tokens.nextToken();
			// Elimina signos de puntuación
			if (word.lastIndexOf("<em>") == 0) {
				String word2 = word.replaceAll("\\p{Punct}", "");
				String word3 = word2.substring(2, word2.length() - 2);
				foundWords = updateFoundWords(foundWords, word3);
			}
		}

		return foundWords;

	}

	/**
	 * Actualiza la lista de palabras con la nueva entrada
	 * 
	 * @param list
	 * @param input
	 * @return
	 */
	private List<Entry<String, Integer>> updateFoundWords(
			List<Entry<String, Integer>> list, String input) {

		boolean found = false;
		int i = 0;
		for (Entry<String, Integer> entry : list) {
			if (entry.getKey().equalsIgnoreCase(input)) {
				// Actualiza la frecuencia de este término
				found = true;
				Entry<String, Integer> nuEntry = entry;
				nuEntry.setValue(entry.getValue() + 1);
				list.set(i, nuEntry);
			}
			i++;
		}

		if (!found) {
			// Crea una nueva entrada en la lista
			list.add(new AbstractMap.SimpleEntry<String, Integer>(input, 1));
		}
		return list;
	}

}
