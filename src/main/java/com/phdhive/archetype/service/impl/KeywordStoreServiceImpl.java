package com.phdhive.archetype.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.phdhive.archetype.dao.IKeywordStoreDAO;
import com.phdhive.archetype.dto.word.Keyword;
import com.phdhive.archetype.service.IKeywordStoreService;

/**
 * Servicio para la gestión de las palabras almacenadas.
 * 
 * @author ADORU3N
 *
 */
@Service
public class KeywordStoreServiceImpl implements IKeywordStoreService {

	@Autowired
	private IKeywordStoreDAO keywordStoreDAO;
	
	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IKeywordStoreService#saveKeyword(com.phdhive.archetype.dto.word.Keyword)
	 */
	@Override
	@Transactional
	public Keyword saveKeyword(Keyword word) {
		word.setLastUpdate(new Date());
		if (this.readByKeyword(word)!=null)
			keywordStoreDAO.deleteKeyword(word);
		keywordStoreDAO.save(word);
		return word;
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IKeywordStoreService#deleteKeyword(com.phdhive.archetype.dto.word.Keyword)
	 */
	@Override
	@Transactional
	public void deleteKeyword(Keyword word) {
		keywordStoreDAO.deleteKeyword(word);
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IKeywordStoreService#deleteAllKeywords()
	 */
	@Override
	@Transactional
	public void deleteAllKeywords() {
		keywordStoreDAO.deleteAllKeywords();

	}
	
	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IKeywordStoreService#isEmpty()
	 */
	@Override
	@Transactional(readOnly = true)
	public Boolean isEmpty() {
		return keywordStoreDAO.isEmpty();
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IKeywordStoreService#readById(com.phdhive.archetype.dto.word.Keyword)
	 */
	@Override
	@Transactional(readOnly = true)
	public Keyword readById(Keyword word) {
		return keywordStoreDAO.findById(word.getId());
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IKeywordStoreService#readAll()
	 */
	@Override
	@Transactional(readOnly = true)
	public List<Keyword> readAll() {
		return keywordStoreDAO.readAll();
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IKeywordStoreService#updateFrequency(java.lang.String, java.lang.Integer)
	 */
	@Override
	@Transactional
	public void updateFrequency(String word, Integer freq){
		Keyword currentWord = new Keyword();
		currentWord.setWord(word);
		currentWord = this.readByKeyword(currentWord);				
		Keyword updatedWord = new Keyword();
		updatedWord.setWord(word);
		boolean removed = false;
		if (currentWord!=null)
			if (freq>0){
			   updatedWord.setTotalFreq(currentWord.getTotalFreq()+freq);
			   if (updatedWord.getTotalFreq()==0){
				 //Se ha borrado un documento y además era el único que referenciaba esta palabra
				 //Por tanto hay que eliminarla
				 keywordStoreDAO.deleteKeyword(updatedWord);
				 removed = true;
			   }
			}
		else
			updatedWord.setTotalFreq(freq);
		if (!removed)
		    this.saveKeyword(updatedWord);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IKeywordStoreService#readByKeyword(com.phdhive.archetype.dto.word.Keyword)
	 */
	@Override
	@Transactional(readOnly = true)
	public Keyword readByKeyword(Keyword word) {
		return keywordStoreDAO.readByKeyword(word);
	}
}
