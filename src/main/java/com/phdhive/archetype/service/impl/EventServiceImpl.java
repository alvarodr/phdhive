package com.phdhive.archetype.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.phdhive.archetype.dao.IEventDAO;
import com.phdhive.archetype.dao.IUserDAO;
import com.phdhive.archetype.dto.community.Community;
import com.phdhive.archetype.dto.event.Event;
import com.phdhive.archetype.dto.user.User;
import com.phdhive.archetype.service.IEventService;
import com.phdhive.archetype.service.IUserService;
import com.phdhive.archetype.service.validation.ServiceValidationException;
import com.phdhive.archetype.service.validation.fails.BusinessLogicFailCode;
import com.phdhive.archetype.service.validation.fails.ServiceValidationFail;

/**
 * Servicio para la gestión de eventos.
 * 
 * @author Alvaro
 * 
 */
@Service
public class EventServiceImpl implements IEventService {

	@Autowired
	private IEventDAO eventDAO;
	
	@Autowired
	private IUserDAO userDAO;

	@Autowired
	private IUserService userService;

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IEventService#getAllEvents()
	 */
	@Override
	@Transactional(readOnly = true)
	public Collection<Event> getAllEvents() {
		return eventDAO.getAll();
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IEventService#getEventById(org.bson.types.ObjectId)
	 */
	@Override
	@Transactional(readOnly = true)
	public Event getEventById(ObjectId id) {
		return eventDAO.findById(id);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IEventService#saveEvent(com.phdhive.archetype.dto.event.Event)
	 */
	@Override
	@Transactional
	public void saveEvent(Event event) {
		//Lo activa en el momento de la creacción
		if (event.getState()==null)
			event.setState(com.phdhive.archetype.dto.event.states.EventState.ACTIVE);
		eventDAO.save(event);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IEventService#getEventPaging(java.lang.Integer, java.lang.Integer, com.phdhive.archetype.dto.community.Community)
	 */
	@Override
	@Transactional(readOnly = true)
	public Collection<Event> getEventPaging(Integer start, Integer limit,
			Community community) {
		return eventDAO.getPaging(start, limit, community);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IEventService#removeEvent(com.phdhive.archetype.dto.event.Event)
	 */
	@Transactional
	@Override
	public void removeEvent(Event event) {
		userService.deleteEventForAllUsers(event);		
		eventDAO.remove(event);
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IEventService#getEventsByCommunityId(org.bson.types.ObjectId)
	 */
	@Override
	@Transactional(readOnly = true)
	public Collection<String> getEventsByCommunityId(ObjectId id) {
		List<String> ids = new ArrayList<String>();
		Collection<Event> events = eventDAO.getEventsByCommunityId(id);
		for (Event event : events) {
			ids.add(event.getName());
		}
		return ids; 
	}

	@Override
	@Transactional(readOnly = true)
	public Event getEventByName(String evenName){
		return eventDAO.getEventByName(evenName);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IEventService#linkUserToEvent(org.bson.types.ObjectId, java.lang.String)
	 */
	@Override
	@Transactional
	public Event linkUserToEvent(ObjectId id, String email) {
		List<ServiceValidationFail> fails = new ArrayList<ServiceValidationFail>();
		
		//Recuperamos el evento por id
		Event event = eventDAO.findById(id);
		
		//Recuperamos el usuario
		final User user = userDAO.findByEmail(email);
		
		//Verificamos si el usuario ya esta vinculado al evento
		if (event.getUsers()==null) event.setUsers(new ArrayList<User>());
		
		Boolean exists = false;
		if (event.getUsers() != null) {
			exists = CollectionUtils.exists(event.getUsers(), new Predicate() {
				@Override
				public boolean evaluate(Object object) {
					User item = (User) object;
					return user.getEmail().equals(item.getEmail());
				}
			});			
		}
		
		if (exists){
			fails.add(new ServiceValidationFail(BusinessLogicFailCode.USER_ALREADY_LINKED_EVENT));
			throw new ServiceValidationException(this.getClass().getName(), fails);
		}
			
		event.getUsers().add(user);
		eventDAO.save(event);
		
		//Actualizamos el usuario
		if (user.getEventsNames()==null) user.setEventsNames(new ArrayList<String>());
		user.getEventsNames().add(event.getName());
		userDAO.save(user);
		
		return event;
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IEventService#getEventByUser(org.bson.types.ObjectId)
	 */
	@Override
	@Transactional(readOnly = true)
	public Collection<Event> getEventByUser(ObjectId id) {
		return eventDAO.getEventByUser(id);
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IEventService#getActiveEvents()
	 */
	@Override
	@Transactional(readOnly = true)
	public Collection<Event> getActiveEvents() {
		return eventDAO.getActiveEvents();
	}

}
