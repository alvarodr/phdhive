package com.phdhive.archetype.service.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.xml.transform.stream.StreamResult;

import org.bson.types.ObjectId;
import org.docx4j.convert.in.xhtml.XHTMLImporter;
import org.docx4j.convert.out.html.AbstractHtmlExporter;
import org.docx4j.convert.out.html.AbstractHtmlExporter.HtmlSettings;
import org.docx4j.convert.out.html.HtmlExporterNG2;
import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.io.SaveToZipFile;
import org.docx4j.openpackaging.packages.OpcPackage;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.openpackaging.parts.WordprocessingML.NumberingDefinitionsPart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.xml.sax.InputSource;

import com.phdhive.archetype.dao.IDocumentDAO;
import com.phdhive.archetype.dto.document.PhdDocument;
import com.phdhive.archetype.dto.master.types.TypeDocumentMetaData;
import com.phdhive.archetype.service.IDocumentService;
import com.phdhive.archetype.service.validation.ServiceValidationException;
import com.phdhive.archetype.service.validation.fails.BusinessLogicFail;
import com.phdhive.archetype.service.validation.fails.BusinessLogicFailCode;
import com.phdhive.archetype.service.validation.fails.ServiceValidationFail;

/**
 * Servicio para la gestión de los documentos.
 * 
 * @author Alvaro
 * 
 */
@Service
public class DocumentServiceImpl implements IDocumentService {

	@Autowired
	private IDocumentDAO documentDAO;

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IDocumentService#saveDocument(com.phdhive.archetype.dto.document.PhdDocument)
	 */
	@Override
	@Transactional
	public ObjectId saveDocument(PhdDocument document) {
		return documentDAO.saveDocument(document);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IDocumentService#findDocumentById(org.bson.types.ObjectId)
	 */
	@Override
	@Transactional(readOnly = true)
	public PhdDocument findDocumentById(ObjectId id) {
		return documentDAO.findById(id);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IDocumentService#findDocumentsByMetada(java.util.Map)
	 */
	@Override
	@Transactional(readOnly = true)
	public Collection<PhdDocument> findDocumentsByMetada(Map<TypeDocumentMetaData, String> metaData) {
		return documentDAO.findDocument(metaData);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IDocumentService#toHTML(com.phdhive.archetype.dto.document.PhdDocument)
	 */
	@Override
	@Transactional
	public String toHTML(PhdDocument document) {
		try{
			WordprocessingMLPackage wordMLPackage = WordprocessingMLPackage.load(document.getFile());
			HtmlSettings htmlSettings = new HtmlSettings();
			AbstractHtmlExporter exporter = new HtmlExporterNG2();
			Writer writer = new StringWriter();
			StreamResult result = new StreamResult(writer);
			exporter.html(wordMLPackage, result, htmlSettings);
			StringWriter stringWriter = (StringWriter)result.getWriter();
			return stringWriter.getBuffer().toString();
		}catch (Exception e){
			List<ServiceValidationFail> fail = new ArrayList<ServiceValidationFail>();
			fail.add(new BusinessLogicFail(BusinessLogicFailCode.DOCUMENT_FORMAT));
			throw new ServiceValidationException(this.getClass().getName(), fail);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IDocumentService#toDocument(java.lang.String, com.phdhive.archetype.dto.document.PhdDocument)
	 */
	@Override
	@Transactional
	public void toDocument(String html, PhdDocument document) {
		List<ServiceValidationFail> fail = new ArrayList<ServiceValidationFail>();
		try {
			WordprocessingMLPackage wordMLPackage = WordprocessingMLPackage.createPackage();
			NumberingDefinitionsPart ndp = new NumberingDefinitionsPart();
			wordMLPackage.getMainDocumentPart().addTargetPart(ndp);
			ndp.unmarshalDefaultNumbering();
			StringReader reader = new StringReader(html);
			InputSource inputSource = new InputSource(reader);
			wordMLPackage.getMainDocumentPart().getContent().addAll(XHTMLImporter.convert(inputSource, null, wordMLPackage));			
			document.setFile(packageToInputStream(wordMLPackage));
		} catch (InvalidFormatException e) {
			fail.add(new BusinessLogicFail(BusinessLogicFailCode.DOCUMENT_FORMAT));
			throw new ServiceValidationException(this.getClass().getName(), fail);
		} catch (Exception e) {
			fail.add(new BusinessLogicFail(BusinessLogicFailCode.DOCUMENT_FAIL_LOAD));
			throw new ServiceValidationException(this.getClass().getName(), fail);
		}
	}
	
	@Override
	@Transactional
	public void deleteDocument(ObjectId id) {
		documentDAO.delete(id);		
	}
	
	/*
	 * PRIVATE METHODS
	 */
	/**
	 * 
	 * @param p
	 * @return
	 * @throws Exception
	 */
	private static InputStream packageToInputStream(OpcPackage p) throws Exception{
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		SaveToZipFile zipFile = new SaveToZipFile(p);
		zipFile.save(os);
		return new ByteArrayInputStream(os.toByteArray());
	}
}
