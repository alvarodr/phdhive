package com.phdhive.archetype.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.phdhive.archetype.dto.master.types.TypeLanguage;
import com.phdhive.archetype.service.ISnowballService;
import com.phdhive.archetype.service.validation.ServiceValidationException;
import com.phdhive.archetype.service.validation.fails.BusinessLogicFail;
import com.phdhive.archetype.service.validation.fails.BusinessLogicFailCode;
import com.phdhive.archetype.service.validation.fails.ServiceValidationFail;
import com.phdhive.archetype.snowball.SnowballStemmer;

/**
 * Servicio que Snowball que obtiene las raices de las palabras.
 * 
 * @author ADORU3N
 *
 */
@Service
public class SnowballServiceImpl implements ISnowballService {
	
	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.ISnowballService#getListStemmer(java.lang.String[], com.phdhive.archetype.dto.master.types.TypeLanguage)
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public void getListStemmer(String[] wordList, TypeLanguage language) {
		List<ServiceValidationFail> fails = new ArrayList<ServiceValidationFail>();
		SnowballStemmer stemmer = null;
		Class stemClass = null;
		
		try{
			stemClass = Class.forName("com.phdhive.archetype.snowball.ext." + language.getLanguage() + "Stemmer");
			stemmer = (SnowballStemmer) stemClass.newInstance();
			for (int i = 0; i < wordList.length; i++){
				stemmer.setCurrent(wordList[i]);
				stemmer.stem();
				wordList[i] = stemmer.getCurrent();
			}
		} catch (ClassNotFoundException e) {
			fails.add(new BusinessLogicFail(BusinessLogicFailCode.CLASS_NOT_FOUND, 
				new Object[]{"com.phdhive.archetype.snowball.ext." + language.getLanguage() + "Stemmer"}));
			throw new ServiceValidationException(this.getClass().getName(), fails);
		} catch (IllegalAccessException e) {
			fails.add(new BusinessLogicFail(BusinessLogicFailCode.ILEGAL_CLASS_ACCESS,
				new Object[]{stemClass.getCanonicalName()}));
			throw new ServiceValidationException(this.getClass().getName(), fails);
		} catch (InstantiationException e) {
			fails.add(new BusinessLogicFail(BusinessLogicFailCode.INCONSISTENT_PARAMETERS));
			throw new ServiceValidationException(this.getClass().getName(), fails);
		}
	}
}
