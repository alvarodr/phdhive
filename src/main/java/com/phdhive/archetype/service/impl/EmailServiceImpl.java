package com.phdhive.archetype.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.mail.internet.MimeMessage;

import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;
import org.springframework.ui.velocity.VelocityEngineUtils;

import com.phdhive.archetype.dto.document.PhdDocument;
import com.phdhive.archetype.dto.event.Event;
import com.phdhive.archetype.dto.master.types.TypeDocumentMetaData;
import com.phdhive.archetype.dto.user.User;
import com.phdhive.archetype.service.IDocumentService;
import com.phdhive.archetype.service.IEmailService;

/**
 * Servicio para envio de mails.
 * 
 * @author Alvaro
 * 
 */
@Service
public class EmailServiceImpl implements IEmailService {

	@Value("#{applicationConfig['activation.email.from']}")
	private String from;

	@Autowired
	private IDocumentService documentService;
	
	@Autowired
	private MessageSource messageSource;

	@Autowired
	private JavaMailSender mailSender;

	@Autowired
	private VelocityEngine velocityEngine;

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IEmailService#sendEmailActivation(com.phdhive.archetype.dto.user.User, java.util.Locale)
	 */
	@Override
	public void sendEmailActivation(final User user, final Locale locale) {
		MimeMessagePreparator preparator = new MimeMessagePreparator() {
			@Override
			public void prepare(MimeMessage mimeMessage) throws Exception {
				MimeMessageHelper message = new MimeMessageHelper(mimeMessage);

				message.setTo(user.getEmail());
				message.setFrom(from);
				message.setSubject(messageSource.getMessage("email.subject.activation", new Object[]{}, locale));

				Map<String, User> userActive = new HashMap<String, User>();
				userActive.put("user", user);

				String text = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "email/template/activationTemplate.vm", userActive);

				mimeMessage.setText(text, "UTF-8");
				mimeMessage.setHeader("Content-Type", "text/html; charset=UTF-8");
				mimeMessage.setHeader("From", "PhD Hive activation<" + from + ">");
			}
		};

		mailSender.send(preparator);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IEmailService#sendEventInvitation(com.phdhive.archetype.dto.user.User, com.phdhive.archetype.dto.event.Event, java.util.Locale)
	 */
	@Override
	public void sendEventInvitation(final User user, final Event event,	final Locale locale) {
		MimeMessagePreparator preparator = new MimeMessagePreparator() {
			@Override
			public void prepare(MimeMessage mimeMessage) throws Exception {
				MimeMessageHelper message = new MimeMessageHelper(mimeMessage);

				message.setTo(user.getEmail());
				message.setFrom(from);
				message.setSubject(messageSource.getMessage("email.subject.event.invite", new Object[]{}, locale));

				Map<String, Object> params = new HashMap<String, Object>();
				params.put("user", user);
				params.put("event", event);

				String text = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "email/template/invitationTemplate.vm", params);

				mimeMessage.setText(text, "UTF-8");
				mimeMessage.setHeader("Content-Type", "text/html; charset=UTF-8");
				mimeMessage.setHeader("From", "[PhD Hive] " + event.getName() + " event<" + from + ">");
			}
		};
		mailSender.send(preparator);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IEmailService#sendEventEndMessage(com.phdhive.archetype.dto.user.User, com.phdhive.archetype.dto.event.Event, java.util.Locale)
	 */
	@Override
	public void sendEventEndMessage(final User user, final Event event,
			final Locale locale) {
		MimeMessagePreparator preparator = new MimeMessagePreparator() {
			@Override
			public void prepare(MimeMessage mimeMessage) throws Exception {
				MimeMessageHelper message = new MimeMessageHelper(mimeMessage);

				message.setTo(user.getEmail());
				message.setFrom(from);
				message.setSubject(messageSource.getMessage("email.subject.event.finish", new Object[]{}, locale));

				Map<String, Object> params = new HashMap<String, Object>();
				params.put("user", user);
				params.put("event", event);

				Map<TypeDocumentMetaData, String> metaData = new HashMap<TypeDocumentMetaData, String>();
				metaData.put(TypeDocumentMetaData.METADATA_OWNER, user.getEmail());
				metaData.put(TypeDocumentMetaData.METADATA_EVENT, event.getId());
				List<PhdDocument> documents = (List<PhdDocument>) documentService.findDocumentsByMetada(metaData);
				//Sólo debería haber un documento por event
				if (!documents.isEmpty())
				    params.put("document", documents.get(0));
				
				
				String text = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "email/template/eventEndTemplate.vm", params);

				mimeMessage.setText(text, "UTF-8");
				mimeMessage.setHeader("Content-Type", "text/html; charset=UTF-8");
				mimeMessage.setHeader("From", "[PhD Hive] " + event.getName() + " event results<" + from + ">");
			}
		};
		mailSender.send(preparator);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IEmailService#sendMessageContact(com.phdhive.archetype.dto.user.User, com.phdhive.archetype.dto.user.User, java.lang.String, java.util.Locale)
	 */
	@Override
	public void sendMessageContact(final User userTo, final User userFrom, final String textMessage, final Locale locale) {
		MimeMessagePreparator preparator = new MimeMessagePreparator() {
			@Override
			public void prepare(MimeMessage mimeMessage) throws Exception {
				MimeMessageHelper message = new MimeMessageHelper(mimeMessage);

				message.setTo(userTo.getEmail());
				message.setFrom(from);
				message.setSubject(messageSource.getMessage("email.subject.contact.user", new Object[]{} ,locale));
				mimeMessage.setText(textMessage, "UTF-8");
				mimeMessage.setHeader("Content-Type", "text/html; charset=UTF-8");
				mimeMessage.setHeader("From", "[PhD Hive] new contact request<" + from + ">");
			}
		};
		mailSender.send(preparator);
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IEmailService#sendNewPassword(com.phdhive.archetype.dto.user.User, java.lang.String, java.util.Locale)
	 */
	@Override
	public void sendNewPassword(final User user,final String password, final Locale locale) {
		MimeMessagePreparator preparator = new MimeMessagePreparator() {
			@Override
			public void prepare(MimeMessage mimeMessage) throws Exception {
				MimeMessageHelper message = new MimeMessageHelper(mimeMessage);

				message.setTo(user.getEmail());
				message.setFrom(from);
				message.setSubject(messageSource.getMessage("email.subject.user.newPassword", new Object[]{}, locale));

				Map<String, Object> params = new HashMap<String, Object>();
				params.put("user", user);
				params.put("password", password);

				String text = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "email/template/newPasswordTemplate.vm", params);

				mimeMessage.setText(text, "UTF-8");
				mimeMessage.setHeader("Content-Type", "text/html; charset=iso-8859-1");
				mimeMessage.setHeader("From", "[PhD Hive] recovery password<" + from + ">");
			}
		};
		mailSender.send(preparator);
	}
}
