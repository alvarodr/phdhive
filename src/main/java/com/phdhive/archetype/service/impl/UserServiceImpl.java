package com.phdhive.archetype.service.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.phdhive.archetype.dao.IUserDAO;
import com.phdhive.archetype.dto.document.PhdDocument;
import com.phdhive.archetype.dto.event.Event;
import com.phdhive.archetype.dto.filter.UserFilter;
import com.phdhive.archetype.dto.geolocation.Geolocation;
import com.phdhive.archetype.dto.master.DataMaster;
import com.phdhive.archetype.dto.master.types.TypeDataMaster;
import com.phdhive.archetype.dto.user.User;
import com.phdhive.archetype.dto.word.Keyword;
import com.phdhive.archetype.service.IBulkService;
import com.phdhive.archetype.service.IDataMasterService;
import com.phdhive.archetype.service.IDocumentService;
import com.phdhive.archetype.service.IEventService;
import com.phdhive.archetype.service.IKeyWordsService;
import com.phdhive.archetype.service.IKeywordStoreService;
import com.phdhive.archetype.service.ISolrService;
import com.phdhive.archetype.service.IUserService;
import com.phdhive.archetype.service.validation.ServiceValidationException;
import com.phdhive.archetype.service.validation.fails.BusinessLogicFail;
import com.phdhive.archetype.service.validation.fails.BusinessLogicFailCode;
import com.phdhive.archetype.service.validation.fails.ServiceValidationFail;
import com.phdhive.archetype.utils.PhdhiveUtils;

/**
 * Servicio para la gestión de usuarios.
 * 
 * @author ADORU3N
 *
 */
@Service
public class UserServiceImpl implements IUserService{		
	
	final static int OP_NEW = 1;
	final static int OP_UPDATE = 2;
	final static int OP_REMOVE = 3;	
	
	@Value("#{applicationConfig['geolocation_url']}")
	private String geolocation_url;
	
	@Value("#{applicationConfig['number_of_search_keywords']}")
	private String number_of_keywords;	
	
	@Autowired
	private IUserDAO userDao;
	
	@Autowired
	private IKeyWordsService keyWordsService;
	
	@Autowired
	private IEventService eventService;
	
	@Autowired
	private IBulkService bulkService;
	
	@Autowired
	private ISolrService solrService;
	
	@Autowired
	private IKeywordStoreService keyWordStoreService;
	
	@Autowired
	private IDataMasterService dataMasterService;
	
	@Autowired
	private IDocumentService documentService;
	
	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IUserService#createUser(com.phdhive.archetype.dto.user.User)
	 */
	@Override
	@Transactional
	public User createUser(User user) {
		if (user.getLastIP()!=null) user.setCountry(getUserCountry(user));
		userDao.createUser(user);
		solrService.updateUserIndex(user, OP_NEW);
		return user;
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IUserService#readById(com.phdhive.archetype.dto.user.User)
	 */
	@Override
	@Transactional(readOnly = true)
	public User readById(User user) {
		return userDao.findById(user.getObjectId());
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IUserService#findById(org.bson.types.ObjectId)
	 */
	@Override
	@Transactional(readOnly = true)
	public User findById(ObjectId id) {
		return userDao.findById(id);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IUserService#readAll()
	 */
	@Override
	@Transactional(readOnly = true)
	public List<User> readAll() {
		return userDao.findAll();
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IUserService#readByCriteriaName(java.lang.String)
	 */
	@Override
	@Transactional(readOnly = true)
	public List<User> readByCriteriaName(String name) {
		return userDao.findByName(name);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IUserService#updateUser(com.phdhive.archetype.dto.user.User)
	 */
	@Override
	@Transactional
	public User updateUser(User user) {
		user = userDao.updateUser(user);
		solrService.updateUserIndex(user, OP_UPDATE);
		return user;
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IUserService#deleteUser(com.phdhive.archetype.dto.user.User)
	 */
	@Override
	@Transactional
	public void deleteUser(User user) {		
		userDao.deleteUser(user);
		solrService.updateUserIndex(user, OP_REMOVE);
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IUserService#isEmpty()
	 */
	@Override
	@Transactional(readOnly = true)
	public Boolean isEmpty(){
		return userDao.findAll().isEmpty();
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IUserService#readByEmail(java.lang.String)
	 */
	@Override
	@Transactional(readOnly = true)
	public User readByEmail(String email){
		return userDao.findByEmail(email);
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IUserService#deleteAllUsers()
	 */
	@Override
	@Transactional
	public void deleteAllUsers(){
		userDao.deleteAll();
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IUserService#getQueryWords()
	 */
	@Override
	@Transactional(readOnly = true)
	public List<Entry<String, Integer>> getQueryWords() {
		return keyWordsService.getQueryWords();
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IUserService#getQueryWords(com.phdhive.archetype.dto.document.PhdDocument, java.util.Locale)
	 */
	@Override
	@Transactional(readOnly = true)
	public List<Entry<String, Integer>> getQueryWords(PhdDocument doc, Locale locale) {
		String contentDocumentText = keyWordsService.getKeyWordsAndFrequency(doc);
		List<Entry<String, Integer>> words = new ArrayList<Entry<String,Integer>>(keyWordsService.getKeyWordsAndFrecuency(contentDocumentText, locale));
		Collections.sort(words,  PhdhiveUtils.BY_VALUE);
		return words;
	}	
	
	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IUserService#getUserByToken(java.lang.String)
	 */
	@Override
	@Transactional(readOnly = true)
	public User getUserByToken(String token){
		return userDao.findByToken(token);
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IUserService#udpateKeywords(com.phdhive.archetype.dto.user.User, java.util.List, boolean)
	 */
	@Override
	@Transactional
	public void udpateKeywords(User user, List<Entry<String, Integer>> queryWords, boolean removing) {
		
		if (user.getKeywords()==null){
			user.setKeywords(new HashMap<String, Integer>());
		}
		
		HashMap<String, Integer> currentKeywordsMap = new HashMap<String, Integer>(user.getKeywords());
		
		for(Entry<String, Integer> newKeyword: queryWords ){
			if (currentKeywordsMap.containsKey(newKeyword.getKey())){
				Integer freq = currentKeywordsMap.get(newKeyword.getKey());				
				if (removing){
					currentKeywordsMap.put(newKeyword.getKey(), freq-newKeyword.getValue());	
				}else{					
					currentKeywordsMap.put(newKeyword.getKey(), newKeyword.getValue()+freq);
				}	
					
				//Si ya la contiene significa que existe en la tabla maestra de palabras
				//Por tanto, hay que actualizar la frequencia
				keyWordStoreService.updateFrequency(newKeyword.getKey(), newKeyword.getValue());
				
			}else{
				currentKeywordsMap.put(newKeyword.getKey(), newKeyword.getValue());
				Keyword currentWord = new Keyword();
				currentWord.setWord(newKeyword.getKey());
				//Comprueba si ya existe en la tabla maestra
				if (keyWordStoreService.readByKeyword(currentWord)==null){
					Keyword newWord = new Keyword();
					newWord.setWord(newKeyword.getKey());
					newWord.setTotalFreq(newKeyword.getValue());
					keyWordStoreService.saveKeyword(newWord);
				}else{
					keyWordStoreService.updateFrequency(newKeyword.getKey(), newKeyword.getValue());
				}
			}
		}
		
		//Recorre el hashmap para eliminar las keywords con 0 ocurrencias
		if (removing){			
			Iterator<Map.Entry<String, Integer>> iter = currentKeywordsMap.entrySet().iterator();
			while (iter.hasNext()) {
			    Map.Entry<String,Integer> entry = iter.next();
			    if(entry.getValue()==0){
			        iter.remove();
			    }
			}
		}
		
		user.setKeywords(currentKeywordsMap);
		this.updateUser(user);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IUserService#deleteKeyword(User user,String keyword)
	 */
	@Override
	@Transactional
	public void deleteKeyword(User user,String keyword) {		
		HashMap<String, Integer> currentKeywordsMap = new HashMap<String, Integer>(user.getKeywords());
		if (currentKeywordsMap.containsKey(keyword)){			
			Iterator<Map.Entry<String, Integer>> iter = currentKeywordsMap.entrySet().iterator();
			while (iter.hasNext()) {
			    Map.Entry<String,Integer> entry = iter.next();
			    if(entry.getKey().equals(keyword)){
			        iter.remove();
			    }
			}
		}
		user.setKeywords(currentKeywordsMap);	
		updateUser(user);
	}	
	
	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IUserService#getUsersByPaging(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	@Transactional(readOnly = true)
	public List<User> getUsersByPaging(Integer start, Integer limit) {
		return userDao.getUsersByPaging(start, limit);
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IUserService#getUserByCommunity(java.util.Collection)
	 */
	@Override
	@Transactional(readOnly = true)
	public Collection<User> getUserByCommunity(Collection<String> events) {
		return userDao.getUserByCommunity(events);
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IUserService#getUserByFilter(com.phdhive.archetype.dto.filter.UserFilter)
	 */
	@Override
	@Transactional(readOnly = true)
	public List<User> getUserByFilter(UserFilter filter) {
		return userDao.getUsersByFilter(filter);
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IUserService#getUserCountry(com.phdhive.archetype.dto.user.User)
	 */
	@Override
	@Transactional(readOnly = true)
	public DataMaster getUserCountry(User user) {
		List<ServiceValidationFail> fails = new ArrayList<ServiceValidationFail>();
		Gson gson = new Gson();
        String response = "";
        String response2 = "";
		
		try{
	        URL url = new URL(geolocation_url + user.getLastIP());
	        BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
	        while ((response = in.readLine()) != null){
	        	response2 += response;
	        }
		} catch (MalformedURLException e){
			fails.add(new BusinessLogicFail(BusinessLogicFailCode.GEOLOCATION_USER_ERROR));
			throw new ServiceValidationException(this.getClass().getName(), fails);
		} catch (IOException e){
			fails.add(new BusinessLogicFail(BusinessLogicFailCode.GEOLOCATION_USER_ERROR));
			throw new ServiceValidationException(this.getClass().getName(), fails);
		}
		
		Geolocation geoloc = gson.fromJson(response2, Geolocation.class);
		String countryStr = geoloc.getCountry_name();
		DataMaster result = dataMasterService.getDataMasterByDesc(TypeDataMaster.TYPES_COUNTRIES, countryStr.toUpperCase());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IUserService#activationUser(org.bson.types.ObjectId, java.lang.String)
	 */
	@Transactional
	@Override
	public void activationUser(ObjectId id, String token) {
		List<ServiceValidationFail> fails = new ArrayList<ServiceValidationFail>();
		User user = userDao.findUserActive(id, token);
		if (user != null) {
			user.setEnabled(Boolean.TRUE);
			userDao.updateUser(user);
		} else {
			fails.add(new ServiceValidationFail(BusinessLogicFailCode.USER_NOT_EXISTS));
			throw new ServiceValidationException(this.getClass().getName(), fails);
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IUserService#deleteDocumentFromUser(com.phdhive.archetype.dto.user.User, org.bson.types.ObjectId)
	 */
	@Transactional
	@Override
	public void deleteDocumentFromUser(User user, ObjectId docId) {
		
		List<ObjectId> docs =  user.getDocuments();
		docs.remove(docId);
		user.setDocuments(docs);
		userDao.updateUser(user);
		
		//Elimina las ocurrencias de las keyowrds del documento
		//en el usuario y en el almacén central
		PhdDocument document = documentService.findDocumentById(docId);
		String contentDocument = keyWordsService.getKeyWordsAndFrequency(document);
		//TODO: cambiar la forma de obtener el locale al meter multidioma
		keyWordsService.getKeyWordsAndFrecuency(contentDocument, Locale.ENGLISH);
		List<Entry<String, Integer>> foundWords = getQueryWords();
		udpateKeywords(user, foundWords,true);
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IUserService#deleteEventForAllUsers(com.phdhive.archetype.dto.event.Event)
	 */
	@Transactional
	@Override
	public void deleteEventForAllUsers(Event event) {
		
		UserFilter filter = new UserFilter();
		filter.setEvent(event.getName());
		List<User> users = getUserByFilter(filter);

		for(User user:users){			
			List<String> eventNames = user.getEventsNames();
			if (eventNames.remove(new String(event.getName()))){				
				user.setEventsNames(eventNames);
				updateUser(user);
			}
			
		}
	}
	
	
}
