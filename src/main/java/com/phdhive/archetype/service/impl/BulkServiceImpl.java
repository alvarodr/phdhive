package com.phdhive.archetype.service.impl;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import au.com.bytecode.opencsv.CSVReader;

import com.phdhive.archetype.dto.document.PhdDocument;
import com.phdhive.archetype.dto.master.DataMaster;
import com.phdhive.archetype.dto.user.User;
import com.phdhive.archetype.security.Role;
import com.phdhive.archetype.service.IBulkService;
import com.phdhive.archetype.service.IDataMasterService;
import com.phdhive.archetype.service.IDocumentService;
import com.phdhive.archetype.service.IKeyWordsService;
import com.phdhive.archetype.service.IKeywordStoreService;
import com.phdhive.archetype.service.IUserService;
import com.phdhive.archetype.service.exception.ServiceException;

@Service
public class BulkServiceImpl implements IBulkService {

	
	@Autowired
	private IUserService userService;
	
	@Autowired
	private IDataMasterService dataMasterService;
	
	@Autowired
	private IKeyWordsService keyWordsService;

	@Autowired
	private IKeywordStoreService keyWordStoreService;
	
	@Autowired
	private IDocumentService documentService;
	
	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IBulkService#loadUsers(org.springframework.core.io.Resource)
	 */
	@Override
	public void loadUsers(Resource bulkUser) throws Exception {
        
		reloadIndex();		
		if (!userService.isEmpty()) {
		} else {
			try {
				InputStreamReader ir = new InputStreamReader(bulkUser.getInputStream(), "UTF-8");
				CSVReader reader = new CSVReader(ir, ';');
				String[] aux;
				User user = null;
				while ((aux = reader.readNext()) != null) {
					try {
						user = new User(aux[0] + "@gmail.com", // email
								aux[0], // name
								aux[1], // surname
								aux[5], // latest_article
								aux[2], // affiliation
								aux[4], // research
								null, // studes_level
								null // bio
						);

						Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
						authorities.add((GrantedAuthority) Role.ROLE_USER);
						user.setAuthorities(authorities);
						user.setEnabled(Boolean.FALSE);
						user.setLoginCount(0);
						userService.createUser(user);
					} catch (Exception e) {
						reader.close();
						throw new ServiceException(e.getMessage());
					}
				}
				reader.close();
			} catch (IOException e) {
				throw new ServiceException(e.getMessage());
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.phdhive.archetype.service.IBulkService#loadDataMaster(org.springframework.core.io.Resource)
	 */
	@Override
	public void loadDataMaster(Resource resource) throws Exception {
		reloadIndex();
		try {
			InputStreamReader ir = new InputStreamReader(resource.getInputStream(), "UTF-8");
			CSVReader reader = new CSVReader(ir, ';');
			String[] aux;
			while ((aux = reader.readNext()) != null) {
				try {
					Integer type = Integer.parseInt(aux[0]);
					DataMaster dataMaster = null;
					if ((dataMaster = dataMasterService.existsDataMaster(type, aux[1])) != null) {
						dataMaster.setDescription(aux[2]);
					} else {
						dataMaster = DataMaster.createDatoMaestro(type, aux[1], aux[2]);
					}
					dataMasterService.createDataMaster(dataMaster);
				} catch (Exception e) {
					reader.close();
					throw new ServiceException(e.getMessage());
				}
			}
			reader.close();
		} catch (IOException e) {
			throw new ServiceException(e.getMessage());
		}
	}
	
	/*
	 * PRIVATE METHODS
	 */
	private void reloadIndex() throws Exception {
		List<User> users = userService.readAll();
		for (User user : users) {
			userService.updateUser(user);
		}
	}
	
	public void resetKeyWords() throws Exception{
		
		keyWordStoreService.deleteAllKeywords();
		
		List<User> users = userService.readAll();
		for (User user : users) {	
			if (user.getKeywords()!=null && user.getKeywords().size()>0){
				HashMap<String, Integer> currentKeywordsMap = new HashMap<String, Integer>(user.getKeywords());			
				Iterator<Map.Entry<String, Integer>> iter = currentKeywordsMap.entrySet().iterator();
				while (iter.hasNext()) {
				    Map.Entry<String,Integer> entry = iter.next();
				    userService.deleteKeyword(user, entry.getKey());
				}		
			}

			List<ObjectId> docs =  user.getDocuments();
			if (docs!=null && docs.size()>0){
				for (ObjectId docId : docs) {
					PhdDocument document = documentService.findDocumentById(docId);
					String contentDocument = keyWordsService.getKeyWordsAndFrequency(document);
					//TODO: cambiar la forma de obtener el locale al meter multidioma
					keyWordsService.getKeyWordsAndFrecuency(contentDocument, Locale.ENGLISH);
					List<Entry<String, Integer>> foundWords = userService.getQueryWords();
					userService.udpateKeywords(user, foundWords,false);
				}
			}
		}
		
	}
}
