package com.phdhive.archetype.service.validation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;
import org.springframework.security.core.AuthenticationException;

import com.phdhive.archetype.service.validation.fails.ServiceValidationFail;

/**
 * 
 * @author Alvaro
 *
 */
public class ServiceValidationException extends AuthenticationException {
	public static final String ALL_CODES = "*";

	private static final long serialVersionUID = 8545097834898968761L;

	private List<ServiceValidationFail> fails;

	/**
	 * Nombre de servicio (Nombre y metodo de servicio).
	 */
	private String serviceName;

	/**
	 * Crea una excepcion de validacion de servicio.<BR>
	 * 
	 * @param message
	 *            Mensaje generico para la validacion.
	 * 
	 * @param serviceName
	 *            Nombre de servicio que realiza la validacion (Nombre y metodo
	 *            de servicio).
	 * @param code
	 *            Codigo de la validacion fallida
	 * 
	 */
	public ServiceValidationException(String serviceName,
			List<ServiceValidationFail> fails) {
		super("Error de validacion del servicio");

		Validate.notNull(fails);
		Validate.noNullElements(fails);
		Validate.notEmpty(fails);
		this.serviceName = serviceName;
		this.fails = Collections.unmodifiableList(fails);
	}

	/**
	 * Obtiene los errores de validacion.
	 * 
	 * @return
	 */
	public List<ServiceValidationFail> getFails() {
		return fails;
	}

	/**
	 * Obtiene los errores de validacion de un determinado tipo.
	 * 
	 * @return lista con los fallos
	 */
	public List<ServiceValidationFail> getFails(String code) {
		if (code == null) {
			return Collections.emptyList();
		}
		List<ServiceValidationFail> returned;
		if (StringUtils.isBlank(code) || ALL_CODES.equals(code)) {
			returned = fails;
		} else {
			List<ServiceValidationFail> l = new ArrayList<ServiceValidationFail>();

			for (ServiceValidationFail fail : fails) {
				if (code.equals(fail.getCode())) {
					l.add(fail);
				}
			}
			returned = Collections.unmodifiableList(l);
		}
		return returned;
	}

	/**
	 * Obtiene los errores de validacion de un determinado tipo.
	 * 
	 * @return lista con los fallos
	 */
	@SuppressWarnings("unchecked")
	public <T extends ServiceValidationFail> List<T> getFails(String code,
			Class<T> clazz) {

		if (clazz == null) {
			return Collections.emptyList();
		}
		ArrayList<T> l = new ArrayList<T>();

		if (StringUtils.isBlank(code) || ALL_CODES.equals(code)) {
			for (ServiceValidationFail fail : fails) {
				if (clazz.isAssignableFrom(fail.getClass())) {
					l.add((T) fail);
				}
			}
		} else {
			for (ServiceValidationFail fail : fails) {
				if (code.equals(fail.getCode())) {
					if (clazz.isAssignableFrom(fail.getClass())) {
						l.add((T) fail);
					}
				}
			}
		}
		return Collections.unmodifiableList(l);
	}

	/**
	 * Obtiene el nombre del servicio que realiza la validacion (Nombre y metodo
	 * de servicio).
	 * 
	 * @return
	 */
	public String getServiceName() {
		return serviceName;
	}

	/**
	 * Obtiene el fallo de validacion cuando hay un solo error.
	 * 
	 * @return el fallo de validacion
	 */
	public ServiceValidationFail getSingleFail() {
		if (!isSingleFail()) {
			throw new IllegalStateException("Tiene mas de un error");
		}
		return fails.get(0);
	}

	/**
	 * Obtiene el codigo del fallo de validacion cuando hay un solo error.
	 * 
	 * @return el codigo del fallo de validacion
	 */
	public String getSingleFailCode() {
		if (!isSingleFail()) {
			throw new IllegalStateException("Tiene mas de un error");
		}
		return fails.get(0).getCode();
	}

	/**
	 * Devuelve si la excepcion solo tiene un error.
	 * 
	 * @return true si contiene un unico error.
	 */
	public boolean isSingleFail() {
		return fails.size() == 1;
	}

	@Override
	public String toString() {
		return "ServiceValidationException [serviceName = " + serviceName
				+ ", fails = " + fails + "]";
	}
}
