package com.phdhive.archetype.service.validation.fails;

import com.phdhive.archetype.service.validation.ServiceValidationContext;

/**
 * Representa un error de parametro fuera de un rango numerico.
 * 
 * @author emilio
 * 
 */
public class LimitFail extends InvalidParameterFail {

	private static final long serialVersionUID = -6576551200638204854L;

	/**
	 * Limite bajo.
	 */
	private Number min;

	/**
	 * Limite alto.
	 */
	private Number max;

	/**
	 * Crea un error para indicar que un parametro (especificado de forma
	 * relativa al contexto) esta fuera de rango.<BR>
	 * 
	 * La ruta del parametro es relativa a los nombres de los parametros de los
	 * metodos del servicio.<BR>
	 * 
	 * @param code
	 *            Codigo de la validacion fallida
	 * @param ctx
	 *            Contexto de validacion
	 * @param relativePath
	 *            Ruta del parametro relativa al contexto.
	 * @param loLimit
	 *            Limite bajo
	 * @param hiLimit
	 *            Limite alto
	 */
	public LimitFail(String code, ServiceValidationContext ctx, String relativePath, Number lowLimit, Number highLimit) {
		super(code, ctx, relativePath);
		this.min = lowLimit;
		this.max = highLimit;
	}

	/**
	 * Crea un error para indicar que un parametro esta fuera de rango.<BR>
	 * 
	 * La ruta del parametro es relativa a los nombres de los parametros de los
	 * metodos del servicio.<BR>
	 * 
	 * @param code
	 *            Codigo de la validacion fallida
	 * @param parameterPath
	 *            Ruta del parametro.
	 * @param loLimit
	 *            Limite bajo
	 * @param hiLimit
	 *            Limite alto
	 */
	public LimitFail(String code, String parameterPath, Number min, Number max) {
		super(code, parameterPath);
		this.min = min;
		this.max = max;
	}

	/**
	 * Obtiene el limite alto.
	 * 
	 * @return el limite
	 */
	public Number getMax() {
		return max;
	}

	/**
	 * Obtiene el limite bajo.
	 * 
	 * @return el limite
	 */
	public Number getMin() {
		return min;
	}

	/**
	 * Pinta el error.
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("{ ");
		sb.append("code='");
		sb.append(getCode());
		sb.append("' parameterPath='");
		sb.append(getParameterPath());
		sb.append("' min='");
		sb.append(getMin());
		sb.append("' max='");
		sb.append(getMax());
		sb.append("' } ");
		return sb.toString();
	}

}
