package com.phdhive.archetype.service.validation;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Validations {
	
	private static String expr = "^[a-zA-Z0-9]+$";

	private static String exprMail = "^\\w+([.]?[\\w-])*@\\w+([.]?[\\w-])*(\\.\\w{2,4})$";
	
	private static String expr1= "^[a-zA-Z��������������������������������]+(([- ])*[a-zA-Z��������������������������������]+)*$";
	
	private static String exprDescrip= "^[a-zA-Z0-9��������������������������������\\s,;.:_��{��}`^+*!�$%&/()=?�'�|@#~��]+(([- ])*[a-zA-Z0-9��������������������������������\\s,;.:_��{��}`^+*!�$%&/()=?�'�|@#~��]+)*$";
	
	private static Pattern descripcion;
	
	private static Pattern nombre;
	
	private static Pattern ptrAlfanumerico;
	
	private static Pattern mail;
	
	// Expresiones regulares
	static String exprDocument = "^[KLM0-9]{1}[0-9]{7}[A-Z]{1}$";

	static String expr2 = "^[ABCDEFGHJPQRSUVNW]{1}[0-9]{7}[A-Z0-9]{1}$";

	static String expr3 = "^[XYZ]{1}[0-9]{7}[A-Z]{1}$";

	static String expr4 = "^[KLM]{1}";

	static String expr5 = "^[ABCDEFGHJUV]{1}[0-9]{7}[0-9]{1}$";

	static String expr6 = "^[PQRSNW]{1}[0-9]{7}[A-Z]{1}$";

	// Patrones
	static Pattern patron1 = Pattern.compile(expr1, Pattern.CASE_INSENSITIVE);

	static Pattern patron2 = Pattern.compile(expr2, Pattern.CASE_INSENSITIVE);

	static Pattern patron3 = Pattern.compile(expr3, Pattern.CASE_INSENSITIVE);

	static Pattern nif = Pattern.compile(expr1);

	static Pattern cif = Pattern.compile(expr2);

	static Pattern nie = Pattern.compile(expr3);

	// patrones especiales
	static Pattern cifCodControlNumerico = Pattern.compile(expr5);

	static Pattern cifCodControlLetra = Pattern.compile(expr6);
	
	/**
	 * Valida si la cadena introducida es un alfanumerico, con posibles valores
	 * de la a/A->z/Z y de 0->9
	 * 
	 * @param valor
	 * @return
	 */
	public static boolean isAlfanumerico(String valor) {
		if (valor != null) {
			if (ptrAlfanumerico == null)
				ptrAlfanumerico = Pattern.compile(expr);
			Matcher matchAlfaNumerico = ptrAlfanumerico.matcher(valor.trim()); //quitamos whitespaces 
			return (matchAlfaNumerico.matches());
		}
		return false;
	}

	/**
	 * Valida si la cadena introducida es un Codigo Postal El Codigo Postal
	 * tiene una longitud de digitos Los dos primeros digitos coinciden con el
	 * codigo de provincia y van desde el 1-> 52
	 * 
	 * @param valor
	 * @return
	 */
	public static boolean isCodigoPostal(String valor) {
		if (isNumeroPositivoEntero(valor) && valor.trim().length() == 5) {	//quitamos whitespaces 
			String dosPrimerosDigitos = valor.trim().substring(0, 2);
			if (Integer.parseInt(dosPrimerosDigitos) <= 52) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Metodo que recibe los cuatro parametros que componen una Cuenta Corriente
	 * Cogemos los 2 primeros parametros (entidad+sucursal) los tratamos y
	 * obtenemos el 1�digito de control Cogemos el ultimo parametro (cuenta) y
	 * obtenemos el 2�digito de control Una vez obtenido dicho digito de
	 * control, lo comparamos con el digito de control pasado como parametro al
	 * metodo Si son iguales la CC es correcta
	 * 
	 * @param entidad
	 * @param sucursal
	 * @param dc
	 * @param cuenta
	 * @return
	 */
	public static boolean isCuentaCorriente(String entidad, String sucursal,
			String dc, String cuenta) {
		if (entidad != null && sucursal != null && dc != null && cuenta != null) {

			//Quitamos espacios en blanco
			entidad= entidad.trim();
			sucursal= sucursal.trim();
			dc= dc.trim();
			cuenta= cuenta.trim();
			
			// Validamos que todos los campos sean enteros positivos y tengan la
			// longitud adecuada
			
			if (entidad.length() == 4 && isNumeroPositivoEntero(entidad)
					&& sucursal.length() == 4
					&& isNumeroPositivoEntero(sucursal)
					&& dc.length() == 2 && isNumeroPositivoEntero(dc)
					&& cuenta.length() == 10
					&& isNumeroPositivoEntero(cuenta)) {

				String entidadSucursal = "00" + entidad + sucursal;

				// Comparamos si el 1�digito de control de la cc es igual al
				// digito obtenido de pasar al algoritmo la entidadSucursal
				boolean primerDC = obtenerDigito(entidadSucursal) == Integer.parseInt( //cambio de new Integer a Integer.parseInt, es mas optimo
						String.valueOf(dc.charAt(0)));
				
				if (!primerDC) {
					return false;
				}
				
				// Validamos si el 2�digito de control de la cc es igual al
				// digito obtenido de pasar al algoritmo la cuenta
				boolean segundoDC = obtenerDigito(cuenta) == Integer.parseInt(String
						.valueOf(dc.charAt(1)));

				// Devolvemos true o false en funcion de si ambos son o no
				// iguales
				return (primerDC && segundoDC);
			}
		}
		return false;
	}

	/**
	 * Algoritmo para la obtencion de los 2 digitos de control Lo ejecutamos 2
	 * veces: La primera, obtenemos el primer digito-> pasamos como parametro la
	 * entidadSucursal La segunda, obtenemos el segundo digito-> pasamos como
	 * parametro la cuenta
	 * 
	 * @param valor
	 * @return
	 */
	private static int obtenerDigito(String valor) {
		String valores = "1,2,4,8,5,10,9,7,3,6";
		String[] valoresEnteros = valores.split(",");
		int control = 0;
		for (int i = 0; i <= 9; i++)
			control += (valor.charAt(i) * Integer.parseInt(valoresEnteros[i]));
		control = 11 - (control % 11);
		if (control == 11) {
			control = 0;
		} else if (control == 10) {
			control = 1;
		}
		return control;
	}
	
	/**
	 * Valida si la cadena introducida es una fecha valida, acepta fechas tipo
	 * dd/MM/yyyy como fechas tipo dd-MM-yyyy
	 * 
	 * @param valor
	 * @return
	 */
	public static boolean isFecha(String valor) {
		if (valor != null && !"".equals(valor)) {
			valor= valor.trim(); //Quitamos espacios en blanco
			if (valor.contains("-"))
				valor = valor.replace("-", "/");
			try {
				SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
				formatoFecha.setLenient(false);
				formatoFecha.parse(valor);
			} catch (ParseException e) {
				return false;
			}
			return true;
		}
		return false;
	}

	/**
	 * Se considera anio validos aquellos superiores al 1500, principio del
	 * calendario gregoriano.
	 * 
	 * @param valor
	 * @return
	 */
	public static boolean isAnio(String valor) {
		if(valor == null) 
			return false; 
		try{
			int anio = Integer.parseInt(valor.trim()); //Quitamos espacios en blanco
			if(anio < 1500) return false;
			return true;
		}catch(NumberFormatException e){
			return false;			
		}
	}

	/**
	 * Valida si la cadena introducida es un email
	 * 
	 * @param valor
	 * @return
	 */
	public static boolean isMail(String valor) {
		if (valor != null) {
			if (mail==null){
				mail=Pattern.compile(exprMail);
			}

			Matcher matchMail = mail.matcher(valor.trim()); //Quitamos espacios en blanco
			return (matchMail.matches());
		}
		return false;
	}

	/**
	 * Valida si la cadena introducida es un nombre, no acepta valores numericos
	 * ni caracteres extra�os
	 * 
	 * @param valor
	 * @return
	 */
	public static boolean isNombre(String valor) {
		if (valor != null) {
			if (nombre== null)
				nombre= Pattern.compile(expr1, Pattern.CASE_INSENSITIVE);			
				Matcher matchNombre = nombre.matcher(valor.trim()); //Quitamos espacios en blanco
				return (matchNombre.matches());
		}
		return false;
	}

	/**
	 * Valida si la cadena introducida es un nombre, no acepta valores numericos
	 * ni caracteres extranios Permite aceptar, ademas de los caracteres validos
	 * para un nombre, un caracter de inicio m�s, como pueden ser dos puntos,
	 * guion,etc.. pasado como segundo parametro
	 * 
	 * @param valor
	 *            , opcional
	 * @return
	 */
	public static boolean isNombreExtendido(String valor, String opcional) {
		if (valor != null && opcional!=null) {
			String valor2="";
			
			for (int i=0; i<valor.length();i++){
				if (!String.valueOf(valor.charAt(i)).equals("\u0009") && !String.valueOf(valor.charAt(i)).equals(" ")){ //u0009 por si existirera una tabulacion
					valor2+=String.valueOf(valor.charAt(i));						
				}
			}
			String exprExtendida="^[a-zA-Z��������������������������������]+(([- ])*[(["+opcional+"])a-zA-Z��������������������������������]+)*$";				
			Pattern nombreExtendido = Pattern.compile(exprExtendida,Pattern.CASE_INSENSITIVE);
			Matcher matchNombreExtendido = nombreExtendido.matcher(valor2);
			return (matchNombreExtendido.matches());
	}
	return false;
	}

	/**
	 * Valida si la cadena introducida es una descripci�n, entendi�ndose como
	 * tal una cadena que admite caracteres alfanum�ricos y caracteres extra�os.
	 * 
	 * @param valor
	 * @return true si la cadena es v�lida, false en caso contrario.
	 * @author Juan Garc�a Heredero - JGAHE9L
	 */
	public static boolean isDescripcion(String valor) {
		if (descripcion == null) 
			descripcion = Pattern.compile(exprDescrip, Pattern.CASE_INSENSITIVE);
		Matcher matchNombre = descripcion.matcher(valor.trim());
		return (matchNombre.matches());
	}

	/**
	 * Valida si la cadena introducida es una descripci�n, entendi�ndose como
	 * tal una cadena que admite caracteres alfanum�ricos y caracteres extra�os,
	 * y con una longitud m�xima.
	 * 
	 * @param valor
	 *            - La cadena a validar.
	 * @param longitud
	 *            - El n�mero m�ximo de caracteres que puede tener la cadena.
	 * @return true si la cadena es v�lida, false en caso contrario.
	 * @author Juan Garc�a Heredero - JGAHE9L
	 */
	public static boolean isDescripcion(String valor, int longitud) {
		if (valor.length() > longitud) 
			return false;		
		return isDescripcion(valor);
	}

	/**
	 * Valida si la cadena introducida es un numero entero positivo (incluido el
	 * 0)
	 * 
	 * @param valor
	 * @return
	 */
	public static boolean isNumeroPositivoEntero(String valor) {
		if (valor != null && valor != "") {
			try {
				long entero = Long.parseLong(valor.trim());
				if (entero >= 0)
					return true;
			} catch (NumberFormatException e) {
				return false;
			}
		}
		return false;
	}

	/**
	 * Valida si la cadena introducida es un numero entero positivo
	 * 
	 * @param valor
	 * @return
	 */
	public static boolean isNumeroEntero(String valor) {
		if (valor != null && valor != "") {
			try {
				Integer.parseInt(valor.trim());
				return true;
			} catch (NumberFormatException e) {
				return false;
			}
		}
		return false;
	}

	/**
	 * Valida si el numero introducido es un numero decimal Sin el numero
	 * introducido llevaba como separador de decimales una coma lo sustituimos
	 * por punto
	 * 
	 * @param valor
	 * @return
	 */
	public static boolean isNumeroDecimal(String valor) {
		if (valor != null && valor != ""
				&& (valor.contains(",") || valor.contains("."))) {
			String valor2 = valor.replace(",", ".");
			try {
				Float.parseFloat(valor2.trim());
				return true;
			} catch (NumberFormatException e) {
				return false;
			}
		}
		return false;
	}

	/**
	 * Valida si el documento introducido (dni, nif o nie) es un documento
	 * valido
	 * 
	 * @param documento
	 * @return
	 */
	public static boolean isDocumento(String documento) {
		if (documento != null) {
			documento= documento.trim(); //Quitamos espacios en blanco
			
			Matcher matcher1 = patron1.matcher(documento);
			Matcher matcher2 = patron2.matcher(documento);
			Matcher matcher3 = patron3.matcher(documento);

			if (matcher1.matches()) {
				return validaNIF(documento);
			} else if (matcher2.matches()) {
				return validaCIF(documento);
			} else if (matcher3.matches()) {
				return validaNIE(documento);
			}
			return false;

		}
		return false;
	}

	/**
	 * Metodo que valida si el NIF pasado como parametro es un NIF valido
	 * 
	 * @param documento
	 * @return
	 */
	public static boolean validaNIF(String documento) {

		if (documento != null && !"".equals(documento)){	
			documento= documento.trim(); //Quitamos espacios en blanco
			String temp = documento.toUpperCase();

			// si no tiene un formato valido devuelve error
			Matcher matcherNif = nif.matcher(temp);
			if (!matcherNif.matches()) {
				return false;
			}

			String cadenadni = "TRWAGMYFPDXBNJZSQVHLCKE";

			// NIF especiales
			temp = temp.replaceFirst(expr4, "0"); // para K,L,M

			int posicion = Integer.parseInt(temp.substring(0, 8)) % 23; // % resto de division, el dni se divide entre 23
			return ((String.valueOf(temp.charAt(8)) //Conversion de Char a String
					.equals(String.valueOf(cadenadni.charAt(posicion))))); // devuelve true o false si la letra es la misma
		}
		return false;
	}
	
	/**
	 * Valida si el nif introducido es un nif valido
	 * 
	 * @param documento
	 * @return
	 */
	public static boolean isNIF(String documento) {
		return validaNIF(documento);
	}

	/**
	 * Valida si el cif introducido es un cif valido
	 * 
	 * @param documento
	 * @return
	 */
	public static boolean isCIF(String documento) {
		return validaCIF(documento);
	}

	/**
	 * Metodo que valida si el CIF pasado como parametro es un CIF valido
	 * 
	 * @param documento
	 * @return
	 */
	public static boolean validaCIF(String documento) {

		if (documento != null && !"".equals(documento)){
			documento= documento.trim(); //Quitamos espacios en blanco
			String temp = documento.toUpperCase();

			// si no tiene un formato valido devuelve error
			Matcher matcherCif = cif.matcher(temp);
			if (!matcherCif.matches()) {
				return false;
			}

			// algoritmo para comprobacion de codigos tipo CIF
			int suma = Integer.parseInt(String.valueOf(documento.charAt(2))) //Conversion de Char a String para convertir a entero,ya que si convirtieramos de char directam obtendriamos el valor en Ascii
					+ Integer.parseInt(String.valueOf(documento.charAt(4)))	
					+ Integer.parseInt(String.valueOf(documento.charAt(6))); 
			for (int i = 1; i < 8; i += 2) {
				int n = 2 * (Integer.parseInt(String.valueOf(documento.charAt(i))));
				if (n < 10)
					suma += n;
				else
					suma += n / 10 + n % 10;
			}

			int n = 10 - suma % 10;

			// comprobacion de CIFs con codigo de control de tipo numerico
			Matcher matcherCifCodControlNumerico = cifCodControlNumerico
					.matcher(temp);

			if (matcherCifCodControlNumerico.matches()) {
				char caracter = documento.charAt(8);
				return (Integer.parseInt(String.valueOf(caracter)) == (n % 10));	//cambiado new Integer por parseInt, es mas optimo 
			}

			// comprobacion de CIFs con codigo de control de tipo letra
			Matcher matcherCifCodControlLetra = cifCodControlLetra
					.matcher(temp);

			if (matcherCifCodControlLetra.matches()) {
				return (documento.charAt(8) == (char) (64 + n));
			}

		}
		return false;
	}
	
	/**
	 * Metodo que valida si el NIE pasado como parametro es un NIE valido
	 * 
	 * @param documento
	 * @return
	 */
	public static boolean validaNIE(String documento) {

		if (documento != null && !"".equals(documento)){
			documento= documento.trim(); //Quitamos espacios en blanco
			String temp = documento.toUpperCase();

			// si no tiene un formato valido devuelve error
			Matcher matcherNie = nie.matcher(temp);
			if (!matcherNie.matches()) {
				return false;
			}
			String cadenadni = "TRWAGMYFPDXBNJZSQVHLCKE";
			String ch = "0";

			String letra = String.valueOf(temp.charAt(0)); 	//Conversion de Char a String
															
			if (letra.equals("Y")) {
				ch = "1";
			} else if (letra.equals("Z")) {
				ch = "2";
			}

			temp = temp.replaceFirst(String.valueOf(temp.charAt(0)), ch);

			int posicion = Integer.parseInt(temp.substring(0, 8)) % 23;

			return ((String.valueOf(temp.charAt(8)))
					.equals(String.valueOf(cadenadni.charAt(posicion))));

		}
		return false;
	}
	
	/**
	 * Valida si el nie introducido es un nie valido
	 * 
	 * @param documento
	 * @return
	 */
	public static boolean isNIE(String documento) {
		return validaNIE(documento);
	}

	/**
	 * Valida si el telefono introducido es un telefono fijo valido. Se
	 * consideran telefonos fijos a todos aquellos telefonos cuya longitud sea
	 * de 9 digitos y comiencen por 8 o 9 Tiene en cuenta aquellos fijos
	 * pertenecientes a la empresa ONO cuyo numero inicial es un 8
	 * 
	 * @param valor
	 * @return
	 */
	public static boolean isTelefonoFijo(String valor) {
		if (valor != null) {
			String fijo = validacionTelefono(valor);
			if (fijo.length() != 9 || (!isNumeroPositivoEntero(fijo))) {
				return false;
			}
			int primerDigito = Integer.parseInt(fijo.substring(0, 1));
			if (primerDigito == 8 || primerDigito == 9) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Metodo que recibe como parametro un telefono y quita los posibles
	 * espacios en blanco que tenga
	 * 
	 * @param valor
	 * @return
	 */
	private static String validacionTelefono(String valor) {
		if (valor.length() > 9) {
//			if (valor.contains(" ")) { // eliminamos espacios en blanco
				String telef = "";
				String[] valor2 = valor.split("\\s+");	//eliminamos espacios en blanco y tabulaciones.
				for (int i = 0; i < valor2.length; i++) {
					telef += valor2[i];
				}
				if (telef.length() == 9) {
					return telef;
				}
//			}
		}
		return valor;
	}
	
	/**
	 * Valida si el telefono introducido es un telefono movil valido Se
	 * consideran telefonos moviles a todos aquellos telefonos cuya longitud sea
	 * de 9 digitos y comiencen por 6 o 7
	 * 
	 * @param valor
	 * @return
	 */
	public static boolean isTelefonoMovil(String valor) {
		if (valor != null) {
			String movil = validacionTelefono(valor);
			if (movil.length() != 9 || (!isNumeroPositivoEntero(movil))) {
				return false;
			}
			int primerDigito = Integer.parseInt(movil.substring(0, 1));
			if (primerDigito == 6 || primerDigito == 7) { // validamos si
															// empieza por 6 y
															// tambien por 7
															// (para los nuevos
															// numeros de movil,
															// a partir de 2009)
				return true;
			}
		}
		return false;
	}

	/**
	 * Validador gen�rico seg�n el algoritmo de Luh. Sirve para cualquier
	 * longitud de tarjeta
	 * 
	 * @param codigo
	 * @return
	 */
	public static boolean validarLuhn(String codigo) {
		int suma = 0;

		boolean  alternancia = false;
		for (int i = codigo.length() - 1; i >= 0; i--) {
			int n = Integer.parseInt(codigo.substring(i, i + 1));
			if (alternancia) {
				n *= 2;
				if (n > 9) {
					n = (n % 10) + 1;
				}
			}
			suma += n;
			alternancia = !alternancia;
		}

		return (suma % 10 == 0);
	}

	/**
	 * Valida seg�n el algoritmo de Luhn una tarjeta Iberia Plus de 8 d�gitos
	 * 
	 * @param codigo
	 * @return
	 */
	public static boolean validarTarjetaIP(String codigo) {
		boolean resultado = false;
		
		if (codigo.length() == 8 && validarLuhn(codigo)){
			resultado = true;
		}
		return resultado;
	}

	/**
	 * Valida si el telefono introducido es un telefono movil o un telefono fijo
	 * 
	 * @param valor
	 * @return
	 */
	public static boolean isTelefono(String valor) {
		return isTelefonoMovil(valor) || isTelefonoFijo(valor);
	}
}
