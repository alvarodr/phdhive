package com.phdhive.archetype.service.validation.fails;

/**
 * Codigos de fallo de validacion de servicio.
 * 
 * @author Alvaro
 * 
 */
public final class FailCode {

	/**
	 * Codigo para error de logica de negocio.
	 */
	public static final String BUSINESS = "business";

	/**
	 * Codigo para parametro con formato incorrecto.
	 */
	public static final String FORMAT = "format";

	/**
	 * Codigo para parametro tiene un tamaño incorrecto.
	 */
	public static final String INVALID_BANK_ACCOUNT = "invalid_bank_account";

	/**
	 * Codigo para parametro no es una opcion valida.
	 */
	public static final String INVALID_OPTION = "not_option";

	/**
	 * Codigo para parametro fuera de limites.
	 */
	public static final String LIMITS = "limits";

	/**
	 * Codigo para parametro fuera de limites.
	 */
	public static final String NOT_AUTHORIZED = "not_authorized";

	/**
	 * Codigo para ausencia de parametro requerido.
	 */
	public static final String REQUIRED = "required";

	/**
	 * Codigo para parametro tiene un tamaño incorrecto (por ejemplo para
	 * colecciones).
	 */
	public static final String WRONG_SIZE = "wrong_size";

	/**
	 * Codigo para parametro tiene una longitud.
	 */
	public static final String WRONG_LENGHT = "wrong_lenght";

	/**
	 * Codigo de error para un valor que no es valido.
	 */
	public static final String WRONG_VALUE = "wrong_value";

	/**
	 * Constructor imposible.
	 */
	private FailCode() {

	}

}
