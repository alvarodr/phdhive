package com.phdhive.archetype.service.validation;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.context.MessageSource;
import org.springframework.validation.Errors;

import com.phdhive.archetype.service.validation.fails.BusinessLogicFail;
import com.phdhive.archetype.service.validation.fails.InvalidBusinessObjectFail;
import com.phdhive.archetype.service.validation.fails.InvalidParameterFail;
import com.phdhive.archetype.service.validation.fails.MultipleInvalidBusinessObjectFail;
import com.phdhive.archetype.service.validation.fails.ServiceValidationFail;

/**
 * Traductor entre fallos de servicio y errores en controlador.
 * 
 * 
 * @author Alvaro
 * 
 */
public class ServiceFailTranslator {

	private static final String MSG_ERROR_INESPERADO = "error.inesperado";

	private static final String MSG_ERROR_VALIDACION_SERVICIO = "error.validacionServicio";

	private static final String MSG_ERROR_PARAMETROS_SERVICIO = "error.parametrosServicio";

	private List<PathGrouper> groupers;

	private MessageSource messageSource;
	private Locale locale;
	
	public ServiceFailTranslator(MessageSource mMessageSource, Locale lLocale) {
		this.messageSource = mMessageSource;
		this.locale = lLocale;
	}
	
	public ServiceFailTranslator addSummarize(String pattern, String message) {
		addSummary(new PathGrouper(null, pattern, message, null));
		return this;
	}

	public ServiceFailTranslator addSummarize(String pattern, String message, int... groupBy) {
		addSummary(new PathGrouper(null, pattern, message, groupBy));
		return this;
	}

	public ServiceFailTranslator addSummarizeField(String field, String pattern, String message) {
		addSummary(new PathGrouper(field, pattern, message, null));
		return this;
	}

	public ServiceFailTranslator addSummarizeField(String field, String pattern, String message, int... groupBy) {
		addSummary(new PathGrouper(field, pattern, message, groupBy));
		return this;
	}

	/**
	 * Agrega un agrupador para construir errores que sumarizan.
	 * 
	 * @param translator
	 */
	private void addSummary(PathGrouper translator) {
		if (groupers == null) {
			groupers = new ArrayList<PathGrouper>();
		}
		groupers.add(translator);
	}

	public String getMessage(ServiceValidationFail f, ServiceValidationException e) {

		MessageFormat form = null;
		String mensaje = null;

		if (f instanceof InvalidBusinessObjectFail) {
			InvalidBusinessObjectFail ivf = (InvalidBusinessObjectFail) f;
			form = new MessageFormat(messageSource.getMessage(ivf.getBussinessCode(), new Object[0], locale));
			mensaje = form.format(ivf.getParam());
		} else if (f instanceof BusinessLogicFail) {
			BusinessLogicFail blf = (BusinessLogicFail) f;
			mensaje = messageSource.getMessage(blf.getBussinessCode(), blf.getArgs(), locale);
		} else if (f instanceof InvalidParameterFail) {
			mensaje = messageSource.getMessage(MSG_ERROR_PARAMETROS_SERVICIO, new Object[0], locale);
		} else {
			mensaje = messageSource.getMessage(f.getCode(), new Object[0], locale);
		}

		return mensaje;
	}

	public void handle(Errors errors, ServiceValidationException e) {

		if (e.getFails() == null || e.getFails().isEmpty()) {

			errors.reject(MSG_ERROR_VALIDACION_SERVICIO, new Object[] { e.toString() }, MSG_ERROR_INESPERADO);

		} else {

			// Fallos de negocio
			List<BusinessLogicFail> bussinesFails = e.getFails(null, BusinessLogicFail.class);
			handleBusinessFails(bussinesFails, errors, e);

			// Fallos de parametros
			List<InvalidParameterFail> parameterFails = e.getFails(null, InvalidParameterFail.class);
			handleInvalidParameters(parameterFails, errors);

		}

	}

	private void handleBusinessFails(List<BusinessLogicFail> fails, Errors errors, ServiceValidationException e) {

		for (BusinessLogicFail f : fails) {

			if (f instanceof InvalidBusinessObjectFail) {

				handleFail((InvalidBusinessObjectFail) f, errors, e);

			} else if (f instanceof MultipleInvalidBusinessObjectFail) {

				handleFail((MultipleInvalidBusinessObjectFail) f, errors, e);

			} else {

				handleFail(f, errors, e);
			}
		}

	}

	private void handleFail(BusinessLogicFail f, Errors errors, ServiceValidationException e) {
		errors.reject(f.getBussinessCode(), f.getArgs(), f.getBussinessCode());
	}

	private void handleFail(InvalidBusinessObjectFail f, Errors errors, ServiceValidationException e) {

		errors.reject(f.getBussinessCode(), new Object[] { f.getParam() }, "ERROR");

	}

	private void handleFail(MultipleInvalidBusinessObjectFail f, Errors errors, ServiceValidationException e) {
		Map<String, List<ServiceValidationFail>> fails = (Map<String, List<ServiceValidationFail>>) ((MultipleInvalidBusinessObjectFail) f).getParam();
		for (Map.Entry<String, List<ServiceValidationFail>> entry : fails.entrySet()) {
			List<ServiceValidationFail> failsByOperation = entry.getValue();
			for (ServiceValidationFail fail : failsByOperation) {
				errors.reject(f.getBussinessCode(), new Object[] { entry.getKey(), getMessage(fail, e) }, "ERROR");
			}
		}

	}

	private void handleInvalidParameters(List<InvalidParameterFail> fails, Errors errors) {

		List<InvalidParameterFail> toFilter = new LinkedList<InvalidParameterFail>(fails);
		if (groupers != null) {
			for (PathGrouper grouper : groupers) {
				grouper.groupAndExclude(toFilter, errors);
			}
		}

		if (!toFilter.isEmpty()) {
			errors.reject(MSG_ERROR_PARAMETROS_SERVICIO, new Object[] {}, MSG_ERROR_INESPERADO);
		}

	}

	
}
