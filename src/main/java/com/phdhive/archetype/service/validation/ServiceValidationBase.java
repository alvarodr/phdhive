package com.phdhive.archetype.service.validation;

import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import com.phdhive.archetype.dto.master.DataMaster;
import com.phdhive.archetype.dto.master.types.TypeDataMaster;
import com.phdhive.archetype.service.validation.fails.DetailFail;
import com.phdhive.archetype.service.validation.fails.FailCode;
import com.phdhive.archetype.service.validation.fails.InvalidParameterFail;
import com.phdhive.archetype.service.validation.fails.LimitFail;
import com.phdhive.archetype.utils.Constants;
import com.phdhive.archetype.utils.DataMasterUtils;
import com.phdhive.archetype.utils.PhdhiveUtils;

/**
 * Clase base para validadores de beans que contiene metodos para validaciones
 * frecuentes de campos.
 * 
 * @author emilio
 * 
 * @param <Type>
 *            tipo de objeto a validar
 */
public class ServiceValidationBase<Type> {

	/**
	 * Comprueba obligatoriedad.
	 * 
	 * @param value
	 * @param path
	 * @param ctx
	 * @return true si pasa la validacion, false si no.
	 */
	protected boolean checkMandatory(Object value, String path, ServiceValidationContext ctx) {
		boolean error = false;

		if (value == null) {
			error = true;
		} else if (value instanceof String) {
			if (StringUtils.isBlank((String) value)) {
				error = true;
			}
		}

		if (error) {
			ctx.add(new InvalidParameterFail(FailCode.REQUIRED, ctx, path));
		}

		return !error;
	}

	/**
	 * Comprueba que la longitud del valor no supere el valor indicado.
	 * 
	 * @param value
	 * @param path
	 * @param maxLength
	 * @param ctx
	 * @return true si pasa la validacion, false si no.
	 */
	protected boolean checkMaxLength(String value, String path, int maxLength, ServiceValidationContext ctx) {
		if (value != null) {
			if (value.length() > maxLength) {
				ctx.add(new LimitFail(FailCode.WRONG_LENGHT, ctx, path, null, maxLength));
				return false;
			}
		}

		return true;
	}

	/**
	 * Comprueba que la longitud del valor no sea menor que el valor indicado.
	 * 
	 * @param value
	 * @param path
	 * @param minLength
	 * @param ctx
	 * @return true si pasa la validacion, false si no.
	 */
	protected boolean checkMinLength(String value, String path, int minLength, ServiceValidationContext ctx) {
		if (value != null) {
			if (value.length() < minLength) {
				ctx.add(new LimitFail(FailCode.WRONG_LENGHT, ctx, path, minLength, null));
				return false;
			}
		}

		return true;
	}

	/**
	 * Comprueba que la longitud del valor se encuentre entre los limites
	 * indicados.
	 * 
	 * @param value
	 * @param path
	 * @param minLength
	 * @param maxLength
	 * @param ctx
	 * @return true si pasa la validacion, false si no.
	 */
	protected boolean checkBetweenLength(String value, String path, int minLength, int maxLength, ServiceValidationContext ctx) {
		if (value != null) {
			if ((value.length() < minLength) || (value.length() > maxLength)) {
				ctx.add(new LimitFail(FailCode.WRONG_LENGHT, ctx, path, minLength, maxLength));
				return false;
			}
		}

		return true;
	}

	/**
	 * Comprueba que el valor corresponda al tipo de dato maestro indicado.
	 * 
	 * @param value
	 * @param path
	 * @param tipoDatoMaestro
	 * @param ctx
	 * @return true si pasa la validacion, false si no.
	 */
	protected boolean checkDatoMaestro(String value, String path, TypeDataMaster typeDataMaster, ServiceValidationContext ctx) {
		if (StringUtils.isNotBlank(value)) {
			Collection<DataMaster> validValues = DataMasterUtils.getDataMasters(typeDataMaster);
			DataMaster datoMaestro = DataMaster.createDatoMaestro(typeDataMaster.type(), value, null);
			if (!validValues.contains(datoMaestro)) {
				ctx.add(new DetailFail(FailCode.INVALID_OPTION, ctx, path, PhdhiveUtils.dataMasterToString(validValues)));
				return false;
			}
		}
		return true;
	}

	/**
	 * Comprueba que el valor se encuentre en la lista indicada.
	 * 
	 * @param value
	 * @param path
	 * @param values
	 * @param ctx
	 * @return true si pasa la validacion, false si no.
	 */
	protected boolean checkInValues(String value, String path, Collection<String> values, ServiceValidationContext ctx) {
		if (StringUtils.isNotBlank(value) && (values != null) && !values.isEmpty()) {
			if (!values.contains(value)) {
				ctx.add(new DetailFail(FailCode.INVALID_OPTION, ctx, path, values.toString()));
				return false;
			}
		}
		return true;
	}

	/**
	 * Comprueba que el valor cumpla la expresion regular indicada.
	 * 
	 * @param value
	 * @param path
	 * @param pattern
	 * @param ctx
	 * @return true si pasa la validacion, false si no.
	 */
	protected boolean checkPattern(String value, String path, Pattern pattern, ServiceValidationContext ctx) {
		if (StringUtils.isNotBlank(value)) {
			Matcher m = pattern.matcher(value);
			if (!m.matches()) {
				ctx.add(new DetailFail(FailCode.FORMAT, ctx, path, pattern.toString()));
				return false;
			}
		}
		return true;
	}

	/**
	 * Comprueba que el valor introducido corresponda aun CIF.
	 * 
	 * @param value
	 * @param path
	 * @param ctx
	 * @return true si pasa la validacion, false si no.
	 */
	protected boolean checkCIF(String value, String path, ServiceValidationContext ctx) {
		if (StringUtils.isNotBlank(value)) {
			if (!Validations.isCIF(value)) {
				ctx.add(new DetailFail(FailCode.FORMAT, ctx, path, "Formato de CIF incorrecto"));
				return false;
			}
		}
		return true;
	}

	/**
	 * Comprueba que el valor introducido corresponda a un NIF.
	 * 
	 * @param value
	 * @param path
	 * @param ctx
	 * @return true si pasa la validacion, false si no.
	 */
	protected boolean checkNIF(String value, String path, ServiceValidationContext ctx) {
		if (StringUtils.isNotBlank(value)) {
			if (!Validations.isNIF(value)) {
				ctx.add(new DetailFail(FailCode.FORMAT, ctx, path, "Formato de NIF incorrecto"));
				return false;
			}
		}
		return true;
	}

	/**
	 * Comprueba que le valor introducido corresponda a un NIE.
	 * 
	 * @param value
	 * @param path
	 * @param ctx
	 * @return true si pasa la validacion, false si no.
	 */
	protected boolean checkNIE(String value, String path, ServiceValidationContext ctx) {
		if (StringUtils.isNotBlank(value)) {
			if (!Validations.isNIE(value)) {
				ctx.add(new DetailFail(FailCode.FORMAT, ctx, path, "Formato de NIE incorrecto"));
				return false;
			}
		}
		return true;
	}

	/**
	 * Comprueba que el valor introducido correponda a un codigo postal.
	 * 
	 * @param value
	 * @param path
	 * @param ctx
	 * @return true si pasa la validacion, false si no.
	 */
	protected boolean checkCodigoPostal(String value, String path, ServiceValidationContext ctx) {
		if (StringUtils.isNotBlank(value)) {
			if (!Validations.isCodigoPostal(value)) {
				ctx.add(new DetailFail(FailCode.FORMAT, ctx, path, "Formato de código postal incorrecto"));
				return false;
			}
		}
		return true;
	}

	/**
	 * Comprueba la validez del codigo cuenta cliente indicado.
	 * 
	 * @param entidad
	 * @param oficina
	 * @param dc
	 * @param cuenta
	 * @param path
	 * @param ctx
	 * @return true si pasa la validacion, false si no.
	 */
	protected boolean checkBankAccountFormat(String entidad, String oficina, String dc, String cuenta, String path, ServiceValidationContext ctx) {
		if (!Validations.isCuentaCorriente(entidad, oficina, dc, cuenta)) {
			ctx.add(new InvalidParameterFail(FailCode.INVALID_BANK_ACCOUNT, ctx, path));
			return false;
		}
		return true;
	}

	/**
	 * Comprueba que el valor entero introducido se encuentre en el rango
	 * indicado.
	 * 
	 * @param value
	 * @param min
	 * @param max
	 * @param path
	 * @param ctx
	 * @return true si pasa la validacion, false si no.
	 */
	protected boolean checkBetween(Integer value, int min, int max, String path, ServiceValidationContext ctx) {
		if ((value != null) && (value < min || value > max)) {
			ctx.add(new LimitFail(FailCode.WRONG_VALUE, ctx, path, min, max));
			return false;
		}

		return true;
	}

	/**
	 * Comprueba que el valor entero introducido no exceda el limite indicado.
	 * 
	 * @param value
	 * @param max
	 * @param path
	 * @param ctx
	 * @return true si pasa la validacion, false si no.
	 */
	protected boolean checkMax(Integer value, int max, String path, ServiceValidationContext ctx) {
		if ((value != null) && (value > max)) {
			ctx.add(new LimitFail(FailCode.WRONG_VALUE, path, null, max));
			return false;
		}

		return true;
	}

	/**
	 * Comprueba que el valor entero introducido no sea menor que el limite
	 * indicado.
	 * 
	 * @param value
	 * @param min
	 * @param path
	 * @param ctx
	 * @return true si pasa la validacion, false si no.
	 */
	protected boolean checkMin(Integer value, int min, String path, ServiceValidationContext ctx) {
		if ((value != null) && (value < min)) {
			ctx.add(new LimitFail(FailCode.WRONG_VALUE, path, min, null));
			return false;
		}

		return true;
	}

	/**
	 * Comprueba que el valor real introducido se encuentre en el rango
	 * indicado.
	 * 
	 * @param value
	 * @param min
	 * @param max
	 * @param path
	 * @param ctx
	 * @return true si pasa la validacion, false si no.
	 */
	protected boolean checkBetween(Double value, double min, double max, String path, ServiceValidationContext ctx) {
		if ((value != null) && (value < min || value > max)) {
			ctx.add(new LimitFail(FailCode.WRONG_VALUE, ctx, path, min, max));
			return false;
		}

		return true;
	}

	/**
	 * Comprueba que el valor real introducido no exceda el limite indicado.
	 * 
	 * @param value
	 * @param max
	 * @param path
	 * @param ctx
	 * @return true si pasa la validacion, false si no.
	 */
	protected boolean checkMax(Double value, double max, String path, ServiceValidationContext ctx) {
		if ((value != null) && (value > max)) {
			ctx.add(new LimitFail(FailCode.WRONG_VALUE, path, null, max));
			return false;
		}

		return true;
	}

	/**
	 * Comprueba que el valor real introducido no sea menor que el limite
	 * indicado.
	 * 
	 * @param value
	 * @param min
	 * @param path
	 * @param ctx
	 * @return true si pasa la validacion, false si no.
	 */
	protected boolean checkMin(Double value, double min, String path, ServiceValidationContext ctx) {
		if ((value != null) && (value < min)) {
			ctx.add(new LimitFail(FailCode.WRONG_VALUE, path, min, null));
			return false;
		}

		return true;
	}

	/**
	 * Comprueba que los valores introducidos sean iguales.
	 * 
	 * @param value1
	 * @param value2
	 * @param path
	 * @param ctx
	 * @return true si pasa la validacion, false si no.
	 */
	protected boolean checkEquals(String value1, String value2, String path, ServiceValidationContext ctx) {
		if (!StringUtils.equals(value1, value2)) {
			ctx.add(new DetailFail(FailCode.WRONG_VALUE, path, "Los valores introducidos no coinciden"));
			return false;
		}

		return true;
	}

	/**
	 * Comprueba que el valor introducido corresponda a una fecha válida.
	 * 
	 * @param value
	 * @param path
	 * @param ctx
	 * @return true si pasa la validacion, false si no.
	 */
	protected boolean checkFecha(String value, String path, ServiceValidationContext ctx) {
		// Comprobar que la longitud de la cadena fecha coincida con el formato esperado para que no valide por ejemplo 01/01/12
		if (StringUtils.isNotBlank(value) && (value.length() >= 0) && (value.length() != Constants.FORMATO_FECHA_POR_DEFECTO.length())) {
			ctx.add(new InvalidParameterFail(FailCode.WRONG_VALUE, ctx, path));
			return false;
		} else {
			if (!Validations.isFecha(value)) {
				ctx.add(new InvalidParameterFail(FailCode.WRONG_VALUE, ctx, path));
				return false;
			}
		}

		return true;
	}
	
	/**
	 * Comprueba que el valor introducido corresponda a un entero.
	 * 
	 * @param value
	 * @param path
	 * @param ctx
	 * @return true si pasa la validacion, false si no.
	 */
	protected boolean checkInteger(String value, String path, ServiceValidationContext ctx) {
		if (StringUtils.isNotBlank(value) && !Validations.isNumeroEntero(value)) {
			ctx.add(new InvalidParameterFail(FailCode.WRONG_VALUE, ctx, path));
			return false;
		}

		return true;
	}

	/**
	 * Comprueba que el valor introducido se encuentre en la enumeracion
	 * indicada.
	 * 
	 * @param value
	 * @param enumeration
	 * @param path
	 * @param ctx
	 * @return true si pasa la validacion, false si no.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected boolean checkInEnum(String value, Class<? extends Enum> enumeration, String path, ServiceValidationContext ctx) {
		if (value != null) {
			try {
				Enum.valueOf(enumeration, value);
			} catch (IllegalArgumentException ex) {
				ctx.add(new DetailFail(FailCode.INVALID_OPTION, ctx, path, "El valor no pertenece a la enumeracion"));
				return false;
			}
		}

		return true;
	}
	
	/**
	 * Value is present in list.
	 * 
	 * @param field
	 * @param collection
	 * @param value
	 * @param errors
	 */
	protected void checkInList(String value, Collection<?> collection, String path, ServiceValidationContext ctx){
		boolean error = false;
		if (value == null || collection == null) {
			return;
		}
		
		try {
			error = !collection.contains(value);
		} catch (Exception e) {
			error = true;
		}
		
		if (error){
			ctx.add(new DetailFail(FailCode.INVALID_OPTION, ctx, path, "El valor no pertenece a la lista "+collection));
		}
	}
}
