package com.phdhive.archetype.service.validation.fails;

import org.apache.commons.lang.Validate;

/**
 * Representa errores en validaciones que son referentes a la logica de negocio.<BR>
 * 
 * Por ejemplo validaciones que afectan a varios campos, o a situaciones
 * complejas.
 * 
 * @author Alvaro
 * 
 */
public class BusinessLogicFail extends ServiceValidationFail {

	private static final long serialVersionUID = 2690125557876799638L;

	private String bussinessCode;
	
	private Object[] args = new Object[0];

	/**
	 * Crea un error de validacion de logica de negocio con codigo
	 * {@link FailCode#BUSINESS}.<br>
	 * 
	 * Los codigos de este tipo de validaciones no deben repetirse.<BR>
	 * 
	 * @param serviceName
	 *            Nombre de servicio que realiza la validacion (Nombre y metodo
	 *            de servicio).
	 * @param code
	 *            Codigo de la validacion fallida
	 */
	public BusinessLogicFail(String bussinessCode) {
		super(FailCode.BUSINESS);
		this.bussinessCode = bussinessCode;
	}

	public BusinessLogicFail(String bussinessCode, Object[] args) {
		super(FailCode.BUSINESS);
		this.bussinessCode = bussinessCode;
		Validate.notNull(args);
		this.args = args;
	}

	/**
	 * Crea un error de validacion de logica de negocio.<br>
	 * 
	 * Los codigos de este tipo de validaciones no deben repetirse.<BR>
	 * 
	 * @param serviceName
	 *            Nombre de servicio que realiza la validacion (Nombre y metodo
	 *            de servicio).
	 * @param code
	 *            Codigo de la validacion fallida
	 */
	public BusinessLogicFail(String code, String bussinessCode) {
		super(code);
		this.bussinessCode = bussinessCode;
	}

	/**
	 * Devuelve el codigo de fallo de logica de negocio.
	 * 
	 * @return el codigo de error de logica de negocio.
	 */
	public String getBussinessCode() {
		return bussinessCode;
	}

	public Object[] getArgs() {
		return args;
	}

	/**
	 * Pinta el error.
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("{ ");
		sb.append("code='");
		sb.append(getCode());
		sb.append("' businessCode='");
		sb.append(getBussinessCode());
		sb.append("' } ");
		return sb.toString();
	}

}
