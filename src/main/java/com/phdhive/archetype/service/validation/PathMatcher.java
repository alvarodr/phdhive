package com.phdhive.archetype.service.validation;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Patron de match de rutas de fallos de servicio.
 * 
 * @author Alvaro
 * 
 */
public class PathMatcher {

	private static final String REPLACE_ARRAY = "\\[([0-9]+)\\]";

	private static final String REPLACE_NO_CAPTURE_ARRAY = "\\[(?:[0-9]+)\\]";

	private static final Pattern ARRAY_WILDCARD = Pattern.compile("(.*?)\\[([*#])\\]");

	private Pattern pattern;

	private String source;

	private int wilcardCount;

	private PathWildcardType[] wilcardTypes;

	public PathMatcher(String sPattern) {
		this.source = sPattern;
		translatePattern(sPattern);

	}

	public String getPattern() {
		return pattern.toString();
	}

	public String getSource() {
		return source;
	}

	public int getWilcardCount() {
		return wilcardCount;
	}

	public PathWildcardType[] getWilcardTypes() {
		return wilcardTypes;
	}

	public PathMatcherResult matches(String path) {

		Matcher m = pattern.matcher(path);
		PathMatcherResult res;
		if (m.find()) {

			String[] wilcards = new String[wilcardCount];
			for (int i = 0; i < wilcardCount; i++) {
				String wildcard = m.group(i + 1);
				if (wildcard == null) {
					wildcard = "";
				}
				wilcards[i] = wildcard;
			}
			res = new PathMatcherResult(wilcards, wilcardTypes);

		} else {
			res = null;
		}
		return res;
	}

	public void setGroupCount(int groupCount) {
		this.wilcardCount = groupCount;
	}

	private void translatePattern(String sourcePattern) {

		List<PathWildcardType> types = new ArrayList<PathWildcardType>();

		StringBuilder sb = new StringBuilder("^");
		Matcher m = ARRAY_WILDCARD.matcher(sourcePattern);
		int last = 0;
		while (m.find()) {
			last = m.end();

			String prefix = m.group(1);

			sb.append("\\Q");
			sb.append(prefix);
			sb.append("\\E");

			String wildcard = m.group(2);
			if ("*".equals(wildcard)) {
				sb.append(REPLACE_NO_CAPTURE_ARRAY);
			} else {
				// Pensado para exten
				types.add(PathWildcardType.INDEX);
				wilcardCount++;
				sb.append(REPLACE_ARRAY);
			}

		}
		if (last < sourcePattern.length()) {
			sb.append("\\Q");
			sb.append(sourcePattern.substring(last));
			sb.append("\\E");
		}

		this.pattern = Pattern.compile(sb.toString());
		this.wilcardTypes = types.toArray(new PathWildcardType[types.size()]);
	}

}
