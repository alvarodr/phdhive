package com.phdhive.archetype.service.validation.fails;

import com.phdhive.archetype.service.validation.ServiceValidationContext;

/**
 * Representa un error en un parametro con un detalle.<BR>
 * 
 * @author emilio
 * 
 */
public class DetailFail extends InvalidParameterFail {

	private static final long serialVersionUID = -221471566454813382L;

	/**
	 * Cadena que describe el detalle de un parametro.
	 */
	private String detail;

	/**
	 * Crea un error que indica que un parametro (especificado de forma relativa
	 * al contexto) tiene un error, especificando un detalle.<BR>
	 * 
	 * La ruta del parametro es relativa a los nombres de los parametros de los
	 * metodos del servicio.<BR>
	 * 
	 * @param code
	 *            Codigo de la validacion fallida
	 * @param ctx
	 *            Contexto de validacion
	 * @param relativePath
	 *            Ruta del parametro relativa al contexto.
	 * @param format
	 *            Cadena que describe el formato requerido
	 */
	public DetailFail(String code, ServiceValidationContext ctx, String relativePath, String format) {
		super(code, ctx, relativePath);
		this.detail = format;
	}

	/**
	 * Crea un error que indica que un parametro tiene un error, especificando
	 * un detalle.<BR>
	 * 
	 * La ruta del parametro es relativa a los nombres de los parametros de los
	 * metodos del servicio.<BR>
	 * 
	 * @param code
	 *            Codigo de la validacion fallida
	 * @param parameterPath
	 *            Ruta del parametro.
	 * @param format
	 *            Cadena que describe el formato requerido
	 */
	public DetailFail(String code, String parameterPath, String format) {
		super(code, parameterPath);
		this.detail = format;
	}

	/**
	 * Obtiene la descripcion textual del detalle del error.
	 * 
	 * @return el detalle
	 */
	public String getDetail() {
		return detail;
	}

	/**
	 * Pinta el error.
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("{ ");
		sb.append("code='");
		sb.append(getCode());
		sb.append("' parameterPath='");
		sb.append(getParameterPath());
		sb.append("' detail='");
		sb.append(getDetail());
		sb.append("' } ");
		return sb.toString();
	}

}
