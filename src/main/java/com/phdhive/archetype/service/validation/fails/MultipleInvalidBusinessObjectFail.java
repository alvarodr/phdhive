package com.phdhive.archetype.service.validation.fails;

import java.util.List;
import java.util.Map;

/**
 * Fallo de negocio que engloba fallos de negocio de servicios con parametros
 * multiples.
 * 
 * @author Alvaro
 */
public class MultipleInvalidBusinessObjectFail extends BusinessLogicFail {

	private static final long serialVersionUID = -891160577194257211L;

	private Map<String, List<ServiceValidationFail>> param;

	public MultipleInvalidBusinessObjectFail(String bussinessCode, Map<String, List<ServiceValidationFail>> param) {
		super(bussinessCode);
		this.param = param;
	}

	public MultipleInvalidBusinessObjectFail(String bussinessCode) {
		super(bussinessCode);
	}

	public Map<String, List<ServiceValidationFail>> getParam() {
		return param;
	}
}
