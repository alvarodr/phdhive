package com.phdhive.archetype.service.validation.fails;

/**
 * Error de negocio asociado a una entidad que no es necesariamente un parametro
 * del objeto validado. Ademas de informar del codigo de error, incluye un
 * parametro disponible para componer mensajes de error.
 * 
 * @author Alvaro
 * 
 */
public class InvalidBusinessObjectFail extends BusinessLogicFail {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3739139086674411141L;
	private Object param;

	public InvalidBusinessObjectFail(String bussinessCode, Object param) {
		super(bussinessCode);
		this.param = param;
	}

	public InvalidBusinessObjectFail(String bussinessCode) {
		super(bussinessCode);
	}

	public Object getParam() {
		return param;
	}

	/**
	 * Pinta el error.
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("{ ");
		sb.append("code='");
		sb.append(getCode());
		sb.append("' businessCode='");
		sb.append(getBussinessCode());
		if (param != null) {
			sb.append("' param='");
			sb.append(param.toString());
		}
		sb.append("' } ");
		return sb.toString();
	}

}
