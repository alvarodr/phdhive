package com.phdhive.archetype.service.validation;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import org.apache.commons.lang.StringUtils;

import com.phdhive.archetype.service.validation.fails.ServiceValidationFail;

/**
 * Representa el contexto de validacion de un servicio.<br>
 * 
 * Facilita la gestion de errores de campos de parametros de servicio,
 * gestionando rutas de beans anidados y gestionando bucles.<BR>
 * 
 * Facilita la gestion de excepciones de validacion.
 * 
 * @author Alvaro
 * 
 */
public class ServiceValidationContext {

	/**
	 * Entrada de la pila del contexto de validacion. Almacena el estado.
	 * 
	 */
	private class StackEntry {

		private String path;

		private boolean loop;

		private int counter = -1;

		public StackEntry(String path, boolean loop) {
			super();
			this.path = path;
			if (loop) {
				this.loop = loop;

			}
		}

		public String getEffectivePath() {

			String efPath;
			if (loop) {
				if (counter < 0) {
					throw new IllegalStateException("Iteracion no inicializada");
				}
				StringBuilder sb = new StringBuilder();
				sb.append(path);
				sb.append('[');
				sb.append(counter);
				sb.append(']');
				efPath = sb.toString();

			} else if (StringUtils.isBlank(path)) {
				return "";
			} else {
				efPath = path;
			}

			return efPath;
		}

		public void inc() {
			if (loop) {
				counter++;
				return;
			}
			throw new IllegalStateException("No esta dentro de una iteracion");

		}
	}

	/**
	 * Nombre del servicio.
	 */
	private String serviceName;

	private Stack<StackEntry> stack = new Stack<StackEntry>();

	private boolean fastFail;

	private List<ServiceValidationFail> fails;

	/**
	 * Crea un contexto de validacion de servicio, dado una clase y el nombre de
	 * metodo.
	 * 
	 * @param clazz
	 *            Clase del servicio
	 * @param methodName
	 *            Nombre de del metodo del servicio
	 */
	public ServiceValidationContext(Class<?> clazz, String methodName) {
		this(clazz.getSimpleName() + "." + methodName);
	}

	/**
	 * Crea un contexto de validacion de servicio, dado el nombre del servicio.
	 * 
	 * @param serviceName
	 *            Nombre de servicio que realiza la validacion (Nombre y metodo
	 *            de servicio).
	 */
	public ServiceValidationContext(String serviceName) {
		super();
		this.serviceName = serviceName;
		this.stack.push(new StackEntry("", false));
	}

	/**
	 * Crea un contexto de validacion de servicio, dado el nombre del servicio.
	 * 
	 * @param serviceName
	 *            Nombre de servicio que realiza la validacion (Nombre y metodo
	 *            de servicio).
	 * @param fastFailSi
	 *            debe lanzar excepcion en el primer error de validacion.
	 */
	public ServiceValidationContext(String serviceName, boolean fastFail) {
		super();
		this.serviceName = serviceName;
		this.fastFail = fastFail;
	}

	/**
	 * Agrega un fallo de validacion a los servicios.<br>
	 * 
	 * Si esta configurado para fallo rapido, lanza la excepcion.
	 * 
	 * @param fail
	 */
	public void add(ServiceValidationFail fail) {
		if (fails == null) {
			fails = new ArrayList<ServiceValidationFail>();
		}
		fails.add(fail);
		if (fastFail) {
			throw new ServiceValidationException(this.serviceName, this.fails);
		}
	}

	/**
	 * Lanza una excepcion de validacion si hay algun error acumulado.
	 */
	public void check() {

		if (fails != null && !fails.isEmpty()) {
			throw new ServiceValidationException(this.serviceName, this.fails);
		}

	}

	/**
	 * Indica si hay algun error acumulado.
	 */
	public boolean hasFails() {
		return (fails != null && !fails.isEmpty());
	}

	/**
	 * Agrega un elemento en la pila de rutas para que las rutas devueltas por
	 * el metodo {@link #pathFor(String)} sean relativas a este elemento.
	 * 
	 * @param pathComponent
	 *            el componente anidado.
	 */
	public void enterTo(String pathComponent) {
		String newPath = pathFor(pathComponent, false);
		StackEntry newEntry = new StackEntry(newPath, false);
		stack.push(newEntry);
	}

	/**
	 * Restaura el estado de la pila de rutas al estado anterior.
	 */
	public void exit() {
		if (stack.size() > 1) {
			stack.pop();
		}
	}

	/**
	 * Agrega un elemento en la pila de rutas para que las rutas devueltas por
	 * el metodo {@link #pathFor(String)} sean relativas a este elemento en una
	 * iteracion. <BR>
	 * 
	 * El bucle empieza en 0 a partir del primer incLoop()
	 * 
	 * 
	 * @param pathComponent
	 *            el componente anidado.
	 */
	public void enterLoop(String pathComponent) {
		String newPath = pathFor(pathComponent, false);
		StackEntry newEntry = new StackEntry(newPath, true);
		stack.push(newEntry);

	}

	/**
	 * Agrega un elemento en la pila de rutas para que las rutas devueltas por
	 * el metodo {@link #pathFor(String)} sean relativas a este elemento en una
	 * iteracion (util para listas de listas y arrays multidimensionales). <BR>
	 * 
	 * El bucle empieza en 0 a partir del primer incLoop()
	 * 
	 * 
	 * @param pathComponent
	 *            el componente anidado.
	 */
	public void enterLoop() {
		String newPath = pathFor("", true);
		StackEntry newEntry = new StackEntry(newPath, true);
		stack.push(newEntry);

	}

	/**
	 * Incrementa el valor de iteracion del bucle.
	 * 
	 */
	public void incLoop() {
		StackEntry e = stack.peek();
		e.inc();
	}

	/**
	 * Devuelve una ruta de parametro relativa a la ruta actual.
	 * 
	 * @param pathComponent
	 * @return
	 */
	public String pathFor(String pathComponent) {
		return pathFor(pathComponent, false);
	}

	private String pathFor(String pathComponent, boolean permitBlank) {
		StackEntry e = stack.peek();
		boolean empty = StringUtils.isBlank(pathComponent);

		if (empty && !permitBlank) {
			throw new IllegalArgumentException("No se permite una ruta vacia");
		}
		String path;

		String base = e.getEffectivePath();
		if (empty) {
			path = base;
		} else if (StringUtils.isNotBlank(base)) {
			StringBuilder sb = new StringBuilder();
			sb.append(base);
			sb.append('.');
			sb.append(pathComponent);
			path = sb.toString();
		} else {
			path = pathComponent;
		}

		return path;
	}

}
