package com.phdhive.archetype.service.validation.fails;
/**
 * 
 * @author Alvaro
 *
 */
public final class BusinessLogicFailCode {
	
	public static final String USER_NOT_FOUND = "BUS-00001";
	public static final String USER_BAD_CREDENTIALS = "BUS-00002";
	public static final String USER_PASSWORD_INCORRECT = "BUS-00003";
	public static final String USER_INACTIVE = "BUS-00004";
	public static final String USER_NOT_ADMIN = "BUS-00005";
	public static final String SOLR_CONNECTION_TIMEOUT = "BUS-00006";
	public static final String SOLR_EXCEPTION = "BUS-00007";
	public static final String DOCUMENT_FORMAT = "BUS-00008";
	public static final String DOCUMENT_FAIL_LOAD = "BUS-00009";
	public static final String DATA_LOAD_ERROR = "BUS-00010";
	public static final String GEOLOCATION_USER_ERROR = "BUS-00011";
	public static final String INCONSISTENT_PARAMETERS = "BUS-00012";
	public static final String CLASS_NOT_FOUND = "BUS-00013";
	public static final String ILEGAL_CLASS_ACCESS = "BUS-00014";
	public static final String INVALID_FORMAT_DOCUMENT = "BUS-00015";
	public static final String USER_ALREADY_LINKED_EVENT = "BUS-00016";
	public static final String USER_NOT_EXISTS = "BUS-00017";
}
