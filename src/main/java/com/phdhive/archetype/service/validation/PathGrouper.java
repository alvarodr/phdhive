package com.phdhive.archetype.service.validation;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.validation.Errors;

import com.phdhive.archetype.service.validation.fails.InvalidParameterFail;

/**
 * Traductor de paths a errores.
 * 
 * @author Alvaro
 * 
 */
public class PathGrouper {

	private PathMatcher matcher;

	private PathReplacer replacer;

	private String field;

	private int[] groupConfig;

	public PathGrouper(String sField, String pattern, String message, int... grpCfg) {

		this.matcher = new PathMatcher(pattern);
		this.replacer = new PathReplacer(message);

		int maxGroup = this.matcher.getWilcardCount() + 1;

		if (this.replacer.getMaxGroup() > maxGroup) {
			throw new IllegalArgumentException("Hay mas grupos en el patron de reemplazo que en el de matching");
		}
		if (grpCfg != null) {
			this.groupConfig = new int[grpCfg.length];
			for (int i = 0; i < grpCfg.length; i++) {
				int val = grpCfg[i];
				if (val <= 0 || val > maxGroup) {
					throw new IllegalArgumentException("Configuracion ilegal de grupos, posicion " + i + ", valor " + val);
				}
				this.groupConfig[i] = val;
			}
		}
	}

	public void groupAndExclude(List<InvalidParameterFail> fails, Errors errors) {

		Set<PathGrouperKey> alreadySetGroups = new HashSet<PathGrouperKey>();
		Iterator<InvalidParameterFail> it = fails.iterator();
		while (it.hasNext()) {
			InvalidParameterFail fail = it.next();
			PathMatcherResult res = matcher.matches(fail.getParameterPath());
			if (res != null) {

				// Matched
				it.remove();

				PathGrouperKey key = new PathGrouperKey(res, this.groupConfig);

				// Comprobamos agrupacion
				if (alreadySetGroups.contains(key)) {
					// Agrupado
					continue;
				}
				alreadySetGroups.add(key);

				// Si agrupa, la primera ocurrencia, si no, todas y cada una.
				String message = replacer.replace(res);
				if (field == null) {

					errors.reject(message, wildcardsToObjects(res), "agrupado");
				} else {
					errors.rejectValue(field, message, wildcardsToObjects(res), "agrupado");
				}
			}
		}
	}

	private Object[] wildcardsToObjects(PathMatcherResult res) {

		Object[] arr = new Object[res.getWildcardCount()];
		for (int i = 0; i < arr.length; i++) {
			String wildcard = res.getWildcard(i);
			PathWildcardType type = res.getWildcardType(i);
			Object val;

			if (PathWildcardType.INDEX == type) {
				val = Integer.parseInt(wildcard) + 1;
			} else {
				val = wildcard;
			}
			arr[i] = val;
		}
		return arr;
	}

}
