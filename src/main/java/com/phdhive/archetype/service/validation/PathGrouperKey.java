package com.phdhive.archetype.service.validation;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Path Grouper Key.
 * 
 * @author Alvaro
 *
 */
public class PathGrouperKey implements Serializable {

	private static final long serialVersionUID = -7515348646265669417L;

	private String[] groups;

	public PathGrouperKey() {
		super();
	}

	public PathGrouperKey(PathMatcherResult res, int[] groupConfig) {
		if (groupConfig == null) {
			groups = res.getWildcards();
		} else {
			groups = new String[groupConfig.length];
			for (int i = 0; i < groupConfig.length; i++) {
				groups[i] = res.getWildcard(groupConfig[i] - 1);
			}
		}

	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PathGrouperKey other = (PathGrouperKey) obj;
		if (!Arrays.equals(groups, other.groups))
			return false;
		return true;
	}

	public String[] getGroups() {
		return groups;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(groups);
		return result;
	}

	public void setGroups(String[] groups) {
		this.groups = groups;
	}

}