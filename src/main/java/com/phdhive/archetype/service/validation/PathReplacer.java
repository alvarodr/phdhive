package com.phdhive.archetype.service.validation;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Reemplaza patrones.<BR>
 * 
 * del formato
 * 
 * <pre>
 * a.b[$1].c[$2]
 * </pre>
 * 
 * Los $n son sustituidos por los comodines de un {@link PathMatcherResult}
 * 
 * @author Alvaro
 * 
 */
public class PathReplacer {

	private static final Pattern REFERENCE_PATTERN = Pattern.compile("\\$([0-9]+)");

	private String source;

	private Object[] parts;

	private int maxGroup = -1;

	public PathReplacer(String sSource) {

		this.source = sSource;

		Matcher m = REFERENCE_PATTERN.matcher(sSource);

		List<Object> p = new ArrayList<Object>();

		int last = 0;
		while (m.find()) {
			String ref = m.group(1);

			int pos = m.start();
			String part = sSource.substring(last, pos);
			p.add(part);

			int refN = Integer.parseInt(ref);
			p.add(refN);
			if (refN > maxGroup) {
				maxGroup = refN;
			}
			last = m.end();

		}
		if (last != sSource.length()) {
			String part = sSource.substring(last);
			p.add(part);
		}
		parts = p.toArray(new Object[p.size()]);
	}

	public int getMaxGroup() {
		return maxGroup;
	}

	public Object[] getParts() {
		return parts;
	}

	public String getSource() {
		return source;
	}

	/**
	 * Reemplaza un patron dado un resultado de matcheo.
	 * 
	 * @param result
	 * @return
	 */
	public String replace(PathMatcherResult result) {

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < parts.length; i++) {
			Object part = parts[i];
			if (part instanceof Integer) {
				int refN = (Integer) part;
				String ref = result.getWildcard(refN - 1);
				sb.append(ref);
			} else {
				sb.append(part.toString());
			}
		}
		return sb.toString();
	}
}
