package com.phdhive.archetype.service.validation.fails;

import java.io.Serializable;

/**
 * 
 * @author Alvaro
 *
 */
public class ServiceValidationFail implements Serializable {

	private static final long serialVersionUID = -8453978860822282201L;

	/**
	 * Codigo de error.
	 */
	private String code;

	/**
	 * Crea un error de validacion de servicio.<BR>
	 * 
	 * @param message
	 *            Mensaje generico para la validacion.
	 * 
	 * @param code
	 *            Codigo de la validacion fallida
	 * 
	 */
	public ServiceValidationFail(String code) {
		this.code = code;
	}

	/**
	 * Obtiene el codigo del error.
	 * 
	 * @return el codigo
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Pinta el error.
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("{ ");
		sb.append("code='");
		sb.append(getCode());
		sb.append("' } ");
		return sb.toString();
	}
}
