package com.phdhive.archetype.service.validation;

import java.util.Arrays;

/**
 * Resultado de patrones.
 * 
 * @author Alvaro
 * 
 */
public class PathMatcherResult {

	private String[] wildcards;

	private PathWildcardType[] wildcardTypes;

	public PathMatcherResult(String[] sWildcards, PathWildcardType[] wilcardTypes) {
		super();
		this.wildcards = sWildcards;
		this.wildcardTypes = wilcardTypes;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PathMatcherResult other = (PathMatcherResult) obj;
		if (!Arrays.equals(wildcards, other.wildcards))
			return false;
		return true;
	}

	public String getWildcard(int i) {
		if (i >= wildcards.length) {
			return "";
		}
		return wildcards[i];
	}

	public int getWildcardCount() {
		return wildcards.length;
	}

	public String[] getWildcards() {
		return wildcards;
	}

	public PathWildcardType getWildcardType(int i) {
		if (i >= wildcardTypes.length) {
			return null;
		}
		return wildcardTypes[i];
	}

	public PathWildcardType[] getWildcardTypes() {
		return wildcardTypes;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(wildcards);
		return result;
	}

}
