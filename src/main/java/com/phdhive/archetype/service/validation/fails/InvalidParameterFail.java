package com.phdhive.archetype.service.validation.fails;

import com.phdhive.archetype.service.validation.ServiceValidationContext;


/**
 * Representa un error generico para las validaciones de parameteros de
 * servicios.
 * 
 * 
 * @author Alvaro
 * 
 */
public class InvalidParameterFail extends ServiceValidationFail {

	private static final long serialVersionUID = 7496914495602027959L;

	/**
	 * Ruta del parametro en el servicio.
	 */
	private String parameterPath;

	/**
	 * Crea un error de parametro invalido (especificado de forma relativa al
	 * contexto).<br>
	 * 
	 * La ruta del parametro es relativa a los nombres de los parametros de los
	 * metodos del servicio.<BR>
	 * 
	 * <B>ANTES DE USAR ESTE OBJETO, HAY QUE ASEGURARSE QUE NO HAY UNA
	 * ESPECIALIZACION MEJOR</B><BR>
	 * 
	 * @param code
	 *            Codigo de la validacion fallida
	 * @param ctx
	 *            Contexto de validacion
	 * @param relativePath
	 *            Ruta del parametro relativa al contexto.
	 */
	public InvalidParameterFail(String code, ServiceValidationContext ctx, String relativePath) {
		super(code);
		this.parameterPath = ctx.pathFor(relativePath);
	}

	/**
	 * Crea un error de parametro invalido.<br>
	 * 
	 * La ruta del parametro es relativa a los nombres de los parametros de los
	 * metodos del servicio.<BR>
	 * 
	 * <B>ANTES DE USAR ESTE OBJETO, HAY QUE ASEGURARSE QUE NO HAY UNA
	 * ESPECIALIZACION MEJOR</B><BR>
	 * 
	 * @param code
	 *            Codigo de la validacion fallida
	 * @param parameterPath
	 *            Ruta del parametro.
	 */
	public InvalidParameterFail(String code, String parameterPath) {
		super(code);
		this.parameterPath = parameterPath;
	}

	/**
	 * Obtiene la ruta del parametro con error.<br>
	 * 
	 * La ruta del parametro es relativa a los nombres de los parametros de los
	 * metodos del servicio.
	 * 
	 * @return la ruta
	 */
	public String getParameterPath() {
		return parameterPath;
	}
	
	/**
	 * Pinta el error.
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder() ;
		sb.append("{ ") ;
		sb.append("code='") ;
		sb.append(getCode()) ;
		sb.append("' parameterPath='") ;
		sb.append(getParameterPath()) ;
		sb.append("' } ") ;
		return sb.toString() ;
	}

}
