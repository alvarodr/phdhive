package com.phdhive.archetype.service.integration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.LocaleResolver;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.phdhive.archetype.dto.document.DocumentMultipart;
import com.phdhive.archetype.dto.user.User;
import com.phdhive.archetype.dto.ws.NewUserWebService;
import com.phdhive.archetype.dto.ws.UserWebService;
import com.phdhive.archetype.security.Role;
import com.phdhive.archetype.service.IKeyWordsService;
import com.phdhive.archetype.service.ISolrService;
import com.phdhive.archetype.service.IUserService;

/**
 * 
 * @author Alvaro
 * 
 * Web Services para comunicación con app externas
 *
 */

@Controller
public class IntegrationWebServices {
	
	@Autowired
	private IUserService userService;
	
	@Autowired
	private ISolrService solrService;
	
	@Autowired
	private IKeyWordsService keyWordsService;
	
	@Autowired
	private LocaleResolver localeResolver;
	
	private Logger log = Logger.getLogger(IntegrationWebServices.class);
	
	@RequestMapping(value="/services/user/{userEmail:.*}", method=RequestMethod.GET, produces="application/json;charset=UTF-8")
	public @ResponseBody Map<String, Object> existUser(@PathVariable(value="userEmail") String userEmail, HttpServletRequest request) throws Exception{
		Map<String, Object> response = new HashMap<String, Object>();
		Boolean exist = false;
		User user = null;
		if ((user = userService.readByEmail(userEmail))!=null){
			exist = true;
			response.put("token", request.getSession().getId());
			user.setToken(request.getSession().getId());
			userService.updateUser(user);
		}
		response.put("response", exist);
		return response;
	}
	
	@RequestMapping(value="/services/user/profile/{userToken:.*}", method=RequestMethod.GET, produces="application/json;charset=UTF-8")
	public @ResponseBody Map<String, Object> userProfile(@PathVariable(value="userToken") String userToken, HttpServletRequest request){
		Map<String, Object> response = new HashMap<String, Object>();
		if (request.getSession().getId().equals(userToken)){
			response.put("response", userService.getUserByToken(userToken));
		}else{
			response.put("response", "ERROR: Your session has expired");
		}
		return response;
	}
	
	@RequestMapping(value="/services/user/upload/{userToken}", method=RequestMethod.POST, produces="application/json;charset=UTF-8")
	public @ResponseBody String analizeDocument(@PathVariable(value="userToken") String userToken, 
			DocumentMultipart documento, HttpServletRequest request) throws Exception{
		Map<String, String> response = new HashMap<String, String>();
		Gson gson = new Gson();
		try{
			if (request.getSession().getId().equals(userToken)){
				//TODO: incluir evento como parámetro
				List<User> users = solrService.searhUsersSolr(documento.toDocument(), localeResolver.resolveLocale(request), null);
				List<UserWebService> usersResult = new ArrayList<UserWebService>();
				for (User user : users){
					UserWebService userAux = new UserWebService();
					userAux.setEmail(user.getEmail());
					userAux.setName(user.getName());
					userAux.setSurname(user.getSurname());
					userAux.setInstitutionAffiliation(user.getInstitutionAffiliation());
					List<Map<String, String>> listaWords = new ArrayList<Map<String, String>>();
					Map<String, String> mapa = null;
					for (Entry<String, Integer> word : user.getFoundWords()){
						mapa = new HashMap<String, String>();
						mapa.put(word.getKey(), word.getValue().toString());
						listaWords.add(mapa);
					}
					userAux.setKeyList(listaWords);
					usersResult.add(userAux);
				}
				return gson.toJson(usersResult, List.class);
			}else{
				response.put("response", "ERROR: Your session has expired");
				return gson.toJson(response);
			}			
		}catch (Exception e){
			response.put("response", e.getMessage());
			return gson.toJson(response);
		}
	}
	
	@RequestMapping(value="/services/user/peers/{userToken}", method=RequestMethod.GET, produces="application/json;charset=UTF-8")
	public @ResponseBody Map<String, Object> findPeers(@PathVariable(value="userToken") String userToken, HttpServletRequest request){
		Map<String, Object> response = new HashMap<String, Object>();
		try{
			if (request.getSession().getId().equals(userToken)){
				List<User> users = solrService.searchUsersSolr(userService.getUserByToken(userToken), localeResolver.resolveLocale(request));
				List<UserWebService> usersResult = new ArrayList<UserWebService>();
				for (User user : users){
					UserWebService userAux = new UserWebService();
					userAux.setEmail(user.getEmail());
					userAux.setName(user.getName());
					userAux.setSurname(user.getSurname());
					userAux.setInstitutionAffiliation(user.getInstitutionAffiliation());
					List<Map<String, String>> listaWords = new ArrayList<Map<String, String>>();
					Map<String, String> mapa = null;
					for (Entry<String, Integer> word : user.getFoundWords()){
						mapa = new HashMap<String, String>();
						mapa.put(word.getKey(), word.getValue().toString());
						listaWords.add(mapa);
					}
					userAux.setKeyList(listaWords);
					usersResult.add(userAux);
				}
				response.put("result", usersResult);
				return response;
			}else{
				response.put("response", "ERROR: Your session has expired");
			}
		}catch (Exception e){
			response.put("response", "ERROR: internal error");
		}
		return response;
	}
	
	@RequestMapping(value="/services/user/new", method=RequestMethod.POST)
	public @ResponseBody Map<String, String> newUser(NewUserWebService data){
		Map<String, String> map = new HashMap<String, String>();
		try{
			User user = new User();
			DocumentMultipart documento = new DocumentMultipart();
			documento.setUserfile(data.getUserfile());
			BeanUtils.copyProperties(data, user, User.class);
			user.setLatestArticle(keyWordsService.getKeyWordsAndFrequency(documento.toDocument()));
			Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
			authorities.add((GrantedAuthority)Role.ROLE_USER);
			user.setAuthorities(authorities);
			userService.createUser(user);
			map.put("response", "OK");
		}catch (JsonSyntaxException e){
			log.error("Se ha producido un error en la obtencion de los datos", e);
			map.put("response", "ERROR: Se ha producido un error en la obtencion de los datos");
		}catch (Exception e) {
			log.error("Se ha producido un error al guardar el usuario", e);
			map.put("response", "ERROR: Se ha producido un error al guardar el usuario");
		}
		return map;
	}
}
