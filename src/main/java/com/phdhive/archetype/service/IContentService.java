package com.phdhive.archetype.service;

import java.util.Collection;
import java.util.List;

import org.bson.types.ObjectId;

import com.phdhive.archetype.dto.content.Content;
import com.phdhive.archetype.dto.master.DataMaster;


/**
 * 
 * @author Jorge
 *
 */
public interface IContentService {

	/**
	 * Crea un nuevo contenido
	 * @param content
	 * @return
	 * @throws Exception
	 */
	void createContent(Content content);
	
	/**
	 * Actualiza el contenido
	 * @param content
	 * @return
	 */
	Content updateContent(Content content);
	
	/**
	 * Borra el contenido
	 * @param content
	 */
	void deleteContent(Content content);
		
	/**
	 * Borra todos los usuarios de la coleccion
	 */
	void deleteAllContents();
	
	/**
	 * Obtiene un contenido por su numero de identificacion
	 * @param content
	 * @return
	 */
	Content findById(ObjectId id);
	
	/**
	 * Obtiene todos los contenidos
	 * @return
	 */
	Collection<Content> readAll();
	
	/**
	 * Listado paginado
	 * @param start
	 * @param limit
	 * @return
	 */
	List<Content> getContentsPaging(Integer start, Integer limit);
	
	/**
	 * Obtiene todos los contenidos de cierto tipo
	 * @return
	 */
	Collection<Content> findByType(DataMaster type);
	
	/**
	 * 
	 * @return
	 */
	Boolean isEmpty();

	/**
	 * 
	 * @param content1
	 * @param content2
	 * @throws Exception 
	 */
	void relateContents(Content content1,Content content2);
	
    /**
     * Elimina todas las referencias entre dos contenidos relacionados
     * @param content1
     * @param content2
     */
	void unRelateContents(Content content1,Content content2);

	
	/**
	 * 
	 * @param content
	 */
	void publish(Content content);
	
	/**
	 * 
	 * @param content
	 */
	void unPublish(Content content);
}
