package com.phdhive.archetype.service;

import java.util.Collection;
import java.util.Map;

import org.bson.types.ObjectId;

import com.phdhive.archetype.dto.document.PhdDocument;
import com.phdhive.archetype.dto.master.types.TypeDocumentMetaData;

/**
 * 
 * @author Alvaro
 *
 */
public interface IDocumentService {
	
	/**
	 * 
	 * @param document
	 * @return
	 */
	ObjectId saveDocument(PhdDocument document);
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	PhdDocument findDocumentById(ObjectId id);
	
	/**
	 * 
	 * @param metaData
	 * @return
	 */
	Collection<PhdDocument> findDocumentsByMetada(Map<TypeDocumentMetaData, String> metaData);
	
	/**
	 * Transforma un documento en formato HTML
	 * @param document
	 * @return
	 */
	String toHTML(PhdDocument document);
	
	/**
	 * Transforma codigo HTML en un documento
	 * @param html
	 * @return
	 */
	void toDocument (String html, PhdDocument document);
	
	/**
	 * Elimina un documento dado su ID
	 * @param id
	 */
	void deleteDocument (ObjectId id);
}
