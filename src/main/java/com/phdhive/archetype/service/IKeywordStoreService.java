package com.phdhive.archetype.service;

import java.util.List;
import com.phdhive.archetype.dto.word.Keyword;

/**
 * 
 * @author Jorge
 * 
 */
public interface IKeywordStoreService {

	/**
	 * Crea una nueva palabra
	 * 
	 * @param word
	 * @return
	 * @throws Exception
	 */
	Keyword saveKeyword(Keyword word);

	/**
	 * Obtiene el objeto Keyword
	 * 
	 * @param word
	 * @return
	 */
	Keyword readById(Keyword word);
	
	/**
	 * 
	 * @param word
	 * @return
	 */
	Keyword readByKeyword(Keyword word);

	/**
	 * Obtiene todas las palabras
	 * 
	 * @return
	 */
	List<Keyword> readAll();

	/**
	 * Borra la palabra
	 * 
	 * @param word
	 */
	void deleteKeyword(Keyword word);

	/**
	 * 
	 * @return
	 */
	Boolean isEmpty();

	/**
	 * Borra todas las palabras de la coleccion
	 */
	void deleteAllKeywords();

	/**
	 * Actualiza la frequencia de una palabra incrementándola en freq unidades
	 * 
	 * @param word
	 * @param freq
	 */
	void updateFrequency(String word, Integer freq);

}
