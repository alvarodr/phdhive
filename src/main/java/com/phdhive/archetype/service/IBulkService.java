package com.phdhive.archetype.service;

import org.springframework.core.io.Resource;

/**
 * 
 * @author Alvaro
 *
 */
public interface IBulkService {
	
	/**
	 * 
	 * @param bulkUser
	 * @throws Exception
	 */
	void loadUsers(Resource bulkUser) throws Exception;
	
	/**
	 * 
	 * @param resource
	 * @throws Exception
	 */
	void loadDataMaster(Resource resource) throws Exception;
	
	/**
	 *  Recalcula las keywords para todos los usuarios en base a los documentos actuales
	 * @throws Exception
	 */
	public void resetKeyWords() throws Exception;
}
