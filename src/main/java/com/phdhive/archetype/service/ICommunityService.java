package com.phdhive.archetype.service;

import java.util.List;

import org.bson.types.ObjectId;

import com.phdhive.archetype.dto.community.Community;

/**
 * Interfaz para los servicios de las comunidades
 * @author ADORU3N
 *
 */
public interface ICommunityService {

	/**
	 * Recupera todas las comunidades
	 * @return
	 */
	List<Community> getAllCommunities();
	
	/**
	 * Recupera una comunidad dado un ID
	 * @param id
	 * @return
	 */
	Community getCommunityById(ObjectId id);
	
	/**
	 * Almacena una comunidad
	 * @param community
	 */
	void saveCommunity(Community community);
	
	/**
	 * Borra una comunidad
	 * @param community
	 */
	void deleteCommunity(Community community);
	
	/**
	 * Obtiene una lista de comunidades paginadas
	 * @param start
	 * @param limit
	 * @return
	 */
	List<Community> getCommunitiesPaging(Integer start, Integer limit);
	
	/**
	 * Devuelve la comunidad de la que el usuario es administrador
	 * @param email
	 * @return
	 */
	Community getCommunityForAdmin(String email);
}
