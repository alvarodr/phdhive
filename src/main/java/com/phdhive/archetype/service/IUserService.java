package com.phdhive.archetype.service;

import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map.Entry;

import org.bson.types.ObjectId;

import com.phdhive.archetype.dto.document.PhdDocument;
import com.phdhive.archetype.dto.event.Event;
import com.phdhive.archetype.dto.filter.UserFilter;
import com.phdhive.archetype.dto.master.DataMaster;
import com.phdhive.archetype.dto.user.User;

/**
 * 
 * @author Alvaro
 *
 */
public interface IUserService {

	/**
	 * Crea un nuevo usuario
	 * @param user
	 * @return
	 * @throws Exception
	 */
	User createUser(User user);
	
	/**
	 * Obtiene un usuario por su numero de identificacion
	 * @param user
	 * @return
	 */
	User readById(User user);
	
	/**
	 * Obtiene un usuario por su numero de identificacion
	 * @param user
	 * @return
	 */
	User findById(ObjectId id);
	/**
	 * Obtiene todos los usuarios
	 * @return
	 */
	List<User> readAll();
	
	/**
	 * Obtiene una lista de usuarios dado un nombre
	 * @param name
	 * @return
	 */
	List<User> readByCriteriaName(String name);
	
	/**
	 * Actualiza el usuario
	 * @param user
	 * @return
	 */
	User updateUser(User user);
	
	/**
	 * Borra el usuario
	 * @param user
	 */
	void deleteUser(User user);
	
	/**
	 * 
	 * @return
	 */
	Boolean isEmpty();
	
	/**
	 * Borra todos los usuarios de la coleccion
	 */
	void deleteAllUsers();
	
	/**
	 * Obtiene un usuario dado su email
	 * @param email
	 * @return
	 */
	User readByEmail(String email);
	
	/**
	 * Obtiene un usuario por el token
	 * @param token
	 * @return
	 */
	User getUserByToken(String token);
	
	/**
	 * 
	 * @return
	 */
	List<Entry<String, Integer>> getQueryWords();
	
	/**
	 * 
	 * @param doc
	 * @param locale
	 * @return
	 */
	List<Entry<String, Integer>> getQueryWords(PhdDocument doc, Locale locale);
	
	/**
	 * 
	 * @param user
	 * @param queryWords
	 * @param removing  True si se están eliminando un documento
	 * @throws Exception
	 */
	void udpateKeywords(User user, List<Entry<String, Integer>> queryWords, boolean removing) throws Exception;

	/**
	 * No actualiza la frecuencia, sólo elimina la palabra de la lista del usuario
	 * @param keyword
	 * @param user
	 * @return
	 */
	public void deleteKeyword(User user,String keyword);

	
	/**
	 * 
	 * @param start
	 * @param limit
	 * @return
	 */
	List<User> getUsersByPaging(Integer start, Integer limit);
	
	/**
	 * 
	 * @param events
	 * @return
	 */
	Collection<User> getUserByCommunity(Collection<String> events);
	
	/**
	 * Devuelve una lista de usuarios filtrada
	 * @param filter
	 * @return
	 */
	List<User> getUserByFilter(UserFilter filter);
	
	/**
	 * Recupera el pais desde la IP de la última conexión
	 * @param user
	 * @return
	 */
	DataMaster getUserCountry(User user);
	
	/**
	 * Activa un usuario
	 * @param id
	 * @param token
	 * @return
	 */
	void activationUser(ObjectId id, String token);
	
	/**
	 * Borra del usuario la referencia a un documento
	 * @param id
	 */
	void deleteDocumentFromUser(User user, ObjectId docId);
	
	/**
	 * Borra un evento de lista de eventos que tiene cada usuario
	 * @param event
	 */
	public void deleteEventForAllUsers(Event event);
}
