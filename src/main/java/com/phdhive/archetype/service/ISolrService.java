package com.phdhive.archetype.service;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Map.Entry;

import org.apache.solr.client.solrj.SolrServerException;

import com.phdhive.archetype.dto.document.PhdDocument;
import com.phdhive.archetype.dto.event.Event;
import com.phdhive.archetype.dto.user.User;

/**
 * 
 * @author Alvaro
 *
 */
public interface ISolrService {

	/**
	 * 
	 * @param document
	 * @return
	 * @throws Exception
	 */
	List<User> searhUsersSolr(PhdDocument document, Locale locale, Event event);
	
	/**
	 * 
	 * @param documentKeyWordsMap
	 * @return
	 */
	List<User> findUsers(List<Entry<String, Integer>> documentKeyWordsMap,Event event);
	
	/**
	 * 
	 * @param user
	 * @param operation
	 * @throws SolrServerException
	 * @throws IOException
	 */
	void updateUserIndex(User user, int operation);
	
	/**
	 * 
	 * @param user
	 * @return
	 * @throws Exception
	 */
	List<User> searchUsersSolr(User user, Locale locale);
}
