package com.phdhive.archetype.service.exception;
/**
 * 
 * @author Alvaro
 *
 */
@SuppressWarnings("serial")
public class ServiceException extends RuntimeException{
	

	public ServiceException() {
		super();
		
	}

	public ServiceException(String message, Throwable cause) {
		super(message, cause);
		
	}

	public ServiceException(String message) {
		super(message);
		
	}

	public ServiceException(Throwable cause) {
		super(cause);
		
	}
}


