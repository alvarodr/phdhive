package com.phdhive.archetype.service;

import java.util.Collection;
import java.util.Map;

import com.phdhive.archetype.dto.master.DataMaster;
import com.phdhive.archetype.dto.master.ITypeDataMaster;

/**
 * 
 * @author ADORU3N
 *
 */
public interface IDataMasterService {

	/**
	 * Busca los datos maestros de un tipo.
	 * 
	 * @param tipo
	 *            Tipo de dato maestro
	 * @return
	 */
	Collection<DataMaster> findByType(ITypeDataMaster type);

	/**
	 * Devuelve el listado de datos maestros del tipo solicitado en forma de
	 * mapa indexado por los códigos de los datos maestros.
	 * 
	 * @param tipo
	 *            Tipo de dato maestro
	 * @return
	 */
	Map<String, DataMaster> findByTypeAsMap(ITypeDataMaster type);

	/**
	 * Busca todos los datos maestros.
	 * 
	 * @return
	 */
	Collection<DataMaster> findAll();

	/**
	 * Obtiene un dato maestro dado un tipo y un codigo.
	 * 
	 * @param tipo
	 *            Tipo de dato maestro
	 * @param codigo
	 * @return
	 */
	DataMaster getDataMaster(ITypeDataMaster type, String code);

	/**
	 * Obtiene un dato maestro dado un tipo y una descripción
	 * 
	 * @param type
	 * @param description
	 * @return
	 */
	 DataMaster getDataMasterByDesc(ITypeDataMaster type, String description);
	/**
	 * Crea un nuevo dato maestro
	 * 
	 * @param dataMaster
	 */
	void createDataMaster(DataMaster dataMaster);
	
	/**
	 * 
	 * Obtiene un data maestro por el codigo
	 * @param code
	 * @return
	 */
	DataMaster getDataMasterByCode(String code);
	
	/**
	 * Verifica si existe o no un dato maestro
	 * @param type
	 * @param code
	 * @return
	 */
	DataMaster existsDataMaster(Integer type, String code);
}
