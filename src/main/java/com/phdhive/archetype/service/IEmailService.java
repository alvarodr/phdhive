package com.phdhive.archetype.service;

import java.util.Locale;

import com.phdhive.archetype.dto.event.Event;
import com.phdhive.archetype.dto.user.User;

public interface IEmailService {
	
	/**
	 * Envia correo electronico de activación al usuario
	 * @param user
	 * @param locale
	 */
	public void sendEmailActivation(User user, Locale locale);
	
	/**
	 * Envía email con la invitación a un evento
	 * @param user
	 * @param locale
	 */
	void sendEventInvitation(final User user,Event event, final Locale locale);
	
	/**
	 * Envía el resultado de un evento a un determinado usuario
	 * @param user   
	 * @param event
	 * @param userList    Lista de usuarios resultado de la búsqueda
	 * @param foundWords   Lista de palabras usadas en la búsuqeda y su frecuencia
	 */
	void sendEventEndMessage(final User user,final Event event,final Locale locale );
	
	/**
	 * Envíar el contacto a otro usuario
	 * @param userTo
	 * @param userFrom
	 * @param textMessage
	 * @param locale
	 */
	void sendMessageContact(final User userTo, final User userFrom, final String textMessage, final Locale locale);
	
	/**
	 * Manda una nueva password a cierto usuario
	 * @param user
	 * @param locale
	 */
	void sendNewPassword(final User user,final String password, final Locale locale);

}
