package com.phdhive.archetype.service;

import com.phdhive.archetype.dto.master.types.TypeLanguage;

/**
 * 
 * @author ADORU3N
 *
 */
public interface ISnowballService {
	
	/**
	 * 
	 * @param wordList
	 * @param language
	 */
	public void getListStemmer(String[] wordList, TypeLanguage language);
}
