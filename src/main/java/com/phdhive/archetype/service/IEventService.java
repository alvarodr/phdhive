package com.phdhive.archetype.service;

import java.util.Collection;

import org.bson.types.ObjectId;

import com.phdhive.archetype.dto.community.Community;
import com.phdhive.archetype.dto.event.Event;

/**
 * 
 * @author Alvaro
 *
 */
public interface IEventService {

	/**
	 * Devuelve todos los eventos
	 * @return
	 */
	Collection<Event> getAllEvents();
	
	/**
	 * Devuelve un evento dado su id
	 * @param id
	 * @return
	 */
	Event getEventById(ObjectId id);
	
	/**
	 * Guarda en evento
	 * @param event
	 */
	void saveEvent(Event event);
	
	/**
	 * Devuelve una colleccion paginada
	 * @param start
	 * @param limit
	 * @return
	 */
	Collection<Event> getEventPaging(Integer start, Integer limit, Community community);
	
	/**
	 * Elimina un evento
	 * @param event
	 */
	void removeEvent(Event event);
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	Collection<String> getEventsByCommunityId(ObjectId id);
	
	/**
	 * Recupera un evento dado un nombre
	 * @param evenName
	 * @return
	 */
	Event getEventByName(String evenName);
	
	/**
	 * Recupera la lista de eventos de un usuario
	 * @param email
	 * @return
	 */
	Collection<Event> getEventByUser(ObjectId id);
	
	/**
	 * Vincula un usuario con un evento
	 * @param id
	 * @param email
	 */
	Event linkUserToEvent(ObjectId id, String email);
	
	/**
	 *  Lista los eventos activos
	 * @return
	 */
	public Collection<Event> getActiveEvents();
}
