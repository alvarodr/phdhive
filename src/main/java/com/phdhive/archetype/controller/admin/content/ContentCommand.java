package com.phdhive.archetype.controller.admin.content;

import com.phdhive.archetype.dto.content.Content;
import com.phdhive.archetype.dto.master.types.TypeDataMaster;
import com.phdhive.archetype.utils.DataMasterUtils;
import com.phdhive.archetype.utils.PhdhiveUtils;

/**
 * 
 * @author Alvaro
 *
 */
public class ContentCommand {

	private String id;
	private String title;
	private String header;
	private String type;
	private String body;
	private String updateDate;
	private String relatedContents; //ids de los contenidos relacionados separados por ';'
	private boolean published;
	private Integer order;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public String getType() {
		return type;
	}

	public void setType(String contentType) {
		this.type = contentType;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getRelatedContents() {
		return relatedContents;
	}

	public void setRelatedContents(String relatedContents) {
		this.relatedContents = relatedContents;
	}

	public boolean getPublished() {
		return published;
	}

	public void setPublished(boolean published) {
		this.published = published;
	}
	
	public Integer getOrder() {
		return order;
	}

	public void setOrder(Integer order) {
		this.order = order;
	}

	public Content toContent(){
		Content content = new Content();
		content.setBody(body);
		content.setHeader(header);
		content.setTitle(title);		
		content.setType(DataMasterUtils.getDataMaster(TypeDataMaster.TYPES_CONTENT_TYPES, type));
		content.setPublished(published);
		content.setUpdateDate(PhdhiveUtils.toDate(this.updateDate + " " + this.updateDate));
		content.setOrder(order);
		return content;
	}
}
