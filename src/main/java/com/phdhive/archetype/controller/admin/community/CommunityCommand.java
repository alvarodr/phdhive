package com.phdhive.archetype.controller.admin.community;

import java.util.Date;

/**
 * Command de la Comunidad
 * 
 * @author ADORU3N
 * 
 */
public class CommunityCommand {

	private String id;
	private String name;
	private String country;
	private String admin;
	private Date createDate;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getAdmin() {
		return admin;
	}

	public void setAdmin(String admin) {
		this.admin = admin;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
