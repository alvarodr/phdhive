package com.phdhive.archetype.controller.admin.login;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.phdhive.archetype.controller.BaseSenchaController;
import com.phdhive.archetype.dto.community.Community;
import com.phdhive.archetype.dto.user.User;
import com.phdhive.archetype.utils.SecurityUtils;

@Controller
@RequestMapping("/admin")
public class AdministrationDashboardController extends BaseSenchaController {
	
	@RequestMapping(value="/login")
	public String loginAdminDashboard(Model model){
//		if (getUserAuthenticated()!=null)
//			return "redirect:/action/admin/dashboard";
		model.addAttribute("community", new Community(null, null, null));
		return "loginAdminDashboard";
	}
	
	@RequestMapping(value="/dashboard")
	public String adminDashboard(Model model){
		model.addAttribute("community", getCommunityAuthenticated());
		return "adminDashboard";
	}
	
	@RequestMapping(value="/head")
	public String head(Model model){
		model.addAttribute("user", getUserAuthenticated());
		model.addAttribute("community", getCommunityAuthenticated());
		return "headDashboard";
	}
	
	@RequestMapping(value="/foot")
	public String foot(){
		return "footDashboard";
	}
	
	@RequestMapping(value="/privileges", method=RequestMethod.GET)
	public @ResponseBody Map<Object, Object> getPrivileges(HttpSession session){
		User user = getUserAuthenticated();
		Map<Object, Object> response = new HashMap<Object, Object>();			
		
		if (SecurityUtils.isAdmin(user) && user.getCommunity()==null){
			response.put("success", Boolean.TRUE);
			session.setAttribute("admin", "admin");
		} else {
			response.put("success", Boolean.FALSE);
		}
		
		return response;
	}
}
