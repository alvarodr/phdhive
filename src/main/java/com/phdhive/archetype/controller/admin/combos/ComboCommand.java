package com.phdhive.archetype.controller.admin.combos;

/**
 * 
 * @author Alvaro
 * 
 */
public class ComboCommand {

	private String code;
	private String description;

	public ComboCommand(String code, String description){
		this.code = code;
		this.description = description;
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
