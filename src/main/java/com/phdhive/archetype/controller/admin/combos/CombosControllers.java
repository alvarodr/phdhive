package com.phdhive.archetype.controller.admin.combos;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.phdhive.archetype.controller.BaseSenchaController;
import com.phdhive.archetype.dto.community.Community;
import com.phdhive.archetype.dto.event.Event;
import com.phdhive.archetype.dto.master.DataMaster;
import com.phdhive.archetype.dto.master.types.TypeDataMaster;
import com.phdhive.archetype.service.ICommunityService;
import com.phdhive.archetype.service.IDataMasterService;
import com.phdhive.archetype.service.IEventService;

@Controller
@RequestMapping("/admin")
public class CombosControllers extends BaseSenchaController {

	@Autowired
	private IDataMasterService dataMasterService;
	
	@Autowired
	private ICommunityService communityService;
	
	@Autowired
	private IEventService eventService;
	
	@RequestMapping(value="/dataMaster", method=RequestMethod.GET)
	public @ResponseBody Map<Object, Object> getDataMaster(@RequestParam("type") Integer type){
		Collection<DataMaster> datas = dataMasterService.findByType(TypeDataMaster.toTipoDatoMaestro(type));
		return toJSONMaV("items", datas, "totalCount", datas.size());
	}
	
	@RequestMapping(value="/combo/dataMaster", method=RequestMethod.GET)
	public @ResponseBody Map<Object, Object> getComboDataMaster(@RequestParam("type") Integer type){
		Collection<DataMaster> datas = dataMasterService.findByType(TypeDataMaster.toTipoDatoMaestro(type));
		List<ComboCommand> comboDataMaster = new ArrayList<ComboCommand>();
		for (DataMaster dataMaster : datas) {
			comboDataMaster.add(new ComboCommand(dataMaster.getCode(), dataMaster.getDescription()));
		}
		return toJSONMaV("items", comboDataMaster, "totalCount", datas.size());
	}
	
	@RequestMapping(value="/combo/events", method=RequestMethod.GET)
	public @ResponseBody Map<Object, Object> getComboEvents() {
		
		List<ComboCommand> combo = new ArrayList<ComboCommand>();
		Collection<Event> events = null;
		
		if (getCommunityAuthenticated() == null) {
			events = eventService.getAllEvents();
		} else {
			events = new ArrayList<Event>();
			Collection<String> eventsNames = eventService.getEventsByCommunityId(new ObjectId(getCommunityAuthenticated().getId()));
			for (String name : eventsNames) {
				events.add(eventService.getEventByName(name));
			}
		}
		
		for (Event e : events) {
			combo.add(new ComboCommand(e.getId(), e.getName()));
		}
		
		return toJSONMaV("items", combo, "totalCount", events.size());
	}
	
	@RequestMapping(value="/combo/communities", method=RequestMethod.GET)
	public @ResponseBody Map<Object, Object> getCommunities(){
		Collection<Community> communities = communityService.getAllCommunities();
		List<ComboCommand> combo = new ArrayList<ComboCommand>();
		for (Community community : communities){
			combo.add(new ComboCommand(community.getId(), community.getName()));
		}
		return toJSONMaV("items", combo, "totalCount", combo.size());
	}
}
