package com.phdhive.archetype.controller.admin.document;

import java.util.List;

import com.phdhive.archetype.dto.BaseObject;

/**
 * 
 * @author Alvaro
 * 
 */
public class DocumentCommand extends BaseObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5510273319156959783L;

	private String document;

	private String author;

	private Boolean expanded;

	private String email;

	private String id;

	private Boolean leaf;

	private List<DocumentCommand> children;

	public String getDocument() {
		return document;
	}

	public void setDocument(String document) {
		this.document = document;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public Boolean getExpanded() {
		return expanded;
	}

	public void setExpanded(Boolean expanded) {
		this.expanded = expanded;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Boolean getLeaf() {
		return leaf;
	}

	public void setLeaf(Boolean leaf) {
		this.leaf = leaf;
	}

	public List<DocumentCommand> getChildren() {
		return children;
	}

	public void setChildren(List<DocumentCommand> children) {
		this.children = children;
	}

}
