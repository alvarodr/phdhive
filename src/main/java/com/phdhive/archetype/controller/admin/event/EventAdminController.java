package com.phdhive.archetype.controller.admin.event;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Locale;
import java.util.Map;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.phdhive.archetype.controller.BaseSenchaController;
import com.phdhive.archetype.dto.community.Community;
import com.phdhive.archetype.dto.event.Event;
import com.phdhive.archetype.dto.user.User;
import com.phdhive.archetype.service.ICommunityService;
import com.phdhive.archetype.service.IEmailService;
import com.phdhive.archetype.service.IEventService;
import com.phdhive.archetype.service.IUserService;
import com.phdhive.archetype.service.validation.ServiceValidationException;
import com.phdhive.archetype.service.validation.Validations;
import com.phdhive.archetype.utils.PhdhiveUtils;

@Controller
@RequestMapping("/admin")
public class EventAdminController extends BaseSenchaController {
	
	@Autowired
	private IEventService eventService;
	
	@Autowired
	private ICommunityService communityService;
	
	@Autowired
	private IUserService userService;
	
	@Autowired
	private IEmailService emailService;
	
	@RequestMapping(value = "/events", method = RequestMethod.GET)
	public @ResponseBody Map<Object, Object> getAllEvents(
			@RequestParam(required = false) Integer start, 
			@RequestParam(required = false) Integer limit){
		
		Community community = getCommunityAuthenticated();
		
		Collection<Event> events = eventService.getEventPaging(start, limit, community);
		
		return toJSONMaV("items", events, "totalCount", eventService.getAllEvents().size());
	}
	
	@RequestMapping(value="/events/remove", method=RequestMethod.POST)
	public @ResponseBody Map<Object, Object> removeEvent(@RequestParam("id") String id){
		try{
			Event event = eventService.getEventById(new ObjectId(id));
			eventService.removeEvent(event);
			return responseForm(Boolean.TRUE);
		} catch (ServiceValidationException e) {
			return handleServiceValidationException(e);
		}
	}
	
	@RequestMapping(value="/events/save", method=RequestMethod.POST)
	public @ResponseBody Map<Object, Object> saveEvent(@ModelAttribute EventCommand command){
		try{
			Community community = getCommunityAuthenticated();
			Event event = command.toEvent();
			if (community == null){
				community = communityService.getCommunityById(new ObjectId(command.getOwner()));
			}
			
			if (!command.getId().equals("-1")){
				event = eventService.getEventById(new ObjectId(command.getId()));
				event.setDescription(command.getDescription());
				event.setName(command.getName());
				event.setStartDate(PhdhiveUtils.toDate(command.getStartDate() + " " + command.getStartTime()));
				event.setEndDate(PhdhiveUtils.toDate(command.getEndDate() + " " + command.getEndTime()));
			}
			event.setOwner(community);
				
			eventService.saveEvent(event);
			return responseForm(Boolean.TRUE);
		} catch (ServiceValidationException e){
			return handleServiceValidationException(e);
		}
	}
	
	@RequestMapping(value="/events/load", method=RequestMethod.GET)
	public @ResponseBody Map<Object, Object> loadEvent(@RequestParam String id){
		try{
			Event event = eventService.getEventById(new ObjectId(id));
			EventCommand command = new EventCommand();
			command.setId(id);
			command.setDescription(event.getDescription());
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			command.setEndDate(sdf.format(event.getEndDate()));
			command.setStartDate(sdf.format(event.getStartDate()));
			command.setName(event.getName());
			command.setStartTime(event.getStartTime());
			command.setEndTime(event.getEndTime());
			command.setLanguage(event.getLanguage());
			command.setOwner(event.getOwner().getId());
			return toJSONMaV("success", Boolean.TRUE, "data", command);
		} catch (ServiceValidationException e) {
			return handleServiceValidationException(e);
		}
	}
	
	@RequestMapping(value="/events/invite", method=RequestMethod.POST)
	public @ResponseBody Map<Object, Object> inviteUsers(@ModelAttribute EventCommand command){
		try{
			Event event =  eventService.getEventById(new ObjectId(command.getId()));
			if (command.getInvitation()!=null && !command.getInvitation().equalsIgnoreCase("")){
				String[] users = command.getInvitation().split(",");
				for(String email : users){
					if (Validations.isMail(email)){						
						User user = userService.readByEmail(email);
						if (user==null){
							//Invitación a usuarios no registrados
							user = new User();
							user.setEmail(email);
						}
						emailService.sendEventInvitation(user, event, Locale.ENGLISH);
					}
				}
			}
			return responseForm(Boolean.TRUE);
		} catch (ServiceValidationException e) {
			return handleServiceValidationException(e);
		}
	}
}
