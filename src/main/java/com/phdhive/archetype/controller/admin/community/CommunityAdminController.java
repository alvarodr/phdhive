package com.phdhive.archetype.controller.admin.community;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.phdhive.archetype.controller.BaseSenchaController;
import com.phdhive.archetype.dto.community.Community;
import com.phdhive.archetype.dto.master.DataMaster;
import com.phdhive.archetype.dto.master.types.TypeDataMaster;
import com.phdhive.archetype.dto.user.User;
import com.phdhive.archetype.security.Role;
import com.phdhive.archetype.service.ICommunityService;
import com.phdhive.archetype.service.IDataMasterService;
import com.phdhive.archetype.service.IEventService;
import com.phdhive.archetype.service.IUserService;
import com.phdhive.archetype.service.validation.ServiceValidationException;

@Controller
@RequestMapping("/admin")
public class CommunityAdminController extends BaseSenchaController {

	@Autowired
	private ICommunityService communityService;

	@Autowired
	private IEventService eventService;
	
	@Autowired
	private IDataMasterService dataMasterService;

	@Autowired
	private IUserService userService;

	@RequestMapping(value = "/communities", method = RequestMethod.GET)
	public @ResponseBody Map<Object, Object> getCommunities(
			@RequestParam(required = false) Integer start, 
			@RequestParam(required = false) Integer limit) {
		
		Collection<Community> communities = communityService.getCommunitiesPaging(start, limit);
		return toJSONMaV("items", communities, "totalCount", communityService.getAllCommunities().size());
	}

	@RequestMapping(value = "/communities/add", method = RequestMethod.POST)
	public @ResponseBody Map<Object, Object> newCommunity(@ModelAttribute CommunityCommand communityCommand) {
		try {
			User userAdmin = userService.readByEmail(communityCommand.getAdmin());
			HashSet<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
			authorities.add((GrantedAuthority)Role.ROLE_ADMIN);
			userAdmin.setAuthorities(authorities);
			userService.updateUser(userAdmin);
			DataMaster country = dataMasterService.getDataMaster(TypeDataMaster.TYPES_COUNTRIES, communityCommand.getCountry());
			if (communityCommand.getId().equals("-1")){
				communityService.saveCommunity(new Community(communityCommand.getName(), country, userAdmin));
			} else {
				Community community = communityService.getCommunityById(new ObjectId(communityCommand.getId()));
				community.setName(communityCommand.getName());
				community.setAdmin(userAdmin);
				community.setCountry(country);
				communityService.saveCommunity(community);
			}
		} catch (ServiceValidationException e) {
			return handleServiceValidationException(e);
		}
		return responseForm(Boolean.TRUE);
	}
	
	@RequestMapping(value="/communities/edit", method = {RequestMethod.GET, RequestMethod.POST})
	public @ResponseBody Map<Object, Object> editCommunity(@RequestParam(required=false) String id){
		try{
			Community community = communityService.getCommunityById(new ObjectId(id));
			CommunityCommand command = new CommunityCommand();
			command.setCountry(community.getCountry().getCode());
			command.setName(community.getName());
			command.setAdmin(community.getAdmin().getEmail());
			command.setId(community.getId());
			return toJSONMaV("success", Boolean.TRUE, "data", command);
		} catch (ServiceValidationException e){
			return handleServiceValidationException(e);
		}
	}
	
	@RequestMapping(value="/communities/remove", method = RequestMethod.POST)
	public @ResponseBody Map<Object, Object> removeCommunity(@RequestParam String id){
		try{
			Community community = communityService.getCommunityById(new ObjectId(id));
			communityService.deleteCommunity(community);
			return responseForm(Boolean.TRUE);
		} catch (ServiceValidationException e){
			return handleServiceValidationException(e);
		}
	}
}
