package com.phdhive.archetype.controller.admin.event;

import com.phdhive.archetype.dto.event.Event;
import com.phdhive.archetype.utils.PhdhiveUtils;


/**
 * 
 * @author Alvaro
 * 
 */
public class EventCommand {

	private String id;
	private String name;
	private String description;
	private String startDate;
	private String startTime;
	private String endDate;
	private String endTime;
	private String owner;
	private String language;
	private String nameCommunity;
	private Integer suscriber;
	private String invitation;

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getNameCommunity() {
		return nameCommunity;
	}

	public void setNameCommunity(String nameCommunity) {
		this.nameCommunity = nameCommunity;
	}

	public Integer getSuscriber() {
		return suscriber;
	}

	public void setSuscriber(Integer suscriber) {
		this.suscriber = suscriber;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getInvitation() {
		return invitation;
	}

	public void setInvitation(String invitation) {
		this.invitation = invitation;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public Event toEvent(){
		Event event = new Event();
		event.setName(this.name);
		event.setStartDate(PhdhiveUtils.toDate(this.startDate + " " + this.startTime));
		event.setEndDate(PhdhiveUtils.toDate(this.endDate + " " + this.endTime));
		event.setDescription(this.description);
		event.setLanguage(this.language);
		return event;
	}
}
