package com.phdhive.archetype.controller.admin.document;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.phdhive.archetype.controller.BaseSenchaController;
import com.phdhive.archetype.dto.community.Community;
import com.phdhive.archetype.dto.document.PhdDocument;
import com.phdhive.archetype.dto.document.DocumentMetaData;
import com.phdhive.archetype.dto.event.Event;
import com.phdhive.archetype.dto.master.types.TypeDocumentMetaData;
import com.phdhive.archetype.service.ICommunityService;
import com.phdhive.archetype.service.IDocumentService;
import com.phdhive.archetype.service.IEventService;

@Controller
public class DocumentAdminController extends BaseSenchaController {

	@Autowired
	private ICommunityService communityService;
	
	@Autowired
	private IEventService eventService;
	
	@Autowired
	private IDocumentService documentService;
	
	@RequestMapping(value = "/admin/documents", method=RequestMethod.GET)
	public @ResponseBody DocumentCommand getDocuments() {
		DocumentCommand root = new DocumentCommand();
		root.setExpanded(Boolean.TRUE);
		root.setLeaf(false);
		
		if (getCommunityAuthenticated()!=null) {
			// Recuperamos los ids de los eventos de la comunidad
			Collection<String> events = eventService.getEventsByCommunityId(new ObjectId(getCommunityAuthenticated().getId()));
			List<DocumentCommand> evts = new ArrayList<DocumentCommand>();
			for (String name : events) {
				Event ev = eventService.getEventByName(name);
				DocumentCommand evt = new DocumentCommand();
				evt.setDocument(ev.getName());
				evt.setId(ev.getId());
				evt.setLeaf(false);
				Map<TypeDocumentMetaData, String> metadata = new HashMap<TypeDocumentMetaData, String>();
				metadata.put(TypeDocumentMetaData.METADATA_EVENT, ev.getId());
				Collection<PhdDocument>docs = documentService.findDocumentsByMetada(metadata);
				if (!CollectionUtils.isEmpty(docs)) {
					List<DocumentCommand> documents = new ArrayList<DocumentCommand>();
					for (PhdDocument doc : docs) {
						metadata = doc.getMetaData();
						DocumentCommand document = new DocumentCommand();
						document.setAuthor(metadata.get(DocumentMetaData.METADATA_OWNER));
						document.setEmail(metadata.get(DocumentMetaData.METADATA_OWNER));
						document.setLeaf(true);
						document.setDocument(doc.getDocumentName());
						documents.add(document);
					}
					evt.setChildren(documents);	
				} else {
					evt.setChildren(new ArrayList<DocumentCommand>());
				}
				evts.add(evt);
			}
			root.setChildren(evts);
		} else {
			List<DocumentCommand> comms = new ArrayList<DocumentCommand>();
			Collection<Community> communities = communityService.getAllCommunities();
			for (Community community : communities) {
				DocumentCommand comm = new DocumentCommand();
				comm.setDocument(community.getName());
				comm.setId(community.getId().toString());
				comm.setLeaf(false);
				Collection<String> evts = eventService.getEventsByCommunityId(new ObjectId(community.getId()));
				if (!CollectionUtils.isEmpty(evts)) {
					List<DocumentCommand> colEvts = new ArrayList<DocumentCommand>();
					for (String name : evts) {
						Event evt = eventService.getEventByName(name);
						DocumentCommand event = new DocumentCommand();
						event.setDocument(evt.getName());
						event.setId(evt.getId());
						event.setLeaf(false);
						event.setChildren(new ArrayList<DocumentCommand>());
						Map<TypeDocumentMetaData, String> metadata = new HashMap<TypeDocumentMetaData, String>();
						metadata.put(TypeDocumentMetaData.METADATA_EVENT, evt.getId());
						Collection<PhdDocument>docs = documentService.findDocumentsByMetada(metadata);
						if (!CollectionUtils.isEmpty(docs)) {
							List<DocumentCommand> documents = new ArrayList<DocumentCommand>();
							for (PhdDocument doc : docs) {
								metadata = doc.getMetaData();
								DocumentCommand document = new DocumentCommand();
								document.setAuthor(metadata.get(DocumentMetaData.METADATA_OWNER));
								document.setEmail(metadata.get(DocumentMetaData.METADATA_OWNER));
								document.setLeaf(true);
								document.setDocument(doc.getDocumentName());
								documents.add(document);
							}
							event.setChildren(documents);	
						} else {
							event.setChildren(new ArrayList<DocumentCommand>());
						}
						colEvts.add(event);
					}
					comm.setChildren(colEvts);
				} else {
					comm.setChildren(new ArrayList<DocumentCommand>());
				}
				comms.add(comm);
			}
			root.setChildren(comms);
		}
		return root;
	}
}
