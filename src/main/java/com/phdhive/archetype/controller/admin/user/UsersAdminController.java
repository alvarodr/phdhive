package com.phdhive.archetype.controller.admin.user;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.phdhive.archetype.controller.BaseSenchaController;
import com.phdhive.archetype.dto.event.Event;
import com.phdhive.archetype.dto.filter.UserFilter;
import com.phdhive.archetype.dto.master.types.TypeDataMaster;
import com.phdhive.archetype.dto.user.User;
import com.phdhive.archetype.service.IBulkService;
import com.phdhive.archetype.service.IDataMasterService;
import com.phdhive.archetype.service.IEventService;
import com.phdhive.archetype.service.IUserService;
import com.phdhive.archetype.service.validation.ServiceValidationException;
import com.phdhive.archetype.utils.Constants;

@Controller
@RequestMapping("/admin")
public class UsersAdminController extends BaseSenchaController {

	@Autowired
	private IUserService userService;

	@Autowired
	private IEventService eventService;

	
	@Autowired
	private IDataMasterService dataMasterService;
	
	@Autowired
	private IBulkService bulkService;
	
	@RequestMapping(value="/users", method=RequestMethod.GET)
	public @ResponseBody Map<Object, Object> users(@ModelAttribute UserSearchCommand command, HttpServletRequest request){
		Integer start = ServletRequestUtils.getIntParameter(request, "start", -1);
		Integer limit = ServletRequestUtils.getIntParameter(request, "limit", -1);
		UserFilter filter = new UserFilter();
		
		//Obtenemos el area
		if (StringUtils.isNotBlank(command.getArea())) {
			filter.setArea(dataMasterService.getDataMaster(TypeDataMaster.TYPES_KNOWLEDGES_UNSECO, command.getArea()).getId());
		}
		
		//Obtenemos la disciplina
		if (StringUtils.isNotBlank(command.getDiscipline())) {
			filter.setDiscipline(dataMasterService.getDataMaster(TypeDataMaster.toTipoDatoMaestro(Integer.parseInt(command.getArea())), 
				command.getDiscipline()).getId());
		}
		
		//Obtenemos el evento
		if (StringUtils.isNotBlank(command.getEvent())) {
			Event event = eventService.getEventById(new ObjectId(command.getEvent()));
			filter.setEvent(event.getName());
		}
		
		//Obtenemos la comunidad
		if (StringUtils.isNotBlank(command.getCommunity())) {
			filter.setCommunity(new ObjectId(command.getCommunity()));
		}
		
		//Obtenemos la lista filtrada y paginada
		filter.setLimit(limit);
		filter.setStart(start);
		List<User> filteredUsers = userService.getUserByFilter(filter);
		
		//Obtenemos la lista filtrada y sin paginar
		filter.setLimit(-1);
		filter.setStart(-1);
		List<User> totalUsers = userService.getUserByFilter(filter);
		
		return toJSONMaV("users", filteredUsers, "totalCount", totalUsers.size());
	}
	
	@RequestMapping(value="/removeUser", method=RequestMethod.POST)
	public @ResponseBody Map<Object, Object> removeContent(@RequestParam("id") String id){
		try{
		    User user = userService.findById(new ObjectId(id));
			userService.deleteUser(user);
			return responseForm(Boolean.TRUE);
		} catch (ServiceValidationException e) {
			return handleServiceValidationException(e);
		}
	}

	@RequestMapping(value = "/users/excel", method = RequestMethod.GET)
	public String generarExcelByParametro(
			@RequestParam(value="country", required=false) String country,
			@RequestParam(value="area", required=false) String area,
			@RequestParam(value="discipline", required=false) String discipline,
			@RequestParam(value="community", required=false) String community,
			@RequestParam(value="event", required=false) String event,
			Model model, HttpServletResponse response) {
		
		UserFilter filter = new UserFilter();
		filter.setStart(-1);
		filter.setLimit(-1);
				
		if (StringUtils.isNotBlank(country) && !"null".equals(country)) {
			filter.setCountry(dataMasterService.getDataMaster(TypeDataMaster.TYPES_COUNTRIES, country).getId());
		}
		
		//Obtenemos el área
		if (StringUtils.isNotBlank(area) && !"null".equals(area)) {
			filter.setArea(dataMasterService.getDataMaster(TypeDataMaster.TYPES_KNOWLEDGES_UNSECO, area).getId());
		}
		
		//Obtenemos la disciplina
		if (StringUtils.isNotBlank(discipline) && !"null".equals(discipline)) {
			filter.setDiscipline(dataMasterService.getDataMaster(TypeDataMaster.toTipoDatoMaestro(Integer.parseInt(area)), 
					discipline).getId());
		}
		
		//Obtenemos el evento
		if (StringUtils.isNotBlank(event) && !"null".equals(event)) {
			Event event2 = eventService.getEventById(new ObjectId(event));
			filter.setEvent(event2.getName());
		}
		
		//Obtenemos la comunidad
		if (StringUtils.isNotBlank(community) && !"null".equals(community)) {
			filter.setCommunity(new ObjectId(community));
		}
		
	    model.addAttribute("users", userService.getUserByFilter(filter));
 
        response.setHeader("Content-type", "application/vnd.ms-excel");
        String name = "users" + System.currentTimeMillis() + ".xls";
        response.setHeader("Content-Disposition","attachment; filename=" + name);
        return "excelReport";
	}
	
	@RequestMapping(value="users/import", method=RequestMethod.GET)
	public String importUsers() throws Exception {
		bulkService.loadUsers(new ClassPathResource(Constants.USERS_FILE));
		return "redirect:/action/login";
	}
	
	@RequestMapping(value="users/resetKeywords", method=RequestMethod.GET)
	public String resetKeywords() throws Exception {
		bulkService.resetKeyWords();
		return "redirect:/action/login";
	}
	
	@RequestMapping(value="dataMaster/import", method=RequestMethod.GET)
	public String dataMasterUsers() throws Exception {
		bulkService.loadDataMaster(new ClassPathResource(Constants.DATA_MASTER_FILE));
		return "redirect:/action/login";
	}
	
	@RequestMapping(value="/lockUser", method=RequestMethod.POST)
	public @ResponseBody Map<Object, Object> userActivation(@RequestParam("id") String id){
		try{
		    User user = userService.findById(new ObjectId(id));
		    
		    if (user.getEnabled())
		       user.setEnabled(false);
		    else
		    	 user.setEnabled(true);
		    
			userService.updateUser(user);
			return responseForm(Boolean.TRUE);
		} catch (ServiceValidationException e) {
			return handleServiceValidationException(e);
		}
	}
}
