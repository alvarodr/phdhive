package com.phdhive.archetype.controller.admin.content;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.phdhive.archetype.controller.BaseSenchaController;
import com.phdhive.archetype.dto.content.Content;
import com.phdhive.archetype.dto.content.RelatedContent;
import com.phdhive.archetype.dto.master.DataMaster;
import com.phdhive.archetype.dto.master.types.TypeDataMaster;
import com.phdhive.archetype.service.IContentService;
import com.phdhive.archetype.service.IDataMasterService;
import com.phdhive.archetype.service.validation.ServiceValidationException;

@Controller
@RequestMapping("/admin")
public class ContentAdminController extends BaseSenchaController {

	@Autowired
	private IContentService contentService;
	
	@Autowired
	private IDataMasterService dataMasterService;	
	
	
	@RequestMapping(value = "/contents/list", method = RequestMethod.GET)
	public @ResponseBody Map<Object, Object> getContents(
			@RequestParam(required = false) Integer start, 
			@RequestParam(required = false) Integer limit) {
		
		Collection<Content> contents = contentService.getContentsPaging(start, limit);
	
		return toJSONMaV("items", contents, "totalCount", contentService.readAll().size());

	}
	
	
	@RequestMapping(value="/contents/save", method=RequestMethod.POST)
	public @ResponseBody Map<Object, Object> saveContent(@ModelAttribute ContentCommand command){
		try{
			Content content = null;
			DataMaster type = dataMasterService.getDataMaster(TypeDataMaster.TYPES_CONTENT_TYPES, command.getType());
			if (NEW_ENTITY.equals(command.getId())){
				content = command.toContent();
				//TODO apaño temporal porque toContent no extrae bien el tipo de contenido
				if (content.getTypeAsObject()==null)
					content.setType(type);
			}else {
				content = contentService.findById(new ObjectId(command.getId()));
				content.setBody(command.getBody());
				content.setTitle(command.getTitle());
				content.setHeader(command.getHeader());
				content.setType(type);
				content.setPublished(command.getPublished());
				content.setOrder(command.getOrder());
				content.setUpdateDate(new Date());				
			}			
			
			contentService.createContent(content);
			
			//La relación de contenidos sólo se puede efectuar cuando se ha creado
			if (!command.getRelatedContents().equalsIgnoreCase(content.getRelatedContents()))
			     updateRelatedContents(content,command);
			
			return responseForm(Boolean.TRUE);
		} catch (ServiceValidationException e) {
			return handleServiceValidationException(e);
		}
	}
	
	private void  updateRelatedContents(Content content,ContentCommand command){
		
		//Elimina las relaciones actuales
		if (!content.getRelatedContents().equals("")){
			String[] currentRelIds = content.getRelatedContents().split(";");
			for(int i=0;i<currentRelIds.length;i++){			
				String id = currentRelIds[i];
				Content contentAux = contentService.findById(new ObjectId(id));
				contentService.unRelateContents(content, contentAux);
			}
		}
		//Añade las nuevas
		if(!command.getRelatedContents().equalsIgnoreCase("")){
			String[] newRelIds = command.getRelatedContents().split(";");
			for(int i=0;i<newRelIds.length;i++){			
				String id = newRelIds[i];
				Content contentAux = contentService.findById(new ObjectId(id));
				contentService.relateContents(content, contentAux);		
			}
	    }
	}
	
	@RequestMapping(value="/contents/remove", method=RequestMethod.POST)
	public @ResponseBody Map<Object, Object> removeContent(@RequestParam("id") String id){
		try{
			Content content = contentService.findById(new ObjectId(id));
			contentService.deleteContent(content);
			return responseForm(Boolean.TRUE);
		} catch (ServiceValidationException e) {
			return handleServiceValidationException(e);
		}
	}

	
	@RequestMapping(value="/contents/load", method=RequestMethod.GET)
	public @ResponseBody Map<Object, Object> loadContent(@RequestParam String id){
		try{
			Content content = contentService.findById(new ObjectId(id));
			ContentCommand command = new ContentCommand();
			command.setId(id);
			command.setTitle(content.getTitle());
			command.setBody(content.getBody());
			command.setOrder(content.getOrder());
			command.setHeader(content.getHeader());
			DataMaster type = dataMasterService.getDataMasterByDesc(TypeDataMaster.TYPES_CONTENT_TYPES, content.getType());
			command.setType(type.getCode());
			command.setRelatedContents(content.getRelatedContents());
			command.setPublished(content.isPublished());
			return toJSONMaV("success", Boolean.TRUE, "data", command);
		
		} catch (ServiceValidationException e) {
			return handleServiceValidationException(e);
		}
	}

	@RequestMapping(value="/contents/relatedContents", method=RequestMethod.GET)
	public @ResponseBody Map<Object, Object> getRelatedContents(@RequestParam("relatedIds") String relatedIds){

		String[] newRelIds = relatedIds.split(";");
		List<RelatedContent> relatedContentList = new ArrayList<RelatedContent>();
		if (!relatedIds.equalsIgnoreCase("")){
			for(String id:newRelIds){
				Content contentAux = contentService.findById(new ObjectId(id));
				RelatedContent relCon = new RelatedContent();
				relCon.setRelContentRef(new ObjectId( contentAux.getId()) );
				relCon.setRelContentName(contentAux.getTitle());
				relatedContentList.add(relCon);
			}
	    }
		
		return toJSONMaV("items", relatedContentList, "totalCount", relatedContentList.size());

	}

}
