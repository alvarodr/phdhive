package com.phdhive.archetype.controller;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.activation.MimetypesFileTypeMap;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.LocaleResolver;

import com.phdhive.archetype.dto.community.Community;
import com.phdhive.archetype.dto.master.types.TypeContentDisposition;
import com.phdhive.archetype.dto.user.User;
import com.phdhive.archetype.security.CustomAuthentication;
import com.phdhive.archetype.service.validation.ServiceFailTranslator;
import com.phdhive.archetype.service.validation.ServiceValidationException;

public class BaseSenchaController {

	public static final String NEW_ENTITY = "-1";
	
	private MimetypesFileTypeMap mimeMap = null;

	private Logger log = LoggerFactory.getLogger(BaseSenchaController.class);

	@Autowired
	protected MessageSource messageSource;

	@Autowired
	protected LocaleResolver localeResolver;

	public void initialize() {
		try {
			this.mimeMap = new MimetypesFileTypeMap(new ClassPathResource("/WEB-INF/mime.types").getInputStream());
		} catch (IOException e) {
			this.log.error("No se cargaron los MimeTypes: " + e.getMessage());
		}
	}
	
	protected User getUserAuthenticated(){
		if (SecurityContextHolder.getContext().getAuthentication() != null){
			return ((CustomAuthentication) SecurityContextHolder.getContext().getAuthentication()).getUser();
		} else {
			return new User();
		}
	}

	protected Community getCommunityAuthenticated(){
		return getUserAuthenticated().getCommunity();
	}
	
	protected Map<Object, Object> toJSONMaV(String arg1, Object items, String arg2, Object totalCount){
		Map<Object, Object> result = new HashMap<Object, Object>();
		
		result.put(arg1, items);
		result.put(arg2, totalCount);
		
		return result;
	}
	
	protected Map<Object, Object> responseForm(Boolean success){
		Map<Object, Object> response = new HashMap<Object, Object>();
		
		response.put("success", success);
		
		return response;
	}
	
	protected String getContentType(File file) {
		return mimeMap.getContentType(file);
	}

	protected Map<Object, Object> handleServiceValidationException(ServiceFailTranslator tr, Locale locale, Exception e) {
		Map<Object, Object> errors = new HashMap<Object, Object>();
		errors.put("success", Boolean.FALSE);
		if (e instanceof ServiceValidationException) {
			errors.put("msg", tr.getMessage(((ServiceValidationException) e).getSingleFail(), (ServiceValidationException) e));
		} else {
			errors.put("msg", messageSource.getMessage("error.inesperado", new Object[]{}, locale));
		}
		return errors;
	}

	protected Map<Object, Object> handleServiceValidationException(Exception e) {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();

		Locale locale = localeResolver.resolveLocale(request);
		
		ServiceFailTranslator tr = new ServiceFailTranslator(messageSource,	locale);

		return handleServiceValidationException(tr, locale, e);
	}

	protected void downloadReport(HttpServletResponse response,
			HttpServletRequest request, byte[] data, String contentType,
			String reportName, TypeContentDisposition tipoContentDisposition)
			throws IOException {
		ServletOutputStream sos = null;
		InputStream is = null;
		if (tipoContentDisposition == null) {
			tipoContentDisposition = TypeContentDisposition.INLINE;
		}
		try {
			is = new ByteArrayInputStream(data);

			if (is != null) {
				response.setHeader("Content-Disposition",
						tipoContentDisposition.getDescripcion() + ";filename="
								+ reportName);

				if (contentType != null) {
					response.setContentType(contentType);
				}

				Long size = (long) data.length;
				if (size != null) {
					response.setContentLength(size.intValue());
				}

				sos = response.getOutputStream();
				IOUtils.copy(is, sos);

				sos.flush();
			}
		} finally {
			try {
				if (is != null) {
					is.close();
				}
			} catch (Exception ex) {
			}

			try {
				if (sos != null) {
					sos.close();
				}
			} catch (Exception ex) {
			}
		}
	}

	protected String getAbsoluteRedirect(HttpServletRequest request, String url) {
		return "redirect:" + request.getScheme() + "://"
				+ request.getServerName() + url;
	}
}
