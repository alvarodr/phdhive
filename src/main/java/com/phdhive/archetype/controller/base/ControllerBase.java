package com.phdhive.archetype.controller.base;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.LocaleResolver;

import com.phdhive.archetype.dto.master.types.TypeContentDisposition;
import com.phdhive.archetype.dto.master.types.TypeMessage;
import com.phdhive.archetype.dto.user.User;
import com.phdhive.archetype.security.CustomAuthentication;
import com.phdhive.archetype.service.validation.ServiceFailTranslator;
import com.phdhive.archetype.service.validation.ServiceValidationException;

/**
 * 
 * @author ADORU3N
 *
 */
public class ControllerBase {

	@Autowired
	protected MessageSource messageSource;

	@Autowired
	protected LocaleResolver localeResolver;
	
	protected final String LABEL_OK = "OK";
	
	protected User getUserAuthenticated() {
		if (SecurityContextHolder.getContext().getAuthentication() != null){
			return ((CustomAuthentication) SecurityContextHolder.getContext().getAuthentication()).getUser();
		} else {
			return new User();
		}
	}
	
	protected String handleServiceValidationException(ServiceFailTranslator tr, Model model, Locale locale, Exception e) {
		MessageCommand message = new MessageCommand();
		message.setLinkButton("/action/index");
		message.setTextButton(LABEL_OK);
		message.setTypeMessage(TypeMessage.MESSAGE_ERROR);
		if (e instanceof ServiceValidationException) {
			message.setText(tr.getMessage(((ServiceValidationException) e).getSingleFail(), (ServiceValidationException) e));
		} else {
			message.setText(messageSource.getMessage("error.inesperado", new Object[]{}, locale));
		}
		model.addAttribute("message", message);
		return "message";
	}

	protected String handleServiceValidationException(Exception e, Model model) {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();

		Locale locale = localeResolver.resolveLocale(request);
		
		ServiceFailTranslator tr = new ServiceFailTranslator(messageSource,	locale);

		return handleServiceValidationException(tr, model, locale, e);
	}
	
	protected void downloadReport(HttpServletResponse response,
			HttpServletRequest request, byte[] data, String contentType,
			String reportName, TypeContentDisposition tipoContentDisposition)
			throws IOException {
		ServletOutputStream sos = null;
		InputStream is = null;
		if (tipoContentDisposition == null) {
			tipoContentDisposition = TypeContentDisposition.INLINE;
		}
		try {
			is = new ByteArrayInputStream(data);

			if (is != null) {
				response.setHeader("Content-Disposition",
						tipoContentDisposition.getDescripcion() + ";filename="
								+ reportName);

				if (contentType != null) {
					response.setContentType(contentType);
				}

				Long size = (long) data.length;
				if (size != null) {
					response.setContentLength(size.intValue());
				}

				sos = response.getOutputStream();
				IOUtils.copy(is, sos);

				sos.flush();
			}
		} finally {
			try {
				if (is != null) {
					is.close();
				}
			} catch (Exception ex) {
			}

			try {
				if (sos != null) {
					sos.close();
				}
			} catch (Exception ex) {
			}
		}
	}
}
