package com.phdhive.archetype.controller.base;

import com.phdhive.archetype.dto.master.types.TypeMessage;

/**
 * 
 * @author Alvaro
 * 
 */
public class MessageCommand {

	private String text;
	private String textButton;
	private String linkButton;
	private TypeMessage typeMessage;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getTextButton() {
		return textButton;
	}

	public void setTextButton(String textButton) {
		this.textButton = textButton;
	}

	public String getLinkButton() {
		return linkButton;
	}

	public void setLinkButton(String linkButton) {
		this.linkButton = linkButton;
	}

	public TypeMessage getTypeMessage() {
		return typeMessage;
	}

	public void setTypeMessage(TypeMessage typeMessage) {
		this.typeMessage = typeMessage;
	}

}
