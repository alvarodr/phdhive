package com.phdhive.archetype.controller.open.document.upload;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.LocaleResolver;

import com.phdhive.archetype.controller.base.ControllerBase;
import com.phdhive.archetype.dto.document.DocumentMultipart;
import com.phdhive.archetype.dto.document.PhdDocument;
import com.phdhive.archetype.dto.master.types.TypeDocumentMetaData;
import com.phdhive.archetype.dto.user.User;
import com.phdhive.archetype.service.IDocumentService;
import com.phdhive.archetype.service.ISolrService;
import com.phdhive.archetype.service.IUserService;
import com.phdhive.archetype.utils.Constants;

/**
 * 
 * @author Alvaro
 *
 */
@Controller
public class UploadDocumentController extends ControllerBase {

	@Autowired
	private ISolrService solrService;
	
	@Autowired
	private LocaleResolver localeResolver;
	
	@Autowired
	private IUserService userService;
	
	@Autowired
	private IDocumentService documentService;
	
	@RequestMapping(value="upload", method=RequestMethod.POST)
	public @ResponseBody String upload (@ModelAttribute DocumentMultipart documento, Model model, HttpServletResponse response, HttpServletRequest request){
		
		try{
			User user = userService.readByEmail(getUserAuthenticated().getEmail());
			
			// Obtenemos los usuarios afines
			solrService.searhUsersSolr(documento.toDocument(), localeResolver.resolveLocale(request), null);
			
			List<Entry<String, Integer>> foundWords = userService.getQueryWords();
			
			//Actualiza las palabras clave del usuario actual con las encontradas
			userService.udpateKeywords (user, foundWords, false);
			
			Map<TypeDocumentMetaData, String> metaData = new HashMap<TypeDocumentMetaData, String>();
			metaData.put(TypeDocumentMetaData.METADATA_OWNER, getUserAuthenticated().getEmail());
			SimpleDateFormat sdf = new SimpleDateFormat(Constants.FORMATO_FECHA_HORA );
			metaData.put(TypeDocumentMetaData.METADATA_DATE, sdf.format(new Date()));
			
			PhdDocument phdDocument = documento.toDocument();
			phdDocument.setMetaData(metaData);
			
			ObjectId docId = documentService.saveDocument(phdDocument);			
			
			if (user.getDocuments() == null) {
				user.setDocuments(new ArrayList<ObjectId>());
			}
			user.getDocuments().add(docId);
			userService.updateUser(user);

			return docId.toString();
		} catch (Exception e) {
			return handleServiceValidationException(e, model);
		}
	}
	
}
