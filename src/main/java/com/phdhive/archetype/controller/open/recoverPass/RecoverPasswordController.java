package com.phdhive.archetype.controller.open.recoverPass;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.token.Sha512DigestUtils;
import org.springframework.security.crypto.keygen.KeyGenerators;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.LocaleResolver;

import com.phdhive.archetype.controller.base.ControllerBase;
import com.phdhive.archetype.controller.base.MessageCommand;
import com.phdhive.archetype.dto.master.types.TypeMessage;
import com.phdhive.archetype.dto.recoveryPass.RecoveryPassCommand;
import com.phdhive.archetype.dto.user.User;
import com.phdhive.archetype.service.IEmailService;
import com.phdhive.archetype.service.IUserService;

/**
 * 
 * @author Alvaro
 * 
 */
@Controller
public class RecoverPasswordController extends ControllerBase {

	@Autowired
	private IEmailService emailService;

	@Autowired
	private LocaleResolver localeResolver;

	@Autowired
	private IUserService userService;

	@Autowired
	private MessageSource messageSource;

	@RequestMapping(value = "recoveryPass", method = RequestMethod.GET)
	public String recover(@ModelAttribute("recoveryPassCommand") RecoveryPassCommand recoveryPassCommand, Model model) {
		
		model.addAttribute("recoveryPassCommand", recoveryPassCommand);
		return "recoveryPass";
		
	}

			
	@RequestMapping(value = "recoveryPass", method = RequestMethod.POST)
	public String getNewPassword(@ModelAttribute("recoveryPassCommand") RecoveryPassCommand recoveryPassCommand, Model model, HttpServletRequest request) {
		Locale locale = localeResolver.resolveLocale(request);
		MessageCommand message = new MessageCommand();
		try {
			User user = userService.readByEmail(recoveryPassCommand.getEmail());
			if ( user != null) {
				
				// generates a random 8-byte salt that is then hex-encoded
				String newPass = KeyGenerators.string().generateKey(); 
				user.setPassword(Sha512DigestUtils.shaHex(newPass));
				userService.updateUser(user);
				
				emailService.sendNewPassword(user,newPass, locale);
				message.setText(messageSource.getMessage("recoveryPass.success.submit.text", null, locale));
				message.setTypeMessage(TypeMessage.MESSAGE_INFO);

			}else{
				
				message.setText(messageSource.getMessage("recoveryPass.fail.submit.text",null, locale));
				message.setTypeMessage(TypeMessage.MESSAGE_WARNING);
				message.setLinkButton("/action/login");
				message.setTextButton(LABEL_OK);
				model.addAttribute("message", message);
				return "message";
			}
				
			

		} catch (Exception e) {
			e.printStackTrace();
			return handleServiceValidationException(e, model);
		}
		message.setTextButton(LABEL_OK);
		message.setLinkButton("/action/login");
		model.addAttribute("message", message);
		return "message";
	}

}
