package com.phdhive.archetype.controller.open.events;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.LocaleResolver;

import com.phdhive.archetype.controller.base.ControllerBase;
import com.phdhive.archetype.controller.base.MessageCommand;
import com.phdhive.archetype.dto.document.DocumentMultipart;
import com.phdhive.archetype.dto.document.PhdDocument;
import com.phdhive.archetype.dto.event.Event;
import com.phdhive.archetype.dto.master.types.TypeDocumentMetaData;
import com.phdhive.archetype.dto.master.types.TypeMessage;
import com.phdhive.archetype.dto.user.User;
import com.phdhive.archetype.security.CustomAuthentication;
import com.phdhive.archetype.service.IDocumentService;
import com.phdhive.archetype.service.IEventService;
import com.phdhive.archetype.service.ISolrService;
import com.phdhive.archetype.service.IUserService;

/**
 * 
 * @author Alvaro
 *
 */
@Controller
public class EventsController extends ControllerBase {

	@Autowired
	private IEventService eventService;
	
	@Autowired
	private IDocumentService documentService;
	
	@Autowired
	private ISolrService solrService;
	
	@Autowired
	private IUserService userService;
	
	@Autowired
	private LocaleResolver localeResolver;
	
	@Autowired
	private MessageSource messageSource;
	
	@RequestMapping(value="events", method= {RequestMethod.GET})
	public String listEventSubmitted(Model model, HttpSession session) {
		try {
			User user = getUserAuthenticated();
			Collection<Event> events = eventService.getEventByUser(new ObjectId(user.getId()));
			model.addAttribute("events", events);
		} catch (Exception e) {
			return handleServiceValidationException(e, model);
		}
		return "events";
	}
	
	@RequestMapping(value = "events/{id}", method = {RequestMethod.GET})
	public String submitToEvent(@PathVariable("id") String id, Model model, HttpSession session, HttpServletRequest request) {
		MessageCommand message = new MessageCommand();
		try {
			User user = getUserAuthenticated();
			Event event = eventService.linkUserToEvent(new ObjectId(id), user.getEmail());
			message.setLinkButton("/action/events");
			message.setTextButton(LABEL_OK);
			message.setTypeMessage(TypeMessage.MESSAGE_INFO);
			message.setText(messageSource.getMessage("event.link.successful", new Object[]{event.getName()}, localeResolver.resolveLocale(request)));
			model.addAttribute("message", message);
		} catch (Exception e) {
			return handleServiceValidationException(e, model);
		}
		return "message";
	}
	
	@RequestMapping(value = "events/{id}/documents", method = {RequestMethod.GET})
	public String listDocumentsForEvent(@PathVariable("id") String id, Model model) {
		try {
			User user = getUserAuthenticated();
			Map<TypeDocumentMetaData, String> metaData = new HashMap<TypeDocumentMetaData, String>();
			metaData.put(TypeDocumentMetaData.METADATA_OWNER, user.getEmail());
			metaData.put(TypeDocumentMetaData.METADATA_EVENT, id);
			Collection<PhdDocument> documents = documentService.findDocumentsByMetada(metaData);
			model.addAttribute("documents", documents);
		} catch (Exception e) {
			return handleServiceValidationException(e, model);
		}
		return "documents";
	}
	
	@RequestMapping(value = "events/{id}/documents/upload", method = {RequestMethod.GET})
	public String uploadDocument(@PathVariable("id") String id, Model model, HttpServletRequest request) {
		try {
			Event event = eventService.getEventById(new ObjectId(id));
			
			User user = getUserAuthenticated();
			Map<TypeDocumentMetaData, String> metaData = new HashMap<TypeDocumentMetaData, String>();
			metaData.put(TypeDocumentMetaData.METADATA_OWNER, user.getEmail());
			metaData.put(TypeDocumentMetaData.METADATA_EVENT, event.getId());
			Collection<PhdDocument> documents = documentService.findDocumentsByMetada(metaData);
			
			model.addAttribute("event", event);
			model.addAttribute("documents", documents);
		} catch (Exception e) {
			return handleServiceValidationException(e, model);
		}
		return "uploadDocEvents";
	}
	
	@RequestMapping(value = "events/{id_evt}/documents/{id_doc}", method=RequestMethod.GET)
	public String resultDocumentEvent(@PathVariable("id_evt") String idEvt,
			@PathVariable("id_doc") String idDoc, Model model, HttpServletRequest request) {
		
		try {
			Event evt = eventService.getEventById(new ObjectId(idEvt));
			PhdDocument document = documentService.findDocumentById(new ObjectId(idDoc));
			Collection<User> users = solrService.searhUsersSolr(document, localeResolver.resolveLocale(request), evt);
			List<Entry<String, Integer>> foundWords = userService.getQueryWords();
			
			// Eliminamos de la lista al usuario logado
			CollectionUtils.filter(users, new Predicate() {
				@Override
				public boolean evaluate(Object object) {
					User user = (User) object;
					return !user.getEmail().equals(getUserAuthenticated().getEmail());
				}
			});
			
			model.addAttribute("users", users);
			model.addAttribute("foundWords", foundWords);
			model.addAttribute("nameDocument", document.getDocumentName());
		} catch (Exception e) {
			return handleServiceValidationException(e, model);
		}
		
		return "resultUpload";
	}
	
	@RequestMapping(value = "events/{id}/documents/upload", method = {RequestMethod.POST})
	public String uploadDocumentForEvent(@PathVariable("id")String id, @ModelAttribute DocumentMultipart document,
			Model model, HttpServletRequest request) {
		MessageCommand message = new MessageCommand();
		try {
			solrService.searhUsersSolr(document.toDocument(), localeResolver.resolveLocale(request),null);

			List<Entry<String, Integer>> foundWords = userService.getQueryWords();
			
			//Actualiza las palabras clave del usuario actual con las encontradas
			CustomAuthentication ca = (CustomAuthentication) SecurityContextHolder.getContext().getAuthentication();
			User user = userService.readByEmail(ca.getUser().getEmail());
			userService.udpateKeywords (user, foundWords,false);
			
			Map<TypeDocumentMetaData, String> metaData = new HashMap<TypeDocumentMetaData, String>();
			metaData.put(TypeDocumentMetaData.METADATA_OWNER, ca.getUser().getEmail());
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			metaData.put(TypeDocumentMetaData.METADATA_DATE, sdf.format(new Date()));
			metaData.put(TypeDocumentMetaData.METADATA_EVENT, id);
			
			PhdDocument phdDocument = document.toDocument();
			phdDocument.setMetaData(metaData);
			
			documentService.saveDocument(phdDocument);
			
			message.setLinkButton("/action/events");
			message.setTextButton(LABEL_OK);
			message.setText(messageSource.getMessage("event.upload.document", new Object[]{}, localeResolver.resolveLocale(request)));
			message.setTypeMessage(TypeMessage.MESSAGE_INFO);
			model.addAttribute("message", message);
		} catch (Exception e) {
			return handleServiceValidationException(e, model);
		}
		return "message";
	}
}
