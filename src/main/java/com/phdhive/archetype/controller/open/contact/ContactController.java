package com.phdhive.archetype.controller.open.contact;

import javax.servlet.http.HttpServletRequest;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.LocaleResolver;

import com.phdhive.archetype.controller.base.ControllerBase;
import com.phdhive.archetype.controller.base.MessageCommand;
import com.phdhive.archetype.dto.master.types.TypeMessage;
import com.phdhive.archetype.dto.user.User;
import com.phdhive.archetype.service.IEmailService;
import com.phdhive.archetype.service.IUserService;

/**
 * 
 * @author Alvaro
 *
 */
@Controller
public class ContactController extends ControllerBase {

	@Autowired
	private IUserService userService;
	
	@Autowired
	private IEmailService emailService;
	
	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private LocaleResolver localeResolver;
	
	@RequestMapping(value = "contact/{id}", method = RequestMethod.GET)
	public String contactToUser(@PathVariable("id") String id, @ModelAttribute("contactCommand") ContactCommand target,
			Model model) {
		try {
			target = new ContactCommand();
			target.setUserTo(id);
			User userto = userService.findById(new ObjectId(id));
			model.addAttribute("userto", userto);
			model.addAttribute("contactCommand", target);
		} catch (Exception e) {
			return handleServiceValidationException(e, model);
		}
		return "contact";
	}
	
	@RequestMapping(value = "contact", method = RequestMethod.POST)
	public String contact(@ModelAttribute("contactCommand") ContactCommand target, Model model,
			HttpServletRequest request) {
		MessageCommand message = new MessageCommand();
		try {
			User userTo = userService.findById(new ObjectId(target.getUserTo()));
			User userFrom = getUserAuthenticated();
			String textMessage = target.getMessage() + messageSource.getMessage("contact.user.from", 
					new Object[] {userFrom.getName()}, localeResolver.resolveLocale(request));
			emailService.sendMessageContact(userTo, userFrom, textMessage, localeResolver.resolveLocale(request));
			message.setLinkButton("/action/index");
			message.setTextButton(LABEL_OK);
			message.setText(messageSource.getMessage("contact.successful", new Object[]{}, localeResolver.resolveLocale(request)));
			message.setTypeMessage(TypeMessage.MESSAGE_INFO);
		} catch (Exception e) {
			return handleServiceValidationException(e, model);
		}
		model.addAttribute("message", message);
		return "message";
	}
}
