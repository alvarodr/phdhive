package com.phdhive.archetype.controller.open.register;

import java.util.HashSet;
import java.util.Locale;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.token.Sha512DigestUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.LocaleResolver;

import com.phdhive.archetype.controller.base.ControllerBase;
import com.phdhive.archetype.controller.base.MessageCommand;
import com.phdhive.archetype.dto.master.types.TypeMessage;
import com.phdhive.archetype.dto.user.User;
import com.phdhive.archetype.security.Role;
import com.phdhive.archetype.service.IEmailService;
import com.phdhive.archetype.service.IUserService;

/**
 * 
 * @author Alvaro
 * 
 */
@Controller
public class RegisterPublicController extends ControllerBase {

	@Autowired
	private IEmailService emailService;

	@Autowired
	private LocaleResolver localeResolver;

	@Autowired
	private IUserService userService;

	@Autowired
	private MessageSource messageSource;

	@RequestMapping(value = "register", method = RequestMethod.GET)
	public String registro(@ModelAttribute("user") User user, Model model) {
		return "registro";
	}

	@RequestMapping(value = "register", method = RequestMethod.POST)
	public String newuser(@ModelAttribute User user, Model model, HttpServletRequest request) {
		Locale locale = localeResolver.resolveLocale(request);
		MessageCommand message = new MessageCommand();
		try {
			if (userService.readByEmail(user.getEmail()) != null) {
				message.setText(messageSource.getMessage("register.already.exist", new Object[]{user.getEmail()}, locale));
				message.setTypeMessage(TypeMessage.MESSAGE_WARNING);
				message.setLinkButton("/action/login");
				message.setTextButton(LABEL_OK);
				model.addAttribute("message", message);
				return "message";
			}
				
			user.setPassword(Sha512DigestUtils.shaHex(user.getPassword()));
			HashSet<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
			authorities.add((GrantedAuthority) Role.ROLE_USER);
			user.setAuthorities(authorities);
			user.setEnabled(Boolean.FALSE);
			user.setIdActivation(UUID.randomUUID().toString());
			user.setLoginCount(0);
			userService.createUser(user);
			user = userService.readByEmail(user.getEmail());
			emailService.sendEmailActivation(user, locale);
			message.setText(messageSource.getMessage("register.success.submit.text", new Object[] { user.getEmail() }, locale));
			message.setTypeMessage(TypeMessage.MESSAGE_INFO);
		} catch (Exception e) {
			e.printStackTrace();
			return handleServiceValidationException(e, model);
		}
		message.setTextButton(LABEL_OK);
		message.setLinkButton("/action/login");
		model.addAttribute("message", message);
		return "message";
	}

}
