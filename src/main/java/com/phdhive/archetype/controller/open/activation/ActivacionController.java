package com.phdhive.archetype.controller.open.activation;

import javax.servlet.http.HttpServletRequest;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.LocaleResolver;

import com.phdhive.archetype.controller.base.ControllerBase;
import com.phdhive.archetype.controller.base.MessageCommand;
import com.phdhive.archetype.dto.master.types.TypeMessage;
import com.phdhive.archetype.service.IUserService;

/**
 * 
 * @author Alvaro
 *
 */
@Controller
public class ActivacionController extends ControllerBase {

	@Autowired
	private IUserService userService;
	
	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private LocaleResolver localeResolver;
	
	@RequestMapping(value = "activation/{id}/{token}", method = RequestMethod.GET)
	public String activeUser (@PathVariable("id") String id, @PathVariable("token") String token,
			HttpServletRequest request, Model model) {
		MessageCommand message = new MessageCommand();
		try {
			userService.activationUser(new ObjectId(id), token);
			message.setTextButton(LABEL_OK);
			message.setLinkButton("/action/index");
			message.setTypeMessage(TypeMessage.MESSAGE_INFO);
			message.setText(messageSource.getMessage("activation.successful", new Object[]{}, localeResolver.resolveLocale(request)));
		} catch (Exception e) {
			return handleServiceValidationException(e, model);
		}
		model.addAttribute("message", message);
		return "message";
	}
}
