package com.phdhive.archetype.controller.open.document;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.phdhive.archetype.controller.base.ControllerBase;
import com.phdhive.archetype.dto.document.PhdDocument;
import com.phdhive.archetype.dto.master.types.TypeDocumentMetaData;
import com.phdhive.archetype.dto.user.User;
import com.phdhive.archetype.service.IDocumentService;
import com.phdhive.archetype.service.ISolrService;
import com.phdhive.archetype.service.IUserService;
import com.phdhive.archetype.utils.ComparatorUtils;

/**
 * Controladora de los documentos
 * 
 * @author ADORU3N
 *
 */
@Controller
public class DocumentsController extends ControllerBase {

	@Autowired
	private IDocumentService documentService;
	
	@Autowired
	private ISolrService solrService;
	
	@Autowired
	private IUserService userService;
	
	@RequestMapping(value="documents", method={RequestMethod.GET})
	public String listDocuments(Model model) {
		Map<TypeDocumentMetaData, String> metaDataFilter = new HashMap<TypeDocumentMetaData, String>();
		try {
			metaDataFilter.put(TypeDocumentMetaData.METADATA_OWNER, getUserAuthenticated().getEmail());
			
			List<PhdDocument> documents = new ArrayList<PhdDocument>(documentService.findDocumentsByMetada(metaDataFilter));
			
			Collections.sort(documents, ComparatorUtils.DOCUMENT_BY_DATE_HOUR);
			
			model.addAttribute("documents", documents);
			return "documents";
		} catch (Exception e) {
			return handleServiceValidationException(e, model);
		}
	}
	
	@RequestMapping(value="documents/delete", method={RequestMethod.POST})
	public @ResponseBody Boolean deleteDocument(@RequestParam("id") String id, Model model) {
		try {
			//Esto es necesario porque el usuario autenticado no se actualiza con los cambios de la sesión actual
			//Si no hacemos esto, un usuario no puede borrar un documento que haya subdo en esta misma sesión
			User user = userService.readByEmail(getUserAuthenticated().getEmail());
			
			userService.deleteDocumentFromUser(user, new ObjectId(id));
			documentService.deleteDocument(new ObjectId(id));
			return Boolean.TRUE;
		} catch (Exception e) {
			return Boolean.FALSE;
		}
	}
	
	@RequestMapping(value = "documents/{id}", method = {RequestMethod.GET})
	public String resultDocument(@PathVariable("id") String id, Model model, HttpServletRequest request) {
		try {
			// Buscamos el documento
			PhdDocument document = documentService.findDocumentById(new ObjectId(id));
			
			// Obtenemos los usuarios afines
			List<User> users = solrService.searhUsersSolr(document, localeResolver.resolveLocale(request), null);
			
			// Eliminamos de la lista al usuario logado
			CollectionUtils.filter(users, new Predicate() {
				@Override
				public boolean evaluate(Object object) {
					User user = (User) object;
					return !user.getEmail().equals(getUserAuthenticated().getEmail());
				}
			});
			
			// Buscamos palabras clave del documento
			List<Entry<String, Integer>> foundWords = userService.getQueryWords();
			
			// Rellenamos el model
			model.addAttribute("users", users);
			model.addAttribute("foundWords", foundWords);
			model.addAttribute("nameDocument", document.getDocumentName());
		} catch (Exception e) {
			return handleServiceValidationException(e, model);
		}
		return "resultUpload";
	}
}
