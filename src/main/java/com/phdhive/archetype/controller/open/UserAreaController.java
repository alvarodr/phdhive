package com.phdhive.archetype.controller.open;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.LocaleResolver;

import com.phdhive.archetype.controller.base.ControllerBase;
import com.phdhive.archetype.controller.base.MessageCommand;
import com.phdhive.archetype.dto.master.types.TypeMessage;

@Controller
public class UserAreaController extends ControllerBase {
	
	@Autowired
	private LocaleResolver localeResolver;
	
	@Autowired
	private MessageSource messageSource;
	
	@RequestMapping(value="index", method=RequestMethod.GET)
	public String index(){
		return "home";
	}
	
	@RequestMapping(value="login", method=RequestMethod.GET)
	public String loginPage(){
		return "login";
	}
	
	@RequestMapping(value = "/login_error/{error}", method = RequestMethod.GET)	 
	public String displayLoginError(@PathVariable(value = "error") String error, Model model, HttpServletRequest request) {
		MessageCommand message = new MessageCommand();
		message.setLinkButton("/action/login");
		message.setTextButton(LABEL_OK);
		message.setTypeMessage(TypeMessage.MESSAGE_ERROR);
		if (error.equalsIgnoreCase("user_notfound"))
			message.setText(messageSource.getMessage("error.user.not.found", new Object[]{}, localeResolver.resolveLocale(request)));
		else if (error.equalsIgnoreCase("external_user"))
			message.setText(messageSource.getMessage("error.external.user", new Object[]{}, localeResolver.resolveLocale(request)));
		else if (error.equalsIgnoreCase("bad_password"))
			message.setText(messageSource.getMessage("error.bad.password", new Object[]{}, localeResolver.resolveLocale(request)));
		else if (error.equalsIgnoreCase("accountDisabled"))
			message.setText(messageSource.getMessage("error.inactive.user", new Object[]{}, localeResolver.resolveLocale(request)));
		else 
			return "index";
		
		model.addAttribute("message", message);
		return "message";
	}
	
}
