package com.phdhive.archetype.controller.open.profile;

import com.phdhive.archetype.dto.user.User;

public class UserProfileCommand {

	private String name;
	private String surname;
	private String email;
	private String biography;
	private String institutionAffiliation;
	private String disciplines;
	private String descriptionDisciplines;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getBiography() {
		return biography;
	}

	public void setBiography(String biography) {
		this.biography = biography;
	}

	public String getInstitutionAffiliation() {
		return institutionAffiliation;
	}

	public void setInstitutionAffiliation(String institutionAffiliation) {
		this.institutionAffiliation = institutionAffiliation;
	}

	public String getDisciplines() {
		return disciplines;
	}

	public void setDisciplines(String disciplines) {
		this.disciplines = disciplines;
	}

	public String getDescriptionDisciplines() {
		return descriptionDisciplines;
	}

	public void setDescriptionDisciplines(String descriptionDisciplines) {
		this.descriptionDisciplines = descriptionDisciplines;
	}

	public User toUser() {
		User user = new User();
		user.setEmail(email);
		user.setName(name);
		user.setSurname(surname);
		user.setBiograph(biography);
		user.setInstitutionAffiliation(institutionAffiliation);
		return user;
	}

}
