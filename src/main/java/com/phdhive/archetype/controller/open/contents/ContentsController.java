package com.phdhive.archetype.controller.open.contents;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.phdhive.archetype.controller.base.ControllerBase;
import com.phdhive.archetype.dto.content.Content;
import com.phdhive.archetype.dto.master.DataMaster;
import com.phdhive.archetype.service.IContentService;
import com.phdhive.archetype.service.IDataMasterService;
import com.phdhive.archetype.utils.ComparatorUtils;

@Controller
public class ContentsController extends ControllerBase {

	@Autowired
	private IContentService contentService;
	
	@Autowired
	private IDataMasterService dataMasterService;
	
	@RequestMapping(value = "contents/{code}", method = RequestMethod.GET)
	public String getRelatedContents(@PathVariable(value = "code") String code, Model model) {
		try {
			DataMaster dm = dataMasterService.getDataMasterByCode(code);
			Collection<Content> contents = contentService.findByType(dm);
			CollectionUtils.filter(contents, new Predicate() {	
				@Override
				public boolean evaluate(Object object) {
					return ((Content) object).isPublished();
				}
			});
			
			List<Content> contentsList = new ArrayList<Content>(contents);
			
			Collections.sort(contentsList, ComparatorUtils.CONTENT_BY_ORDER);
			model.addAttribute("typeContent", dm.getDescription());
			model.addAttribute("contents", contentsList);
		} catch (Exception e) {
			return handleServiceValidationException(e, model);
		}
		
		return "contents";
	}
	
}
