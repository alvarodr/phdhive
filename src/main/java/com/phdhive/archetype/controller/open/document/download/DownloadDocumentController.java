package com.phdhive.archetype.controller.open.document.download;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.phdhive.archetype.controller.base.ControllerBase;
import com.phdhive.archetype.dto.document.PhdDocument;
import com.phdhive.archetype.dto.master.types.TypeContentDisposition;
import com.phdhive.archetype.service.IDocumentService;

@Controller
public class DownloadDocumentController extends ControllerBase {

	@Autowired
	private IDocumentService documentService;
	
	@RequestMapping(value="document/download/{id}", method=RequestMethod.GET)
	public void downloadDocument(@PathVariable("id")String id, Model model, 
			HttpServletRequest request, HttpServletResponse response){
		try {
			PhdDocument document = documentService.findDocumentById(new ObjectId(id));
			downloadReport(response, request, IOUtils.toByteArray(document.getFile()), document.getContentType(), 
					document.getDocumentName(), TypeContentDisposition.ATTACHMENT);
		} catch (Exception e) {
			handleServiceValidationException(e, model);
		}
	}
	
}
