package com.phdhive.archetype.controller.open.contact;

/**
 * 
 * @author Alvaro
 *
 */
public class ContactCommand {

	private String userTo;
	private String subject;
	private String message;

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getUserTo() {
		return userTo;
	}

	public void setUserTo(String userTo) {
		this.userTo = userTo;
	}

}
