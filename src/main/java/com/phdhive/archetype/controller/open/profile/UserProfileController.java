package com.phdhive.archetype.controller.open.profile;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.LocaleResolver;

import com.phdhive.archetype.controller.base.ControllerBase;
import com.phdhive.archetype.controller.base.MessageCommand;
import com.phdhive.archetype.dto.master.DataMaster;
import com.phdhive.archetype.dto.master.types.TypeDataMaster;
import com.phdhive.archetype.dto.master.types.TypeMessage;
import com.phdhive.archetype.dto.user.User;
import com.phdhive.archetype.service.IDataMasterService;
import com.phdhive.archetype.service.IUserService;

@Controller
public class UserProfileController extends ControllerBase {

	@Autowired
	private IUserService userService;
	
	@Autowired
	private IDataMasterService dataMasterService;
	
	@Autowired
	private LocaleResolver localeResolver;
	
	@Autowired
	private MessageSource messageSource;
	
	@RequestMapping(value = "profile", method = RequestMethod.GET)
	public String initProfile(@ModelAttribute("userCommand")UserProfileCommand userCommand, Model model, HttpServletRequest request) {
		try{
			User user = getUserAuthenticated();
			initProfileCommand(user, userCommand);
			model.addAttribute("userCommand", userCommand);
			model.addAttribute("edit", Boolean.FALSE);
			return "profile";
		} catch (Exception e) {
			MessageCommand message = new MessageCommand();
			message.setLinkButton("/action/index");
			message.setTextButton(LABEL_OK);
			message.setText(messageSource.getMessage("profile.fail.load", new Object[]{}, localeResolver.resolveLocale(request)));
			model.addAttribute("message", message);
			return "message";
		}
	}
	
	@RequestMapping(value = "profile", method = RequestMethod.POST)
	public String saveProfile(@ModelAttribute("userCommand") UserProfileCommand userCommand, Model model,
			HttpServletRequest request) {
		MessageCommand message = new MessageCommand();
		User user = getUserAuthenticated();
		try {
			user.setName(userCommand.getName());
			user.setSurname(userCommand.getSurname());
			user.setBiograph(userCommand.getBiography());
			user.setInstitutionAffiliation(userCommand.getInstitutionAffiliation());
			getDisciplinesArea(user, userCommand);
			userService.updateUser(user);
			message.setLinkButton("/action/profile");
			message.setText(messageSource.getMessage("profile.success.save", new Object[]{}, localeResolver.resolveLocale(request)));
			message.setTypeMessage(TypeMessage.MESSAGE_INFO);
		} catch (Exception e) {
//			initProfileCommand(user, userCommand);
			message.setLinkButton("/action/profile");
			message.setText(messageSource.getMessage("profile.failure.save", new Object[]{}, localeResolver.resolveLocale(request)));
			message.setTypeMessage(TypeMessage.MESSAGE_ERROR);
		}
		message.setTextButton(LABEL_OK);
		model.addAttribute("message", message);
		return "message";
	}
	
	@RequestMapping
	public String editProfile() {
		return "message";
	}
	
	protected void getDisciplinesArea(User user, UserProfileCommand target) {
		user.setAreas(new ArrayList<DataMaster>());
		user.setDisciplines(new ArrayList<DataMaster>());
		if (StringUtils.isNotBlank(target.getDisciplines())) {
			String[] disciplines = target.getDisciplines().split(",");
			for (String discipline : disciplines) {
				String area = discipline.substring(0, 2);
				DataMaster dmArea = dataMasterService.getDataMasterByCode(area);
				user.getAreas().add(dmArea);

				DataMaster dmDiscipline = dataMasterService.
						getDataMaster(TypeDataMaster.toTipoDatoMaestro(Integer.parseInt(area)), discipline);
				user.getDisciplines().add(dmDiscipline);
			}
		}
	}
	
	protected void initProfileCommand(User user, UserProfileCommand userCommand) {
		userCommand.setEmail(user.getEmail());
		userCommand.setName(user.getName());
		userCommand.setSurname(user.getSurname());
		userCommand.setBiography(user.getBiograph());
		userCommand.setInstitutionAffiliation(user.getInstitutionAffiliation());
		StringBuffer sb = new StringBuffer();
		if (user.getDisciplines() != null) {
			for (DataMaster dm : user.getDisciplines()) {
				if (dm != null){
					sb.append(dm.getCode());
					sb.append(",");
				}
			}
			userCommand.setDisciplines(sb.toString());
		}
	}
}
