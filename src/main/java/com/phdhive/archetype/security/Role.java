package com.phdhive.archetype.security;

import java.io.Serializable;

import org.springframework.security.core.GrantedAuthority;

public enum Role implements GrantedAuthority, Serializable{
	
	ROLE_ADMIN (0),
    ROLE_USER (1);
    
    @SuppressWarnings("unused")
        private int bit;
 
    Role(int bit) {
        this.bit = bit;
    }
 
    public String getAuthority() {
        return toString();
    }
    
}
