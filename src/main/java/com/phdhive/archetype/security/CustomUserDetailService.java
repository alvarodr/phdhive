package com.phdhive.archetype.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.phdhive.archetype.dto.document.PhdDocument;
import com.phdhive.archetype.dto.master.types.TypeDocumentMetaData;
import com.phdhive.archetype.service.IDocumentService;
import com.phdhive.archetype.service.IEventService;
import com.phdhive.archetype.service.IUserService;

@Service
public class CustomUserDetailService implements UserDetailsService{

	@Autowired
	private IDocumentService documentService;
	
	@Autowired
	private IUserService userService;
	
	@Autowired
	private IEventService eventService;

	@Override
	public UserDetails loadUserByUsername(String userEmail) throws UsernameNotFoundException {
		//Recuperamos los documentos del usuario
		Map<TypeDocumentMetaData, String> metaData = new HashMap<TypeDocumentMetaData, String>();
		metaData.put(TypeDocumentMetaData.METADATA_OWNER, userEmail);
		Collection<String> documents = new ArrayList<String>();
		for (PhdDocument doc : documentService.findDocumentsByMetada(metaData)) {
			documents.add(doc.getDocumentName());
		}
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		
		return new CustomUserDetail((String)auth.getPrincipal(), (String)auth.getCredentials(),
				auth.getAuthorities(), documents);
	}

}
