package com.phdhive.archetype.security;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;

public class CustomUserDetail extends org.springframework.security.core.userdetails.User {

	public CustomUserDetail(String username, String password,
			Collection<? extends GrantedAuthority> authorities,
			Collection<String> documents) {
		super(username, password, authorities);
		this.documents = documents;
	}

	private static final long serialVersionUID = -5905340866811251106L;
	
	private Collection<String> documents;

	public Collection<String> getDocuments() {
		return documents;
	}
	
	public void setDocuments(Collection<String> documents) {
		this.documents = documents;
	}
	
}
