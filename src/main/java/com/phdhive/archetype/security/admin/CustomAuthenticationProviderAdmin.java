package com.phdhive.archetype.security.admin;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.token.Sha512DigestUtils;

import com.phdhive.archetype.dto.user.User;
import com.phdhive.archetype.security.CustomAuthentication;
import com.phdhive.archetype.security.Role;
import com.phdhive.archetype.service.ICommunityService;
import com.phdhive.archetype.service.IUserService;
import com.phdhive.archetype.service.validation.ServiceValidationException;
import com.phdhive.archetype.service.validation.fails.BusinessLogicFail;
import com.phdhive.archetype.service.validation.fails.BusinessLogicFailCode;
import com.phdhive.archetype.service.validation.fails.ServiceValidationFail;

public class CustomAuthenticationProviderAdmin implements AuthenticationProvider {

	private Logger log = LoggerFactory.getLogger(CustomAuthenticationProviderAdmin.class);
	
	@Autowired
	private IUserService userService;
	
	@Autowired
	private ICommunityService communityService;
	
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		List<ServiceValidationFail> fails = new ArrayList<ServiceValidationFail>();
		
		String passwordHash = Sha512DigestUtils.shaHex((String)authentication.getCredentials());
		String email = (String)authentication.getPrincipal();
		
		User user = new User();
		user.setEmail(email);
	
		user = userService.readByEmail(email);
		
		if (user == null){
			log.debug("USER NOT FOUND MESSAGE");
			fails.add(new BusinessLogicFail(BusinessLogicFailCode.USER_NOT_FOUND));
			throw new ServiceValidationException(this.getClass().getName(), fails);
		} else if (!user.getPassword().equalsIgnoreCase(passwordHash)) {
			log.debug("PASSWORD NOT CORRECT");
			fails.add(new BusinessLogicFail(BusinessLogicFailCode.USER_PASSWORD_INCORRECT));
			throw new ServiceValidationException(this.getClass().getName(), fails);
		} else if (user.getEnabled()==null || !user.getEnabled()) {
			log.debug("USER INACTIVE");
			fails.add(new BusinessLogicFail(BusinessLogicFailCode.USER_INACTIVE));
			throw new ServiceValidationException(this.getClass().getName(), fails);
		} else if (!user.getAuthorities().contains(Role.ROLE_ADMIN)) {
			log.debug("USER NOT ADMIN");
			fails.add(new BusinessLogicFail(BusinessLogicFailCode.USER_NOT_ADMIN));
			throw new ServiceValidationException(this.getClass().getName(), fails);
		}
		
		user.setCommunity(communityService.getCommunityForAdmin(user.getEmail()));
		
		Authentication auth = new CustomAuthentication(user, authentication.getDetails());
		auth.setAuthenticated(true);
		return auth;
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}
}