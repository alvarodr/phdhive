package com.phdhive.archetype.security.admin;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import com.phdhive.archetype.service.validation.ServiceFailTranslator;
import com.phdhive.archetype.service.validation.ServiceValidationException;

public class CustomAuthenticationFailureAdmin implements AuthenticationFailureHandler {

	@Autowired
	private MessageSource messageSource;
	
	@Override
	public void onAuthenticationFailure(HttpServletRequest request,	HttpServletResponse response,
			AuthenticationException authentication) throws IOException, ServletException {
		response.setContentType("application/json");

		ServiceValidationException serviceValidationException = (ServiceValidationException) authentication;
		ServiceFailTranslator serviceFailTranslator = new ServiceFailTranslator(messageSource, request.getLocale());
		
		Map<String, Object> modelMap = new HashMap<String, Object>();
		Map<String, String> data = new HashMap<String, String>();

		data.put("success", String.valueOf(false));
		data.put("msg", serviceFailTranslator.getMessage(serviceValidationException.getSingleFail(), serviceValidationException));
		
		modelMap.put("data", data);
		
		OutputStream out = response.getOutputStream();
		ObjectMapper objectMapper = new ObjectMapper();

		objectMapper.writeValue(out, modelMap);
	}
}
