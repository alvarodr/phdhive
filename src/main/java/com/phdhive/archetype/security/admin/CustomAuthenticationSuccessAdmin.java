package com.phdhive.archetype.security.admin;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import com.phdhive.archetype.security.CustomAuthentication;
import com.phdhive.archetype.service.ICommunityService;


public class CustomAuthenticationSuccessAdmin implements AuthenticationSuccessHandler {

	@Autowired
	private ICommunityService communityService;
	
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request,
			HttpServletResponse response, Authentication authentication) throws IOException,
			ServletException {
		response.setContentType("application/json");

		Map<String, Object> modelMap = new HashMap<String, Object>();
		Map<String, String> data = new HashMap<String, String>();

		data.put("success", String.valueOf(true));

		modelMap.put("data", data);

		OutputStream out = response.getOutputStream();
		ObjectMapper objectMapper = new ObjectMapper();

		CustomAuthentication auth = (CustomAuthentication) authentication;

		auth.getUser().setCommunity(communityService.getCommunityForAdmin(auth.getUser().getEmail()));
		
		request.getSession().setAttribute("user", auth.getUser());

		objectMapper.writeValue(out, modelMap);
	}

	
}
