package com.phdhive.archetype.security;

import java.util.Collection;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import com.phdhive.archetype.dto.user.User;

/**
 * Detail data authentication.
 * 
 * @author ADORU3N
 *
 */
public class CustomAuthentication implements Authentication{

	private static final long serialVersionUID = 1L;
	
	private User user;
	private CustomUserDetail userDetail;
	private Object details;
	private Boolean authenticated;
	
	public CustomAuthentication(User user, Object userDetails){
		this.user = user;
		this.details = userDetails;
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.security.Principal#getName()
	 */
	@Override
	public String getName() {
		return user.getName();
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.springframework.security.core.Authentication#getAuthorities()
	 */
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return user.getAuthorities();
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.springframework.security.core.Authentication#getCredentials()
	 */
	@Override
	public Object getCredentials() {
		throw new UnsupportedOperationException();
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.springframework.security.core.Authentication#getDetails()
	 */
	@Override
	public Object getDetails() {
		return details;
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.springframework.security.core.Authentication#getPrincipal()
	 */
	@Override
	public Object getPrincipal() {
		return user.getEmail();
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.springframework.security.core.Authentication#isAuthenticated()
	 */
	@Override
	public boolean isAuthenticated() {
		return this.authenticated;
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.springframework.security.core.Authentication#setAuthenticated(boolean)
	 */
	@Override
	public void setAuthenticated(boolean flag) throws IllegalArgumentException {
		this.authenticated = flag;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Boolean getAuthenticated() {
		return authenticated;
	}

	public void setAuthenticated(Boolean authenticated) {
		this.authenticated = authenticated;
	}

	public CustomUserDetail getUserDetail() {
		return userDetail;
	}

	public void setUserDetail(CustomUserDetail userDetail) {
		this.userDetail = userDetail;
	}
	
}
