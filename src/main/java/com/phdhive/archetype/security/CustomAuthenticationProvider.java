package com.phdhive.archetype.security;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.token.Sha512DigestUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.phdhive.archetype.dto.user.User;
import com.phdhive.archetype.service.IDocumentService;
import com.phdhive.archetype.service.IUserService;

/**
 * Authentication provider for login PhD Hive.
 * 
 * @author ADORU3N
 *
 */
public class CustomAuthenticationProvider implements AuthenticationProvider {

	public static final String USERNOTFOUNDMESSAGE = "Usuario o password incorrectos";
	public static final String PASSWORDNOTCORRECT = "Usuario o password incorrectos";
	public static final String USERINACTIVE = "Usuario inactivo";
	public static final String EXTERNALUSER = "Usuario externo";
	
	private Logger log = LoggerFactory.getLogger(CustomAuthenticationProvider.class);
	
	@Autowired
	private IUserService userService;
	
	@Autowired
	private IDocumentService documentService;
	
	/*
	 * (non-Javadoc)
	 * @see org.springframework.security.authentication.AuthenticationProvider#authenticate(org.springframework.security.core.Authentication)
	 */
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		
		String passwordHash = Sha512DigestUtils.shaHex((String)authentication.getCredentials());
		String email = (String)authentication.getPrincipal();
		
		User user = new User();
		user.setEmail(email);
		
		log.debug("USER --> " + email);
		log.debug(passwordHash);
		user = userService.readByEmail(email);
		
		if(user == null){
			log.debug("USERNOTFOUNDMESSAGE");
			throw new AuthenticationCredentialsNotFoundException(USERNOTFOUNDMESSAGE);
		} else if(user.getPassword()==null) {
			log.debug("EXTERNAL USER: "+user.getName() +" IS TRYING TO CONNECT WITH INTERNAL LOGIN.");			
			throw new BadCredentialsException(EXTERNALUSER);
		} else if(!user.getPassword().equalsIgnoreCase(passwordHash)) {
			log.debug("PASSWORDNOTCORRECT");
			throw new InsufficientAuthenticationException(PASSWORDNOTCORRECT);
		} else if(!user.getEnabled()) {
			log.debug("USERINACTIVE");
			throw new DisabledException(USERINACTIVE);
		}
		
		log.debug("USER --> " + user.getId());
		Authentication auth = new CustomAuthentication(user, authentication.getDetails());
		auth.setAuthenticated(true);
		log.debug("USER AUTHENTICADO");
		
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		String ip = request.getHeader("X-Forwarded-For");
		if (StringUtils.isNotBlank(ip))
			user.setLastIP(ip);
		
		if (user.getLoginCount()!=null) {
			user.setLoginCount(user.getLoginCount()+1);
		} else {
			user.setLoginCount(1);
		}
		
		try {
			userService.updateUser(user);
		} catch (Exception e) {
			log.error("Error updating IP of the user "+ user.getName() );			
		}
		return auth;
	}

	/*
	 * (non-Javadoc)
	 * @see org.springframework.security.authentication.AuthenticationProvider#supports(java.lang.Class)
	 */
	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}
	
}
