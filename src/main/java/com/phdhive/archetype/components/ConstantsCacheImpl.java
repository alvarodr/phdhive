package com.phdhive.archetype.components;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.lang.Validate;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import com.phdhive.archetype.dto.master.types.TypeDataMaster;
import com.phdhive.archetype.service.exception.ServiceException;
import com.phdhive.archetype.utils.Constants;

/**
 * Cache de constantes para jsps.
 * 
 * Almacena las constantes que se pueden exportar desde las JSP, extraidas de las clases.
 * 
 * @author emilio
 *
 */
@Component("constantsCache")
public class ConstantsCacheImpl implements IConstantsCache, InitializingBean {

	private Map<String, Map<String, Object>> constantsByClassName = new TreeMap<String, Map<String, Object>>();

	@Override
	public Map<String, Object> constantsFrom(Class<?> clazz) {
		Validate.notNull(clazz);

		String name = clazz.getName();
		Map<String, Object> map;
		synchronized (constantsByClassName) {
			map = constantsByClassName.get(name);
		}
		if (map == null) {
			map = extractMap(clazz);
			if (map != null) {
				synchronized (constantsByClassName) {
					constantsByClassName.put(name, map);
				}
			}

		}

		return map;
	}

	@Override
	public Map<String, Object> constantsFrom(String className) {

		try {
			Class<?> clazz = Class.forName(className);

			return constantsFrom(clazz);
		} catch (ClassNotFoundException ex) {
			throw new ServiceException("No puedo obtener clase para " + className, ex);
		}
	}

	private Map<String, Object> extractMap(Class<?> clazz) {
		if (clazz.isEnum()) {
			return extractMapFromEnum(clazz);
		}
		return extractMapFromClass(clazz);
	}

	private Map<String, Object> extractMapFromClass(Class<?> clazz) {
		Map<String, Object> values = new TreeMap<String, Object>();
		Field[] fields = clazz.getDeclaredFields();
		for (Field field : fields) {
			int modifier = field.getModifiers();
			if (Modifier.isPublic(modifier) && Modifier.isStatic(modifier) && Modifier.isFinal(modifier)) {
				try {
					values.put(field.getName(), field.get(null));
				} catch (IllegalAccessException e) {
				}
			}
		}
		return values;
	}

	@SuppressWarnings("rawtypes")
	private Map<String, Object> extractMapFromEnum(Class<?> clazz) {
		Map<String, Object> values = new TreeMap<String, Object>();

		try {
			Method m = clazz.getDeclaredMethod("values");

			Enum[] enums = (Enum[]) m.invoke(null);

			for (Enum value : enums) {
				values.put(value.name(), value);
			}

			return values;
		} catch (Exception ex) {
			throw new ServiceException("Imposible obtener constantes de enumeracion " + clazz.getName(), ex);
		}
	}

	private void prepopulate() {
		constantsFrom(Constants.class);
		constantsFrom(TypeDataMaster.class);	
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		prepopulate();

	}
}
