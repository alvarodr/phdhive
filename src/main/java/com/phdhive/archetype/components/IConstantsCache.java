package com.phdhive.archetype.components;

import java.util.Map;

/**
 * Cache de constantes de aplicacion para JSPs.
 * 
 * @author emilio
 *
 */
public interface IConstantsCache {

	/**
	 * Obtiene un mapa de constantes a partir de un nombre de clase o
	 * enumeracion.
	 * 
	 * @param className
	 * @return
	 */
	Map<String, Object> constantsFrom(String className);

	/**
	 * Obtiene un mapa de constantes a partir de una clase o enumeracion.
	 * 
	 * @param clazz
	 * @return
	 */
	Map<String, Object> constantsFrom(Class<?> clazz);

}
