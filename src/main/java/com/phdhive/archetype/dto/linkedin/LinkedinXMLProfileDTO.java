package com.phdhive.archetype.dto.linkedin;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;;

/** 
<person>
	<id>J0KTKyOGc8</id>
	<first-name>Jorge</first-name>
	<last-name>Garrido</last-name>
	<industry>Internet</industry>
	<headline>Desarrollador Java</headline>
</person>
*/

@Root(name="person")
public class LinkedinXMLProfileDTO {
	
	@Element
	private String id;
		   
	@Element
	private String emailAddress;
	
	@Element
	private String firstName;
	   
	@Element
	private String lastName;
	   
	@Element(required=false)
	private String industry;
	
	@Element(required=false)
	private String headline;
	
	@Element(required=false)
	private String summary;
		   
	public LinkedinXMLProfileDTO() {
		super();
	}  
	
	public LinkedinXMLProfileDTO(String id,String industry,String headline) {
		this.id = id;
		this.industry = industry;
		this.headline = headline;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public String getHeadline() {
		return headline;
	}

	public void setHeadline(String headline) {
		this.headline = headline;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}
}

