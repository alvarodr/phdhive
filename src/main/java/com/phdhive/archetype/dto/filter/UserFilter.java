package com.phdhive.archetype.dto.filter;

import org.bson.types.ObjectId;

import com.phdhive.archetype.dto.BaseObject;

/**
 * Filtro de usuarios
 * @author ADORU3N
 *
 */
public class UserFilter extends BaseObject {

	private static final long serialVersionUID = -221617678739432990L;

	private ObjectId country;
	private ObjectId area;
	private ObjectId discipline;
	private ObjectId community;
	private String event;
	private Integer start = -1;
	private Integer limit = -1;

	public ObjectId getCountry() {
		return country;
	}

	public void setCountry(ObjectId country) {
		this.country = country;
	}

	public ObjectId getArea() {
		return area;
	}

	public void setArea(ObjectId area) {
		this.area = area;
	}

	public ObjectId getDiscipline() {
		return discipline;
	}

	public void setDiscipline(ObjectId discipline) {
		this.discipline = discipline;
	}

	public ObjectId getCommunity() {
		return community;
	}

	public void setCommunity(ObjectId community) {
		this.community = community;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public Integer getStart() {
		return start;
	}

	public void setStart(Integer start) {
		this.start = start;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

}
