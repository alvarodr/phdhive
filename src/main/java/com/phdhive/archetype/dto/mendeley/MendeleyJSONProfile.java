package com.phdhive.archetype.dto.mendeley;


public class MendeleyJSONProfile {

	private MainMendeley main;
	
	private CurriculumVitaeMendeley cv;
	
	private ContactMendeley contact;

	public MainMendeley getMain() {
		return main;
	}

	public void setMain(MainMendeley main) {
		this.main = main;
	}

	public CurriculumVitaeMendeley getCv() {
		return cv;
	}

	public void setCv(CurriculumVitaeMendeley cv) {
		this.cv = cv;
	}

	public ContactMendeley getContact() {
		return contact;
	}

	public void setContact(ContactMendeley contact) {
		this.contact = contact;
	}
}
