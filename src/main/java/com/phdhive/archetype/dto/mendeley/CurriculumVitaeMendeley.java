package com.phdhive.archetype.dto.mendeley;

public class CurriculumVitaeMendeley {
	
	private String[] consulting;
	
	private EducationMendeley[] education;
	
	private EmploymentMendeley[] employment;

	public String[] getConsulting() {
		return consulting;
	}

	public void setConsulting(String[] consulting) {
		this.consulting = consulting;
	}

	public EducationMendeley[] getEducation() {
		return education;
	}

	public void setEducation(EducationMendeley[] education) {
		this.education = education;
	}

	public EmploymentMendeley[] getEmployment() {
		return employment;
	}

	public void setEmployment(EmploymentMendeley[] employment) {
		this.employment = employment;
	}
}
