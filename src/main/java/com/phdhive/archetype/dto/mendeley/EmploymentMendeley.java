package com.phdhive.archetype.dto.mendeley;

public class EmploymentMendeley {
	private String position;
	private String institution;
	private String location;
	private String[] classes_taught;
	private String website;
	private String start_date;
	private String end_date;
	public String getPosition() {
		return position;
	}
	
	public void setPosition(String position) {
		this.position = position;
	}
	
	public String getInstitution() {
		return institution;
	}
	
	public void setInstitution(String institution) {
		this.institution = institution;
	}
	
	public String getLocation() {
		return location;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}
	
	public String[] getClasses_taught() {
		return classes_taught;
	}
	
	public void setClasses_taught(String[] classes_taught) {
		this.classes_taught = classes_taught;
	}
	
	public String getWebsite() {
		return website;
	}
	
	public void setWebsite(String website) {
		this.website = website;
	}
	
	public String getStart_date() {
		return start_date;
	}
	
	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}
	
	public String getEnd_date() {
		return end_date;
	}
	
	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}
}
