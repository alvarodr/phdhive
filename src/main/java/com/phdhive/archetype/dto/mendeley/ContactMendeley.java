package com.phdhive.archetype.dto.mendeley;

import com.phdhive.archetype.dto.BaseObject;

/**
 * 
 * @author Alvaro
 * 
 */
public class ContactMendeley extends BaseObject {

	private static final long serialVersionUID = -7825219601134492183L;

	private String address;

	private String email;

	private String im;

	private String fax;

	private String mobile;

	private String phone;

	private String webpage;

	private String zipcode;

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getIm() {
		return im;
	}

	public void setIm(String im) {
		this.im = im;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getWebpage() {
		return webpage;
	}

	public void setWebpage(String webpage) {
		this.webpage = webpage;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
}
