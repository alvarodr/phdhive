package com.phdhive.archetype.dto.mendeley;

public class MainMendeley {

	private String academic_status;
	
	private String bio;
	
	private String discipline_id;
	
	private String discipline_name;
	
	private String location;
	
	private String name;
	
	private String photo;
	
	private String profile_id;
	
	private String research_interests;

	public String getAcademic_status() {
		return academic_status;
	}

	public void setAcademic_status(String academic_status) {
		this.academic_status = academic_status;
	}

	public String getBio() {
		return bio;
	}

	public void setBio(String bio) {
		this.bio = bio;
	}

	public String getDiscipline_id() {
		return discipline_id;
	}

	public void setDiscipline_id(String discipline_id) {
		this.discipline_id = discipline_id;
	}

	public String getDiscipline_name() {
		return discipline_name;
	}

	public void setDiscipline_name(String discipline_name) {
		this.discipline_name = discipline_name;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getProfile_id() {
		return profile_id;
	}

	public void setProfile_id(String profile_id) {
		this.profile_id = profile_id;
	}

	public String getResearch_interests() {
		return research_interests;
	}

	public void setResearch_interests(String research_interests) {
		this.research_interests = research_interests;
	}
	

}
