package com.phdhive.archetype.dto.ws;

import java.util.List;
import java.util.Map;

import com.phdhive.archetype.dto.user.User;

public class UserWebService extends User {
	/**
	 * 
	 */
	private static final long serialVersionUID = -791139960565227795L;
	private List<Map<String, String>> keyList;
	public List<Map<String, String>> getKeyList() {
		return keyList;
	}
	public void setKeyList(List<Map<String, String>> keyList) {
		this.keyList = keyList;
	}
}
