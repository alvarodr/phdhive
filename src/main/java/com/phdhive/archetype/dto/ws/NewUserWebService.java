package com.phdhive.archetype.dto.ws;

import org.springframework.web.multipart.MultipartFile;


public class NewUserWebService {
	
	private java.lang.String email;

	private java.lang.String name;

	private java.lang.String surname;
	
	private MultipartFile userfile;

	private java.lang.String institution_affiliation;

	private java.lang.String research_area;

	private java.lang.String bio;

	private java.lang.String studies_level;

	private java.lang.String post_id;

	private java.lang.String latest_article;
	
	private java.lang.String token;

	public java.lang.String getToken() {
		return token;
	}

	public void setToken(java.lang.String token) {
		this.token = token;
	}

	public java.lang.String getEmail() {
		return email;
	}

	public void setEmail(java.lang.String email) {
		this.email = email;
	}

	public java.lang.String getName() {
		return name;
	}

	public void setName(java.lang.String name) {
		this.name = name;
	}

	public java.lang.String getSurname() {
		return surname;
	}

	public void setSurname(java.lang.String surname) {
		this.surname = surname;
	}

	public MultipartFile getUserfile() {
		return userfile;
	}

	public void setUserfile(MultipartFile userfile) {
		this.userfile = userfile;
	}

	public java.lang.String getInstitution_affiliation() {
		return institution_affiliation;
	}

	public void setInstitution_affiliation(java.lang.String institution_affiliation) {
		this.institution_affiliation = institution_affiliation;
	}

	public java.lang.String getResearch_area() {
		return research_area;
	}

	public void setResearch_area(java.lang.String research_area) {
		this.research_area = research_area;
	}

	public java.lang.String getBio() {
		return bio;
	}

	public void setBio(java.lang.String bio) {
		this.bio = bio;
	}

	public java.lang.String getStudies_level() {
		return studies_level;
	}

	public void setStudies_level(java.lang.String studies_level) {
		this.studies_level = studies_level;
	}

	public java.lang.String getPost_id() {
		return post_id;
	}

	public void setPost_id(java.lang.String post_id) {
		this.post_id = post_id;
	}

	public java.lang.String getLatest_article() {
		return latest_article;
	}

	public void setLatest_article(java.lang.String latest_article) {
		this.latest_article = latest_article;
	}
}
