package com.phdhive.archetype.dto.master.types;

import java.util.HashMap;
import java.util.Map;

public enum TypeMessage {

	MESSAGE_QUESTION("Q", "msg-quest"),
	MESSAGE_INFO("I", "msg-info"),
	MESSAGE_WARNING("W", "msg-warn"),
	MESSAGE_ERROR("E", "msg-error");
	
	// Transformacion inversa mediante un integer
	private static final Map<String, TypeMessage> REVERSE = new HashMap<String, TypeMessage>();

	static {
		for (TypeMessage item : TypeMessage.values()) {
			REVERSE.put(item.code, item);
		}
	}

	private String code;
	private String css;
	
	private TypeMessage(String code, String css){
		this.code = code;
		this.css = css;
	}
	
	public String getCode(){
		return code;
	}
	
	public String getCss(){
		return css;
	}
	
	public TypeMessage fromCode(String code){
		return REVERSE.get(code);
	}
	
}
