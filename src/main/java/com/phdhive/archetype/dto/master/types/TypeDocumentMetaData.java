package com.phdhive.archetype.dto.master.types;

import java.util.HashMap;
import java.util.Map;

public enum TypeDocumentMetaData {
	METADATA_OWNER ("OWNER"),
	METADATA_TITLE ("TITLE"),
	METADATA_AREA ("AREA"),
	METADATA_DISCIPLINE("DISCIPLINE"),
	METADATA_EVENT ("EVENT"),
	METADATA_DATE ("DATE");
	

	// Transformacion inversa mediante un integer
	private static final Map<String, TypeDocumentMetaData> REVERSE = new HashMap<String, TypeDocumentMetaData>();

	static {
		for (TypeDocumentMetaData item : TypeDocumentMetaData.values()) {
			REVERSE.put(item.type, item);
		}
	}

	private String type;
	
	private TypeDocumentMetaData(String type){
		this.type = type;
	}
	
	public String getType(){
		return type;
	}
	
	public static TypeDocumentMetaData fromType(String type){
		return REVERSE.get(type);
	}
}
