package com.phdhive.archetype.dto.master;

import org.apache.commons.lang.StringUtils;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.phdhive.archetype.dto.BaseObject;

/**
 * 
 * @author ADORU3N
 *
 */

@Document(collection = "PHD_MASTER")
public class DataMaster extends BaseObject {

	private static final long serialVersionUID = -1926246581175664182L;

	@Id
	@Field("MAST_ID")
	private ObjectId id;
	
	@Field("MAST_TYPE")
	private java.lang.Integer type;
	
	@Field("MAST_CODE")
	private java.lang.String code;
	
	@Field("MAST_DESCRIPTION")
	private java.lang.String description;
	
	protected DataMaster(){
		super();
	}
	
	@PersistenceConstructor
	protected DataMaster(Integer type, String code, String description){
		super();
		this.type = type;
		this.code = code;
		this.description = description;
	}

	/**
	 * Metodo factoria para la creacion de datos maestros tipados.
	 * 
	 * @param type
	 * @param code
	 * @param description
	 * @return
	 */
	public static DataMaster createDatoMaestro(Integer type, String code, String description) {
		return new DataMaster(type, code, description);
	}
	
	public ObjectId getId() {
		return id;
	}

	public java.lang.Integer getType() {
		return type;
	}

	public void setType(java.lang.Integer type) {
		this.type = type;
	}

	public java.lang.String getCode() {
		return code;
	}

	public void setCode(java.lang.String code) {
		this.code = code;
	}

	public java.lang.String getDescription() {
		return description;
	}

	public void setDescription(java.lang.String description) {
		this.description = description;
	}
	
	@Override
	public String toString() {
		final String TAB = "    ";

		String retValue = "";

		retValue = "DatoMaestro ( " + super.toString() + TAB + "type = " + this.type + TAB + "code = " + this.code+ TAB + "description = "
				+ this.description + " )";

		return retValue;
	}

	@Override
	public boolean equals(Object obj) {
		boolean result = false;

		if ((obj != null) && (obj instanceof DataMaster)) {
			DataMaster dataMaster = (DataMaster) obj;
			if (dataMaster.type == this.type &&
					!StringUtils.isBlank(dataMaster.getCode()) && !StringUtils.isBlank(this.code)
					&& dataMaster.getCode().equals(this.code)) {
				result = true;
			}
		}

		return result;
	}	
}
