package com.phdhive.archetype.dto.master.types;

import java.util.HashMap;
import java.util.Map;

/**
 * Tipo de formato permitido en los documentos
 * @author ADORU3N
 *
 */
public enum TypeDocumentFormat {
	MIMETYPE_DOC ("doc", "application/msword", "application/x-tika-msoffice"),
	MIMETYPE_DOCX ("docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/x-tika-ooxml");

	// Transformacion inversa mediante un integer
	private static final Map<String, TypeDocumentFormat> REVERSE = new HashMap<String, TypeDocumentFormat>();

	static {
		for (TypeDocumentFormat item : TypeDocumentFormat.values()) {
			REVERSE.put(item.extension, item);
		}
	}

	private String extension;
	private String type;
	private String typeTika;
	
	private TypeDocumentFormat(String extension, String type, String typeTika){
		this.extension = extension;
		this.type = type;
		this.typeTika = typeTika;
	}
	
	public String getExtension(){
		return extension;
	}
	
	public String getType(){
		return type;
	}
	
	public String getTypeTika(){
		return typeTika;
	}
	
	public TypeDocumentFormat fromExtension(String extension){
		return REVERSE.get(extension);
	}
}
