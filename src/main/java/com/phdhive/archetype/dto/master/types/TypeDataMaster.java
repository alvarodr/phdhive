package com.phdhive.archetype.dto.master.types;

import java.util.HashMap;
import java.util.Map;

import com.phdhive.archetype.dto.master.DataMaster;
import com.phdhive.archetype.dto.master.ITypeDataMaster;

public enum TypeDataMaster implements ITypeDataMaster {
	TYPES_EDUCATION_LEVEL (1),
	TYPES_KNOWLEDGES_UNSECO (2),
	TYPES_COUNTRIES (3),
	TYPES_CONTENT_TYPES (4),
	LANGUAGES(5),
	LOGIC (11),
	MATHEMATICS (12),
	ASTRONOMY_ASTROPHYSICS (21),
	PHYSICS (22),
	CHEMISTRY (23),
	LIFE_SCIENCES (24),
	EARTH_AND_SPACE_SCIENCE (25),
	AGRICULTURAL_SCIENCES (31),
	MEDICAL_SCIENCES (32),
	TECHNOLOGICAL_SCIENCES (33),
	ANTHROPOLOGY (51),
	DEMOGRAPHY (52),
	ECONOMIC_SCIENCES (53),
	GEOGRAPHY (54),
	HISTORY (55),
	JURIDICAL_SCIENCE_AND_LAW (56),
	LINGUISTICS (57),
	PEDAGOGY (58),
	POLITICAL_SCIENCE (59),
	PSYCHOLOGY (61),
	SCIENCES_OF_ARTS_AND_LETTERS (62),
	SOCIOLOGY (63),
	ETHICS (71),
	PHILOSOPHY (72);

	// Transformacion inversa mediante un integer
	private static final Map<Integer, TypeDataMaster> REVERSE = new HashMap<Integer, TypeDataMaster>();

	private static final Map<Class<? extends DataMaster>, TypeDataMaster> REVERSE_CLASS = new HashMap<Class<? extends DataMaster>, TypeDataMaster>();

	static {
		for (TypeDataMaster item : TypeDataMaster.values()) {
			REVERSE.put(item.type, item);
		}
		for (TypeDataMaster item : TypeDataMaster.values()) {
			if (item.clazz != null) {
				REVERSE_CLASS.put(item.clazz, item);
			}
		}
	}

	private int type;
	private Class<? extends DataMaster> clazz;

	private TypeDataMaster(int type) {
		this.type = type;
	}

	private TypeDataMaster(int type, Class<? extends DataMaster> clazz) {
		this.type = type;
		this.clazz = clazz;
	}

	public static TypeDataMaster toTipoDatoMaestro(Class<? extends DataMaster> value) {
		return REVERSE_CLASS.get(value);
	}

	public static TypeDataMaster toTipoDatoMaestro(Integer value) {
		return REVERSE.get(value);
	}
	
	public Class<? extends DataMaster> clazz() {
		return clazz;
	}

	public Integer type() {
		return type;
	}
}