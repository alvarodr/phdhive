package com.phdhive.archetype.dto.master.types;

import java.util.HashMap;
import java.util.Map;

public enum TypeLanguage {

	LANGUAGE_SPANISH("SP", "Spanish"),
	LANGUAGE_ENGLISH("EN", "English");
	
	// Transformacion inversa mediante un integer
	private static final Map<String, TypeLanguage> REVERSE = new HashMap<String, TypeLanguage>();

	static {
		for (TypeLanguage item : TypeLanguage.values()) {
			REVERSE.put(item.code, item);
		}
	}

	private String code;
	private String language;
	
	private TypeLanguage(String code, String language){
		this.code = code;
		this.language = language;
	}
	
	public String getCode(){
		return code;
	}
	
	public String getLanguage(){
		return language;
	}
	
	public TypeLanguage fromCode(String code){
		return REVERSE.get(code);
	}
	
}
