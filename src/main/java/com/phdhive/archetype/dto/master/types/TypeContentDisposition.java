package com.phdhive.archetype.dto.master.types;

/**
 * Tipo Content Disposition para la descarga de ficheros.
 * @author ADORU3N
 *
 */
public enum TypeContentDisposition {
	INLINE("inline"),
	ATTACHMENT("attachment");
	
	 private String descripcion;
	 
	 private TypeContentDisposition(String descripcion) {
		 this.descripcion = descripcion;
	 }

	public String getDescripcion() {
		return descripcion;
	}
}
