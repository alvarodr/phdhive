package com.phdhive.archetype.dto.document;

import java.io.InputStream;
import java.util.Map;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.phdhive.archetype.dto.BaseObject;
import com.phdhive.archetype.dto.master.types.TypeDocumentMetaData;


/**
 * 
 * @author ADORU3N
 *
 */

@Document(collection = "PHD_DOCUMENTS")
public class PhdDocument extends BaseObject {

	private static final long serialVersionUID = 6916342975536746647L;

	
	@Id
	@Field("DOCUMENT_ID")
	private String id;
	
	@Field("DOCUMENT_FILE")
	private InputStream file;
	
	@Field("DOCUMENT_type")
	private String contentType;
	
	@Field("DOCUMENT_NAME")
	private String documentName;
	
	@Field("DOCUMENT_METADATA")
	private Map<TypeDocumentMetaData, String> metaData;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public InputStream getFile() {
		return file;
	}

	public void setFile(InputStream file) {
		this.file = file;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getDocumentName() {
		return documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	public Map<TypeDocumentMetaData, String> getMetaData() {
		return metaData;
	}

	public void setMetaData(Map<TypeDocumentMetaData, String> metaData) {
		this.metaData = metaData;
	}	
}
