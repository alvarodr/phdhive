package com.phdhive.archetype.dto.document;

public class DocumentMetaData {
	public static final String METADATA_OWNER = "owner"; //Propietario
	public static final String METADATA_TITLE = "title"; //Titulo
	public static final String METADATA_AREA = "area"; //Area
	public static final String METADATA_DISCIPLINE = "discipline"; //Disciplina
	public static final String METADATA_EVENT = "event"; // Evento
}
