package com.phdhive.archetype.dto.document;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

import com.phdhive.archetype.dto.BaseObject;

/**
 * 
 * @author Alvaro
 *
 */
public class DocumentMultipart extends BaseObject {

	private static final long serialVersionUID = 7137000699754966535L;
	
	private MultipartFile userfile;

	public MultipartFile getUserfile() {
		return userfile;
	}

	public void setUserfile(MultipartFile userfile) {
		this.userfile = userfile;
	}
	
	public PhdDocument toDocument() throws IOException{
		PhdDocument document = new PhdDocument();
		document.setFile(new ByteArrayInputStream(this.userfile.getBytes()));
		document.setDocumentName(this.userfile.getOriginalFilename());
		return document;
	}
}
