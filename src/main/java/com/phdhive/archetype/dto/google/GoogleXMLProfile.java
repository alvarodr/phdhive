package com.phdhive.archetype.dto.google;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;;

/** JSON
	{
	 "id": "115762351899421678110",
	 "email": "alvarodr1983@gmail.com",
	 "verified_email": true,
	 "name": "Alvaro Donaire Ruiz",
	 "given_name": "Alvaro",
	 "family_name": "Donaire Ruiz",
	 "link": "https://plus.google.com/115762351899421678110",
	 "picture": "https://lh3.googleusercontent.com/-nSMqbC5LIAY/AAAAAAAAAAI/AAAAAAAAALI/t7r--WyaB8A/photo.jpg",
	 "gender": "male",
	 "birthday": "0000-12-04",
	 "locale": "es"
	}
*/

@Root(name="person")
public class GoogleXMLProfile {
	
	@Element
	private String id;
		   
	@Element
	private String email;
	
	@Element
	private String name;
	   
	@Element
	private String given_name;
	   
	@Element
	private String family_name;
	
	@Element
	private String link;
	
	@Element
	private String picture;
	
	@Element
	private String gender;
	
	@Element
	private String birthday;
	
	@Element
	private String locale;
		   
	public GoogleXMLProfile() {
		super();
	}  
	
	public GoogleXMLProfile(String id,String industry,String headline) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGiven_name() {
		return given_name;
	}

	public void setGiven_name(String given_name) {
		this.given_name = given_name;
	}

	public String getFamily_name() {
		return family_name;
	}

	public void setFamily_name(String family_name) {
		this.family_name = family_name;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}
	
}

