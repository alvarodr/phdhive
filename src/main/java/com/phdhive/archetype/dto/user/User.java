package com.phdhive.archetype.dto.user;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.security.core.GrantedAuthority;

import com.phdhive.archetype.dto.BaseObject;
import com.phdhive.archetype.dto.community.Community;
import com.phdhive.archetype.dto.master.DataMaster;
import com.phdhive.archetype.security.Role;
import com.phdhive.archetype.utils.Constants;

/**
 * @author Alvaro
 * 
 */
@Document(collection = "PHD_USERS")
public class User extends BaseObject {

	private static final long serialVersionUID = 7759078940459644711L;

	@Id
	@Field("USER_ID")
	private ObjectId id;

	@Field("USER_EMAIL")
	@Indexed(unique = true)
	private java.lang.String email;

	@Field("USER_NAME")
	private java.lang.String name;

	@Field("USER_SURNAME")
	private java.lang.String surname;

	@Field("USER_PASSWORD")
	private java.lang.String password;

	@Transient
	private java.lang.String cpassword;

	@Transient
	private Community community;

	@Field("USER_ENABLED")
	private java.lang.Boolean enabled;

	@Field("USER_ID_ACTIVACION")
	private java.lang.String idActivation;

	@Field("USER_AUTH")
	private java.util.List<String> authorities;

	@Field("USER_INSTITUTION_AFF")
	private java.lang.String institutionAffiliation;

	@Field("USER_RESEARCH_AREA")
	private java.lang.String researchArea;

	@Field("USER_LOPD")
	private java.lang.Boolean lopd;
	
	@DBRef
	@Field("USER_AREA")
	private java.util.List<DataMaster> areas;

	@DBRef
	@Field("USER_DISCIPLINE")
	private java.util.List<DataMaster> disciplines;

	@Field("USER_BIOGRAPHY")
	private java.lang.String biograph;

	@Field("USER_COUNTRY")
	private DataMaster country;	
	
	@Field("USER_STUDIES_LEVEL")
	private DataMaster studiesLevel;

	@Field("USER_LATEST_ARTICLE")
	private java.lang.String latestArticle;

	@Field("USER_TOKEN")
	private java.lang.String token;

	@Field("USER_KEYWORDS")
	private Map<String, Integer> keywords;

	@Field("USER_FOUNDWORDS")
	private List<Entry<String, Integer>> foundWords;

	@Field("USER_SCORE")
	private java.lang.Float score;

	@Field("USER_CREATE_DATE")
	private java.util.Date createdate;
	
	@Field("USER_UP_DATE")
	private java.util.Date update;

	@Field("USER_LOGIN_COUNT")
	private java.lang.Integer loginCount;

	@Field("USER_LAST_IP")
	private java.lang.String lastIP;
	
	@Field("USER_EVENTS")
	private java.util.List<String> eventsNames;
	
	@Field("USER_DOCUMENTS_IDS")
	private java.util.List<ObjectId> documentsIds;

	@DBRef
	@Field("USER_CONTACTS")
	private java.util.List<User> contacts;

	public User() {
		super();
	};

	public User(String email, String name, String surname,
			String latestArticle, String affiliation, String researchArea,
			String studeslevel, String bio) {
		this.email = email;
		this.name = name;
		this.surname = surname;
		this.institutionAffiliation = affiliation;
		this.researchArea = researchArea;
		this.loginCount = Constants.DEFAULT_START_LOGIN_COUNT;

		setBiograph(bio);
		setLatestArticle(latestArticle);
	}

	public java.util.List<User> getContacts() {
		return contacts;
	}

	public void setContacts(java.util.List<User> contacts) {
		this.contacts = contacts;
	}

	public java.util.List<DataMaster> getAreas() {
		return areas;
	}

	public void setAreas(java.util.List<DataMaster> areas) {
		this.areas = areas;
	}
	
	public java.util.List<String> getEventsNames() {
		return eventsNames;
	}

	public void setEventsNames(java.util.List<String> events) {
		this.eventsNames = events;
	}

	public java.util.List<ObjectId> getDocuments() {
		return documentsIds;
	}

	public void setDocuments(java.util.List<ObjectId> documents) {
		this.documentsIds = documents;
	}

	public java.lang.String getEmail() {
		return email;
	}

	public void setEmail(java.lang.String email) {
		this.email = email;
	}

	public java.lang.String getName() {
		return name;
	}

	public void setName(java.lang.String name) {
		this.name = name;
	}

	public java.lang.Boolean getLopd() {
		return lopd;
	}

	public void setLopd(java.lang.Boolean lopd) {
		this.lopd = lopd;
	}

	public java.lang.String getSurname() {
		return surname;
	}

	public void setSurname(java.lang.String surname) {
		this.surname = surname;
	}

	public java.lang.String getPassword() {
		return password;
	}

	public void setPassword(java.lang.String password) {
		this.password = password;
	}

	public java.lang.Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(java.lang.Boolean enabled) {
		this.enabled = enabled;
	}

	public void setAuthorities(Collection<GrantedAuthority> authorities) {
		Collection<String> newAuthorities = new ArrayList<String>();

		for (GrantedAuthority authority : authorities) {
			newAuthorities.add(authority.getAuthority());
		}
		this.authorities = new ArrayList<String>(newAuthorities);
	}

	public Collection<GrantedAuthority> getAuthorities() {

		Collection<GrantedAuthority> returnAuthorities = new ArrayList<GrantedAuthority>();

		if (this.authorities != null) {
			for (String roleString : this.authorities) {
				for (Role role : Role.values()) {
					if (role.getAuthority().equals(roleString)) {
						returnAuthorities.add((GrantedAuthority) role);
						break;
					}
				}
			}
		}

		return returnAuthorities;
	}

	public java.lang.String getInstitutionAffiliation() {
		return institutionAffiliation;
	}

	public void setInstitutionAffiliation(String institutionAffiliation) {
		this.institutionAffiliation = institutionAffiliation;
	}

	public String getResearchArea() {
		return researchArea;
	}

	public void setResearchArea(String researchArea) {
		this.researchArea = researchArea;
	}

	public Map<String, Integer> getKeywords() {
		return keywords;
	}

	public List<Map.Entry<String, Integer>> getReduceKeyword(){
		if (keywords!=null && !keywords.isEmpty()) {
			List<Map.Entry<String, Integer>> list = new ArrayList<Map.Entry<String,Integer>>(keywords.entrySet());
			Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
				@Override
				public int compare(Entry<String, Integer> o1, Entry<String, Integer> o2) {
					return (o2.getValue().compareTo(o1.getValue()));
				}
			});
			if (list.size()>5)
				return list.subList(0, 5);
			else
				return list;
		}
		return new ArrayList<Map.Entry<String,Integer>>();
	}
	
	public void setKeywords(Map<String, Integer> keywords) {
		this.keywords = keywords;
	}

	public DataMaster getStudiesLevel() {
		return studiesLevel;
	}

	public void setStudiesLevel(DataMaster studiesLevel) {
		this.studiesLevel = studiesLevel;
	}

	public void setBiograph(java.lang.String biograph) {
		this.biograph = biograph;
	}

	public DataMaster getCountry() {
		return country;
	}

	public void setCountry(DataMaster country) {
		this.country = country;
	}

	public void setLatestArticle(java.lang.String latestArticle) {
		this.latestArticle = latestArticle;
	}

	public java.lang.String getBiograph() {
		return biograph;
	}

	public java.lang.String getLatestArticle() {
		return latestArticle;
	}

	public String getId() {
		return id.toString();
	}
	
	public ObjectId getObjectId() {
		return id;
	}
	
	public List<Entry<String, Integer>> getFoundWords() {
		return foundWords;
	}

	public void setFoundWords(List<Entry<String, Integer>> foundWords) {
		this.foundWords = foundWords;
	}

	public java.lang.String getIdActivation() {
		return idActivation;
	}

	public void setIdActivation(java.lang.String idActivation) {
		this.idActivation = idActivation;
	}

	public java.lang.String getCpassword() {
		return cpassword;
	}

	public void setCpassword(java.lang.String cpassword) {
		this.cpassword = cpassword;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public java.lang.Float getScore() {
		return score;
	}

	public void setScore(java.lang.Float score) {
		this.score = score;
	}

	public java.util.Date getCreatedate() {
		return createdate;
	}

	public void setCreatedate(java.util.Date createdate) {
		this.createdate = createdate;
	}

	public java.util.List<DataMaster> getDisciplines() {
		return disciplines;
	}

	public void setDisciplines(java.util.List<DataMaster> disciplines) {
		this.disciplines = disciplines;
	}

	public java.util.Date getUpdate() {
		return update;
	}

	public void setUpdate(java.util.Date update) {
		this.update = update;
	}

	public java.lang.Integer getLoginCount() {
		return loginCount;
	}

	public void setLoginCount(java.lang.Integer loginCount) {
		this.loginCount = loginCount;
	}

	public java.lang.String getLastIP() {
		return lastIP;
	}

	public void setLastIP(java.lang.String lastIP) {
		this.lastIP = lastIP;
	}

	public Community getCommunity() {
		return community;
	}

	public void setCommunity(Community community) {
		this.community = community;
	}
	
	@Override
	public String toString() {
		return "name:		" + this.name + "\n" + "surname:	" + this.surname + "\n"
				+ "email:		" + this.email;
	}
}