package com.phdhive.archetype.dto.community;

import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.phdhive.archetype.dto.BaseObject;
import com.phdhive.archetype.dto.master.DataMaster;
import com.phdhive.archetype.dto.user.User;

/**
 * 
 * @author Alvaro
 *
 */
@Document(collection = "PHD_COMMUNITY")
public class Community extends BaseObject {

	private static final long serialVersionUID = 1521851494035704798L;
	
	@Id
	@Field("COMM_ID")
	private ObjectId id;
	
	@Field("COMM_NAME")
	private java.lang.String name;
	
	@Field("COMM_COUNTRY")
	private DataMaster country;
	
	@Field("COMM_ADMIN")
	private User admin;
	
	@Field("COMM_CREATE_DATE")
	private java.util.Date createDate;

	@PersistenceConstructor
	public Community(String name, DataMaster country, User admin){
		this.name = name;
		this.country = country;
		this.admin = admin;
		this.createDate = new Date();
	}
	
	public String getId() {
		return id.toString();
	}

	public java.lang.String getName() {
		return name;
	}

	public void setName(java.lang.String name) {
		this.name = name;
	}

	public DataMaster getCountry() {
		return country;
	}

	public void setCountry(DataMaster country) {
		this.country = country;
	}

	public User getAdmin() {
		return admin;
	}

	public void setAdmin(User admin) {
		this.admin = admin;
	}

	public java.util.Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(java.util.Date createDate) {
		this.createDate = createDate;
	}
	

	public String getEmail(){
		return this.admin.getEmail();
	}
	
	public String getDsCountry(){
		return this.country.getDescription();
	}
	
	@Override
	public String toString(){
		return "[ Name: " + this.name + "\n" + "Admin: " + admin.getEmail() + " ]"; 
	}
}
