package com.phdhive.archetype.dto.geolocation;

import com.phdhive.archetype.dto.BaseObject;

/**
 * 
 * @author Jorge
 *
 */
/**
 * @author Magolles
 *
 */
public class Geolocation extends BaseObject {

	private static final long serialVersionUID = 2925302780722623472L;

	private String ip;

	private String country_code;

	private String country_name;

	private String region_code;

	private String region_name;

	private String city;

	private String zipcode;

	private String latitude;

	private String longitude;

	private String metroCode;

	private String areaCode;

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getCountry_code() {
		return country_code;
	}

	public void setCountry_code(String country_code) {
		this.country_code = country_code;
	}

	public String getCountry_name() {
		return country_name;
	}

	public void setCountry_name(String country_name) {
		this.country_name = country_name;
	}



	public String getRegion_code() {
		return region_code;
	}

	public void setRegion_code(String region_code) {
		this.region_code = region_code;
	}

	public String getRegion_name() {
		return region_name;
	}

	public void setRegion_name(String region_name) {
		this.region_name = region_name;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getMetro_code() {
		return metroCode;
	}

	public void setMetro_code(String metroCode) {
		this.metroCode = metroCode;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreacode(String areaCode) {
		this.areaCode = areaCode;
	}

}
