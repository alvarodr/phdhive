package com.phdhive.archetype.dto.link;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.mapping.Field;

import com.phdhive.archetype.dto.BaseObject;


public class Link extends BaseObject {

	
	private static final long serialVersionUID = 6126000600864966539L;
		
	@Id
	@Field("LINK_ID")
	private ObjectId id;

	@Field("LINK_DESC")
	private String description;
	
	@Field("LINK_URL")
	private String url;

	public Link() {
		super();
	};
	
	@PersistenceConstructor
	public Link(String description, String url){
		super();
		this.description = description;
		this.url = url;
	}
	
	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
		
}
