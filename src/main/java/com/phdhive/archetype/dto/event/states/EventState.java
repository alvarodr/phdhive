package com.phdhive.archetype.dto.event.states;

/**
 *  estados de un evento
 * @author Margolles
 *
 */
public enum EventState {
	
	ACTIVE("active"),
	INACTIVE("incative");
	
	 private String descripcion;
	 
	 private EventState(String descripcion) {
		 this.descripcion = descripcion;
	 }

	public String getDescripcion() {
		return descripcion;
	}
}
