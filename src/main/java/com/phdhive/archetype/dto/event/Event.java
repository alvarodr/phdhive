package com.phdhive.archetype.dto.event;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.util.CollectionUtils;

import com.phdhive.archetype.dto.BaseObject;
import com.phdhive.archetype.dto.community.Community;
import com.phdhive.archetype.dto.user.User;
import com.phdhive.archetype.utils.Constants;

/**
 * 
 * @author Alvaro
 *
 */
@Document(collection = "PHD_EVENTS")
public class Event extends BaseObject {

	private static final long serialVersionUID = 8853646788669485420L;

	
	@Id
	@Field("EVT_ID")
	private ObjectId id; 
	
	@Indexed(unique = true)
	@Field("EVT_NAME")
	private java.lang.String name;
	
	@Field("EVT_DESCRIPTION")
	private java.lang.String description;
	
	@Field("EVT_START_DATE")
	private java.util.Date startDate;
	
	@Field("EVT_END_DATE")
	private java.util.Date endDate;
	
	@Field("EVT_LANG")
	private java.lang.String language;
	
	@DBRef
	@Field("EVT_OWNER")
	private Community owner;
	
	@DBRef
	@Field("EVT_USERS")
	private java.util.List<User> users;

	@Field("EVT_STATE")
	private com.phdhive.archetype.dto.event.states.EventState state;
	
	public Event(){}
	
	@PersistenceConstructor
	public Event(String name, String description, Date startDate, Date endDate, Community owner){
		this.name = name;
		this.description = description;
		this.startDate = startDate;
		this.endDate = endDate;
		this.owner = owner;
	}

	public String getId() {
		return id.toString();
	}

	public java.lang.String getName() {
		return name;
	}

	public void setName(java.lang.String name) {
		this.name = name;
	}

	public java.lang.String getDescription() {
		return description;
	}

	public void setDescription(java.lang.String description) {
		this.description = description;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(java.util.Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public Date getEndDateAsObject() {		
		return endDate;
	}
	
	public void setEndDate(java.util.Date endDate) {
		this.endDate = endDate;
	}

	public Community getOwner() {
		return owner;
	}

	public void setOwner(Community owner) {
		this.owner = owner;
	}

	public java.util.List<User> getUsers() {
		return users;
	}

	public void setUsers(java.util.List<User> users) {
		this.users = users;
	}
	
	public Integer getSuscribers(){
		if (!CollectionUtils.isEmpty(users))
			return users.size();
		else return 0;
	}
	
	public String getNameCommunity(){
		if (owner != null)
			return owner.getName();
		else return "";
	}
	
	public String getStartTime(){
		SimpleDateFormat sdf = new SimpleDateFormat(Constants.FORMARTO_HORA);
		return sdf.format(startDate);
	}

	public String getEndTime(){
		SimpleDateFormat sdf = new SimpleDateFormat(Constants.FORMARTO_HORA);
		return sdf.format(endDate);
	}
	
	public com.phdhive.archetype.dto.event.states.EventState getState() {
		return state;
	}

	public void setState(com.phdhive.archetype.dto.event.states.EventState active) {
		this.state = active;
	}
	
	public java.lang.String getLanguage() {
		return language;
	}

	public void setLanguage(java.lang.String language) {
		this.language = language;
	}

	@Override
	public String toString(){
		return "[ Name: " + this.name + " ]";
	}
}
