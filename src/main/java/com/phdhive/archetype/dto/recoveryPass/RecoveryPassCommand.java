package com.phdhive.archetype.dto.recoveryPass;

public class RecoveryPassCommand {

	
	private String email;
	 
	private String cemail;
	 
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCemail() {
		return cemail;
	}

	public void setCemail(String cemail) {
		this.cemail = cemail;
	}


	
}
