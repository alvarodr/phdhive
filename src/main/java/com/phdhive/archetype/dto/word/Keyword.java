package com.phdhive.archetype.dto.word;

/**
 * @author Jorge

 * 
 * Esta clase se usa para tener un registro total de las palabras
 * El objetivo es facilitar la generación de informes 
 * 
 */
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.phdhive.archetype.dto.BaseObject;

@Document(collection = "PHD_KEYWORDS")
public class Keyword extends BaseObject {

	private static final long serialVersionUID = -7087871919999171568L;

	@Id
	private ObjectId id;
	
	@Indexed(unique = true)
	@Field("KWORD_WORD")
	private String word;

	@Field("KWORD_FREQ")
	private Integer totalFreq;

	@Field("KWORD_LAST_UPDATE")
	private java.util.Date lastUpdate;
	
	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public String getWord() {
		return word;
	}	
	
	public void setWord(String word) {
		this.word = word;
	}

	public Integer getTotalFreq() {
		return totalFreq;
	}

	public void setTotalFreq(Integer totalFreq) {
		this.totalFreq = totalFreq;
	}

	public java.util.Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(java.util.Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

}
