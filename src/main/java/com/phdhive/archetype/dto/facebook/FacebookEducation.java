package com.phdhive.archetype.dto.facebook;

import java.util.List;

public class FacebookEducation {
	
	private FacebookSubWork school;
	
	private FacebookSubWork year;
	
	private List<FacebookSubWork> concentration;
	
	private String type;

	public FacebookSubWork getSchool() {
		return school!=null?school:new FacebookSubWork();
	}

	public void setSchool(FacebookSubWork school) {
		this.school = school;
	}

	public FacebookSubWork getYear() {
		return year!=null?year:new FacebookSubWork();
	}

	public void setYear(FacebookSubWork year) {
		this.year = year;
	}

	public List<FacebookSubWork> getConcentration() {
		return concentration;
	}

	public void setConcentration(List<FacebookSubWork> concentration) {
		this.concentration = concentration;
	}

	public String getType() {
		return type!=null?type:"";
	}

	public void setType(String type) {
		this.type = type;
	}
	
	@Override
	public String toString(){
		String concentration = "";
		if (this.concentration!=null)
			for (FacebookSubWork elemen : this.concentration)
				concentration += elemen.getName() + "\n";
		
		return "School: " + getSchool().getName() + "\n" +
			"Type: " + getType() + "\n" +
			"Year: " + getYear().getName() + "\n" +
			"Concentration: \n" + "\t" + concentration;
	}
}
