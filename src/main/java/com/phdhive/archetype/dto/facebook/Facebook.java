package com.phdhive.archetype.dto.facebook;

import java.util.List;

import com.phdhive.archetype.dto.BaseObject;

/**
 * 
 * @author Alvaro
 *
 */
public class Facebook extends BaseObject {

	private static final long serialVersionUID = 4974267255901734565L;

	private String first_name;
	
	private String last_name;
	
	private String email;
	
	private List<FacebookWork> work;
	
	private List<FacebookEducation> education;

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<FacebookWork> getWork() {
		return work;
	}

	public void setWork(List<FacebookWork> work) {
		this.work = work;
	}

	public List<FacebookEducation> getEducation() {
		return education;
	}

	public void setEducation(List<FacebookEducation> education) {
		this.education = education;
	}
	
	@Override
	public String toString(){
		String education = "";
		if (this.education != null)
			for (FacebookEducation eduDto : this.education)
				education += eduDto.toString() + "\n\n";
		
		String work = "";
		if (this.work != null)
			for (FacebookWork workDto : this.work)
				work += workDto.toString() + "\n\n";
		
		return education + work;
	}
}
