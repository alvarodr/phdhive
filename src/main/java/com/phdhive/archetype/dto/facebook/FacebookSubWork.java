package com.phdhive.archetype.dto.facebook;

public class FacebookSubWork {
	
	private String id;
	
	private String name;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name!=null?name:"";
	}

	public void setName(String name) {
		this.name = name;
	}
}
