package com.phdhive.archetype.dto.facebook;

public class FacebookWork {

	private FacebookSubWork employer;
	
	private FacebookSubWork location;
	
	private FacebookSubWork position;
	
	private String description;
	
	private String start_date;
	
	private String end_date;

	public FacebookSubWork getEmployer() {
		return employer!=null?employer:new FacebookSubWork();
	}

	public void setEmployer(FacebookSubWork employer) {
		this.employer = employer;
	}

	public FacebookSubWork getLocation() {
		return location!=null?location:new FacebookSubWork();
	}

	public void setLocation(FacebookSubWork location) {
		this.location = location;
	}

	public FacebookSubWork getPosition() {
		return position!=null?position:new FacebookSubWork();
	}

	public void setPosition(FacebookSubWork position) {
		this.position = position;
	}

	public String getDescription() {
		return description!=null?description:"";
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStart_date() {
		return start_date!=null?start_date:"";
	}

	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}

	public String getEnd_date() {
		return end_date!=null?end_date:"";
	}

	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}
	
	@Override
	public String toString(){
		return "Employer: " + getEmployer().getName() + "\n" +
			"Position: " + getPosition().getName() + "\n" +
			"Start Date: " + getStart_date() + "\n" +
			"End Date: " + getEnd_date() + "\n" +
			"Location: " + getLocation().getName() + "\n" +
			"Description: " + getDescription();
	}
}
