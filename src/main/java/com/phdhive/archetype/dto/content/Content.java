package com.phdhive.archetype.dto.content;

/**
 * 
 * @author Jorge
 *
 */

import java.text.SimpleDateFormat;
import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.phdhive.archetype.dto.BaseObject;
import com.phdhive.archetype.dto.link.Link;
import com.phdhive.archetype.dto.master.DataMaster;
import com.phdhive.archetype.utils.Constants;

@Document(collection = "PHD_CONTENTS")
public class Content extends BaseObject {

	private static final long serialVersionUID = 1528452494030704798L;

	@Id
	@Field("CONT_ID")
	private ObjectId id;
	
	@Field("CONT_TITLE")
	private java.lang.String title;
	
	@Field("CONT_HEADER")
	private java.lang.String header;
	
	@Field("CONT_BODY")
	private String body;
	
	@Field("CONT_IMAGE")
	private ObjectId image;
	
	@Field("CONT_TYPE")
	private DataMaster type;

	@Field("CONT_LINKS")
	private java.util.List<Link> links;

	@Field("CONT_RELATED_CONTENTS")
	private java.util.List<RelatedContent> relatedContents;

	@Field("CONT_CREATE_DATE")
	private java.util.Date createDate;
	
	@Field("CONT_UPDATE_DATE")
	private java.util.Date updateDate;
	
	@Field("CONT_PUBLISHED")
	private Boolean published;
	
	@Field("CONT_PUBLISH_DATE")
	private java.util.Date publishDate;	
	
	@Field("CONT_ORDER")
	private java.lang.Integer order;
	
	public Content() {
		super();
	};	

	@PersistenceConstructor
	public Content(String title, String header, String body, DataMaster type){
		this.title = title;
		this.header = header;
		this.body = body;
		this.type = type;
		this.createDate = new Date();
		this.published = Boolean.FALSE;
	}
	
	public String getId() {
		return id.toString();
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public java.lang.String getTitle() {
		return title;
	}

	public void setTitle(java.lang.String title) {
		this.title = title;
	}

	public java.lang.String getHeader() {
		return header;
	}

	public void setHeader(java.lang.String header) {
		this.header = header;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public ObjectId getImage() {
		return image;
	}

	public void setImage(ObjectId image) {
		this.image = image;
	}

	public String getType() {
		return type.getDescription();
	}

	public DataMaster getTypeAsObject() {
		return type;
	}

	public void setType(DataMaster type) {
		this.type = type;
	}

	public java.util.List<Link> getLinks() {
		return links;
	}

	public void setLinks(java.util.List<Link> links) {
		this.links = links;
	}

	//Devuelve un String con los ids de los contenidos relacionados en la forma id1;id2;id3;...
	public String getRelatedContents() {		
		String result = "";
		if (relatedContents!=null){
			for(RelatedContent rel: relatedContents){
				result += rel.getRelContentRef().toString();
				result += ";";
			}
		}
		return result;	
	}

	public java.util.List<RelatedContent> getRelatedContentsAsList() {		
		return relatedContents;	
	}

	public void setRelatedContents(java.util.List<RelatedContent> relatedContents) {
		this.relatedContents = relatedContents;
	}

	public String getCreateDate() {
		SimpleDateFormat sdf = new SimpleDateFormat(Constants.FORMATO_FECHA_POR_DEFECTO);
		return sdf.format(createDate);
	}

	public void setCreateDate(java.util.Date createDate) {
		this.createDate = createDate;
	}

	public String getUpdateDate() {
		SimpleDateFormat sdf = new SimpleDateFormat(Constants.FORMATO_FECHA_POR_DEFECTO);
		return sdf.format(updateDate);
	}

	public void setUpdateDate(java.util.Date updateDate) {
		this.updateDate = updateDate;
	}
	
	public Boolean isPublished() {
		return published;
	}

	public void setPublished(Boolean published) {
		this.published = published;
	}

	public java.util.Date getPublishDate() {
		return publishDate;
	}

	public void setPublishDate(java.util.Date publishDate) {
		this.publishDate = publishDate;
	}

	public java.lang.Integer getOrder() {
		return order;
	}

	public void setOrder(java.lang.Integer order) {
		this.order = order;
	}

	@Override
	public String toString(){
		return "[ Title: " + this.title + "\n" + "Header: " + this.header + " ]";
	}
}
