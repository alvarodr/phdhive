package com.phdhive.archetype.dto.content;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Field;

import com.phdhive.archetype.dto.BaseObject;

public class RelatedContent extends BaseObject {

	private static final long serialVersionUID = -4006530360140190374L;
	
	//Esto es una referencia el contenido que se quiere relacionar
	@Field("RCONT_REF")
	private ObjectId relContentRef;
	
	//Este es el nombre del contenido que se quiere relacionar
	@Field("RCONT_NAME")
	private String relContentName;
	
	public String getRelContentRef() {
		return relContentRef.toString();
	}

	public void setRelContentRef(ObjectId relContentRef) {
		this.relContentRef = relContentRef;
	}

	public String getRelContentName() {
		return relContentName;
	}

	public void setRelContentName(String relContentName) {
		this.relContentName = relContentName;
	}
			

}
