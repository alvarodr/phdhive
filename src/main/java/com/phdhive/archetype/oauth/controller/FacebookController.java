package com.phdhive.archetype.oauth.controller;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import static org.springframework.web.context.request.RequestAttributes.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.view.RedirectView;

import com.google.gson.Gson;
import com.phd.hive.oauth2.api.facebook.APIFacebook;
import com.phd.hive.oauth2.client.service.IOAuthService;
import com.phd.hive.oauth2.model.OAuthRequest;
import com.phd.hive.oauth2.model.Response;
import com.phd.hive.oauth2.model.Token;
import com.phd.hive.oauth2.model.Verifier;
import com.phd.hive.oauth2.types.TypeVerb;
import com.phdhive.archetype.dto.facebook.Facebook;
import com.phdhive.archetype.dto.user.User;
import com.phdhive.archetype.security.CustomAuthentication;
import com.phdhive.archetype.security.Role;
import com.phdhive.archetype.service.IUserService;
import com.phdhive.archetype.utils.Constants;

/**
 * Controller for login with FB.
 * 
 * @author Alvaro
 *
 */
@Controller
public class FacebookController {

	private Logger log = LoggerFactory.getLogger(FacebookController.class);
	
	@Autowired
	private APIFacebook apiFacebook;
	
	@Autowired
	private IOAuthService oAuthService;
	
	@Autowired
	private IUserService userService;
	
	@RequestMapping(value={"/external/login-facebook"}, method = RequestMethod.GET)
	public RedirectView login(WebRequest request) {
		request.setAttribute(Constants.IP_SOURCE, request.getHeader("X-Forwarded-For"), SCOPE_SESSION);
		return new RedirectView(apiFacebook.getAuthorizationUrl());
	}
	
	@RequestMapping(value={"/external/facebook-callback"}, method = RequestMethod.GET)
	public String callback(@RequestParam(value="code") String oauthVerifier, WebRequest request) {
		
		// getting access token
		oAuthService.createInstance(apiFacebook);
		Verifier verifier = new Verifier(oauthVerifier);
		Token accessToken = oAuthService.getAccessToken(verifier);
		
		// getting user profile
		OAuthRequest oauthRequest = new OAuthRequest(TypeVerb.GET, "https://graph.facebook.com/me");
		oAuthService.signRequest(accessToken, oauthRequest);
		Response oauthResponse = oauthRequest.send();
		Gson gson = new Gson();
		Facebook profile = gson.fromJson(oauthResponse.getBody(), Facebook.class);
		
		Map<String, User> model = null;
		User user = null;
		String view = "";
		try {
			model = new HashMap<String, User>();
			
			if ((user = userService.readByEmail(profile.getEmail()))==null){
				user = new User();
				user.setName(profile.getFirst_name());
				user.setSurname(profile.getLast_name());
				user.setEmail(profile.getEmail());
				user.setBiograph(profile.toString());
				user.setEnabled(Boolean.TRUE);
				user.setLoginCount(Constants.DEFAULT_START_LOGIN_COUNT);
				user.setLastIP((String)request.getAttribute(Constants.IP_SOURCE, SCOPE_SESSION));
				HashSet<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
				authorities.add((GrantedAuthority)Role.ROLE_USER);
				user.setAuthorities(authorities);
				user = userService.createUser(user);
			} else {
				if (user.getLoginCount() != null)
					user.setLoginCount(user.getLoginCount() + Constants.DEFAULT_START_LOGIN_COUNT);
				else
					user.setLoginCount(Constants.DEFAULT_START_LOGIN_COUNT);
				user.setLastIP((String)request.getAttribute(Constants.IP_SOURCE, SCOPE_SESSION));
				userService.updateUser(user);
			}
			
			view = "redirect:/action/profile";
			
			if (!user.getEnabled()){
				return "redirect:/action/login_error/accountDisabled";
			}
			
			Authentication auth = new CustomAuthentication(user, null);
			auth.setAuthenticated(true);
			SecurityContextHolder.getContext().setAuthentication(auth);
			
			model.put("user", user);
		} catch (Exception e) {
			log.error("[ERROR] Se ha producido un error al obtener la informacion del perfil " + profile.getEmail(), e);
		}		
		return view;
	}
}
