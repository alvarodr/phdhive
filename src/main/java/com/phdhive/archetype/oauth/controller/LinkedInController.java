package com.phdhive.archetype.oauth.controller;

import static org.springframework.web.context.request.RequestAttributes.SCOPE_SESSION;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import org.simpleframework.xml.stream.Format;
import org.simpleframework.xml.stream.HyphenStyle;
import org.simpleframework.xml.stream.Style;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.view.RedirectView;

import com.phd.hive.oauth2.api.linkedin.APILinkedin;
import com.phd.hive.oauth2.client.service.IOAuthService;
import com.phd.hive.oauth2.model.OAuthRequest;
import com.phd.hive.oauth2.model.Response;
import com.phd.hive.oauth2.model.Token;
import com.phd.hive.oauth2.model.Verifier;
import com.phd.hive.oauth2.types.TypeVerb;
import com.phdhive.archetype.dto.linkedin.LinkedinXMLProfileDTO;
import com.phdhive.archetype.dto.user.User;
import com.phdhive.archetype.security.CustomAuthentication;
import com.phdhive.archetype.security.Role;
import com.phdhive.archetype.service.IUserService;
import com.phdhive.archetype.utils.Constants;

/**
 * Controller for login LinkedIn with OAuth 2.0.
 * 
 * @author Alvaro
 *
 */
@Controller
public class LinkedInController {

	private Logger log = LoggerFactory.getLogger(LinkedInController.class);
	
	@Autowired
	private APILinkedin apiLinkedin;
	
	@Autowired
	private IOAuthService oAuthService;
	
	@Autowired
	private IUserService userService;
	
	@RequestMapping(value={"/external/login-linkedin"}, method = RequestMethod.GET)
	public  RedirectView login(WebRequest request) {
		request.setAttribute(Constants.IP_SOURCE, request.getHeader("X-Forwarded-For"), SCOPE_SESSION);
		return new RedirectView(apiLinkedin.getAuthorizationUrl());
	}
	
	@RequestMapping(value={"/external/linkedin-callback"}, method = RequestMethod.GET)
	public String callback(@RequestParam(value="code", required=false) String oauthVerifier, WebRequest request) {
		
		// getting access token
		oAuthService.createInstance(apiLinkedin);
		Verifier verifier = new Verifier(oauthVerifier);
		Token accessToken = oAuthService.getAccessToken(verifier);
		
		// getting user profile
		OAuthRequest oauthRequest = new OAuthRequest(TypeVerb.GET, "https://api.linkedin.com/v1/people/~:(id,email-address,first-name,last-name,industry,headline,summary)?format=xml");
		oAuthService.signRequest(accessToken, oauthRequest);
		Response oauthResponse = oauthRequest.send();
		
		//Comprueba si es usuario registrado
		Style style = new HyphenStyle();
		Format format = new Format(style);
		Serializer serializer = new Persister(format);
		Map<String, User> model = null;
		String view = "";
		try {
			LinkedinXMLProfileDTO profile = serializer.read(LinkedinXMLProfileDTO.class, oauthResponse.getBody(), false);
			
			User user = null; 
			
			model = new HashMap<String, User>();
			
			if ((user = userService.readByEmail(profile.getEmailAddress()))==null){
				user = new User();
				user.setEmail(profile.getEmailAddress());
				user.setName(profile.getFirstName());
				user.setSurname(profile.getLastName());
				user.setInstitutionAffiliation(profile.getHeadline());
				user.setResearchArea(profile.getIndustry());
				user.setBiograph(profile.getSummary());
				user.setEnabled(Boolean.TRUE);
				user.setLoginCount(Constants.DEFAULT_START_LOGIN_COUNT);
				user.setLastIP((String)request.getAttribute(Constants.IP_SOURCE, SCOPE_SESSION));
				Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
				authorities.add((GrantedAuthority)Role.ROLE_USER);
				user.setAuthorities(authorities);
				userService.createUser(user);
			} else {
				if (user.getLoginCount() != null)
					user.setLoginCount(user.getLoginCount() + Constants.DEFAULT_START_LOGIN_COUNT);
				else
					user.setLoginCount(Constants.DEFAULT_START_LOGIN_COUNT);
				user.setLastIP((String)request.getAttribute(Constants.IP_SOURCE, SCOPE_SESSION));
				userService.updateUser(user);
			}
			
			view = "redirect:/action/profile";
			
			if (!user.getEnabled()) {
				return "redirect:/action/login_error/accountDisabled";
			}
			
			Authentication auth = new CustomAuthentication(user, null);
			auth.setAuthenticated(true);
			SecurityContextHolder.getContext().setAuthentication(auth);
			
			model.put("user", user);
		} catch (Exception e) {
			log.error("[ERROR] Se ha producido un error al obtener la informacion del perfil", e);
		}
		return view;
	}
}