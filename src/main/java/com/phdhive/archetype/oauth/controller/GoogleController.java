package com.phdhive.archetype.oauth.controller;

import static org.springframework.web.context.request.RequestAttributes.SCOPE_SESSION;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.view.RedirectView;

import com.google.gson.Gson;
import com.phd.hive.oauth2.api.google.APIGoogle;
import com.phd.hive.oauth2.client.service.IOAuthService;
import com.phd.hive.oauth2.model.OAuthRequest;
import com.phd.hive.oauth2.model.Response;
import com.phd.hive.oauth2.model.Token;
import com.phd.hive.oauth2.model.Verifier;
import com.phd.hive.oauth2.types.TypeVerb;
import com.phdhive.archetype.dto.google.GoogleXMLProfile;
import com.phdhive.archetype.dto.user.User;
import com.phdhive.archetype.security.CustomAuthentication;
import com.phdhive.archetype.security.Role;
import com.phdhive.archetype.service.IUserService;
import com.phdhive.archetype.utils.Constants;

/**
 * Controller para el acceso desde Google.
 * 
 * @author Alvaro
 *
 */
@Controller
public class GoogleController {
	
	private Logger log = LoggerFactory.getLogger(GoogleController.class);
	
	@Autowired
	private APIGoogle apiGoogle;
	
	@Autowired
	private IOAuthService oAuthService;
	
	@Autowired
	private IUserService userService;
	
	@RequestMapping(value={"/external/login-google"}, method = RequestMethod.GET)
	public  RedirectView login(WebRequest request) {
		request.setAttribute(Constants.IP_SOURCE, request.getHeader("X-Forwarded-For"), SCOPE_SESSION);
		return new RedirectView(apiGoogle.getAuthorizationUrl());
	}
	
	@RequestMapping(value={"/external/google-callback"}, method = RequestMethod.GET)
	public String callback(@RequestParam(value="code", required=false) String oauthVerifier, WebRequest request) {
		
		// getting access token
		oAuthService.createInstance(apiGoogle);
		Verifier verifier = new Verifier(oauthVerifier);
		Token accessToken = oAuthService.getAccessToken(verifier);
		
		// getting user profile
		OAuthRequest oauthRequest = new OAuthRequest(TypeVerb.GET, "https://www.googleapis.com/oauth2/v1/userinfo?alt=json");
		oAuthService.signRequest(accessToken, oauthRequest);
		Response oauthResponse = oauthRequest.send();
		
		GoogleXMLProfile perfil = new Gson().fromJson(oauthResponse.getBody(), GoogleXMLProfile.class);

		User user = null;
		Map<String, User> model = null;
		String view = "";
		try{
			model = new HashMap<String, User>();
			
			if ((user = userService.readByEmail(perfil.getEmail()))==null){
				user = new User();
				user.setEmail(perfil.getEmail());
				user.setName(perfil.getGiven_name());
				user.setSurname(perfil.getFamily_name());
				user.setEnabled(Boolean.TRUE);
				user.setLoginCount(Constants.DEFAULT_START_LOGIN_COUNT);
				user.setLastIP((String)request.getAttribute(Constants.IP_SOURCE, SCOPE_SESSION));
				HashSet<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
				authorities.add((GrantedAuthority)Role.ROLE_USER);
				user.setAuthorities(authorities);
				user = userService.createUser(user);
			} else {
				if (user.getLoginCount() != null)
					user.setLoginCount(user.getLoginCount() + Constants.DEFAULT_START_LOGIN_COUNT);
				else
					user.setLoginCount(Constants.DEFAULT_START_LOGIN_COUNT);
				user.setLastIP((String)request.getAttribute(Constants.IP_SOURCE, SCOPE_SESSION));
				userService.updateUser(user);
			}

			view = "redirect:/action/profile";
			
			if (!user.getEnabled()) {
				return "redirect:/action/login_error/accountDisabled";
			}
			
			Authentication auth = new CustomAuthentication(user, null);
			auth.setAuthenticated(true);
			SecurityContextHolder.getContext().setAuthentication(auth);
			
			model.put("user", user);
		}catch (Exception e){
			log.error("[ERROR] al obtener el perfil del usuario " + perfil.getEmail(), e);
		}
		return view;
	}
}
