
package com.phdhive.archetype.snowball;

/**
 * Snowball.
 * 
 * @author ADORU3N
 *
 */
public abstract class SnowballStemmer extends SnowballProgram {
    public abstract boolean stem();
};
