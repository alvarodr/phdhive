package com.phdhive.archetype.load;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;

import com.phdhive.archetype.service.IBulkService;
import com.phdhive.archetype.utils.Constants;

public class LoadUsers {
	
	private Logger log = LoggerFactory.getLogger(LoadUsers.class);
	
	@Autowired private IBulkService bulkService;
	
	public void init() throws Exception{
		log.debug("Comprobando si hay usuarios cargados");
		bulkService.loadUsers(new ClassPathResource(Constants.USERS_FILE));
		
		log.debug("Comprobando datos maestros");
		bulkService.loadDataMaster(new ClassPathResource(Constants.DATA_MASTER_FILE));
	}
}
