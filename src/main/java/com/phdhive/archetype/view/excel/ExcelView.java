package com.phdhive.archetype.view.excel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.view.document.AbstractExcelView;

import com.phdhive.archetype.dto.master.DataMaster;
import com.phdhive.archetype.dto.user.User;
import com.phdhive.archetype.service.IUserService;
import com.phdhive.archetype.utils.PhdhiveUtils;

public class ExcelView extends AbstractExcelView {

	@Autowired
	private IUserService userService;

	@Override
	@SuppressWarnings("unchecked")
	public void buildExcelDocument(Map<String, Object> model, HSSFWorkbook workbook, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		//recuperar datos
		List<User> listado = (List<User>) model.get("users");

        HSSFSheet hoja = workbook.createSheet("users");

        HSSFCell cell;
        int row = 0;                        
        int col=0;
        //Inserta cabecera
        cell = getCell(hoja, row, col++);
        setText(cell, new String( "Name"));
        cell = getCell(hoja, row, col++);
        setText(cell, "Surname");
        cell = getCell(hoja, row, col++);
        setText(cell, "Email");
        cell = getCell(hoja, row, col++);
        setText(cell, "Institution");
        cell = getCell(hoja, row, col++);
        setText(cell, "Biography");
        cell = getCell(hoja, row, col++);
        setText(cell, "Country");        
        cell = getCell(hoja, row, col++);
        setText(cell, "Documents count");
        cell = getCell(hoja, row, col++);
        setText(cell, "Login count");
        cell = getCell(hoja, row, col++);
        setText(cell, "Keyword");
        cell = getCell(hoja, row, col++);
        setText(cell, "Area");
        
        row++;
        for (User user : listado){
            col = 0;
            
            cell = getCell(hoja, row, col++);
            setText(cell, null == user.getName() ? "[null]" : user.getName());
            
            cell = getCell(hoja, row, col++);
            setText(cell, null == user.getSurname() ? "[null]" : user.getSurname());
            
            cell = getCell(hoja, row, col++);
            setText(cell, null == user.getEmail() ? "[null]" : user.getEmail());
            
            cell = getCell(hoja, row, col++);
            setText(cell, null == user.getInstitutionAffiliation() ? "[null]" : user.getInstitutionAffiliation());
            
            cell = getCell(hoja, row, col++);
            setText(cell, null == user.getBiograph() ? "[null]" : user.getBiograph());
            
            //geolocalización
            cell = getCell(hoja, row, col++);
            //setText(cell, null ==  userService.getUserCountry(user) ? "[null]" :  userService.getUserCountry(user));
            
            cell = getCell(hoja, row, col++);
            setText(cell, null == user.getDocuments() ? "[null]" : (new Integer(user.getDocuments().size())).toString() );
        
            cell = getCell(hoja, row, col++);
            setText(cell, null == user.getLoginCount() ? "[null]" : (user.getLoginCount()).toString() );
            
            //Escribe palabra1:freq1,palabra2:freq2,etc
            String strKwords = "";
            if (user.getKeywords()!=null){
                Iterator<Entry<String, Integer>> iter = user.getKeywords().entrySet().iterator();
	            List<Entry<String, Integer>> kwords = new ArrayList<Entry<String,Integer>>();
	            while(iter.hasNext()){
	            	kwords.add((Entry<String, Integer>) iter.next());             	
	            }
	            Collections.sort(kwords,  PhdhiveUtils.BY_VALUE);
	            
	            for (Entry<String, Integer> kword : kwords){
	            	strKwords += (kword.getKey() + ":" + kword.getValue() + ",");
	            }
	            strKwords = strKwords.substring(0, strKwords.length()-1);
            }else{
            	strKwords = "[null]";
            }
            cell = getCell(hoja, row, col++);
            setText(cell,  strKwords);
            
            String strAreas = "";
            if (user.getAreas()!=null){
	            List<DataMaster> areas = user.getAreas();
	            for(DataMaster area: areas){
	            	strAreas += area.getDescription() + ",";
	            }
	            if (StringUtils.isNotBlank(strAreas))
	            	strAreas = strAreas.substring(0, strAreas.length()-1);
	        }else{
	        	strAreas = "[null]";
	        }
            	            
            row++;
        }
	}

}
