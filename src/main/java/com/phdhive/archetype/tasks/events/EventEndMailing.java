/**
 * 
 */
package com.phdhive.archetype.tasks.events;

/**
 * @author Margolles
 *
 */
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.servlet.LocaleResolver;

import com.phdhive.archetype.dto.event.Event;
import com.phdhive.archetype.dto.event.states.EventState;
import com.phdhive.archetype.dto.user.User;
import com.phdhive.archetype.service.IDocumentService;
import com.phdhive.archetype.service.IEmailService;
import com.phdhive.archetype.service.IEventService;
import com.phdhive.archetype.service.ISolrService;
import com.phdhive.archetype.service.IUserService;

public class EventEndMailing {

	@Autowired
	private IEventService eventService;

	@Autowired
	private IUserService userService;

	@Autowired
	private ISolrService solrService;

	@Autowired
	protected LocaleResolver localeResolver;

	@Autowired
	private IDocumentService documentService;

	@Autowired
	private IEmailService emailService;

	@Scheduled(fixedDelay = 700000)
	//@Scheduled(fixedDelay = 5000)
	public void mailResults() throws Exception {

		// Busca los eventos cuya fecha de finalización haya terminado
		List<Event> events = (List<Event>) eventService.getActiveEvents();
		for (Event event : events) {
			Date now = new Date();

			if (now.after(event.getEndDateAsObject())) {

				// Manda un mail a todos los usuarios del evento
				List<User> users = event.getUsers();
				if (users != null) {
					for (User userEv : users) {
						emailService.sendEventEndMessage(userEv, event,	new Locale("EN"));
					}
				}
			}

			event.setState(EventState.INACTIVE);
			eventService.saveEvent(event);
		}
	}
}
