<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=8, IE=9, IE=10" />
		<meta name="description" content="<fmt:message key="head.description.common"/>" />
		<meta name="keywords" content="<fmt:message key="meta.keywords"/>"/>
		<c:set var="_titleKey"><tiles:getAsString name="title"/></c:set>	
		<title><fmt:message key="${_titleKey}"/></title>
		
		<!-- Favicon -->
		<link rel="shortcut icon" href="/resources/img/ico/phdhive.ico" type="image/x-icon" />
		<link rel="icon" href="/resources/img/ico/phdhive.ico" type="image/x-icon" />
				
		<!-- CSS -->
		<common:common_stylesheets_dash/>
		
		<!-- JS -->
		<common:common_javascript_dash/>
		<%@ include file="/WEB-INF/jsp/common/jsAdditional.jsp" %>
	
		<script type="text/javascript">
			var contextPath="${pageContext.request.contextPath}";
			var isAdmin = false;
		</script>
	</head>
	<body>
		<tiles:insertAttribute name="body" />
	</body>
</html>