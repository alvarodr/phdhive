<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=8, IE=9, IE=10" />
		<meta name="description" content="<fmt:message key="head.description.common"/>" />
		<meta name="keywords" content="<fmt:message key="meta.keywords"/>"/>
		<c:set var="_titleKey"><tiles:getAsString name="title"/></c:set>	
		<title><fmt:message key="${_titleKey}"/></title>
		<c:set var="style"><tiles:importAttribute name="style" ignore="true"/></c:set>
		<fmt:setLocale value="en_EN" scope="session"/>
		<!-- Favicon -->
		<link rel="shortcut icon" href="/resources/img/ico/phdhive.ico" type="image/x-icon" />
		<link rel="icon" href="/resources/img/ico/phdhive.ico" type="image/x-icon" />
		
		<!-- CSS -->
		<%@ include file="/WEB-INF/jsp/common/cssAdditional.jsp" %>
		<common:common_stylesheets/>
		
		<!-- JS -->
		<common:common_javascript/>
		<%@ include file="/WEB-INF/jsp/common/jsAdditional.jsp" %>
		
		<script type="text/javascript">
			var contextPath="${pageContext.request.contextPath}";
		</script>
	</head>
	<body id="page">
		<div class="wrapper ${style}">
			<tiles:insertAttribute name="slider" ignore="true"/>
			<div class="container">
				<!-- Head -->
				<header class="row">
					<tiles:insertAttribute name="header" ignore="true"/>
				</header>
				
				<!-- Content -->
				<div class="row contenido">
					<tiles:insertAttribute name="menu" ignore="true"/>
					<tiles:insertAttribute name="body"/>
				</div>
				
				<div class="push">
					<!--//-->
				</div>
			</div>
		</div>

		<tiles:insertAttribute name="footer"/>

		<!-- Google Analytics -->
		<script>
			(function(i, s, o, g, r, a, m) {
				i['GoogleAnalyticsObject'] = r;
				i[r] = i[r] || function() {
					(i[r].q = i[r].q || []).push(arguments)
				}, i[r].l = 1 * new Date();
				a = s.createElement(o), m = s.getElementsByTagName(o)[0];
				a.async = 1;
				a.src = g;
				m.parentNode.insertBefore(a, m)
			})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
	
			ga('create', 'UA-48526847-1', 'phdhive.com');
			ga('send', 'pageview');
		</script>

	</body>
</html>