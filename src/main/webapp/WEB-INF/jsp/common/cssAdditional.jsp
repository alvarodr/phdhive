<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<tiles:importAttribute name="cssAdditional" ignore="true"/>
<c:if test="${not empty cssAdditional}">
	<c:forEach var="cssFile" items="${cssAdditional}">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}${cssFile}"/>
	</c:forEach>
</c:if>