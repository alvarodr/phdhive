<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<tiles:importAttribute name="jsAdditional" ignore="true"/>
<c:if test="${not empty jsAdditional}">
	<c:forEach var="jsFile" items="${jsAdditional}">
<script src="${pageContext.request.contextPath}${jsFile}" type="text/javascript" charset="UTF-8"></script>
	</c:forEach>
</c:if>