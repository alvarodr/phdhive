<script type="text/javascript">
<!--
$(document).ready(function(){
	var isAccpeted = 
	$("#lopd").dialog({
		autoOpen: false,
		modal: true,
		rezisable: true,
		buttons:[
	     {
	    	 text: 'Accept',
	    	 class: 'leftButton btn btn-oscuro',
	    	 click: function() {
	    		 $("#lopd1").attr("checked","checked");
	    		 $(this).dialog('close');
	    	 }
	     },
	     {
	    	 text: 'Reject',
	    	 class: 'floatRight btn btn-oscuro',
	    	 click: function() {
	    		 $("#lopd1").removeAttr("checked");
	    		 $(this).dialog('close');
	    	 }
	     }
	 	]
	});
	$("#lopdLink").click(function(){
		$("#lopd").dialog("open");
	});
	$("#lopd1").click(function(){
		if ($(this).is(":checked")) {
			$("#lopd").dialog("open");
		}
	});
});
//-->
</script>

<div class="contenido_total">
	<div class="col-sm-12">
		<h1>Create your account</h1>
		<div class="row">
			<div class="col-sm-8">
				<em>We use this information to categorise your profile and improve
				our results. It will only be visible to users of your communities you
				are subscribed to, if applicable</em>
				<!-- formulario -->
				<form:form role="form" class="margin_top20" id="demo-form" name="demo-form" commandName="user" data-validate="parsley">
					<div class="form-group row">
						<div class="col-sm-8">
							<form:input path="name" id="name" type="text" class="form-control margin_top10" placeholder="name" data-required="true" data-trigger="change"/>
							<form:input path="surname" type="text" class="form-control margin_top10" placeholder="surname" data-required="true" data-trigger="change"/>
							<form:input path="email" type="text" class="form-control margin_top10" placeholder="e-mail" data-required="true" data-type="email" data-trigger="change" />
							<form:input path="password" id="password" type="password" class="form-control margin_top10" placeholder="Password" data-required="true" data-trigger="change"/>
							<form:input path="cpassword" type="password" class="form-control margin_top10" placeholder="Repeat password" data-equalto="#password" data-required="true" data-trigger="change" />
							<form:checkbox path="lopd" class="margin_top20 lopd parsley-validated" data-required="true" data-trigger="change"/><span class="margin_left10 margin_top20">I accept the legal terms and conditions of <a id="lopdLink" href="#">Organic Law Protection Data</a></span>
						</div>	
					</div>
					<!-- form-group row -->
					<button type="submit" class="btn btn-oscuro pull-right">Submit</button>
				</form:form>
				<!-- /formulario -->
			</div>
		</div>
	</div>
</div>

<!-- LIGHTTBOX -->
<div id="lopd" title="Terms and conditions">
	<p class="terms">PRUEBAadfasd sdf asdf asdf asdfasdf asdf asdf asdf asdf asdfasdasdf </p>
</div>