<!-- slider -->
<ul class="cb-slideshow">
	<li><span><fmt:message key="login.slideshow.image01"/></span></li>
	<li><span><fmt:message key="login.slideshow.image02"/></span></li>
	<li><span><fmt:message key="login.slideshow.image03"/></span></li>
	<li><span><fmt:message key="login.slideshow.image04"/></span></li>
	<li><span><fmt:message key="login.slideshow.image05"/></span></li>
	<li><span><fmt:message key="login.slideshow.image06"/></span></li>
</ul>
<!-- /slider -->