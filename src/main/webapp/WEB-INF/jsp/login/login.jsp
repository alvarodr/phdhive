<security:authorize access="isAuthenticated()" >
	<script type="text/javascript">
	<!--
	window.location.href="/action/index";
	//-->
	</script>
</security:authorize>

<script type="text/javascript">
<!--
function openCreateAccount(){
	window.location=contextPath + '/action/register';
}
//-->
</script>

<!-- formulario de entrada -->
<div class="row margin_top60"></div>

<div class="col-md-5 margin_top60 hidden-xs">

	<!-- frases manualmente -->
	<!-- <div class="frase1 margin_top60">Don’t tell us who you are,</div>
         <div class="frase2">show us what you write!</div> -->
	<!-- /frases manualmente -->

	<!-- frases con carrusel -->
	<section class="rw-wrapper">
		<h2 class="rw-sentence">
			<div class="rw-words rw-words-1">
				<span><fmt:message key="login.dont.tell"/></span>
				<span><fmt:message key="login.dont.tell01"/></span>
				<span><fmt:message key="login.dont.tell02"/></span>
				<span><fmt:message key="login.dont.tell03"/></span> 
				<span><fmt:message key="login.dont.tell04"/></span>
				<span><fmt:message key="login.dont.tell05"/></span>
			</div>
		</h2>
	</section>
	<section class="rw-wrapper margin_top60">
		<h2 class="rw-sentence">
			<div class="rw-words rw-words-1">
				<span><fmt:message key="login.show.write"/></span>
				<span><fmt:message key="login.show.write01"/></span>
				<span><fmt:message key="login.show.write02"/></span>
				<span><fmt:message key="login.show.write03"/></span>
				<span><fmt:message key="login.show.write04"/></span>
				<span><fmt:message key="login.show.write05"/></span>
			</div>
		</h2>
	</section>
	<!-- /frases con carrusel -->
</div>

<div class="col-md-3 col-md-offset-3 login">
	<fmt:message key="login.description.form"/>
	<!-- botones sociales -->
	<div class="btn_sociales">
		<a href="${pageContext.servletContext.contextPath}/action/external/login-facebook">
			<img src="${pageContext.servletContext.contextPath}/resources/img/ico/fb_big.png" />
		</a>
		<a href="${pageContext.servletContext.contextPath}/action/external/login-linkedin">
			<img src="${pageContext.servletContext.contextPath}/resources/img/ico/in_big.png" />
		</a>
<%-- 		<a href="${pageContext.servletContext.contextPath}/action/external/login-mendeley"> --%>
<%-- 			<img src="${pageContext.servletContext.contextPath}/resources/img/ico/md_big.png" /> --%>
<!-- 		</a> -->
		<a href="${pageContext.servletContext.contextPath}/action/external/login-google">
			<img src="${pageContext.servletContext.contextPath}/resources/img/ico/plus_big.png" />
		</a>
	</div>
	<!-- /botones sociales -->
	<!-- formulario -->
	<form role="form" action="/action/j_spring_security_check" method="post">
		<div class="form-group">
			<label for="j_username"><fmt:message key="login.username.label"/></label> 
			<input type="email" class="form-control" id="j_username" name="j_username">
		</div>
		<div class="form-group">
			<label for="j_password"><fmt:message key="login.password.label"/></label>
			<input type="password" class="form-control" id="j_password" name="j_password">
		</div>
		<button type="submit" class="btn btn-oscuro-home form-control"><fmt:message key="login.sigin.button"/></button>
		<a href="/action/recoveryPass"><p class="help-block pull-right negro"><fmt:message key="login.cant.access"/></p></a>
		<a type="button" class="btn btn-oscuro-home form-control" href="/action/register"><fmt:message key="login.create.account"/></a>
	</form>
	<!-- /formulario -->
</div>