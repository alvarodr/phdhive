<nav class="navbar navbar-inverse" role="navigation">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
			<span class="sr-only">Toggle navigation</span> 
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<!-- <a class="navbar-brand" href="#">PHDHive</a> -->
		<a href="#"><img src="${pageContext.servletContext.contextPath}/resources/img/logo.png" class="logo img-responsive" /></a>
	</div>
	
		<div class="collapse navbar-collapse navbar-ex1-collapse">
			<ul class="nav navbar-nav navbar-right">
				<li>
					<span class="info">
						<div class="box-info">
							<span class="info-community">${not empty community?community.name:'SUPERADMIN'}<br></span>
							<span class="info-user"><i class="glyphicon glyphicon-user"></i> ${user.name} ${user.surname}<br></span>
						</div>
						<a href="${pageContext.servletContext.contextPath}/action/admin/logout" class="pull-right">
							<span class="btn info">
								Logout <i class="glyphicon glyphicon-log-out"></i>
							</span>
						</a>
					</span>
				</li>
				<li>

				</li>
			</ul>
		</div>
		<!-- /.navbar-collapse -->
	</nav>
</nav>