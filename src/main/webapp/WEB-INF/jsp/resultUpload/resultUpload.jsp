<script type="text/javascript">
<!--
$(document).ready(function(){
	$("#wordcloud").awesomeCloud({
		"size" : {
			"grid" : 16,
			"normalize" : false
		},
		"options" : {
			"color" : "random-dark",
			"rotationRatio" : 0.2
		},
		"font" : "'Times New Roman', Times, serif",
		"shape" : "circle"
	});
})
//-->
</script>
<!-- Contenido Central (Find a Reviewer) -->
<div class="row contenido">
	<div class="col-sm-8 contenido_izquierdo" role="main">
		<h1>Frequent words in document</h1>
		<p>${nameDocument}</p>
		<div id="wordcloud" class="wordcloud">
			<c:forEach items="${foundWords}" var="word">
				<span data-weight="${word.value}">${word.key}</span>
			</c:forEach>
		</div>
	</div>
	<div class="col-sm-4 contenido_derecho amarillo mheight_500">
		<h1>You are writing about</h1>
		<div class="list-group lista_lateral">
			<c:forEach items="${foundWords}" var="word">
				<a href="#" class="list-group-item"><span class="badge">${word.value}</span>${word.key}</a>
			</c:forEach>
		</div>
	</div>
</div>
<!-- /.row -->
<div class="row">
	<div class="col-sm-12 contenido_inferior_gris">
		<!-- table -->
		<table class="table table-inferior table-hover-inferior">
			<thead>
				<tr>
					<th colspan="3" width="72%">
						<h2>Users with similar frequent words</h2>
					</th>
					<th colspan="2" width="28%">
						<h2>These users share with you</h2>
					</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td></td>
					<td>Rating</td>
					<td>Similarity</td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<c:forEach items="${users}" var="user">
					<td>
						<!-- contenido interior -->
						<div class="media">
							<a class="pull-left" href="#"> <img class="media-object" src="/resources/img/ico_userPic.png" class="img-responsive" alt="...">
							</a>
							<div class="media-body">
								<h4 class="media-heading">${user.name} ${user.surname}</h4>
								${user.institutionAffiliation} <br />
							</div>
						</div> <!-- /contenido interior -->
					</td>
					<td>
						<!-- Estrellas -->
						<div class="estrellas">
							<a href="#" data-value="1" title="1 estrellas">&#9733;</a>
							<a href="#" data-value="2" title="2 estrellas">&#9733;</a>
							<a href="#" data-value="3" title="3 estrellas">&#9733;</a>
							<a href="#" data-value="4" title="4 estrellas">&#9733;</a>
							<a href="#" data-value="5" title="5 estrellas">&#9733;</a>
						</div> <!-- /Estrellas -->
					</td>
					<td>
						<div class="progress">
							<div class="progress-bar progress-bar-warning" role="progressbar"
								aria-valuenow="${user.score}" aria-valuemin="0" aria-valuemax="100"
								style="width: ${user.score}%">
								<span><fmt:formatNumber type="percent" value="${user.score}" pattern="##.##" />%</span>
							</div>
						</div>
					</td>
					<td class="text_amarillo">
						<c:forEach items="${user.foundWords}" var="word">
							${word.key},
						</c:forEach>
					</td>
					<td><a href="/action/contact/${user.id}" class="btn btn-default btn-contacto">Contact</a>
					</td>
				</tr>
					</c:forEach>
			</tbody>
		</table>
		<!-- /table -->
	</div>
</div>
<!-- /.row -->
<!-- /Contenido Central (Find a Reviewer) -->