<script type="text/javascript">
Ext.require([
             'Ext.direct.*'
         ]);
Ext.onReady(function(){
	Ext.direct.Manager.addProvider({
		type: 'polling',
		url: 'services/user/profile/865AB735C5AE0C0E062BE9CA79064B1B',
		listeners: {
			data: function(provider, event) {
				console.log(event.data.response.name);
			}
		}
	});
});
</script>

	<!-- Contenido Central (upload document) -->
	<div class="col-sm-8 contenido_izquierdo">
		<h1>Upload your document</h1>
		<em><fmt:message key="index.document.allowed"/></em>
		<div class="row margin_top20">
			<button type="button" style="margin-left: 5%;" id="upload-btn" class="btn btn-oscuro pull-left clear" onclick="javascript:document.getElementById('fileselect').click();">Select your document</button>
		</div>
		<form id="upload" action="upload" method="POST" enctype="multipart/form-data" class="margin_top20 form-upload pull-left">
			<div id="drop">
				<input type="file" id="fileselect" name="userfile" multiple="multiple" style="display: none;"/>
			</div>
		</form>
	</div>