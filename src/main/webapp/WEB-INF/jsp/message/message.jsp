<div class="col-sm-12">
	<div class="row">
		<div class="col-sm-8 message ${message.typeMessage.css}">
			<p>${message.text}</p>
			<div class="row">
				<a class="btn btn-oscuro" href="${message.linkButton}">${message.textButton}</a>
			</div>
		</div>
	</div>
</div>