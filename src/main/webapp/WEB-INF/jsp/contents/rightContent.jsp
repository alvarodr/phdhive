<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<script type="text/javascript">
	$(document).ready(function() {
		$(".lista_lateral a").click(function(){
			$(".active").removeClass("active");
			$(this).addClass("active");
			$("div[id*=dv]").addClass("hidden");
			$("#dv" + $(this).attr("id")).removeClass("hidden");
		});
	});
</script>

<div class="col-sm-4 contenido_derecho">
	<h1>${typeContent}</h1>
	<div class="list-group lista_lateral">		
		<!-- documents option -->
		<c:forEach items="${contents}" var="content" varStatus="count">
			<c:set var="active" value=""/>
			<c:if test="${count.index==0}">
				<c:set var="active" value="active"></c:set>
			</c:if>
			<a id="${content.id}" href="javascript:void(0);" class="list-group-item ${active}">${content.title }</a>
		</c:forEach>
	</div>
</div>