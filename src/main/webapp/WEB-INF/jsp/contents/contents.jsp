<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<c:forEach items="${contents}" var="content" varStatus="count">
	<c:set var="hidden" value=""/>
	<c:if test="${count.index>0}">
		<c:set var="hidden" value="hidden"/>
	</c:if>
	<div id="dv${content.id}" class="col-sm-8 contenido_izquierdo ${hidden}">
		<div>
			<h1>${content.title}</h1>
		</div>
		${content.body}
	</div>
</c:forEach>