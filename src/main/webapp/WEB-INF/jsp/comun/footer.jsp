		<!-- Footer -->
		<div class="container navbar-fixed-bottom">
			<footer class="row">
				<p class="pull-right">
					<fmt:message key="foot.find.us"/>
					<a href="#"><img src="${pageContext.servletContext.contextPath}/resources/img/ico/fb.png" /></a>
					<a href="#"><img src="${pageContext.servletContext.contextPath}/resources/img/ico/in.png" /></a> 
<%-- 					<a href="#"><img src="${pageContext.servletContext.contextPath}/resources/img/ico/md.png" /></a>  --%>
					<a href="#"><img src="${pageContext.servletContext.contextPath}/resources/img/ico/plus.png" /></a>
				</p>
                <p><fmt:message key="foot.copyright"/> <a href="${pageContext.servletContext.contextPath}/action/contents/ABOU"><fmt:message key="foot.about"/></a> &middot; <a href="${pageContext.servletContext.contextPath}/action/contents/PEOPL"><fmt:message key="foot.people"/></a> &middot; <a href="${pageContext.servletContext.contextPath}/action/contents/USE"><fmt:message key="foot.terms.use"/></a></p>
                </footer>
		</div>