<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<tiles:importAttribute name="option" ignore="true"/>

<div class="col-sm-4 contenido_derecho">
	<h1>My Profile</h1>
	<div class="list-group lista_lateral">
		<!-- profile option -->
		<c:choose>
			<c:when test="${option == 'profile'}">
				<a href="/action/profile" class="list-group-item active">My Profile</a>
			</c:when>
			<c:otherwise>
				<a href="/action/profile" class="list-group-item">My Profile</a>
			</c:otherwise>
		</c:choose>
		
		<!-- documents option -->
		<c:choose>
			<c:when test="${option == 'documents'}">
				<a href="/action/documents" class="list-group-item active">My Documents</a>
			</c:when>
			<c:otherwise>
				<a href="/action/documents" class="list-group-item">My Documents</a>
			</c:otherwise>
		</c:choose>
		
		<!-- events option -->
		<c:choose>
			<c:when test="${option == 'events'}">
				<a href="/action/events" class="list-group-item active">My Events</a>
			</c:when>
			<c:otherwise>
				<a href="/action/events" class="list-group-item">My Events</a>
			</c:otherwise>
		</c:choose>
	</div>
</div>