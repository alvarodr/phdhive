<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<tiles:importAttribute name="horizontalOption" ignore="true"/>

<security:authorize access="isAuthenticated()">
	<common:comboDataMaster var="typeContents" type="TYPES_CONTENT_TYPES"/>
	<security:authentication var="phduser" scope="page" property="user" />
	<phdhive:userDocuments var="documents"/>
	<phdhive:userKeywords var="keywords"/>
	<nav class="navbar navbar-inverse" role="navigation">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
				<span class="sr-only">Toggle navigation</span> 
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a href="${pageContext.servletContext.contextPath}"><img src="${pageContext.servletContext.contextPath}/resources/img/logo.png" class="logo img-responsive" /></a>
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse navbar-ex1-collapse">
		
			<ul class="nav navbar-nav navbar-right">
<!-- 				<li> -->
<%-- 					<c:choose> --%>
<%-- 						<c:when test="${not empty typeContent && typeContent == 'About'}"> --%>
<%-- 							<a href="${pageContext.servletContext.contextPath}/action/contents/ABOU" class="active"><common:dataMaster codigo="ABOU"/></a> --%>
<%-- 						</c:when> --%>
<%-- 						<c:otherwise> --%>
<%-- 							<a href="${pageContext.servletContext.contextPath}/action/contents/ABOU"><common:dataMaster codigo="ABOU"/></a> --%>
<%-- 						</c:otherwise> --%>
<%-- 					</c:choose> --%>
<!-- 				</li> -->
				<li>
					<c:choose>
						<c:when test="${horizontalOption == 'findReviewer'}">
							<a href="${pageContext.servletContext.contextPath}/action/index" class="active">Find a Reviewer</a>
						</c:when>
						<c:otherwise>
							<a href="${pageContext.servletContext.contextPath}/action/index">Find a Reviewer</a>
						</c:otherwise>
					</c:choose>
				</li>
				<li>
					<c:choose>
						<c:when test="${horizontalOption == 'becomeReviewer'}">
							<a href="${pageContext.servletContext.contextPath}/action/contents/REV" class="active">Become a Reviewer</a>
						</c:when>
						<c:otherwise>
							<a href="${pageContext.servletContext.contextPath}/action/contents/REV">Become a Reviewer</a>
						</c:otherwise>
					</c:choose>
				</li>
<!-- 				<li class="dropdown"> -->
<!-- 					<a href="#" class="dropdown-toggle" data-toggle="dropdown">Related Contents<b class="caret"></b></a> -->
<!-- 					<ul class="dropdown-menu submenu"> -->
<%-- 						<c:forEach items="${typeContents}" var="typeContent"> --%>
<%-- 							<li><a href="${pageContext.servletContext.contextPath}/action/contents/${typeContent.code}">${typeContent.description}</a></li> --%>
<%-- 						</c:forEach> --%>
<!-- 					</ul> -->
<!-- 				</li> -->
				<li class="dropdown">
					<a href="#" class="dropdown-toggle"	data-toggle="dropdown">
						<span class="user"><i class="glyphicon glyphicon-user"></i> ${phduser.name} ${phduser.surname} <b class="caret"></b></span>
					</a>
					<ul class="dropdown-menu submenu">
						<li><a href="#collapse1" class="nav-toggle">Overview</a></li>
						<li><a href="${pageContext.servletContext.contextPath}/action/profile">My profile</a></li>
						<li><a href="${pageContext.servletContext.contextPath}/action/events" class="nav-toggle">My events</a></li>
						<li><a href="${pageContext.servletContext.contextPath}/action/documents" class="nav-toggle">My documents</a></li>
						<li><a href="${pageContext.servletContext.contextPath}/action/logout">Logout</a></li>
					</ul>
				</li>
			</ul>
		</div>
		<!-- /.navbar-collapse -->
	</nav>

	<!-- menu aparece -->
	<div id="collapse1" style="display: none">
		<div class="row">
			<div class="col-sm-12">
				<!-- <a type="button" class="close text-amarillo" aria-hidden="true" style="color: #fff">&times;</a> -->
				<a href="#collapse1" type="button" class="nav-toggle btn btn-xs pull-right btn-cerrarSubmenu">
					<i class="glyphicon glyphicon-remove"></i> Close
				</a>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-3">
				<ul>
					<li><h2><i class="glyphicon glyphicon-user"></i>${phduser.name} ${phduser.surname}</h2></li>
					<li>${phduser.institutionAffiliation}</li>
					<li>${phduser.email}</li>
					<li>Overall rating</li>
					<li>
						<!-- Estrellas -->
						<div class="estrellas">
							<a href="#" data-value="1" title="1 estrellas">&#9733;</a>
							<a href="#" data-value="2" title="2 estrellas">&#9733;</a>
							<a href="#" data-value="3" title="3 estrellas">&#9733;</a>
							<a href="#" data-value="4" title="4 estrellas" >&#9733;</a>
							<a href="#" data-value="5" title="5 estrellas">&#9733;</a>
						</div> <!-- /Estrellas -->
					</li>
				</ul>
			</div>
			<div class="col-sm-3">
				<ul>
					<li><h2 class="text_amarillo">Recent submissions</h2></li>
					<c:forEach items="${documents}" var="document">
					<li><i class="glyphicon glyphicon-file"></i>${document}</li>
					</c:forEach>
					<li><a href="${pageContext.servletContext.contextPath}/action/documents" class="btn_submenu">more...</a></li>
				</ul>
			</div>
			<div class="col-sm-3">
				<ul>
					<li><h2 class="text_amarillo">Recent events submissions</h2></li>
					<c:forEach items="${phduser.eventsNames}" var="evt">
					<li><i class="glyphicon glyphicon-file"></i>${evt}</li>
					</c:forEach>
					<li><a href="${pageContext.servletContext.contextPath}/action/events" class="btn_submenu">more...</a></li>
				</ul>
			</div>
			<div class="col-sm-3">
				<ul>
					<li><h2 class="text_amarillo">Word frequency</h2></li>
					<c:forEach items="${keywords}" var="keyword">
						<li>${keyword.key} (${keyword.value})</li>
					</c:forEach>
				</ul>
			</div>
		</div>
	</div>
	<!-- /menu aparece -->
</security:authorize>

<security:authorize access="!isAuthenticated()">
	<nav class="navbar navbar-inverse" role="navigation">
		<div class="navbar-header">
			<a href="${pageContext.servletContext.contextPath}"><img src="${pageContext.servletContext.contextPath}/resources/img/logo.png" class="logo img-responsive" /></a>
		</div>
	</nav>
</security:authorize>