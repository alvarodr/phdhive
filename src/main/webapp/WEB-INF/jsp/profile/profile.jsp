<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<phdhive:disciplines var="disciplines"/>

<script type="text/javascript">
<!--
	var disciplines = [
		<c:forEach items="${disciplines}" var="discipline">
			{
				value: "${discipline.key}"
				,label: "${discipline.value}"
			},
		</c:forEach>
    ];
//-->
</script>
<script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/profile/profile.js"></script>
<div class="contenido_total">
	<div class="col-sm-8 contenido_izquierdo">
		<h1>My profile</h1>
		<div id="dvDetail" class="row ${edit?'hidden':''}">
			<div class="col-sm-4 lista_blanca">
				<ul class="list-group lista_lateral">
					<!-- <ul> -->
					<li>
						<a href="#">${userCommand.name} ${userCommand.surname}</a>
					</li>
					<li>
						<a href="#">${userCommand.institutionAffiliation}</a>
					</li>
					<li>
						<a href="#">${userCommand.email}</a>
					</li>
					<li>
						<c:forEach items="${userCommand.disciplines}" var="discipline" varStatus="count">
							<span class="btn btn-tag"><common:dataMaster codigo="${discipline}"/></span>
						</c:forEach>
					</li>
				</ul>
			</div>
			<div class="col-sm-8">
				<p>${userCommand.biography}</p>
			</div>
		</div>
		<div id="dvEdit" class="form-group row ${edit?'':'hidden'}">
			<form:form id="profileForm" commandName="userCommand" role="form" class="margin_top20" method="post" name="profileForm" data-validate="parsley">
				<div class="col-sm-4">
					<form:input path="name" type="text" class="form-control margin_top10" placeholder="name" data-required="true" />
					<form:input path="surname" type="text" class="form-control margin_top10" placeholder="surname" data-required="true"/>
					<form:input path="institutionAffiliation" type="text" class="form-control margin_top10" placeholder="institution affiliation" />
					<form:input path="descriptionDisciplines" type="text" class="form-control margin_top10" placeholder="select your disciplines" data-provide="typeahead"/>
					<form:hidden path="disciplines"/>
					<div id="disciplinesTags"></div>
				</div>
				<div class="col-sm-8">
					<form:textarea path="biography" class="form-control margin_top10" rows="15" placeholder="Biography" data-required="true" data-trigger="change" data-minlength="4"/>
				</div>		
			</form:form>
		</div>
		<button id="buttonEdit" class="btn btn-oscuro pull-right">Edit</button>
		<button id="buttonSave" class="btn btn-oscuro pull-right ${edit?'':'hidden'}">Save</button>
		<button id="buttonBack" class="btn btn-oscuro pull-left ${edit?'':'hidden'}">Cancel</button>
	</div>
</div>