<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<script type="text/javascript">
	$(document).ready(function() {
		$("a[id*=id_del_]").click(function(){
			$("#delete").removeClass("hidden");
			$(".lbox-text").find("strong").html($(this).attr("name"));
			id = $(this).attr("id");
			$("#delete").dialog({
				modal: true,
				rezisable: true,
				buttons:[
		         {
		        	 text: 'Accept',
		        	 class: 'leftButton btn btn-oscuro',
		        	 click: function() {
		        		 callDeleteDocument($(this), id.substring(7, id.length));
		        	 }
		         },
		         {
		        	 text: 'Reject',
		        	 class: 'floatRight btn btn-oscuro',
		        	 click: function() {
		        		 $("#delete").addClass("hidden");
		        		 $(this).dialog('close');
		        	 }
		         }
	         	]
			});
		});
	});
	
	function callDeleteDocument(e, id) {
        $.ajax({
			data:  {
				id: id
			},
			url:   contextPath + '/action/documents/delete',
			type:  'post',
			success: function (data) {
				if (data) {
					document.location.href=contextPath + '/action/events/${event.id}/documents/upload';
				}
				$(e).dialog('close');
			}
		});
	}
</script>

<!-- Constants -->
<common:constant tipo="com.phdhive.archetype.dto.master.types.TypeDocumentMetaData" var="TypeDocumentMetaData"/>


	<!-- Contenido Central (upload document) -->
	<div class="col-sm-8 contenido_izquierdo">
		<h1>${event.name}</h1>
		<em>${event.description}</em>
		<table class="form-table">
			<tr>
				<td class="table-label"><strong>Submission Date:</strong></td>
				<td><fmt:formatDate timeZone="" pattern="dd MMM yyyy" value="${event.startDate}" /></td>
			</tr>
			<tr>
				<td class="table-label"><strong>Convener:</strong></td>
				<td>${event.owner.name}</td>
			</tr>
			<tr>
				<td class="table-label"><strong>Participants:</strong></td>
				<td>${fn:length(event.users)}</td>
			</tr>
			<tr>
				<td class="table-label"><strong>Event ID:</strong></td>
				<td>${event.id}</td>
			</tr>
			<tr>
				<td class="table-label"><strong>Language:</strong></td>
				<td><common:dataMaster codigo="${event.language}"/></td>
			</tr>
		</table>
		
		<h2>Documents submitted</h2>
		<c:if test="${fn:length(documents) eq 0}">
			<em><fmt:message key="index.document.allowed"/></em>
			<button type="button" style="margin-left: 5%;" id="upload-btn" class="btn btn-oscuro" onclick="javascript:document.getElementById('fileselect').click();">Upload</button>
		</c:if>
		<!-- table -->
		<table class="table table-blanca table-hover-blanca">
			<thead>
				<tr>
					<th colspan="2">Title</th>
					<th>Date</th>
					<th class="text-center">Action</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${documents}" var="document">
				<tr>
					<td><i class="glyphicon glyphicon-file"></i></td>
					<td>${document.documentName}</td>
					<td>${document.metaData[TypeDocumentMetaData.METADATA_DATE]}</td>
					<td class="text-center">
						<a href="/action/document/download/${document.id}"><img src="/resources/img/cloudDownload_20.png"/></a>
						<a id="id_del_${document.id}" name="${document.documentName}" href="javascript:void(0);"><img alt="Delete this document"  src="/resources/img/deleteRed_16.png"/></a>
						<a id="id_go_${document.id}" name="${document.documentName}" href="/action/events/${event.id}/documents/${document.id}"><img alt="Go to result of document" src="/resources/img/go_16.png"/></a>
					</td>
				</tr>
				</c:forEach>
			</tbody>
		</table>		
		
		<c:if test="${fn:length(documents) eq 0}">
			<c:set var="url" value="/action/events/${event.id}/documents/upload"/>
			<form id="upload" action="${url}" method="POST" enctype="multipart/form-data" class="hidden">
				<fieldset>
					<div>
						<input type="file" id="fileselect" name="userfile" multiple="multiple" style="display: none;"/>
					</div>
			
					<div id="submitbutton" class="hidden">
						<button type="submit">Upload Files</button>
					</div>
				</fieldset>
			</form>
		</c:if>
	</div>

	<div id="delete" title="Confirmation" class="hidden">
		<p class="lbox-text"><fmt:message key="document.msg.confirm"/></p>
	</div>

	<script src="${pageContext.servletContext.contextPath}/resources/js/uploadfile.js" type="text/javascript"></script>