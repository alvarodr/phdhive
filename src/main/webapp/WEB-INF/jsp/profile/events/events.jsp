<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!-- Contenido Central (Find a Reviewer) -->
<div class="col-sm-8 contenido_izquierdo">
	<h1>My Events Submitted</h1>
	<!-- table -->
	<table class="table table-blanca table-hover-blanca">
		<thead>
			<tr>
				<th colspan="2">Event name</th>
				<th>Community</th>
				<th>Status</th>
				<th>Users</th>
				<th>Start date</th>
				<th>End date</th>
				<th class="text-center">Action</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${events}" var="event">
			<tr>
				<td><i class="glyphicon glyphicon-file"></i>
				</td>
				<td>${event.name}</td>
				<td>${event.owner.name}</td>
				<td>--</td>
				<td>${fn:length(event.users)}</td>
				<td><fmt:formatDate value="${event.startDate}" pattern="dd/MM/yyyy HH:mm"/></td>
				<td><fmt:formatDate value="${event.endDate}" pattern="dd/MM/yyyy HH:mm"/></td>
				<td class="text-center">
					<a id="id_del_${event.id}" name="${event.name}" href="javascript:void(0);"><img alt="Unlink to this event"  src="/resources/img/deleteRed_16.png"/></a>
					<a id="id_go_${event.id}" name="${event.name}" href="/action/events/${event.id}/documents/upload"><img alt="Go to this event"  src="/resources/img/go_16.png"/></a>
				</td>
			</tr>
			</c:forEach>
		</tbody>
	</table>
</div>

<!-- lightbox confirmation unlink -->
<div id="delete" title="Confirmation" class="hidden">
	<p class="lbox-text"><fmt:message key="document.msg.confirm"/></p>
</div>