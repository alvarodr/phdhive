<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!-- Constants -->
<common:constant tipo="com.phdhive.archetype.dto.master.types.TypeDocumentMetaData" var="TypeDocumentMetaData"/>

<script type="text/javascript">
	$(document).ready(function() {
		$("a[id*=id_del_]").click(function(){
			$("#delete").removeClass("hidden");
			$(".lbox-text").find("strong").html($(this).attr("name"));
			id = $(this).attr("id");
			$("#delete").dialog({
				modal: true,
				rezisable: true,
				buttons:[
		         {
		        	 text: 'Accept',
		        	 class: 'leftButton btn btn-oscuro',
		        	 click: function() {
		        		 callDeleteDocument($(this), id.substring(7, id.length));
		        	 }
		         },
		         {
		        	 text: 'Reject',
		        	 class: 'floatRight btn btn-oscuro',
		        	 click: function() {
		        		 $("#delete").addClass("hidden");
		        		 $(this).dialog('close');
		        	 }
		         }
	         	]
			});
		});
	});
	
	function callDeleteDocument(e, id) {
        $.ajax({
			data:  {
				id: id
			},
			url:   contextPath + '/action/documents/delete',
			type:  'post',
			success: function (data) {
				if (data) {
					document.location.href=contextPath + '/action/documents';
				}
				$(e).dialog('close');
			}
		});
	}
</script>

<!-- Contenido Central (Find a Reviewer) -->
<div class="col-sm-8 contenido_izquierdo">
	<h1>My Documents</h1>
<!-- </div> -->

		<!-- table -->
		<table class="table table-blanca table-hover-blanca">
			<thead>
				<tr>
					<th colspan="2">Title</th>
					<th>Event</th>
					<th>Date</th>
					<th class="text-center">Action</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${documents}" var="document">
				<tr>
					<td><i class="glyphicon glyphicon-file"></i>
					</td>
					<td>
						${document.documentName }
					</td>
					<td>
						<c:set var="event" value="${document.metaData[TypeDocumentMetaData.METADATA_EVENT]}"/>
						<c:choose>
							<c:when test="${not empty event}">
								${event}
							</c:when>
							<c:otherwise>--</c:otherwise>
						</c:choose>
					</td>
					<td>
						${document.metaData[TypeDocumentMetaData.METADATA_DATE]}
					</td>
					<td class="text-center">
						<a href="/action/document/download/${document.id}"><img src="/resources/img/cloudDownload_20.png"/></a>
						<a id="id_del_${document.id}" name="${document.documentName}" href="javascript:void(0);"><img alt="Delete this document"  src="/resources/img/deleteRed_16.png"/></a>
						<a id="id_go_${document.id}" name="${document.documentName}" href="/action/documents/${document.id}"><img alt="Go to result of document" src="/resources/img/go_16.png"/></a>
					</td>
				</tr>
				</c:forEach>
			</tbody>
		</table>
</div>
	<div id="delete" title="Confirmation" class="hidden">
		<p class="lbox-text"><fmt:message key="document.msg.confirm"/></p>
	</div>