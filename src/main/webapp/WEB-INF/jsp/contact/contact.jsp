<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!-- Contenido Central (Notify User) -->
<div class="contenido_total">
	<div class="col-sm-12">
		<h1><fmt:message key="contact.title"/></h1>
		<em><fmt:message key="contact.description"/></em>
		<!-- formulario -->
		<form:form role="form" action="/action/contact" class="margin_top20" id="demo-form" name="demo-form" commandName="contactCommand" method="POST" data-validate="parsley">
			<form:hidden path="userTo"/>
			<div class="form-group">
				<form:input path="subject" class="form-control" placeholder="Subject" id="subject" data-required="true" data-trigger="change"/>
				<p class="help-block"><fmt:message key="contact.submitted"/></p>
			</div>
			<form:textarea path="message" class="form-control" rows="7" placeholder="Message" data-required="true" data-trigger="change" data-minlength="4"></form:textarea>
			<button type="submit" class="btn btn-oscuro margin_top20 pull-right">Submit</button>
		</form:form>
		<!-- /formulario -->
	</div>
</div>