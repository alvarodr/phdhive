<div class="contenido_total">
	<div class="col-sm-12">
		<h1>Renew Password</h1>
		<div class="row">
			<div class="col-sm-8">
				<em>You will receive an email with the instructions</em>
				<!-- formulario -->
				<form:form role="form" class="margin_top20" id="demo-form" name="demo-form" commandName="recoveryPassCommand" data-validate="parsley">
					<div class="form-group row">
						<div class="col-sm-8">
							<form:input path="email" id="email" type="text" class="form-control margin_top10" placeholder="e-mail" data-required="true" data-type="email" data-trigger="change"/>
							<form:input path="cemail" type="text" class="form-control margin_top10" placeholder="Repeat email" data-equalto="#email" data-required="true"  data-type="email" data-trigger="change" />
						</div>	
					</div>
					<!-- form-group row -->
					<button type="submit" class="btn btn-oscuro">Send</button>
				</form:form>
				<!-- /formulario -->
			</div>
		</div>
	</div>
</div>
