<%@tag body-content="empty" pageEncoding="UTF-8" description="Pone los js compartidos"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@include file="/WEB-INF/includes/prefix.jspf"%>

<%-- AQUI VAN LOS RECURSOS REQUERIDOS POR LA APLICACION --%>
<script src="${pageContext.servletContext.contextPath}/resources/js/ExtJS/4.2/bootstrap.js" type="text/javascript" charset="UTF-8"></script>
<script src="${pageContext.servletContext.contextPath}/resources/js/jQuery/jquery-2.0.3.min.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/resources/js/jQuery/jquery-ui.min.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/resources/js/jQuery/jquery.knob.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/resources/js/jQuery/jquery.ui.widget.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/resources/js/jQuery/jquery.iframe-transport.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/resources/js/jQuery/jquery.fileupload.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/resources/js/bootstrap.min.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/resources/js/functions.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/resources/js/parsley.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/resources/js/jQuery/jquery.awesomeCloud-0.2.min.js" type="text/javascript"></script>
<%-- <script src="${pageContext.servletContext.contextPath}/resources/js/jQuery/bootstrap3-typeahead.min.js" type="text/javascript"></script> --%>

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<script src="https://raw.github.com/scottjehl/Respond/master/respond.min.js"></script>
<![endif]-->