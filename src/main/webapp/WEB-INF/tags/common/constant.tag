<%@tag import="org.apache.commons.lang.StringUtils"%>
<%@tag body-content="empty" pageEncoding="UTF-8" description="Publica los valores de una clase de constantes o enumerados"
%><%@attribute name="tipo" required="true" type="java.lang.String" rtexprvalue="true" description="El nombre de la clase a publicar"
%><%@attribute name="var" required="false" type="java.lang.String" rtexprvalue="true" description="El nombre del bean a exportar (Por defecto el nembre de la clase)"
%><%@tag import="java.util.*,org.springframework.web.context.*,org.springframework.web.servlet.support.RequestContextUtils,com.phdhive.archetype.components.*"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><c:set scope="page" var="ignore">
<%
	// NO TOCAR NI FORMATEAR ESTE TAG.
	// Los cierres de etiquetas a principio de linea son para evitar la aparicion de saltos de linea en el contenido 
	// que pinta el tag
	
	WebApplicationContext ctx = RequestContextUtils.getWebApplicationContext(request, application);
	IConstantsCache cache = (IConstantsCache) ctx.getBean(IConstantsCache.class);

	Map<String,Object> map = cache.constantsFrom(tipo) ;
	
	String name ;
	if(StringUtils.isNotBlank(var)) {
		name = var.trim() ;
	} else {
		int pos = tipo.lastIndexOf('.') ;
		if(pos == -1) {
			name = tipo ;
		} else {
			name = tipo.substring(pos +1) ;
		}
	}
	request.setAttribute(name, map) ;
%>
</c:set>