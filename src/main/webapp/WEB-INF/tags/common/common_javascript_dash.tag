<%@tag body-content="empty" pageEncoding="UTF-8" description="Pone los js compartidos"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@include file="/WEB-INF/includes/prefix.jspf"%>

<%-- AQUI VAN LOS RECURSOS REQUERIDOS POR LA APLICACION --%>
<script src="${pageContext.request.contextPath}/resources/js/jQuery/jquery-1.6.2.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resources/js/ExtJS/4.2/bootstrap.js" type="text/javascript" charset="UTF-8"></script>
<script src="${pageContext.request.contextPath}/resources/js/ckeditor/ckeditor.js" type="text/javascript"></script>

<%-- Plugins --%>
<script src="${pageContext.request.contextPath}/resources/js/ExtJS/4.2/plugins/CKEditor.js" type="text/javascript" charset="UTF-8"></script>
<script src="${pageContext.request.contextPath}/resources/js/ExtJS/4.2/plugins/BoxSelect.js" type="text/javascript" charset="UTF-8"></script>