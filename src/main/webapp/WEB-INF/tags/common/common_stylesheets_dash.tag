<%@tag body-content="empty" pageEncoding="UTF-8" description="Pone los css compartidos"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@include file="/WEB-INF/includes/prefix.jspf"%>


<%-- AQUI VAN LOS RECURSOS REQUERIDOS POR LA APLICACION --%>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/ext-theme-neptune-all-debug.css" />

<!-- Selected combo -->
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/BoxSelect.css" />

<!-- CSS Custom Hive -->
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/ext-theme-hive-all.css" />

<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/css/bootstrap.css"/>
<%-- <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/css/style.css" media="screen"/> --%>