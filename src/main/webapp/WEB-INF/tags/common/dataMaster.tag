<%@tag body-content="empty" pageEncoding="UTF-8" description="Obtiene el valor de un dato maestro"
%><%@attribute name="codigo" required="true" type="java.lang.String" rtexprvalue="true" description="El codigo del dato maestro a devolver dentro de su tipo concreto"
%><%@attribute name="defaultValue" required="false" type="java.lang.String" rtexprvalue="true" description="El valor a devolver si no se encuentra el codigo"
%><%@attribute name="escapeXml" required="false" type="java.lang.Boolean" description="Si hay que escapar el valor de Xml"
%><%@tag import="java.util.*,org.springframework.web.context.*,org.springframework.web.context.support.*,com.phdhive.archetype.dto.master.types.TypeDataMaster,com.phdhive.archetype.service.IDataMasterService,com.phdhive.archetype.dto.master.DataMaster"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><c:set scope="page" var="ignore">
<%
	// NO TOCAR NI FORMATEAR ESTE TAG.
	// Los cierres de etiquetas a principio de linea son para evitar la aparicion de saltos de linea en el contenido 
	// que pinta el tag
	
	// El siguiente codigo obtiene la configuracion de la aplicacion
	WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(application);
	
	String val = codigo;
	if ((defaultValue != null) && (!defaultValue.trim().equals(""))) {
		val = defaultValue;
	}

	// Obtenemos el valor de los datos maestros
	IDataMasterService dataMasterService = (IDataMasterService) ctx.getBean(IDataMasterService.class);
	
	DataMaster dataMaster = dataMasterService.getDataMasterByCode(val);
	if (dataMaster != null) {
		val = dataMaster.getDescription();
	}
	
	jspContext.setAttribute("__val", val) ;
%>
<c:if test="${! empty escapeXml}"><c:set scope="page" var="__val"><c:out value="${__val}" escapeXml="${escapeXml}"/></c:set></c:if>
</c:set>${__val}