<%@tag body-content="empty" pageEncoding="UTF-8" description="Obtiene el valor de un dato maestro"
%><%@attribute name="type" required="true" type="java.lang.String" rtexprvalue="true" description="El codigo del dato maestro a devolver dentro de su tipo concreto"
%><%@attribute name="var" required="true" type="java.lang.String" rtexprvalue="true" description="El valor a devolver si no se encuentra el codigo"
%><%@attribute name="escapeXml" required="false" type="java.lang.Boolean" description="Si hay que escapar el valor de Xml"
%><%@tag import="java.util.*,org.springframework.web.context.*,org.springframework.web.context.support.*,com.phdhive.archetype.dto.master.types.TypeDataMaster,com.phdhive.archetype.service.IDataMasterService,com.phdhive.archetype.dto.master.DataMaster,org.apache.commons.lang.StringUtils,com.phdhive.archetype.dto.master.ITypeDataMaster"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><c:set scope="page" var="ignore">
<%
	// NO TOCAR NI FORMATEAR ESTE TAG.
	// Los cierres de etiquetas a principio de linea son para evitar la aparicion de saltos de linea en el contenido 
	// que pinta el tag
	
	String name ;
	if(StringUtils.isNotBlank(var)) {
		name = var.trim() ;
	} else {
		int pos = type.lastIndexOf('.') ;
		if(pos == -1) {
			name = type;
		} else {
			name = type.substring(pos +1) ;
		}
	}

	if (request.getAttribute(name) == null) {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(application);
		
		// Obtenemos el valor de los datos maestros
		IDataMasterService dataMasterService = (IDataMasterService) ctx.getBean(IDataMasterService.class);
		
		Collection<DataMaster> data = dataMasterService.findByType(TypeDataMaster.valueOf(type));
		
		request.setAttribute(name, data);
	}
%>
</c:set>