/**
 * Controlador para la gestión de contenidos
 * 
 * @author ADORU3N
 */
Ext.define('phdhive.controller.Content', {
	extend: 'Ext.app.Controller',
	
	models: ['Content'],
	views: ['contents.Content', 'contents.Panel', 'contents.add.Content','contents.add.RelatedContent',
	        'contents.add.RelatedContentPanel','contents.add.ContentAuxPanel','contents.add.ContentAux'],
	stores: ['Content','ComboContentType','RelatedContent'],
	
	init: function(){
		this.control({
			'contentList button[id=addContent]': {
				click: function(){
					Ext.widget('newContent').show();
				}
			},
			
			'contentList button[id=editContent]': {
				click: function() {
					selection = Ext.getCmp('gridContent').getSelectionModel().getSelection();
					if (selection.length==1){
						Ext.widget('newContent').show();
						Ext.getCmp('formNewContent').getForm().load({
							url: contextPath + '/action/admin/contents/load',
							method: 'GET',
							waitMsg: 'Loading...',
							waitTitle: 'Wait',
							params: {
								id: selection[0].get("id")
							}
						});
					}else{
						Ext.Msg.show({
							title: "Error",
							msg: 'You must select one',
							buttons: Ext.MessageBox.OK,
							icon: Ext.MessageBox.ERROR
						});
					}

				}
			},
			
		    'contentList button[id=removeContent]': {
			   click: function(){
				selection = Ext.getCmp('gridContent').getSelectionModel().getSelection();
				if (selection.length==1){
					Ext.Msg.show({
						title: 'Confirm',
						msg: 'Do you want remove this ?',
						buttons: Ext.MessageBox.OKCANCEL,
						fn: function(btn){
							if (btn=='ok'){
								Ext.Ajax.request({
									url: contextPath + '/action/admin/contents/remove',
									method: 'POST',
									params: {										
										id: selection[0].get("id")
									},
									success: function(){
										Ext.getCmp('gridContent').getStore().load();
										Ext.Msg.show({
											title: "Information",
											msg: 'The content has been successfully deleted',
											buttons: Ext.MessageBox.OK,
											icon: Ext.MessageBox.INFO
										});								
									}
								});
							}
						},
						icon: Ext.MessageBox.QUESTION
					
					});
				} else {
					Ext.Msg.show({
						title: "Error",
						msg: 'You must select one',
						buttons: Ext.MessageBox.OK,
						icon: Ext.MessageBox.ERROR
					});
				}
			}
		},
		
		'relatedContentList button[id=addRelatedContent]': {
				click: function(){
					Ext.widget('contentListAuxPanel').show();
				}
		},		
		
		'relatedContentList button[id=removeRelatedContent]': {
			click: function(){				
				selection = Ext.getCmp('gridRelatedContent').getSelectionModel().getSelection();
				if (selection.length==1){
					var values = Ext.getCmp('formNewContent').getForm().getValues();
					var relAux = values.relatedContents;
					var selectedRef =  selection[0].data.relContentRef;
					relAux = relAux.replace(selectedRef+";","");	
					values.relatedContents=relAux;
					Ext.getCmp('formNewContent').getForm().setValues(values);
				} else {
					Ext.Msg.show({
						title: "Error",
						msg: 'You must select one',
						buttons: Ext.MessageBox.OK,
						icon: Ext.MessageBox.ERROR
					});
				}
			}
	    }
		
		});
	}

});
