/**
 * Gestión de usuarios
 * 
 * @author Álvaro
 */
Ext.define('phdhive.controller.Users', {
	extend: 'Ext.app.Controller',
	
	views:['user.User', 'user.Panel', 'user.filter.User', 'user.filter.ButtonSearch'],
	stores:['User', 'ComboCountries', 'ComboAreas','ComboDisciplines','ComboCommunities','ComboEvents'],
	models: ['User', 'Combo'],
	
	init: function(){
		this.control({
			'searchUser combobox[id="comboAreas"]': {
				select: function(e){
					this.getStore('ComboDisciplines').load({
						params: {type: e.value}
					});
				}
			},
			
			'searchUser buttonSearch': {
				click: function(){
					this.getStore("User").load({params: Ext.getCmp("formSearch").getForm().getValues()});
				}
			},
			
		    'userList button[id=removeUser]': {
				   click: function(){
					selection = Ext.getCmp('gridUsers').getSelectionModel().getSelection();
					if (selection.length==1){
						Ext.Msg.show({
							title: 'Confirm',
							msg: 'Do you want remove this ?',
							buttons: Ext.MessageBox.OKCANCEL,
							fn: function(btn){
								if (btn=='ok'){
									Ext.Ajax.request({
										url: contextPath + '/action/admin/removeUser',
										method: 'POST',
										params: {										
											id: selection[0].get("id")
										},
										success: function(){
											Ext.getCmp('gridUsers').getStore().load();
											Ext.Msg.show({
												title: "Information",
												msg: 'The user has been successfully deleted',
												buttons: Ext.MessageBox.OK,
												icon: Ext.MessageBox.INFO
											});								
										}
									});
								}
							},
							icon: Ext.MessageBox.QUESTION
						
						});
					} else {
						Ext.Msg.show({
							title: "Error",
							msg: 'You must select one',
							buttons: Ext.MessageBox.OK,
							icon: Ext.MessageBox.ERROR
						});
					}
				}
			},
			'userList button[id=export]': {
				click: function(){
					country = Ext.getCmp("comboCountry").value;
					area = Ext.getCmp("comboAreas").value;
					discipline = Ext.getCmp("comboDisciplines").value;
					community = Ext.getCmp("comboCommunity").value;
					event = Ext.getCmp("comboEvent").value;
					url = contextPath + '/action/admin/users/excel?country='+country+'&area='+area+'&discipline='+discipline+'&community='+community+'&event='+event;
					Ext.DomHelper.append(document.body, {
						tag: 'iframe',
						id:'exportIframe',
						frameBorder: 0,
						width: 0,
						height: 0,
						css: 'display:none;visibility:hidden;height:0px;',			          
						src: url
					});
				}			
			},
		    'userList button[id=activation]': {
				   click: function(){
					selection = Ext.getCmp('gridUsers').getSelectionModel().getSelection();
					if (selection.length==1){
						Ext.Msg.show({
							title: 'Confirm',
							msg: 'Do you want update the user?',
							buttons: Ext.MessageBox.OKCANCEL,
							fn: function(btn){
								if (btn=='ok'){
									Ext.Ajax.request({
										url: contextPath + '/action/admin/lockUser',
										method: 'POST',
										params: {										
											id: selection[0].get("id")
										},
										success: function(){
											Ext.getCmp('gridUsers').getStore().load();
											Ext.Msg.show({
												title: "Information",
												msg: 'The user has been successfully updated',
												buttons: Ext.MessageBox.OK,
												icon: Ext.MessageBox.INFO
											});								
										}
									});
								}
							},
							icon: Ext.MessageBox.QUESTION
						
						});
					} else {
						Ext.Msg.show({
							title: "Error",
							msg: 'You must select one',
							buttons: Ext.MessageBox.OK,
							icon: Ext.MessageBox.ERROR
						});
					}
				}
			}
		});
	}
});