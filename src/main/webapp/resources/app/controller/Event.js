/**
 * Controlador para la gestión de eventos
 * 
 * @author ADORU3N
 */
Ext.define('phdhive.controller.Event', {
	extend: 'Ext.app.Controller',
	
	views: ['events.Event', 'events.Panel', 'events.add.Event','events.Invitation'],
	models: ['Event', 'Combo'],
	stores: ['Event', 'ComboCommunities', 'ComboLanguage'],
	
	init: function(){
		this.control({
			'eventsList button[id=addEvent]': {
				click: function(){
					Ext.widget('newEvent').show();
				}
			},
			'newEvent combobox': {
				render: function(e){
					e.getStore().load();
				}
			},
			'eventsList button[id=editEvent]': {
				click: function() {
					selection = Ext.getCmp('gridEvent').getSelectionModel().getSelection();
					if (selection.length==1){
						Ext.widget('newEvent').show();
						Ext.getCmp('formNewEvent').getForm().load({
							url: contextPath + '/action/admin/events/load',
							method: 'GET',
							waitMsg: 'Loading...',
							waitTitle: 'Wait',
							params: {
								id: selection[0].get("id")
							}
						});
					}else{
						Ext.Msg.show({
							title: "Error",
							msg: 'You must select one',
							buttons: Ext.MessageBox.OK,
							icon: Ext.MessageBox.ERROR
						});
					}

				}
			},
			'eventsList button[id=removeEvent]': {
				click: function(){
					selection = Ext.getCmp('gridEvent').getSelectionModel().getSelection();
					if (selection.length==1){
						Ext.Msg.show({
							title: 'Confirm',
							msg: 'Do you want remove this Event?',
							buttons: Ext.MessageBox.OKCANCEL,
							fn: function(btn){
								if (btn=='ok'){
									Ext.Ajax.request({
										url: contextPath + '/action/admin/events/remove',
										method: 'POST',
										params: {
											id: selection[0].get("id")
										},
										success: function(){
											Ext.getCmp('gridEvent').getStore().load();
											Ext.Msg.show({
												title: "Information",
												msg: 'The event has been successfully deleted',
												buttons: Ext.MessageBox.OK,
												icon: Ext.MessageBox.INFO
											});								
										}
									});
								}
							},
							icon: Ext.MessageBox.QUESTION
						
						});
					} else {
						Ext.Msg.show({
							title: "Error",
							msg: 'You must select one',
							buttons: Ext.MessageBox.OK,
							icon: Ext.MessageBox.ERROR
						});
					}
				}
			},
			'eventsList button[id=inviteUsers]': {
				click: function() {
					selection = Ext.getCmp('gridEvent').getSelectionModel().getSelection();
					if (selection.length==1){
						Ext.widget('invitation').show();
						var values = Ext.getCmp('formInvite').getForm().getValues();
						values.id = selection[0].get("id");
						Ext.getCmp('formInvite').getForm().setValues(values);
					}else{
						Ext.Msg.show({
							title: "Error",
							msg: 'You must select one',
							buttons: Ext.MessageBox.OK,
							icon: Ext.MessageBox.ERROR
						});
					}
				}
			}
		});
	}
});