/**
 * Controlador para las comunidades
 * 
 * @author ADORU3N
 */

Ext.define('phdhive.controller.Community', {
	extend : 'Ext.app.Controller',

	views : [ 'communities.Community', 'communities.Panel', 'communities.add.Community', 'communities.add.ComboUsers', 'communities.add.ComboCountries' ],
	stores : [ 'Community','ComboUsers' ],
	models : [ 'Community' ],

	init: function(){
		this.control({
			'communityList button[id=addCommunity]': {
				click: function(){
					Ext.widget('newCommunity').show();
				}
			},
			'communityList button[id=editCommunity]': {
				click: function(){
					selection = Ext.getCmp('gridCommunity').getSelectionModel().getSelection();
					if (selection.length==1){
						Ext.widget('newCommunity').show();
						Ext.getCmp('formNewCommunity').getForm().load({
							url: contextPath + '/action/admin/communities/edit',
							waitMsg: 'Loading...',
							waitTitle: 'Wait',
							params: {
								id: selection[0].get("id")
							}
						});
					}else{
						Ext.Msg.show({
							title: "Error",
							msg: 'You must select one',
							buttons: Ext.MessageBox.OK,
							icon: Ext.MessageBox.ERROR
						});
					}
				}
			},
			'communityList button[id=removeCommunity]': {
				click: function(){
					selection = Ext.getCmp('gridCommunity').getSelectionModel().getSelection();
					if (selection.length==1){
						Ext.Msg.show({
							title: 'Confirm',
							msg: 'Do you want remove this Community?',
							buttons: Ext.MessageBox.OKCANCEL,
							fn: function(btn){
								if (btn=='ok'){
									Ext.Ajax.request({
										url: contextPath + '/action/admin/communities/remove',
										method: 'POST',
										params: {
											id: selection[0].get("id")
										},
										success: function(){
											Ext.getCmp('gridCommunity').getStore().load();
											Ext.Msg.show({
												title: "Information",
												msg: 'The community has been successfully deleted',
												buttons: Ext.MessageBox.OK,
												icon: Ext.MessageBox.INFO
											});								
										}
									});
								}
							},
							icon: Ext.MessageBox.QUESTION
						
						});
					} else {
						Ext.Msg.show({
							title: "Error",
							msg: 'You must select one',
							buttons: Ext.MessageBox.OK,
							icon: Ext.MessageBox.ERROR
						});
					}
				}
			}
		});
	}
});