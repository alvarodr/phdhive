/**
 * Panel de opciones de la parte de administración
 * 
 * @author Álvaro
 */
Ext.define('phdhive.controller.Tabpanel', {
	extend: 'Ext.app.Controller',

	views: ['Tabpanel', 'TopPanel', 'template.Template'],
	
	init: function() {
		this.callParent();
	}
});