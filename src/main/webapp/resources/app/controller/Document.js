/**
 * Controlador para la gestión de documentos
 * 
 * @author ADORU3N
 */
Ext.define('phdhive.controller.Document', {
	extend: 'Ext.app.Controller',
	
	models: ['Document', 'Words'],
	stores: ['Document', 'Words'],
	views: ['documents.Document', 'documents.Panel', 'documents.Words'],
	
	init:function(){
		this.control({
			'documentList': {
				itemdblclick: function(grid, record, item, e){
					if (record.data.leaf)
						console.log("Selected!!");
				}
			}
		});
	}

});