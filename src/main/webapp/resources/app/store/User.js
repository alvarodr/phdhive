Ext.define('phdhive.store.User', {
	extend : 'Ext.data.Store',
	model : 'phdhive.model.User',
	sorters : [ 'name', 'phone' ],
	autoLoad: true,
	proxy: {
		type: 'ajax',
		url: '/action/admin/users',
		reader: {
			type: 'json',
			root: 'users',
			totalProperty: 'totalCount'
		}
	},
//	pageSize: 10000,
	initComponent: function() {	
		this.callParent(arguments);
	}
});