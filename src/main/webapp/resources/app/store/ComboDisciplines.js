Ext.define('phdhive.store.ComboDisciplines', {
	extend: 'Ext.data.Store',
	model: 'phdhive.model.Combo',
	alias: 'data.comboStore',
	sorters : [ 'description' ],
	proxy: {
		type: 'ajax',
		url: '/action/admin/combo/dataMaster',
		reader: {
			type: 'json',
			totalProperty: 'totalCount',
			root: 'items'
		}
	},

	initComponent: function() {	
		this.callParent(arguments);
	}
});