Ext.define('phdhive.store.Combo', {
	extend: 'Ext.data.Store',
	model: 'phdhive.model.Combo',
	alias: 'data.comboStore',
	
	proxy: {
		type: 'ajax',
		url: 'app/data/countries.json',
		reader: {
			type: 'json',
			root: 'root'
		}
	},

	initComponent: function() {	
		this.callParent(arguments);
	}
});