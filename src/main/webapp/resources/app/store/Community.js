Ext.define('phdhive.store.Community', {
	extend : 'Ext.data.Store',
	model : 'phdhive.model.Community',
	groupField: 'dsCountry',
	autoLoad: true,
	proxy: {
		type: 'ajax',
		url: '/action/admin/communities',
		reader: {
			type: 'json',
			root: 'items',
			totalProperty: 'totalCount'
		}
	}
});