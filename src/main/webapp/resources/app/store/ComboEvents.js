/**
 * Combo de paises
 * 
 * @author ADORU3N
 */
Ext.define('phdhive.store.ComboEvents', {
	extend: 'Ext.data.Store',
	model: 'phdhive.model.Combo',
	sorters : [ 'description' ],
	autoLoad: true,
	proxy: {
		type: 'ajax',
		url: '/action/admin/combo/events',
		reader: {
			type: 'json',
			totalProperty: 'totalCount',
			root: 'items'
		}
	},

	initComponent: function() {	
		this.callParent(arguments);
	}
});