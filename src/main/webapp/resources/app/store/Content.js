Ext.define('phdhive.store.Content', {
	extend : 'Ext.data.Store',
	model : 'phdhive.model.Content',
	groupField: 'type',
	autoLoad: true,
	proxy: {
		type: 'ajax',
		url: '/action/admin/contents/list',
		reader: {
			type: 'json',
			root: 'items',
			totalProperty: 'totalCount'
		}
	},
	sorters: [{
		property: 'type',
		direction: 'ASC'
	}, {
		property: 'order',
		direction: 'ASC'
	}]
});

