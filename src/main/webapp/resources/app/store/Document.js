Ext.define('phdhive.store.Document', {
	extend: 'Ext.data.TreeStore',
	model: 'phdhive.model.Document',
	autoLoad: true,
	proxy: {
		type: 'ajax',
		url: contextPath + '/action/admin/documents'
	}
});