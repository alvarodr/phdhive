Ext.define('phdhive.store.ComboAreas', {
	extend: 'Ext.data.Store',
	model: 'phdhive.model.Combo',
	alias: 'data.comboStore',
	sorters : [ 'description' ],
	proxy: {
		type: 'ajax',
		url: '/action/admin/combo/dataMaster',
		extraParams: {
			type: '2',
		},
		reader: {
			type: 'json',
			totalProperty: 'totalCount',
			root: 'items'
		}
	},

	initComponent: function() {	
		this.callParent(arguments);
	}
});