/**
 * Combo de tipos de contenido
 * 
 * @author ADORU3N
 */
Ext.define('phdhive.store.ComboContentType', {
	extend: 'Ext.data.Store',
	model: 'phdhive.model.Combo',
	sorters : [ 'description' ],
	autoLoad: true,
	proxy: {
		type: 'ajax',
		url: '/action/admin/combo/dataMaster',
		extraParams: {
			type: '4',
		},
		reader: {
			type: 'json',
			totalProperty: 'totalCount',
			root: 'items'
		}
	},

	initComponent: function() {	
		this.callParent(arguments);
	}
});