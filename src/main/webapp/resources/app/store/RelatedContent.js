Ext.define('phdhive.store.RelatedContent', {
	extend : 'Ext.data.Store',
	model : 'phdhive.model.RelatedContent',
	autoLoad: false,
	alias: 'data.relatedContents',
	
	proxy: {
		type: 'ajax',
		url: '/action/admin/contents/relatedContents',
		reader: {
			type: 'json',
			root: 'items'
		}
	},

	initComponent: function() {	
		this.callParent(arguments);
	}
});