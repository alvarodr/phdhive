Ext.define('phdhive.store.Event', {
	extend: 'Ext.data.Store',
	model: 'phdhive.model.Event',
    sorters: ['suscribers'],
    groupField: 'nameCommunity',
	autoLoad: true,
	proxy: {
		type: 'ajax',
		url: contextPath + '/action/admin/events',
		reader: {
			type: 'json',
			root: 'items',
			totalProperty: 'totalCount'
		}
	}
});