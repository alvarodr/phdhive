/**
 * Combo de paises
 * 
 * @author ADORU3N
 */
Ext.define('phdhive.store.ComboCountries', {
	extend: 'Ext.data.Store',
	model: 'phdhive.model.Combo',
	alias: 'data.comboStore',
	sorters : [ 'description' ],
	autoLoad: true,
	proxy: {
		type: 'ajax',
		url: '/action/admin/combo/dataMaster',
		extraParams: {
			type: '3',
		},
		reader: {
			type: 'json',
			totalProperty: 'totalCount',
			root: 'items'
		}
	},

	initComponent: function() {	
		this.callParent(arguments);
	}
});