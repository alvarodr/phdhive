Ext.define('phdhive.store.Words', {
	extend: 'Ext.data.Store',
	model: 'phdhive.model.Words',
    sorters: [{property:'frecuency', direction:'desc'}],
    data:[
    	{ word: 'Polit', frecuency:'34'},
    	{ word: 'Hist', frecuency:'624' },
    	{ word: 'Scienc', frecuency:'76' },
    	{ word: 'Tech', frecuency:'234' }
  	]
});