/**
 * Vista generica de la aplicación
 * 
 * @author ADORU3N
 */
Ext.define("phdhive.view.Viewport", {
	extend: 'Ext.container.Viewport',
	layout: 'border',
	border: false,
	
    requires: ['phdhive.view.Tabpanel'],
	
	padding: '0 5 0 5',
	
	initComponent: function(){
		var me = this;

		//Deshabilita el menu context de la aplicación
		//Ext.getBody().on("contextmenu", Ext.emptyFn, null, {preventDefault: true});

		Ext.apply(this, {
			items: [{xtype: 'topPanel'}, {xtype: 'mainTabpanel'}]
		});
		
		this.callParent(arguments);
	}
});