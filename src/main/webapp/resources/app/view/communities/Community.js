Ext.define('phdhive.view.communities.Community', {
	extend : 'Ext.grid.Panel',
	alias : 'widget.communityList',
	region : 'center',
	layout : 'fit',
	title : 'Community list',
	forceFit : true,

	initComponent : function() {
		Ext.apply(this, {
			id: 'gridCommunity',
			store : 'Community',
			columns : [ {
				text : 'Name',
				dataIndex : 'name'
			}, {
				text : 'Country',
				dataIndex : 'dsCountry'
			}, {
				text : 'Email',
				dataIndex : 'email'
			} ],
			bbar : Ext.create('Ext.PagingToolbar', {
				store : 'Community',
				displayInfo : true
			}),
			tbar : [ {
				xtype : 'button',
				id: 'addCommunity',
				iconCls : 'add-community',
				text : 'Create'
			}, {
				xtype : 'button',
				id: 'editCommunity',
				iconCls : 'edit-community',
				text : 'Edit'
			}, {
				xtype : 'button',
				id: 'removeCommunity',
				iconCls : 'remove-community',
				text : 'Remove'
			},
//			{
//				xtype : 'button',
//				iconCls : 'export',
//				text : 'Export'
//			} 
			],
			features : [ {
				ftype : 'grouping',
				groupHeaderTpl : '{name} ({rows.length} Item{[values.rows.length > 1 ? "s" : ""]})'
			} ]
		});

		this.callParent(arguments);
	}
});