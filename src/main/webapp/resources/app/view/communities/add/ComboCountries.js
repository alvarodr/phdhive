Ext.define('phdhive.view.communities.add.ComboCountries',{
	extend: 'Ext.form.field.ComboBox',
	alias: 'widget.wComboCountry',
	fieldLabel: 'Country',
	allowBlank: false,
	name: 'country',
	queryMode: 'local',
	valueField: 'code',
	displayField: 'description',
	store: 'ComboCountries',
	autoShow: true,
	typeAhead: false,
	minChars: 2
});
