/**
 * Ventana para crear una nueva comunidad
 * 
 * @author ADORU3N
 */
Ext.define('phdhive.view.communities.add.Community', {
	extend: 'Ext.window.Window',
	alias: 'widget.newCommunity',
	id: 'windowNewCommunity',
	title: 'New Community',
	height: 300,
	width: 500,
	layout: 'fit',
	modal: true,
    
	initComponent: function() {
		Ext.apply(this,{
			items: [{
				xtype: 'form',
				id: 'formNewCommunity',
				layout: 'column',
				url: contextPath + '/action/admin/communities/add',
				bodyPadding: 15,
				border: false,
				defaults:{
					afterLabelTextTpl: new Ext.XTemplate('<tpl if="allowBlank === false"><span style="color:red;"> *</span></tpl>', { disableFormats: true }),
					columnWidth: 1,
					margin: 10,
					labelWidth: 150
				},
				items:[{
					xtype: 'hiddenfield',
					name: 'id',
					value: '-1'
				},{
					xtype: 'textfield',
					allowBlank: false,
					name: 'name',
					fieldLabel: 'Community Name'
				},{
		        	xtype: 'wComboCountry'
				},{
		        	xtype: 'wComboUser'
				}],
				
				buttons:[{
					text: 'Create',
					handler: function(){
						if (Ext.getCmp('formNewCommunity').getForm().isValid()){
							Ext.getCmp('formNewCommunity').getForm().submit({
								waitMsg: 'Creating a new comunity...',
								method: 'POST',
								success: function(response){
									Ext.getCmp('gridCommunity').getStore().load();
									Ext.getCmp('windowNewCommunity').close();
								},
								failure: function(response){
									var parsedResult = Ext.JSON.decode(response.responseText).data;
									Ext.Msg.show({
										title: "Error",
										msg: parsedResult.msg,
										buttons: Ext.MessageBox.OK,
										icon: Ext.MessageBox.ERROR
									});
								}
							});					
						}
					}
				}, {
					text: 'Cancel',
					handler: function(e){
						Ext.getCmp('windowNewCommunity').close();
					}
				}]
		    }]
		});
		this.callParent(arguments);
	}
});