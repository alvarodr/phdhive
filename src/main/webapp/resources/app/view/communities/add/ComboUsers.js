Ext.define('phdhive.view.communities.add.ComboUsers',{
	extend: 'Ext.form.field.ComboBox',
	alias: 'widget.wComboUser',
	allowBlank: false,
	fieldLabel: 'User Admin',
	name: 'admin',
	queryMode: 'local',
	valueField: 'email',
	displayField: 'name',
	store: 'ComboUsers',
	hideTrigger: true,
	typeAhead: true,
	minChars: 2,
	tpl: Ext.create('Ext.XTemplate',
	    '<tpl for=".">',
	        '<div class="x-boundlist-item combo-text"><img src="/resources/img/user.png"/><div class="floatRight wp80">{name} {surname}<br><span class="small-text">{email}</span></div></div>',
	    '</tpl>'
	),
	displayTpl: Ext.create('Ext.XTemplate',
		'<tpl for=".">',
			'{name} {surname}',
		'</tpl>'
	)
});