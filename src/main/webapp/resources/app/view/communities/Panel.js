Ext.define('phdhive.view.communities.Panel', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.communityPanel',
	layout: 'fit',
	region: 'center',
	
	initComponent: function() {
		Ext.apply(this, {
			title: 'Communities',
			items:[
		        { xtype: 'communityList' }
	        ]
		});
		
		this.callParent(arguments);
	}
});