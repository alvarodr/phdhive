Ext.define('phdhive.view.contents.Content', {
	extend: 'Ext.grid.Panel',
	id: 'gridContent',
	alias: 'widget.contentList',
	region: 'center',
	layout: 'fit',
	title : 'Contents',
	forceFit : true,
	
	initComponent: function() {
		Ext.apply(this, {
			store: 'Content',
			title: 'Content list',
			tbar: [
		  		{
		  			xtype: 'button',
		  			id: 'addContent',
		  			iconCls: 'add-event',
		  			text: 'Create'
		  		},
		  		{
		  			xtype: 'button',
		  			id: 'editContent',
		  			iconCls: 'edit-event',
		  			text: 'Edit'
		  		},
		  		{
		  			xtype: 'button',
		  			id: 'removeContent',
		  			iconCls: 'remove-event',
		  			text: 'Remove'
		  		}

		  	],
			
		  	bbar : Ext.create('Ext.PagingToolbar', {
				store : 'Content',
				displayInfo : true
			}),		  	
			
			columns:[		  	         
				     { text: 'Title',  dataIndex: 'title', width: '50%' },
				     { text: 'Category', dataIndex: 'type'},
				     { text: 'Order', dataIndex: 'order', align: 'center'},
			         { text: 'Published',	dataIndex: 'published', width: '5%', align:'center', renderer:function(value){
			              	return value?'<img src="/resources/img/ico/drop-yes.png">':'<img src="/resources/img/ico/drop-no.png">';
			         }},
				     { text: 'Updated', dataIndex: 'updateDate', render:function(value){return Ext.Date.format(new Date(value), 'd/m/Y H:i');}}	  	        	 
            ],

			features : [ {
				ftype : 'grouping',
				groupHeaderTpl: '{columnName}: {name} ({rows.length} Item{[values.rows.length > 1 ? "s" : ""]})',
				startCollapsed: true
			} ]
		});
		this.callParent(arguments);
	}
});