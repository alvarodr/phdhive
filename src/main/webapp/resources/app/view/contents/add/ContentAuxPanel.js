Ext.define('phdhive.view.contents.add.ContentAuxPanel', {
	extend: 'Ext.window.Window',
	alias: 'widget.contentListAuxPanel',
	id: 'windowContentListAux',
	title: 'Contents List to relate',
	height: 380,
	width: 800,
	layout: 'fit',
	modal: true,
	
	initComponent: function() {
		Ext.apply(this, {
			title: 'Contents',
			items: [
		        {xtype: 'contentListAux', border: false}
	        ]
		});
		this.callParent(arguments);
	}
});