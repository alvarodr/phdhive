Ext.define('phdhive.view.contents.add.ContentAux', {
	extend: 'Ext.grid.Panel',
	id: 'gridContentAux',
	alias: 'widget.contentListAux',
	region: 'center',
	layout: 'fit',
	title : 'Contents',
	
	forceFit : true,
	
	initComponent: function() {
		Ext.apply(this, {
			store: 'Content',
			title: 'Contents list',
			
		  	bbar : Ext.create('Ext.PagingToolbar', {
				store : 'Content',
				displayInfo : true
			}),		  	
			
			columns:[		  	         
				     { text: 'Title',  dataIndex: 'title' },
				     { text: 'Category', dataIndex: 'type'},
				     { text: 'Updated', dataIndex: 'updateDate', render:function(value){return Ext.Date.format(new Date(value), 'd/m/Y H:i');}}	  	        	 
            ],

			features : [ {
				ftype : 'grouping',
				groupHeaderTpl : '{type} ({rows.length} Item{[values.rows.length > 1 ? "s" : ""]})'
			} ],
			
			buttons:[{
				text: 'Select',
				handler: function(){
					selection = Ext.getCmp('gridContentAux').getSelectionModel().getSelection();
					var values = Ext.getCmp('formNewContent').getForm().getValues();
					var relAux = values.relatedContents;
					for(var i=0;i<selection.length;i++){	
						relAux +=  selection[i].get("id");
						relAux += ';';						
					}
					
					values.relatedContents=relAux;
					Ext.getCmp('formNewContent').getForm().setValues(values);
					
					Ext.getCmp('windowContentListAux').close();

				}
			}, {
				text: 'Cancel',
				handler: function(e){
					Ext.getCmp('windowContentListAux').close();
				}
			}]
		});
		this.callParent(arguments);
	}
});