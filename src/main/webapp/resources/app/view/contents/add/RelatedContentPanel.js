//Panel de contenidos relacionados
Ext.define('phdhive.view.contents.add.RelatedContentPanel', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.relatedContentPanel',
	region: 'center',
	border: false,
	layout: 'fit',
	
	initComponent: function() {
		Ext.apply(this, {
			title: 'Related Contents',
			items: [
		        {xtype: 'relatedContentList', border: false}
	        ]
		});
		this.callParent(arguments);
	}
});