/**
 * Ventana para el alta/modificación de contenidos
 * 
 * @author ADORU3N
 */
Ext.define('phdhive.view.contents.add.Content',{
	extend: 'Ext.window.Window',
	alias: 'widget.newContent',
	id: 'windowNewContent',
	title: 'New Content',
	height: 650,
	width: 1200,
	layout: 'fit',
	modal: true,
	
	initComponent: function(){
		Ext.apply(this,{
			items: [{
				xtype: 'form',
				id: 'formNewContent',
			    layout: {
//			        align: 'stretch',
			        type: 'anchor'
			    },
				url: contextPath + '/action/admin/contents/save',
				bodyPadding: 15,
				border: false,
				defaults:{
					//duda
					afterLabelTextTpl: new Ext.XTemplate('<tpl if="allowBlank === false"><span style="color:red;"> *</span></tpl>', { disableFormats: true }),
					columnWidth: 1
				},
				items:[{
					xtype: 'panel',
				    layout: {
//				        align: 'stretch',
				        type: 'column'
				    },
				    defaults:{
				    	afterLabelTextTpl: new Ext.XTemplate('<tpl if="allowBlank === false"><span style="color:red;"> *</span></tpl>', { disableFormats: true }),
				    	labelWidth: 60,
				    	columnWidth: .5,
				    	padding: 5
			    	},
				    border: false,
				    items:[{
						xtype: 'textfield',
						allowBlank: false,
						name: 'title',
						fieldLabel: 'Title'
					},{
						xtype: 'textarea',
						allowBlank: true,
						grow: true,
						name: 'header',
						fieldLabel: 'Header'
					},{
						xtype: 'combo',
						fieldLabel: 'Type',
						allowBlank: false,
						name: 'type',
						queryMode: 'local',
						valueField: 'code',
						displayField: 'description',
						store: 'ComboContentType',
						autoShow: true,
		//				typeAhead: false,
						minChars: 2
			        }, {
				    	xtype: 'numberfield',
				    	name: 'order',
				    	fieldLabel: 'Order',
				    	columnWidth: .25,
				    	minValue: 0
				    }, {
			            xtype: 'checkboxfield',
			            name: 'published',
			            fieldLabel: 'Published',
			            labelAlign: 'right',
			            columnWidth: .25,
			            padding: '5 5 5 65'
				    }]
				},{
					xtype: 'hiddenfield',
					name: 'id',
					value: '-1'
				},{
					xtype: 'hiddenfield',
					name: 'relatedContents',
					value: '',
				    listeners:{
				            change:function(cbx, newVal,oldVal,e){
	    						Ext.getStore('RelatedContent').getProxy().setExtraParam('relatedIds',newVal);
	    						Ext.getStore('RelatedContent').load();
				            }
				     }	
				},{

		            xtype:'tabpanel',
		            plain:true,
		            activeTab: 0,
		            height:450,
		            padding: 10,
		            items:[{		                
		            	//TODO: Poner ckeditor en inglés
		            	title:'Body',
						xtype: 'ckeditor',
						allowBlank: false,
//						anchor: '100% 100%',
						CKConfig: {
							uiColor: '#FAAC0F',
							height: 250
						},
						name: 'body'
		            },{		            			            
		            	title: 'Related contents',
		            	border: false,
		                xtype: 'relatedContentPanel',
		                store: 'RelatedContent'	              	               
		            }]
		        }],
				
				buttons:[{
					text: 'Save',
					handler: function(){
						if (Ext.getCmp('formNewContent').getForm().isValid()){
							Ext.getCmp('formNewContent').getForm().submit({
								waitMsg: 'Creating a new content...',
								method: 'POST',
								success: function(response){
									Ext.getCmp('gridContent').getStore().load();
									Ext.getCmp('windowNewContent').close();
								},
								failure: function(response){
									var parsedResult = Ext.JSON.decode(response.responseText).data;
									Ext.Msg.show({
										title: "Error",
										msg: parsedResult.msg,
										buttons: Ext.MessageBox.OK,
										icon: Ext.MessageBox.ERROR
									});
								}
							});					
						}
					}
				}, {
					text: 'Cancel',
					handler: function(e){
						Ext.getCmp('windowNewContent').close();
					}
				}]
		    }]
		});
		this.callParent(arguments);
	}
});