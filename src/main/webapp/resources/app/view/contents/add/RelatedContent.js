Ext.define('phdhive.view.contents.add.RelatedContent', {
	extend: 'Ext.grid.Panel',
	id: 'gridRelatedContent',
	alias: 'widget.relatedContentList',
	region: 'center',
	layout: 'fit',
	title : 'Related Contents',
	border: false,
	forceFit : true,
	
	initComponent: function() {
		Ext.apply(this, {
			store: 'RelatedContent',
			title: 'Related Contents list',
			tbar: [
		  		{
		  			xtype: 'button',
		  			id: 'addRelatedContent',
		  			iconCls: 'add-event',
		  			text: 'Add'
		  		},
		  		{
		  			xtype: 'button',
		  			id: 'removeRelatedContent',
		  			iconCls: 'remove-event',
		  			text: 'Remove'
		  		}

		  	],
			
		  	columns:[		  	         
				{ text: 'Title',  dataIndex: 'relContentName' }     	 
            ]
		});
		this.callParent(arguments);
	}
});