Ext.define('phdhive.view.contents.Panel', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.contentPanel',
	region: 'center',
	layout: 'fit',
	
	initComponent: function() {
		Ext.apply(this, {
			title: 'Contents',
			items: [
		        {xtype: 'contentList'}
	        ]
		});
		this.callParent(arguments);
	}
});