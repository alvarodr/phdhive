Ext.define('phdhive.view.user.filter.ButtonSearch', {
	extend: 'Ext.button.Button',
	alias: 'widget.buttonSearch',
	text: 'Search',
	
	initComponent: function(){
		Ext.apply(this, {
			columnWidth: 0.10
		});
		this.callParent(arguments);
	}
});