Ext.define('phdhive.view.user.filter.User', {
	extend: 'Ext.form.Panel',
	alias: 'widget.searchUser',
	cls: 'foot',
	region: 'north',
	
	initComponent: function(){
		Ext.apply(this, {
			id: 'formSearch',
			title: 'Search users',
			url: contextPath + '/action/admin/users/filter',
			layout: 'column',
			split: false,
			defaults: {
				columnWidth: 0.30,
				margin: 10,
				valueField: 'code',
				displayField: 'description' 
			},
			items: [
		        {
		        	xtype: 'combo',
		        	id:'comboCountry',
		        	fieldLabel: 'Country',
		        	name: 'country',
		        	queryModel: 'local',
		        	store: 'ComboCountries'
		        }, {
		        	xtype: 'combo',
		        	id: 'comboAreas',
		        	name: 'area',
		        	fieldLabel: 'Area',
		        	queryModel: 'local',
		        	store: 'ComboAreas'
		        }, {
		        	xtype: 'combo',
		        	id: 'comboDisciplines',
		        	name: 'discipline',
		        	fieldLabel: 'Discipline',
		        	queryModel: 'local',
		        	store: 'ComboDisciplines'
		        }, {
		        	xtype: 'buttonSearch'
		        }, {
		        	xtype: 'combo',
		        	id: 'comboCommunity',
		        	fieldLabel: 'Community',
		        	name: 'community',
		        	queryModel: 'local',
		        	store: 'ComboCommunities'
		        }, {
		        	xtype: 'combo',
		        	id: 'comboEvent',
		        	fieldLabel: 'Event',
		        	name: 'event',
		        	queryModel: 'local',
		        	store: 'ComboEvents'
		        }
	        ]
		});
		
		this.callParent(arguments);
	}
});