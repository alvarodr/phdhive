Ext.define('phdhive.view.user.User', {
	extend: 'Ext.grid.Panel',
	alias: 'widget.userList',
	id: 'gridUsers',
	title: 'User list',
	region: 'center',
	loadMask: true,
	
	initComponent: function (){
		this.store = 'User';
		this.columns = [
           { text: 'Name',  dataIndex: 'name', width: '15%' },
           { text: 'Surname',  dataIndex: 'surname', width: '20%' },
           { text: 'Email', dataIndex: 'email', width: '20%' },
           { text: 'Phone', dataIndex: 'phone', align: 'right', width: '12%'},
           { text: 'IP Access', dataIndex: 'lastIP', align: 'center', width: '12%'},
           { text: 'Login count', dataIndex: 'loginCount', width: '10%', align: 'right'},
           { text: 'Locked',	dataIndex: 'enabled', width: '10%', align:'center', renderer:function(value){
        	   return value?'<img src="/resources/img/ico/drop-yes.png">':'<img src="/resources/img/ico/drop-no.png">';
           }}
		];
		
		Ext.apply(this, {
            bbar: Ext.create('Ext.PagingToolbar', {
                store: 'User',
                displayInfo: true
                //displayMsg: 'Displaying topics {0} - {1} of {2}',
                //emptyMsg: "No topics to display",
            }),
            tbar:[
	      		{
	      			xtype: 'button',
	      			id: 'removeUser',
	      			iconCls: 'remove-user',
	      			text: 'Remove'
	      		},
	      		{
	      			xtype: 'button',
	      			id: 'export',
	      			iconCls: 'export',
	      			text: 'Export'
	      		},
	      		{
	      			xtype: 'button',
	      			id: 'activation',
	      			iconCls: 'user-lock',
	      			text: 'Activation'
	      		}
	      		
	      	],
		});
		
		this.callParent(arguments);
	},
    listeners:{
        select:function( this_, record, index, eOpts ){
        	selection = Ext.getCmp('gridUsers').getSelectionModel().getSelection();
        	lock = selection[0].get("enabled");        	
        	//alert(lock);
        	if(lock) {
        	      Ext.getCmp('activation').setText('Deactivate');
        	}else{
        		Ext.getCmp('activation').setText('Activate');
        	}
        }
    }	
});