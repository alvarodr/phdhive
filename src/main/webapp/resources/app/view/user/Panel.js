Ext.define('phdhive.view.user.Panel', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.userPanel',
	region: 'center',
	layout: 'border',
	
	initComponent: function() {
		Ext.apply(this, {
			title: 'Users',
			items: [
		        {xtype: 'userList'},
		        {xtype: 'searchUser'}
	        ]
		});
		this.callParent(arguments);
	}
});