/**
 * Ventana para invitar usuarios
 * 
 * @author Margolles
 */
Ext.define('phdhive.view.events.Invitation', {
	extend: 'Ext.window.Window',
	alias: 'widget.invitation',
	id: 'windowInvite',
	title: 'Invite users',
	height: 250,
	width: 600,
	layout: 'fit',
	modal: true,
    
	initComponent: function() {
		Ext.apply(this,{
			items: [{
				xtype: 'form',
				id: 'formInvite',
				layout: 'column',
				url: contextPath + '/action/admin/events/invite',
				bodyPadding: 15,
				border: false,
				defaults:{
					afterLabelTextTpl: new Ext.XTemplate('<tpl if="allowBlank === false"><span style="color:red;"> *</span></tpl>', { disableFormats: true }),
					columnWidth: 1,
					margin: 10,
					labelWidth: 150
				},
				items:[{
					xtype: 'hiddenfield',
					name: 'id',
				},{
					xtype: 'boxselect',
					fieldLabel: 'Invite users',
					name: 'invitation',
					delimiter: ',',
					queryMode: 'local',
					valueField: 'email',
					displayField: 'name',
					autoScroll: true,
					forceSelection: false,
					createNewOnEnter: true,
//					createNewOnBlur: false,
					filterPickList: true,
//					maxHeight: 150,
					store: 'ComboUsers',
					hideTrigger: true,
//					regex: new RegExp(/^((([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z\s?]{2,5}){1,25})*(\s*?;\s*?)*)*$/),
//					regexText:'Please, enter a valid email address',
					minChars: 0,
					tpl: Ext.create('Ext.XTemplate',
					    '<tpl for=".">',
					        '<div class="x-boundlist-item combo-text"><img src="/resources/img/user.png"/><div class="floatRight wp80">{name} {surname}<br><span class="small-text">{email}</span></div></div>',
					    '</tpl>'
					),
					displayTpl: Ext.create('Ext.XTemplate',
						'<tpl for=".">',
							'{name} {surname}',
						'</tpl>'
					)
				}],				
				buttons:[{
					text: 'Send',
					handler: function(){
						if (Ext.getCmp('formInvite').getForm().isValid()){
							Ext.getCmp('formInvite').getForm().submit({
								waitMsg: 'Sending invitation to users...',
								method: 'POST',
								success: function(response){
									Ext.getCmp('gridEvent').getStore().load();
									Ext.getCmp('windowInvite').close();
									Ext.Msg.show({
										title: "Information",
										msg: 'The invitations have been successfully sent. Please, check you spam folder',
										buttons: Ext.MessageBox.OK,
										icon: Ext.MessageBox.INFO
									});	
								},
								failure: function(response){
									var parsedResult = Ext.JSON.decode(response.responseText).data;
									Ext.Msg.show({
										title: "Error",
										msg: parsedResult.msg,
										buttons: Ext.MessageBox.OK,
										icon: Ext.MessageBox.ERROR
									});
								}
							});					
						}
					}
				}, {
					text: 'Cancel',
					handler: function(e){
						Ext.getCmp('windowInvite').close();
					}
				}]
		    }]
		});
		this.callParent(arguments);
	}
});