Ext.define('phdhive.view.events.Panel', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.eventPanel',
	region: 'center',
	layout: 'fit',
	
	initComponent: function() {
		Ext.apply(this, {
			title: 'Events',
			items: [
		        {xtype: 'eventsList'}
	        ]
		});
		this.callParent(arguments);
	}
});