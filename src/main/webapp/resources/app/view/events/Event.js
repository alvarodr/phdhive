Ext.define('phdhive.view.events.Event', {
	extend: 'Ext.grid.Panel',
	id: 'gridEvent',
	alias: 'widget.eventsList',
	region: 'center',
	layout: 'fit',
	forceFit: true,
	
	initComponent: function() {
		Ext.apply(this, {
			store: 'Event',
			title: 'Event list',
			tbar: [
		  		{
		  			xtype: 'button',
		  			id: 'addEvent',
		  			iconCls: 'add-event',
		  			text: 'Create'
		  		},
		  		{
		  			xtype: 'button',
		  			id:'editEvent',
		  			iconCls: 'edit-event',
		  			text: 'Edit'
		  		},
		  		{
		  			xtype: 'button',
		  			id: 'removeEvent',
		  			iconCls: 'remove-event',
		  			text: 'Remove'
		  		},
		  		{
		  			xtype: 'button',
		  			id: 'inviteUsers',
		  			iconCls: 'invite-users',
		  			text: 'Invite'
		  		},
//		  		{
//		  			xtype: 'button',
//		  			id: 'exportEvents',
//		  			iconCls: 'export',
//		  			text: 'Export'
//		  		}
		  	],
			bbar : Ext.create('Ext.PagingToolbar', {
				store : 'Event',
				displayInfo : true
			}),

			features : [ {
				ftype : 'grouping',
				groupHeaderTpl : '{name} ({rows.length} Item{[values.rows.length > 1 ? "s" : ""]})'
			} ]
		});
		var me = this;
		Ext.Ajax.request({
			async: false,
			url: contextPath + '/action/admin/privileges',
			success: function(response){
				var returnData = Ext.JSON.decode(response.responseText);
				if (returnData.success){
					me.columns = [
							        { text: 'Name',  dataIndex: 'name' },
							        { text: 'Community',  dataIndex: 'nameCommunity' },
							        { text: 'Suscribers', dataIndex: 'suscribers' },
							        { text: 'Start date', dataIndex: 'startDate', renderer: function(value) {return Ext.Date.format(new Date(value), "d/m/Y")}},
							        { text: 'End date', dataIndex: 'endDate', renderer: function(value) {return Ext.Date.format(new Date(value), "d/m/Y H:i")}}
								];
				}else{
					me.columns = [
							        { text: 'Name',  dataIndex: 'name' },
							        { text: 'Suscribers', dataIndex: 'suscribers' },
							        { text: 'Start date', dataIndex: 'startDate', renderer: function(value) {return Ext.Date.format(new Date(value), "d/m/Y H:i")}},
							        { text: 'End date', dataIndex: 'endDate', renderer: function(value) {return Ext.Date.format(new Date(value), "d/m/Y H:i")}}
								];
				}
			}
		});

		this.callParent(arguments);
	}
});