/**
 * Ventana para crear un nuev evento
 * 
 * @author ADORU3N
 */
Ext.define('phdhive.view.events.add.Event', {
	extend: 'Ext.window.Window',
	alias: 'widget.newEvent',
	id: 'windowNewEvent',
	title: 'New Event',
	height: 400,
	width: 700,
	layout: 'fit',
	modal: true,
    
	initComponent: function() {
		Ext.apply(this,{
			items: [{
				xtype: 'form',
				id: 'formNewEvent',
				layout: 'column',
				url: contextPath + '/action/admin/events/save',
				bodyPadding: 15,
				border: false,
				defaults:{
					afterLabelTextTpl: new Ext.XTemplate('<tpl if="allowBlank === false"><span style="color:red;"> *</span></tpl>', { disableFormats: true }),
					columnWidth: 1,
					margin: 10,
					labelWidth: 150
				},
				items:[{
					xtype: 'hiddenfield',
					name: 'id',
					value: '-1'
				},{
					xtype: 'combo',
					allowBlank: isAdmin ? false : true,
					hidden: !isAdmin,
		        	id: 'comboOwner',
		        	fieldLabel: 'Community Owner',
		        	name: 'owner',
		        	queryModel: 'local',
		        	store: 'ComboCommunities',
					valueField: 'code',
					displayField: 'description'
				},{
					xtype: 'textfield',
					allowBlank: false,
					name: 'name',
					fieldLabel: 'Event Name'
				},{
		        	xtype: 'datefield',
		        	fieldLabel: 'Start Date',
		        	name: 'startDate',
		        	allowBlank: false,
		        	format: 'd/m/Y',
		        	minValue: new Date(),
		        	columnWidth: .5,
		        	value: new Date()
				},{
					xtype: 'timefield',
					name: 'startTime',
					allowBlank: false,
					fieldLabel: 'Start Time',
					columnWidth: .5,
					increment: 60,
					format: 'H:i',
					value: '00:00'
				},{
		        	xtype: 'datefield',
		        	fieldLabel: 'End Date',
		        	name: 'endDate',
		        	allowBlank: false,
		        	format: 'd/m/Y',
		        	minValue: Ext.Date.add (new Date(),Ext.Date.DAY,1),
		        	columnWidth: .5,
		        	value: Ext.Date.add (new Date(),Ext.Date.DAY,1)
				},{
					xtype: 'timefield',
					name: 'endTime',
					allowBlank: false,
					fieldLabel: 'End Time',
					columnWidth: .5,
					increment: 60,
					format: 'H:i',
					value: '00:00'
				},{
					xtype: 'combo',
					allowBlank: false,
		        	id: 'comboLanguage',
		        	fieldLabel: 'Language',
		        	name: 'language',
		        	queryModel: 'local',
		        	store: 'ComboLanguage',
					valueField: 'code',
					displayField: 'description'
				},{
					xtype: 'textarea',
					allowBlank: false,
					name: 'description',
					fieldLabel: 'Description'
				},{
					xtype: 'panel',
					border: false,
					html: '<p class="x-form-item-label"><span style="color:red;">* </span>NOTE: <span style="font-weight:normal;">The hours are config by GMT (Global Meridian Time)</span></p>'
				}],
				
				buttons:[{
					text: 'Save',
					handler: function(){
						if (Ext.getCmp('formNewEvent').getForm().isValid()){
							Ext.getCmp('formNewEvent').getForm().submit({
								waitMsg: 'Creating a new event...',
								method: 'POST',
								success: function(response){
									Ext.getCmp('gridEvent').getStore().load();
									Ext.getCmp('windowNewEvent').close();
								},
								failure: function(response){
									var parsedResult = Ext.JSON.decode(response.responseText).data;
									Ext.Msg.show({
										title: "Error",
										msg: parsedResult.msg,
										buttons: Ext.MessageBox.OK,
										icon: Ext.MessageBox.ERROR
									});
								}
							});					
						}
					}
				}, {
					text: 'Cancel',
					handler: function(e){
						Ext.getCmp('windowNewEvent').close();
					}
				}]
		    }]
		});
		
		this.callParent(arguments);
	}
});