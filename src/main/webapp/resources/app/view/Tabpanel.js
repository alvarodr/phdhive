Ext.define('phdhive.view.Tabpanel', {
	extend: 'Ext.tab.Panel',
	alias: 'widget.mainTabpanel',
	region: 'center',
	layout: 'border',
	margins: '0 5 0 5',
	tabPosition: 'left',
	border: false,

	initComponent: function() {
		Ext.apply(this, {
			id: 'tabPanel',
			activeTab: 0
		});
		var me = this;
		Ext.Ajax.request({
			async: false,
			url: contextPath + '/action/admin/privileges',
			success: function(response){
				var returnData = Ext.JSON.decode(response.responseText);
				if (returnData.success){
					isAdmin=true;
					me.items = [
						{xtype: 'userPanel'},
						{xtype: 'communityPanel'},
						{xtype: 'eventPanel'},
						{xtype: 'contentPanel'}
//						,{xtype: 'documentPanel'}
					];
				}else{
					me.items = [
						//{xtype: 'userPanel'},
						{xtype: 'eventPanel'}
//						,{xtype: 'documentPanel'}
					];
				}
			}
		});
		this.callParent(arguments);
	}
});