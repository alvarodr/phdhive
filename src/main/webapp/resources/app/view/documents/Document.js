Ext.define('phdhive.view.documents.Document', {
	extend: 'Ext.tree.Panel',
	alias: 'widget.documentList',
	region: 'center',
    title: 'Document list',
    split: true,
    rootVisible: false,
    
    initComponent: function() {
    	Ext.apply(this, {
    		store: 'Document',
            columns:[
                 {
                	 xtype: 'treecolumn',
                	 text: 'Document',
                	 flex: 1,
                	 dataIndex: 'document'
                 },
                 {
                	 header: 'Author',
                	 flex: 1,
                	 dataIndex: 'author',
                	 sortable: true
                 },
                 {
                	 header: 'Email',
                	 flex: 1,
                	 dataIndex: 'email',
                	 sortable: true
                 }
         	]
    	});
    	this.callParent(arguments);
    }
});