Ext.define('phdhive.view.documents.Panel', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.documentPanel',
	region: 'center',
	layout: 'border',
	
	initComponent: function() {
		Ext.apply(this, {
			title: 'Documents',
			items: [
		        {xtype: 'documentList'},
		        {xtype: 'documentWords'}
	        ]
		});
		this.callParent(arguments);
	}
});