Ext.define('phdhive.view.documents.Words', {
	extend: 'Ext.grid.Panel',
	alias: 'widget.documentWords',
	region: 'east',
	split: true,
	width: 300,
	border: false,
	collapsible: true,
	title: 'Detail words document',
	forceFit: true,
	
	initComponent: function() {
		Ext.apply(this, {
			store: 'Words',
		    columns: [
		        { text: 'Word',  dataIndex: 'word' },
		        { text: 'Frecuency',  dataIndex: 'frecuency' }
		    ]
		});
		this.callParent(arguments);
	}
});