Ext.define('phdhive.view.TopPanel', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.topPanel',
	region: 'north',
	cls: 'header',
	border: false,
	height: 100,
	url: '/action/admin/head',
	
	initComponent: function() {
		var template;
		var prueba = this.getTemplate(this.url, function(tpl) {
			template = tpl.html;
		});
		Ext.apply(this, {
			html: template
		});
		this.callParent(arguments);
	},
	getTemplate: function(url, callback) {
		var map = {};
		if (map[url] === undefined) {
			Ext.Ajax.request({
				async: false,
				url: url,
				success: function(xhr){
					var template = new Ext.XTemplate(xhr.responseText);
					template.compile();
					map[url] = template;
					callback(template);
				}
			});
		} else {
			callback(map[url]);
		}
	}
});