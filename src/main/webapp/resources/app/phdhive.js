Ext.Loader.setConfig({
	enabled : true
});

Ext.application({
	requires : [ 'Ext.container.Viewport' ],
	name : 'phdhive',
	appFolder: '/resources/app',

	paths: {'Ext.ux': '/resources/js/ExtJS/4.2/plugins'},
	
	controllers : [ 'Tabpanel', 'Users', 'Community', 'Event', 'Content', 'Document' ],

	autoCreateViewport : true

});