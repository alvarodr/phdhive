/**
 * Función que envía la información de login
 */
var login = function() {
	if (Ext.getCmp('formLogin').getForm().isValid()) {		
		Ext.Ajax.request({
			url : contextPath + '/action/admin/j_spring_security_check',
			method : 'POST',
			params : Ext.getCmp('formLogin').getForm().getValues(),
			success : function(response) {
				var parsedResult = Ext.JSON.decode(response.responseText).data;
				if (parsedResult.success == 'true') {
					window.location = contextPath + '/action/admin/dashboard';
				} else {
					Ext.Msg.show({
						title : 'Error',
						msg : parsedResult.msg,
						buttons : Ext.Msg.OK,
						icon : Ext.Msg.ERROR
					});
				}
			}
		});
	}
};

Ext.onReady(function() {
	Ext.create('Ext.form.Panel', {
		renderTo : 'login',
		id : 'formLogin',
		baseCls : 'transparente',
		labelAlign : 'left',
		margin : '10 5 5 5',
		defaults : {
			margin : 25
		},
		items : [ {
			columnWidth : 1,
			xtype : 'textfield',
			id : 'user',
			name : 'j_username',
			fieldLabel : 'Usuario',
			allowBlank : false
		}, {
			columnWidth : 1,
			xtype : 'textfield',
			inputType : 'password',
			id : 'password',
			fieldLabel : 'Contrase&ntilde;a',
			name : 'j_password',
			allowBlank : false,
			enableKeyEvents : true,
			listeners : {
				keypress : function(textfield, event) {
					if (event.getKey() == 13) {
						login();
					}
				}
			}
		} ],
		dockedItems: [ {
			xtype: 'toolbar',
			dock: 'bottom',
			baseCls: 'transparente',
			items: ['->', {
				xtype: 'button',
				text: 'Acceder',
				cls : 'button-login',
				handler : function() {
					login();
				}
			} ]
		} ]
	});
});