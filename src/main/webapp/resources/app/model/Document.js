Ext.define('phdhive.model.Document', {
	extend : 'Ext.data.Model',
	fields : [ 'document', 'author', 'email' ]
});