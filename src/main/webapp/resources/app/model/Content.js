Ext.define('phdhive.model.Content', {
	extend: 'Ext.data.Model',
	fields : [ {
		name:	'id',
		type:	'string'
	}, {
		name:	'title',
		type:	'string'
	}, {
		name:	'type',
		type:	'string'
	}, {
		name:	'updateDate',
//		type:	'date',
		dateFormat: 'd/m/Y'
	},{
		name:	'order',
		type: 	'int'
	},{
		name:	'published',
		type:	'boolean'
	} ],
    idProperty: 'id'
});