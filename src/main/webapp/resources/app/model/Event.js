Ext.define('phdhive.model.Event', {
	extend : 'Ext.data.Model',
	fields : [ {
			name:	'id',
			type:	'string'
		}, {
			name:	'name',
			type:	'string'
		}, {
			name:	'nameCommunity',
			type:	'string'
		}, {
			name:	'suscribers',
			type:	'int'
		}, {
			name:	'invitation',
			type:	'string'
		}, {
			name:	'startDate',
			type:	'long'
		}, {
			name:	'endDate',
			type:	'long'		
		} ],
	idProperty: 'id'
});