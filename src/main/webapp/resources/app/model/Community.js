Ext.define('phdhive.model.Community', {
	extend : 'Ext.data.Model',
	fields : ['id', 'name', 'dsCountry', 'email'],
	idProperty: 'id'
});