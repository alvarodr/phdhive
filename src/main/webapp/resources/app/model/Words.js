Ext.define('phdhive.model.Words', {
	extend: 'Ext.data.Model',
    fields: [
		{name: 'word', 		type: 'string'}, 
		{name: 'frecuency',	type: 'number'}
	]
});