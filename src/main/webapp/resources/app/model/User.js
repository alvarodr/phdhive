Ext.define('phdhive.model.User', {
	extend : 'Ext.data.Model',
	fields : [ {
		name:	'id',
		type:	'string'
	}, {
		name : 'name',
		type : 'string'
	}, {
		name : 'surname',
		type : 'string'
	}, {
		name : 'email',
		type : 'string'
	}, {
		name : 'phone',
		type : 'string'
	}, {
		name : 'lastIP',
		type : 'string'
	}, {
		name : 'loginCount',
		type : 'string'
	}, {
		name : 'enabled',
		type : 'boolean'
	} ],
	idProperty: 'email'
});