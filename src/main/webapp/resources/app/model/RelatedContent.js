Ext.define('phdhive.model.RelatedContent', {
	extend: 'Ext.data.Model',
	fields : [ {
		name:	'relContentRef',
		type:	'string'
	}, {
		name:	'relContentName',
		type:	'string'
	}, ],
    idProperty: 'relContentRef'
});