Ext.define('phdhive.model.Combo', {
	extend : 'Ext.data.Model',
	fields : [ {
		name : 'code',
		type : 'string'
	}, {
		name : 'description',
		type : 'string'
	} ],
	idProperty: 'code'
});