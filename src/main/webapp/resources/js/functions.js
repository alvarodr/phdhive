// Función para mostrar ocultar el submenú
$(document).ready(function() {
  $('.nav-toggle').click(function(){
    var collapse_content_selector = $(this).attr('href');
    var toggle_switch = $(this);
    $(collapse_content_selector).toggle(function(){
      if($(this).css('display')=='none'){
        $('html, body').animate({scrollTop: 0}, 500);
      }else{
        // vacio
      }
    });
  });
});
