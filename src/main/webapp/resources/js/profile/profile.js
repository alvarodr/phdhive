var disciplineLabels = [];
var disciplineMapped = {};
var disciplineMappedIds = {};
var disciplinesProfile = {};
var disciplinesLabelProfile = [];

function split( val ) {
	return val.split( /,\s*/ );
}
function extractLast( term ) {
	return split( term ).pop();
}

$(document).ready(function(){
	disciplinesProfile = split($("#disciplines").val());
	
	deleteDiscipline();
	
	$("#buttonEdit").click(function(){
		$(this).addClass("hidden");
		$("#buttonSave").removeClass("hidden");
		$("#buttonBack").removeClass("hidden");
		$("#dvEdit").removeClass("hidden");
		$("#dvDetail").addClass("hidden");
	});
	$("#buttonBack").click(function(){
		$(this).addClass("hidden");
		$("#buttonSave").addClass("hidden");
		$("#buttonEdit").removeClass("hidden");
		$("#dvEdit").addClass("hidden");
		$("#dvDetail").removeClass("hidden");
		initDisciplines();
	});

	$("#buttonSave").click(function(){
		$("#profileForm").submit();
	});	

	$("#disciplinesTags").bind("DOMSubtreeModified", function() {
		deleteDiscipline();
	});	
	
	initDisciplines();	

	$("#descriptionDisciplines").typeahead({
		source: function (query, process) {
			process(disciplineLabels)
		},
		updater: function (item) {
			$("#disciplines").val($("#disciplines").val().replace(/[^,]*$/,'')+disciplineMapped[item]+',');
			$("#disciplinesTags").append("<span class='btn btn-tag' id='"+ disciplineMapped[item] +"'>" + item + "<a class='delete' href='javascript:void(0);' id='del_" + disciplineMapped[item] + "'>&nbsp;</a></span>");
			return '';
		},
		matcher: function (item) {
			var tquery = extractor(this.query);
			if(!tquery) return false;
			return ~item.toLowerCase().indexOf(tquery.toLowerCase())
	    },
	    highlighter: function (item) {
			var query = extractor(this.query).replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, '\\$&')
			return item.replace(new RegExp('(' + query + ')', 'ig'), function ($1, match) {
				return '<strong>' + match + '</strong>'
			})
	    }
	});
});

function extractor(query) {
    var result = /([^,]+)$/.exec(query);
    if(result && result[1])
        return result[1].trim();
    return '';
}

function deleteDiscipline() {
	$("a.delete").click(function(){
		var value = $(this).parent().attr("id");
		var valuesIds = split($("#disciplines").val());
		if (valuesIds.indexOf(value)>-1){
			valuesIds.splice(valuesIds.indexOf(value), 1);
			if (valuesIds!="" && valuesIds.length>1)
				$("#disciplines").val(valuesIds.join(","));
			else
				$("#disciplines").val(valuesIds);
			$(this).parent().remove();
		}
	});
}

function initDisciplines(){
	$.each(disciplines, function (i, item) {
       disciplineMapped[item.label] = item.value;
       disciplineMappedIds[item.value] = item.label;
       disciplineLabels.push(item.label);
    });
	
	$.each(disciplinesProfile, function(i, item) {
		if (item != ''  && $("#" + item).length==0)
			$("#disciplinesTags").append("<span class='btn btn-tag' id='"+ item +"'>" + disciplineMappedIds[item] + "<a class='delete' href='javascript:void(0);' id='del_" + item + "'>&nbsp;</a></span>");
	});
}
